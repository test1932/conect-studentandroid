package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.payment.domain.entity.params.GetPaymentParam
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.payment.domain.interactor.PaymentStateUseCase
import com.nagwa.connect.modules.usermanagement.domain.entity.*
import com.nagwa.connect.modules.usermanagement.domain.entity.param.CreatePasswordParam
import com.nagwa.connect.modules.usermanagement.domain.entity.param.SignInParam
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import flatMapScoped
import io.reactivex.Single
import javax.inject.Inject

class CreateUserPasswordUseCase @Inject constructor(
    private val authenticationRepository: AuthenticationRepository,
    private val saveUser: SaveUserUseCase,
    private val paymentStateUseCase: PaymentStateUseCase
) : SingleUseCase<CreatePasswordParam, UserWithPortalIdEntity>() {

    override fun build(params: CreatePasswordParam): Single<UserWithPortalIdEntity> = with(params) {
        return when {
            !areNotEmptyPasswords(password, confirmPassword) ->
                Single.error(EmptyCreationPasswords)
            !areMatching(password, confirmPassword) ->
                Single.error(NotMatchedPasswords)
            !isValidPassword(password) ->
                Single.error(NotWellFormattedPassword)
            else -> paymentStateUseCase.build(GetPaymentParam(userId, portalId))
                .flatMapScoped {
                    when (this) {
                        PaymentAction.BLOCK_USER -> Single.error(PaymentRequiredError)
                        PaymentAction.DEACTIVATED_USER -> Single.error(PaymentUserDeactivatedError)
                        else -> confirmPassword(params)
                    }
                }
        }
    }

    private fun confirmPassword(params: CreatePasswordParam): Single<UserWithPortalIdEntity> {
        return authenticationRepository.confirmPassword(params.password)
            .flatMap {
                authenticationRepository.signinUser(SignInParam(params.email, params.password))
                    .flatMap { userWithPortalEntity ->
                        saveUser.build(userWithPortalEntity)
                            .andThen(Single.just(userWithPortalEntity))
                    }
            }
    }

    private fun areNotEmptyPasswords(newPassword: String, confirmPassword: String): Boolean =
        !(newPassword.isBlank() || confirmPassword.isBlank())

    private fun areMatching(newPassword: String, confirmPassword: String): Boolean =
        newPassword == confirmPassword

    private fun isValidPassword(newPassword: String): Boolean {
        return newPassword.length >= 6
    }

}

