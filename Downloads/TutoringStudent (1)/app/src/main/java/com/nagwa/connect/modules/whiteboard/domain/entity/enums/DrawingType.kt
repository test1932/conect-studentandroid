package com.nagwa.connect.modules.whiteboard.domain.entity.enums


/**
 * Created by Youssef Ebrahim Elerian on 5/18/20.
 * youssef.elerian@gmail.com
 */
enum class DrawingType(val type: Int) {
    PEN(1),
    ERASER(2)
}