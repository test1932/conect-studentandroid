package com.nagwa.connect.modules.whiteboard.domain.entity

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.RejoinState

/**
 * Authored by Mohamed Fathy on 19 Oct, 2020.
 * mohamed.eldesoky@nagwa.com
 */
data class RejoinEntity(val rejoinState: RejoinState)