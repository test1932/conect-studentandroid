package com.nagwa.connect.modules.sessions.presentation.viewmodel

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class SessionUIModel(
    val id: String,
    val title: String,
    val instructorName: String,
    @StringRes val statusText: Int,
    @ColorRes val statusColor: Int,
    @DrawableRes val statusBg: Int,
    @StringRes val positiveActionText: Int? = null,
    val description: String,
    val startingDate: String
)

data class SessionsListUIModel(
    val list: List<SessionUIModel>,
    val resetData: Boolean,
    val showRefreshing: Boolean,
    val showData: Boolean,
    val showLoading: Boolean,
    val showEmptyView: Boolean,
    val showSessionPreparationLoading: Boolean = false,
    val hideSessionPreparationLoading: Boolean = false,
    val preparationPercentage: Int = 0,
    @StringRes val nonInterruptedErrorMessage: Int? = null,
    @StringRes val screenErrorMessage: Int? = null
)
