package    com.nagwa.connect.base.presentation.view

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.view.MotionEvent
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.nagwa.connect.BuildConfig
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.createCustomAlert
import com.nagwa.connect.core.extension.hideKeyboard
import com.nagwa.connect.core.extension.isTablet
import com.nagwa.connect.core.extension.setScreenOrientation
import dagger.android.support.DaggerAppCompatActivity


/**
 * Created by Youssef Ebrahim on 10/21/18.
 */
abstract class TutoringActivity : DaggerAppCompatActivity() {
    private var forcedUpdateDialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (if (isTablet()) getTabletScreenOrientation()
        else getMobileScreenOrientation())?.let(::setScreenOrientation) //todo causes stack problems on phones

        setContentView(layoutId())
    }

    //overide this for forcing orientation
    open protected fun getTabletScreenOrientation(): Int? = null

    open protected fun getMobileScreenOrientation(): Int? = null

    @LayoutRes
    protected abstract fun layoutId(): Int

    private var touchDownTime: Long = 0
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN ->
                touchDownTime = SystemClock.elapsedRealtime()
            MotionEvent.ACTION_UP -> {
                //to avoid drag events
                if (SystemClock.elapsedRealtime() - touchDownTime <= 150) {
                    hideKeyboard()
                    clickOnScreen()
                }
            }
        }

        return super.dispatchTouchEvent(ev)
    }

    protected open fun clickOnScreen() {}

    override fun onStart() {
        if (BuildConfig.BUILD_TYPE == "release")
            checkForAppUpdate()
        super.onStart()
    }

    private fun checkForAppUpdate() {
        val remoteConfig = getFireBaseConfigs()
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val versionCode = remoteConfig.getString(LATEST_APP_VERSION).toIntOrNull()
                        ?: BuildConfig.VERSION_CODE
                    if (versionCode > BuildConfig.VERSION_CODE
                        && remoteConfig.getString(FORCE_UPDATE) == "true"
                    ) {
                        hideKeyboard()
                        showUpdateDialog()
                    }
                }
            }
            .addOnFailureListener {}
    }

    // todo move it as injection from here to app module by providing it as singleton and move the checking for force update to firebase repository
    private fun getFireBaseConfigs(): FirebaseRemoteConfig {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings
            .Builder()
            .setMinimumFetchIntervalInSeconds(0)
            .build()
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config)

        return remoteConfig

    }

    private fun showUpdateDialog() {
        if (forcedUpdateDialog == null) {
            forcedUpdateDialog = AlertDialog.Builder(this)
                .createCustomAlert(message = getString(R.string.update_msg),
                    positiveText = getString(R.string.label_update),
                    cancelable = false,
                    positiveListener = {
                        val appPackageName = packageName
                        try {
                            finish()
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=$appPackageName")
                                )
                            )

                        } catch (exception: android.content.ActivityNotFoundException) {
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                )
                            )
                        }
                    })
        }
    }

    companion object {
        private const val LATEST_APP_VERSION = "student_latest_android_version"
        private const val FORCE_UPDATE = "student_force_update"
    }
}