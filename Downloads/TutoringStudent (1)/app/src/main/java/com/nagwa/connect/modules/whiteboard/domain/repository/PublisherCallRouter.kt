package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import io.reactivex.Completable

interface PublisherCallRouter {
    fun join(channelDataEntity: ChannelDataEntity): Completable
    fun switchRole(isBroadCaster : Boolean): Completable
    fun muteChannel(): Completable
    fun unMuteChannel(): Completable
    fun unHold():Completable
    fun endCall(): Completable
}