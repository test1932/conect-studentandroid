package com.nagwa.connect.modules.usermanagement.domain.entity

data class VerifyUserEntity constructor(
    val portalId: String,
    val userId: String,
    val accountStatus: String,
    val needPasswordReset: Boolean,
    val resetToken: String?
)