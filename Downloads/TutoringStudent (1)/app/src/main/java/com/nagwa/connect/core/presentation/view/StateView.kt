package com.nagwa.connect.core.presentation.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.getFloat
import kotlinx.android.synthetic.main.layout_state_empty_view.view.*
import kotlinx.android.synthetic.main.layout_state_error_view.view.*
import timber.log.Timber


open class StateView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    fun setLoading() {
        showState {
            return@showState inflate(context, R.layout.layout_state_loading_view, null)
        }
        setOnTouchListener { _, _ ->
            true
        }
    }

    fun setEmpty(@StringRes emptyMsg: Int, @DrawableRes emptyIcon: Int) {
        showState {
            val stateView = inflate(context, R.layout.layout_state_empty_view, null)
            stateView.txtEmptyState.text = context.getString(emptyMsg)
            stateView.imgEmptyState.setImageResource(emptyIcon)
            return@showState stateView
        }
    }

    fun setError(@StringRes errorMessage: Int) {
        showState {
            val stateView = inflate(context, R.layout.layout_state_error_view, null)
            stateView.txtErrorState.text = context.getString(errorMessage)
            return@showState stateView
        }
    }

    fun setContent() {
        if (visibility != View.GONE) {
            removeAllViews()
            visibility = View.GONE
        }
        setOnTouchListener(null)
    }

    protected fun showState(inflater: (() -> View)) {
        visibility = View.VISIBLE
        this.post {
            Timber.d("showState: $measuredWidth")
            removeAllViews()//(width* SCREEN_PERCENT)
            //todo remove getting width directly and make it attr
            val frameParams = LayoutParams(
                (width * context.getFloat(R.dimen.empty_screen_width_ration)).toInt(),
                LayoutParams.MATCH_PARENT
            ).apply { gravity = Gravity.CENTER }
            addView(inflater.invoke(), frameParams)
        }
    }


}