package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ActionsRepository {
    fun joinActionsChannel(sessionID: String): Completable
    fun leaveActionsChannel(): Completable
    fun sendAction(raiseHandActionEntity: ActionEntity): Single<Boolean>
    fun getActions(): Flowable<ActionEntity>
}