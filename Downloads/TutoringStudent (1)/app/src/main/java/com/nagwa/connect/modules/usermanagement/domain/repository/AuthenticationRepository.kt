package com.nagwa.connect.modules.usermanagement.domain.repository

import com.nagwa.connect.modules.usermanagement.domain.entity.UserRoleEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.UserWithPortalIdEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.VerifyUserEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.param.SignInParam
import com.nagwa.connect.modules.usermanagement.domain.entity.param.UserRoleParam
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface AuthenticationRepository {
    fun verifyUser(username: String): Single<VerifyUserEntity>

    fun getUserRole(userRoleParam: UserRoleParam): Single<UserRoleEntity>

    fun confirmPassword(newPassword: String): Single<String>

    fun signinUser(signInParam: SignInParam): Single<UserWithPortalIdEntity>

    fun saveUser(user: UserWithPortalIdEntity): Completable

}

interface UserRepository {
    fun getUser(): Maybe<UserWithPortalIdEntity>
    fun signOut(): Completable
}