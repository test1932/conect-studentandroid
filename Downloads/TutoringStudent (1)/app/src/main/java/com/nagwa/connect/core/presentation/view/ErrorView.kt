package com.nagwa.connect.core.presentation.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.onClick
import com.nagwa.connect.core.extension.scaleYAxis
import kotlinx.android.synthetic.main.error_view.view.*

class ErrorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var errorView: View? = null
    var animationDuration: Long = 300
    var duration = ErrorViewDuration.INFINITE
    private val shortDuration: Long = 700
    private val longDuration: Long = 1500
    var isShowing = false
        private set

    init {
        errorView = View.inflate(context, R.layout.error_view, this)
        visibility = View.INVISIBLE
        scaleYAxis(
            100,
            amountToMoveDown = 1F,
            startFrom = 0F,
            onAnimationEnd = {
                scaleYAxis(
                    100,
                    amountToMoveDown = 0F,
                    startFrom = 0F
                )
            })
    }

    fun showError(message: String, actionText: String = "", actionIcon: Drawable? = null) {
        errorView?.run {
            this.errorTextTV.text = message
            if (actionText.isEmpty() && actionIcon != null) {
                this.errorActionTV.setCompoundDrawablesWithIntrinsicBounds(
                    actionIcon,
                    null,
                    null,
                    null
                )
                this.errorActionTV onClick {
                    hideWithAnimation()
                }
            } else if (actionText.isNotEmpty()) {
                this.errorActionTV.text = actionText
            }
            if (!isShowing)
                beginAnimation()
            else
                visibility = View.VISIBLE
        }
    }

    private fun beginAnimation() {
        isShowing = true
        visibility = View.VISIBLE
        scaleYAxis(animationDuration, amountToMoveDown = 1F,
            startFrom = 0F, onAnimationEnd = {
                if (duration != ErrorViewDuration.INFINITE)
                    Handler().postDelayed({
                        hideWithAnimation()
                    }, if (duration == ErrorViewDuration.SHORT) shortDuration else longDuration)
            })
    }

    private fun hideWithAnimation() {
        scaleYAxis(
            animationDuration, amountToMoveDown = 0F,
            startFrom = 0F,
            onAnimationEnd = {
                visibility = View.INVISIBLE
                isShowing = false
            })
    }

    enum class ErrorViewDuration {
        SHORT, LONG, INFINITE
    }
}
