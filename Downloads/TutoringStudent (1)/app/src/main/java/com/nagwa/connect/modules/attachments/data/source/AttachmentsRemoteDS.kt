package com.nagwa.connect.modules.attachments.data.source

import com.nagwa.connect.BuildConfig
import com.nagwa.connect.core.extension.GET
import com.nagwa.connect.core.extension.genericType
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class AttachmentsRemoteDS @Inject constructor(
    private val network: NANetwork,
    @Named("TutoringAuth") private val tutoringAuthScope: NAAuthNetwork
) {
    fun getSessionAttachmentUrl(userID: String, attachmentID: Long): Single<SessionAttachmentURL> {
        val type = genericType<SessionAttachmentURL>()
        return BuildConfig.TUTORING_API.GET(
            path = "/sessions/downloadurl",
            params = mapOf(
                "userId" to userID,
                "fileId" to attachmentID.toString()
            ),
            network = network,
            naAuthNetwork = tutoringAuthScope,
            resultType = type
        )
    }

    data class SessionAttachmentURL(val url: String)
}