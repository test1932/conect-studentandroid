package com.nagwa.connect.modules.whiteboard.domain.entity

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType
import com.nagwa.drawingengine.data.model.enums.EraserType

data class CreateShapeEntity(
    override val id: Int,
    override val actionType: DrawActionType = DrawActionType.CREATE_SHAPE,
    val color: Int,
    val points: List<PointEntity>,
    val strokeWidth: Float,
    val typeOfDrawing: EraserType?
) : DrawEntity()