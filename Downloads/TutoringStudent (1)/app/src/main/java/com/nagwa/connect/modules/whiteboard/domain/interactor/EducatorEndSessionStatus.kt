package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.EducatorStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import io.reactivex.Flowable
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class EducatorEndSessionStatus @Inject constructor(
    private val messengerRepository: MessengerRepository
) : FlowableUseCase<String, EducatorStatusEntity>() {

    override fun build(educatorId: String): Flowable<EducatorStatusEntity> {
        return messengerRepository.getEducatorStatus(educatorId)
    }


}