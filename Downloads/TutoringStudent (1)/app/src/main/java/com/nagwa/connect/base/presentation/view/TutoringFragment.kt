package com.nagwa.connect.base.presentation.view


import android.os.Bundle
import dagger.android.support.DaggerFragment

abstract class TutoringFragment : DaggerFragment() {
    protected var isBack: Boolean = false // flag to check for screen appearance after back

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isBack = false

    }
}