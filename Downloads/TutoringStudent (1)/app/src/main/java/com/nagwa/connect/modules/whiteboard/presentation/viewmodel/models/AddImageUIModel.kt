package com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models

import android.graphics.Bitmap
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class AddImageUIModel(
    override val id: Int,
    val xPos: Float,
    val yPos: Float,
    val image: Bitmap,
    override val actionType: DrawActionType
):DrawEntity()
