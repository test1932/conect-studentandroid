package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.ConnectionStatus
import com.nagwa.connect.modules.whiteboard.domain.repository.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class JoinSessionUseCase @Inject constructor(
    private val drawingRepository: DrawingRepository,
    private val whiteboardRepository: WhiteboardRepository,
    private val messengerRepository: MessengerRepository,
    private val actionsRepository: ActionsRepository,
    private val callRouter: CallRouter,
    private val resetUseCase: ResetUseCase,
    private val getUserConnectionStatusUseCase: GetUserConnectionStatusUseCase,
    private val getEducatorEndSessionStatus: EducatorEndSessionStatus,
    private val getUserOnlineStatus: GetUserOnlineStatus
) : FlowableUseCase<JoinSessionUseCase.JoinSessionParam, CallStatusEntity>() {

    private val interruptionStream = BehaviorSubject.create<CallStatusEntity>()
    private val educatorOfflineTimerDisposable = CompositeDisposable()
    private val connectionTimerDisposable = CompositeDisposable()
    private val connectionDisposable = CompositeDisposable()
    private var isJoined = false
    override fun build(params: JoinSessionParam): Flowable<CallStatusEntity> {
        return Completable.concatArray(
            messengerRepository.prepareChannels(),
            messengerRepository.joinChatChannel(params.sessionID),
            drawingRepository.joinDrawingChannel(params.sessionID),
            actionsRepository.joinActionsChannel(params.sessionID),
            whiteboardRepository.saveSessionTime(params.sessionID, System.currentTimeMillis()),
            resetUseCase.build(params.sessionID)
        ).andThen(Flowable.defer { callRouter.startCall(params.sessionID) })
            .mergeWith(interruptionStream.toFlowable(BackpressureStrategy.LATEST))
            .flatMap { callStatusEntity ->
                if (callStatusEntity == CallStatusEntity.Connected)
                    getEducatorOnlineStatus(params.eduactorID).map {
                        if (!it)
                            CallStatusEntity.EducatorTerminated
                        else callStatusEntity
                    }
                else
                    Flowable.just(callStatusEntity)
            }
            .doOnNext {
                if (it == CallStatusEntity.DisConnected)
                    startReconnectTimeoutTimer()
                else
                    endReconnectTimeoutTimer()
                if (isFirstConnectedStatus(it)) {
                    observeOtherDeviceConnected()
                    observeEducatorStatus(params.eduactorID)
                    isJoined = true
                }
            }
            .doOnTerminate {
                Timber.d("build() doOnTerminate called")
                interruptionStream.onComplete()
                connectionTimerDisposable.clear()
                connectionDisposable.clear()
            }.doOnNext {
                Timber.d("build: $it")
            }.distinctUntilChanged()
    }

    private fun isFirstConnectedStatus(it: CallStatusEntity?) =
        it == CallStatusEntity.Connected && !isJoined

    private fun observeOtherDeviceConnected() {
        getUserConnectionStatusUseCase.build(Unit)
            .filter { it == ConnectionStatus.ABORTED }
            .takeUntil { it == ConnectionStatus.ABORTED }
            .subscribeOn(Schedulers.io())
            .doOnTerminate { endReconnectTimeoutTimer() }
            .subscribe(
                { interruptionStream.onNext(CallStatusEntity.AccountJoinedWithOtherDevice) },
                {
                    Timber.e(it)
                }
            )
            .addTo(connectionDisposable)
    }

    private fun observeEducatorStatus(educatorID: String) {
        getEducatorEndSessionStatus.build(educatorID)
            .map { it.isOnline }
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .doOnTerminate { endEducatorTimeoutTimer() }
            .subscribeBy(onNext = { isOnline ->
                if (isOnline) {
                    endEducatorTimeoutTimer()
                    if (isJoined)
                        interruptionStream.onNext(CallStatusEntity.Connected)
                } else {
                    startEducatorTimeoutTimer()
                    interruptionStream.onNext(CallStatusEntity.EducatorOffline)
                }
            }, onError = Timber::e)
            .addTo(connectionDisposable)
    }

    private fun startReconnectTimeoutTimer() {
        if (connectionTimerDisposable.size() > 0) return
        Completable.timer(REJOIN_TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .subscribe({
                Timber.d("startReconnectTimeoutTimer() [Timer Finished] called")
                interruptionStream.onNext(CallStatusEntity.RejoinTimeout)
            }, Timber::e)
            .addTo(connectionTimerDisposable)
    }

    private fun startEducatorTimeoutTimer() {
        if (connectionTimerDisposable.size() > 0) return
        Completable.timer(REJOIN_TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .subscribe({
                Timber.d("startReconnectTimeoutTimer() [Timer Finished] called")
                interruptionStream.onNext(CallStatusEntity.EducatorTerminated)
            }, Timber::e)
            .addTo(educatorOfflineTimerDisposable)
    }

    private fun endReconnectTimeoutTimer() {
        connectionTimerDisposable.clear()
    }

    private fun getEducatorOnlineStatus(educatorID: String): Flowable<Boolean> {
        return getUserOnlineStatus.build(educatorID).toFlowable()
    }


    private fun endEducatorTimeoutTimer() {
        educatorOfflineTimerDisposable.clear()
    }

    companion object {
        const val REJOIN_TIMEOUT_MINUTES = 3L // 3 Minutes
    }

    data class JoinSessionParam(val sessionID: String, val eduactorID: String)
}