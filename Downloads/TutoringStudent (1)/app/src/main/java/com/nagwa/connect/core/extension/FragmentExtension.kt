package  com.nagwa.connect.core.extension


import android.content.pm.PackageManager
import android.view.KeyEvent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.nagwa.connect.R


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

/*
 *  replace container layout with fragment and attached it to the activity
 *
 *@Param activity - the activity that @Param fragment will be attached to it
 *@Param frameId - the layout container that will be replaced with @Param fragment
 */
fun FragmentManager.add(
    fragment: Fragment,
    frameId: Int? = null,
    addToBackStack: Boolean = false,
    tag: String = ""
) {
    inTransaction {
        frameId?.let { add(it, fragment) } ?: this.add(fragment, tag)
        if (addToBackStack) addToBackStack(tag)
    }
}

/*
 *  replace container layout with fragment and attached it to the activity
 *
 *@Param activity - the activity that @Param fragment will be attached to it
 *@Param frameId - the layout container that will be replaced with @Param fragment
 */
fun FragmentManager.replaceFragment(
    fragment: Fragment,
    frameId: Int,
    addToBackStack: Boolean = false,
    tag: String = ""
) {
    inTransaction {
        replace(frameId, fragment, tag)
        takeIf { addToBackStack }?.apply { addToBackStack(tag) }
    }
}

fun Fragment.disableBack() {
    view?.isFocusableInTouchMode = true
    view?.requestFocus()
    view?.setOnKeyListener { _, keyCode, _ ->
        keyCode == KeyEvent.KEYCODE_BACK
    }
}

fun FragmentManager.removeAllFragments() {
    while (backStackEntryCount > 0)
        popBackStackImmediate()
}

fun Fragment.requestPermission(
    permission: String,
    permissions: Array<String>? = null,
    code: Int,
    message: String
): Boolean {
    return activity?.let {
        if (ContextCompat.checkSelfPermission(it, permission)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(it, permission)) {
                val dialog = it.createAlertWithPositiveButton(
                    title = getString(R.string.openSettings),
                    message = message,
                    positiveText = getString(R.string.ok),
                    positiveButtonListener = { })
                    .apply { setCancelable(false) }
                dialog.show()
                dialog.changeNegativeButtonColor()
            } else {
                requestPermissions(permissions ?: arrayOf(permission), code)
            }
            return false
        } else
            return true
    } ?: false
}