package com.nagwa.connect.modules.attachments.data.source

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.nagwa.connect.core.extension.fromJSON
import com.nagwa.connect.core.extension.toJson
import com.nagwa.connect.modules.attachments.domain.AttachmentInfo
import io.reactivex.Completable
import io.reactivex.Single
import toSingle
import javax.inject.Inject
import javax.inject.Named

@SuppressLint("ApplySharedPref")
class AttachmentsLocalDS @Inject constructor(
    @Named("Attachments") private val prefs: SharedPreferences
) {
    private fun saveAttachmentInfo(
        sessionID: String,
        attachmentID: Long,
        filePath: String? = null
    ) =
        Completable.fromAction {
            prefs.edit()
                .putString(sessionID, AttachmentInfo(sessionID, attachmentID, filePath).toJson())
                .commit()
        }

    fun upsertAttachmentInfo(
        sessionID: String,
        attachmentID: Long,
        filePath: String? = null
    ): Completable {
        return getAttachmentInfo(sessionID).flatMapCompletable {
            saveAttachmentInfo(sessionID, attachmentID, filePath ?: it.filePath)
        }.onErrorResumeNext { saveAttachmentInfo(sessionID, attachmentID, filePath) }
    }

    fun getAttachmentInfo(sessionID: String) = Single.fromCallable {
        prefs.getString(sessionID, "")
    }.flatMap {
        if (it == "")
            Single.error(IllegalArgumentException("no attachment found for sessionID= $sessionID"))
        else it.toSingle()
    }.map {
        it.fromJSON<AttachmentInfo>()!!
    }

    fun updateAttachmentPath(sessionID: String, filePath: String): Completable =
        getAttachmentInfo(sessionID).map {
            it.copy(filePath = filePath)
        }.flatMapCompletable { saveAttachmentInfo(sessionID, it.fileID, it.filePath) }

    fun getAllAttachmentsInfo(): Single<List<AttachmentInfo>> = Single.fromCallable {
        prefs.all.values.map { (it as String).fromJSON<AttachmentInfo>()!! }
    }

    fun clearSessionInfo(sessionID: String): Completable = Completable.fromAction {
        prefs.edit().remove(sessionID).commit()
    }

}

