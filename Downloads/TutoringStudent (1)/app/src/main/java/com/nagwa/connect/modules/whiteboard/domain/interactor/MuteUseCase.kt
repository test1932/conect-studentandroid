package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.sessions.data.model.AGORA_RESPONSE_KEY
import com.nagwa.connect.modules.sessions.data.model.MILLICAST_RESPONSE_KEY
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.params.CallDataEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.PublisherCallRouter
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class MuteUseCase @Inject constructor(
    private val publisherCallRouter: PublisherCallRouter
) :
    CompletableUseCase<CallDataEntity>() {
    override fun build(params: CallDataEntity): Completable {
        return when {
            params.provider.equals(MILLICAST_RESPONSE_KEY, true) -> {
                publisherCallRouter.endCall()
            }
            params.provider.equals(AGORA_RESPONSE_KEY, true) -> {
                publisherCallRouter.switchRole(false)
            }
            else -> Completable.error(IllegalStateException("The api returned unknown provider = [$params.provider] , we only support [Agora,Millicast]"))
        }
    }
}