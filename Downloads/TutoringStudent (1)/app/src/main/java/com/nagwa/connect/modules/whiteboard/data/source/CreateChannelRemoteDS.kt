package com.nagwa.connect.modules.whiteboard.data.source

import com.google.gson.Gson
import com.nagwa.connect.BuildConfig
import com.nagwa.connect.core.extension.POST
import com.nagwa.connect.modules.whiteboard.data.model.PublishStreamResponse
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

@WhiteboardScope
class CreateChannelRemoteDS @Inject constructor(
    private val network: NANetwork,
    @Named("TutoringAuth") private val tutoringAuthScope: NAAuthNetwork,
    private val gson: Gson
) {
    fun publishStream(sessionId: String, userID: String): Single<PublishStreamResponse> {
        return BuildConfig.TUTORING_API.POST(
            path = "sessions/start_raise_hand",
            body = PublishStreamBody(sessionId.toLong(), userID.toLong()),
            network = network,
            naAuthNetwork = tutoringAuthScope,
            resultType = PublishStreamResponse::class.java,
            gson = gson
        )
    }

    fun subscribeToStream(
        subscribeToStreamBody: SubscribeToStreamBody
    ): Single<PublishStreamResponse> {
        return BuildConfig.TUTORING_API.POST(
            path = "sessions/join_raise_hand",
            body = subscribeToStreamBody,
            network = network,
            naAuthNetwork = tutoringAuthScope,
            resultType = PublishStreamResponse::class.java,
            gson = gson
        )
    }

    data class PublishStreamBody(private val sessionId: Long, private val userID: Long)
    data class SubscribeToStreamBody(
        private val sessionId: Long?,
        private val userID: Long?,
        private val StreamName: String?
    )

}