package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.core.domian.DomainException
import com.nagwa.rxdownloadmanger.IDownloadManger
import com.nagwa.rxdownloadmanger.models.DownloadEvent
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
class PrepareSessionAttachment @Inject constructor(
    private val isSessionHasAttachment: IsSessionHasAttachment,
    private val getAttachmentDownloadUrl: GetAttachmentDownloadUrl,
    private val downloadAttachment: DownloadAttachment,
    private val observeAttachmentDownload: ObserveAttachmentDownload,
    private val extractAttachmentFiles: ExtractAttachmentFiles,
    private val saveAttachmentPath: SaveAttachmentPath,
    private val clearFailedDownloadData: ClearFailedDownloadStatus,
    private val isSessionAttachmentsDownloaded: IsSessionAttachmentsDownloaded
) :
    FlowableUseCase<String, AttachmentState>() {
    override fun build(sessionID: String): Flowable<AttachmentState> {
        return isSessionHasAttachment.build(sessionID)
            .zipWith(isSessionAttachmentsDownloaded.build(sessionID),
                BiFunction { t1: Boolean, t2: Boolean -> Pair(t1, t2) })
            .flatMapPublisher {
                val (hasAttachment, downloaded) = it
                if (hasAttachment && !downloaded) prepareAttachment(sessionID)
                else Flowable.just(AttachmentState.Ready)
            }
    }

    private fun prepareAttachment(sessionID: String): Flowable<AttachmentState> {
        return getAttachmentDownloadUrl.build(sessionID).toFlowable()
            .flatMapCompletable { url ->
                downloadAttachment.build(AttachmentDownloadParam(url = url, sessionID = sessionID))
                    .ignoreElement()
            }.andThen(Flowable.defer { observeAttachmentDownload.build(sessionID) })
            .flatMap {
                if (!it.status.isTerminated() || it.status == IDownloadManger.Status.NA)
                    Flowable.just(AttachmentState.Downloading(it) as AttachmentState)
                else if (it.status == IDownloadManger.Status.FINISHED)
                    startExtraction(it, sessionID = sessionID)
                else {
                    clearFailedDownloadData.build(sessionID)
                        .andThen(Flowable.error(FailedToDownload))
                }
            }.startWith(Flowable.just(AttachmentState.GettingDownloadURL as AttachmentState))
    }

    private fun startExtraction(it: DownloadEvent, sessionID: String): Flowable<AttachmentState> {
        return extractAttachmentFiles.build(
            ExtractAttachmentParam(compressedFilePath = it.filePath, sessionID = sessionID)
        ).onErrorResumeNext { err ->
            Single.error(FailedToExtractFiles(err))
        }.flatMapCompletable {
            saveAttachmentPath.build(AttachmentPathParam(sessionID = sessionID, path = it))
        }.andThen(Flowable.just(AttachmentState.Ready as AttachmentState))
            .startWith(Flowable.just(AttachmentState.UnZipping))
    }
}

sealed class AttachmentState {
    object GettingDownloadURL : AttachmentState()
    class Downloading(val event: DownloadEvent) : AttachmentState()
    object UnZipping : AttachmentState()
    object Ready : AttachmentState()
}

object FailedToDownload : DomainException("Failed to download file")
class FailedToExtractFiles(error: Throwable? = null) :
    DomainException("Failed to get extract files", error)