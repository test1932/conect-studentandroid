package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class ClearAllModel(
    override val id: Int,
    @SerializedName("a")
    override val actionType: Int = DrawActionType.CLEAR_ALL.type
) : DrawModel()