package com.nagwa.connect.modules.whiteboard.data.source

import com.google.gson.Gson
import com.nagwa.connect.BuildConfig
import com.nagwa.connect.core.extension.NO_RESPONSE_POST
import com.nagwa.connect.modules.sessions.data.SessionsRemoteDS
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by Youssef Ebrahim Elerian on 9/14/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class LeaveSessionDS @Inject constructor(
    private val network: NANetwork,
    private val gson: Gson,
    @Named("TutoringAuth") private val tutoringAuthScope: NAAuthNetwork
) {

    fun leaveSession(userID: String, sessionID: String): Completable {
        return BuildConfig.TUTORING_API.NO_RESPONSE_POST(
            path = "sessions/leave",
            body = SessionsRemoteDS.SessionStudentActionBody(userID.toLong(), sessionID.toLong()),
            network = network,
            naAuthNetwork = tutoringAuthScope,
            gson = gson
            ,apiVersion = BuildConfig.TUTORING_API_VERSION
        )

    }

}