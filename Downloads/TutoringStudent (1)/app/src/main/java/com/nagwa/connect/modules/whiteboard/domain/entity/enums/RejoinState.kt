package com.nagwa.connect.modules.whiteboard.domain.entity.enums

/**
 * Authored by Mohamed Fathy on 19 Oct, 2020.
 * mohamed.eldesoky@nagwa.com
 */
enum class RejoinState {
    TRYING_TO_REJOIN,
    CALL_STARTED,
    TERMINATE
}