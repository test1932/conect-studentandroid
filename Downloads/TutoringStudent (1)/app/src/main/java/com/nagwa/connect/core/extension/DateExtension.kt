package com.nagwa.connect.core.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Youssef Ebrahim Elerian on 4/19/20.
 * youssef.elerian@gmail.com
 */

fun Date.convertToUiDate(dateFormat: String = "yyyy-MM-dd • hh:mm a"): String {
    val df = SimpleDateFormat(dateFormat, Locale.ENGLISH)
    //df.timeZone = Calendar.getInstance().timeZone
    return df.format(this)
}

fun String.convertToDate(
    dateTimeFormat: String = "yyyy-MM-dd'T'HH:mm",
    timeZone: String? = null
): Date? {
    val df = SimpleDateFormat(dateTimeFormat, Locale.ENGLISH)
    //df.timeZone = TimeZone.getTimeZone(timeZone ?: "UTC")
    return try {
        df.parse(this)
    } catch (e: ParseException) {
        null
    }
}