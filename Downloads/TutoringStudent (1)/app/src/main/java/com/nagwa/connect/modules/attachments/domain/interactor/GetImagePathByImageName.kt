package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetImagePathByImageName @Inject constructor(private val getSessionImages: GetSessionImages) :
    SingleUseCase<GetImagePathParam, String>() {
    override fun build(params: GetImagePathParam): Single<String> {
        return getSessionImages.build(params.sessionID).map { ls ->
            ls.first { it.nameWithoutExtension == params.imageName || it.name == params.imageName }.absolutePath
        }
    }
}

data class GetImagePathParam(val sessionID: String, val imageName: String)