package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.RejoinEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.RejoinCallPublisherRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Authored by Mohamed Fathy on 13 Oct, 2020.
 * mohamed.eldesoky@nagwa.com
 */
@WhiteboardScope
class RejoinCallStateUseCase @Inject constructor(
    private val rejoinCallPublisherRepository: RejoinCallPublisherRepository
) : FlowableUseCase<Unit, RejoinEntity>() {

    override fun build(params: Unit): Flowable<RejoinEntity> {
        return rejoinCallPublisherRepository.rejoinCallState()
    }
}