package com.nagwa.connect.modules.whiteboard.presentation.viewmodel

import addTo
import applyAsyncSchedulers
import com.nagwa.connect.base.presentation.viewmodel.StateViewModel
import com.nagwa.connect.modules.usermanagement.domain.interactor.GetUserUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetChatMessagesUpdates
import com.nagwa.connect.modules.whiteboard.domain.interactor.OnClearMessageRepoUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.SendChatMessage
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.ChatMessageState
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.ChatMessageUIModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.ChatState
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.toUIModel
import io.reactivex.rxkotlin.subscribeBy
import retryToSubscribe
import timber.log.Timber
import javax.inject.Inject

@WhiteboardScope
class ChatViewModel @Inject constructor(
    private val getChatMessagesUpdates: GetChatMessagesUpdates,
    private val sendChatMessage: SendChatMessage,
    private val onClearMessageRepoUseCase: OnClearMessageRepoUseCase,
    getUserUseCase: GetUserUseCase
) : StateViewModel<ChatState, List<ChatMessageUIModel>>(INITIAL_STATE) {

    init {
        getUserUseCase.build(Unit)
            .applyAsyncSchedulers()
            .map { it.user }
            .subscribe({
                val fullName = "${it.firstName} ${it.lastName}"
                updateState(state.copy(studentFullName = fullName, studentID = it.userId))
                observeChatUpdates()
            }, Timber::e)
            .addTo(compositeDisposable)
    }

    override fun mapStateToUIModel(
        oldState: ChatState,
        newState: ChatState
    ): List<ChatMessageUIModel> {
        return newState.list.map { it.toUIModel() }
    }

    fun onMessageClicked(id: Long) {
        getPendingMessages().firstOrNull { it.id() == id }
            ?.takeIf { it.failed && !it.resending } //don't resent unless it failed and not resending
            ?.let { msg ->
                updatePendingMessageStatus(msg, resending = true)
                sendChatMessage(msg)
            }
    }

    fun onSendChatMessageClicked(message: String) = updateState {
        val pendingMessage = ChatMessageState.PendingMessage(
            message = ChatMessageEntity(
                studentFullName,
                studentID,
                message,
                System.currentTimeMillis()
            )
        )
        copy(list = list.toMutableList().apply { add(0, pendingMessage) })
            .also { sendChatMessage(pendingMessage) }
    }

    private fun sendChatMessage(pendingMessage: ChatMessageState.PendingMessage) {
        sendChatMessage.build(pendingMessage.message.text)
            .applyAsyncSchedulers()
            .subscribeBy(
                onComplete = {
                    Timber.d("sendChatMessage() success")
                    updatePendingMessageStatus(pendingMessage, success = true)
                },
                onError = {
                    updatePendingMessageStatus(pendingMessage, resending = false, failed = true)
                })
            .addTo(compositeDisposable)
    }

    private fun observeChatUpdates() {
        getChatMessagesUpdates.build(Unit)
            .map(::mapToUIStateMessages)
            .map(::addPendingMessagesToReceivedMessages)
            .retryToSubscribe()
            .applyAsyncSchedulers()
            .subscribeBy(
                onNext = {
                    Timber.d("observeChatUpdates() called")
                    updateState(state.copy(list = it))
                },
                onError = Timber::e
            )
            .addTo(compositeDisposable)
    }

    //todo can be called after chat updates debug this
    private fun updatePendingMessageStatus(
        myMessage: ChatMessageState.PendingMessage,
        success: Boolean = false,
        failed: Boolean = false,
        resending: Boolean = false
    ) = updateState {
        Timber.d("updatePendingMessageStatus() called")
        val pendingIDX = list.indexOfFirst { it.id() == myMessage.id() }
        list.toMutableList().apply {
            if (pendingIDX > -1) {
                if (success) removeAt(pendingIDX)
                else this[pendingIDX] =
                    myMessage.copy(failed = failed || myMessage.failed, resending = resending)
            }
        }.let {
            copy(list = it)
        }
    }


    private fun mapToUIStateMessages(ls: List<ChatMessageEntity>) =
        ls.map { ChatMessageState.ReceivedMessage(it) }

    private fun addPendingMessagesToReceivedMessages(ls: List<ChatMessageState>): List<ChatMessageState> =
        ls.toMutableList().apply {
            val pendingMessages = getPendingMessages().sortedBy { it.message.date }
            pendingMessages.forEach { pendMsg ->
                indexOfFirst { it.message.date < pendMsg.message.date }
                    .takeIf { it != -1 }
                    ?.let { add(it, pendMsg) } ?: add(pendMsg)

            }
        }

    private fun getPendingMessages() =
        state.list.filterIsInstance<ChatMessageState.PendingMessage>()

    companion object {
        val INITIAL_STATE =
            ChatState(
                emptyList(),
                "You",
                ""
            )
    }

    override fun onCleared() {
        onClearMessageRepoUseCase.build()
        super.onCleared()
    }

}

