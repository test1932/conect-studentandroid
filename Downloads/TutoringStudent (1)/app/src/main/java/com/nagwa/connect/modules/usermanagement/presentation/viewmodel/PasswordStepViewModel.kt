package com.nagwa.connect.modules.usermanagement.presentation.viewmodel

import addTo
import androidx.annotation.StringRes
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.base.presentation.viewmodel.getDefaultErrorMessages
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.entity.EmptyPassword
import com.nagwa.connect.modules.usermanagement.domain.entity.InvalidPassword
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentRequiredError
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentUserDeactivatedError
import com.nagwa.connect.modules.usermanagement.domain.entity.param.SignInParam
import com.nagwa.connect.modules.usermanagement.domain.interactor.SignInUseCase
import javax.inject.Inject

class PasswordStepViewModel @Inject constructor(private val signInUseCase: SignInUseCase) :
    TutoringViewModel() {
    val passwordStepUIModel = SingleLiveEvent<PasswordStepUIModel>()

    fun signIn(email: String, password: String, userId: String, portalId: String) {
        passwordStepUIModel.value = PasswordStepUIModel(showLoading = true)
        signInUseCase.build(SignInParam(email, password, userId, portalId))
            .map { it.user }
            .applyAsyncSchedulers()
            .subscribe({ mapResultToUIModel() }, ::mapErrorToUIModel)
            .addTo(compositeDisposable)
    }

    private fun mapErrorToUIModel(throwable: Throwable) {
        passwordStepUIModel.value = when (throwable) {
            is PaymentRequiredError -> PasswordStepUIModel(returnToEmailStep = true)
            is PaymentUserDeactivatedError -> {
                PasswordStepUIModel(returnToEmailStep = true, isDeactivated = true)
            }
            else -> PasswordStepUIModel(error = getErrorMessage(throwable))
        }
    }

    private fun getErrorMessage(throwable: Throwable): Int {
        return when (throwable) {
            EmptyPassword -> R.string.msgMissingRequiredFields
            InvalidPassword -> R.string.msgIncorrectPassword
            else -> getDefaultErrorMessages(throwable)
        }
    }

    private fun mapResultToUIModel() {
        passwordStepUIModel.value = PasswordStepUIModel(showNextPage = true)
    }

}

data class PasswordStepUIModel(
    val showLoading: Boolean = false,
    @StringRes val error: Int? = null,
    val showNextPage: Boolean = false,
    val returnToEmailStep: Boolean = false,
    val isDeactivated: Boolean = false
)
