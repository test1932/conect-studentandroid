package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.MayableUseCase
import com.nagwa.connect.modules.sessions.data.model.AGORA_RESPONSE_KEY
import com.nagwa.connect.modules.sessions.data.model.MILLICAST_RESPONSE_KEY
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.CallDataEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.CreateChannelRepository
import com.nagwa.connect.modules.whiteboard.domain.repository.PublisherCallRouter
import io.reactivex.Maybe
import javax.inject.Inject

@WhiteboardScope
class ConnectAsPublisherUseCase @Inject constructor(
    private val createChannelRepository: CreateChannelRepository,
    private val publisherCallRouter: PublisherCallRouter
) : MayableUseCase<CallDataEntity, ChannelDataEntity>() {
    override fun build(params: CallDataEntity): Maybe<ChannelDataEntity> {
        return when {
            params.provider.equals(MILLICAST_RESPONSE_KEY, true) -> {
                createChannelRepository.publishStream(params.sessionId, params.studentId)
                    .flatMapMaybe {
                        publisherCallRouter.join(it).andThen(publisherCallRouter.muteChannel())
                            .andThen(Maybe.just(it))
                    }
            }
            params.provider.equals(AGORA_RESPONSE_KEY, true) -> {
                Maybe.empty()
            }
            else -> Maybe.error(IllegalStateException("The api returned unknown provider = [$params.provider] , we only support [Agora,Millicast]"))
        }
    }
}