package com.nagwa.connect.core.presentation.view

import android.graphics.Bitmap
import androidx.annotation.StringRes
import com.bumptech.glide.request.target.Target

interface StringsProvider {
    fun provideString(@StringRes resID: Int): String
    fun provideString(@StringRes resId: Int, vararg formatArgs: Any): String
}

interface BitmapProvider {
    fun provideBitmapAsync(
        imagePath: String,
        width: Int = Target.SIZE_ORIGINAL,
        height: Int = Target.SIZE_ORIGINAL,
        listener: ((Bitmap?) -> Unit)
    )
}