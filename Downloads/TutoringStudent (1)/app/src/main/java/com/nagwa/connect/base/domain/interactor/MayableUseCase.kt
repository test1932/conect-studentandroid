package com.nagwa.connect.base.domain.interactor

import io.reactivex.Maybe


abstract class MayableUseCase<in Params, Type> where Type : Any {

    abstract fun build(params: Params): Maybe<Type>
}