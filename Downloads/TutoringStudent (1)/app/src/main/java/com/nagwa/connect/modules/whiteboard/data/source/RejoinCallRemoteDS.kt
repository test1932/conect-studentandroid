package com.nagwa.connect.modules.whiteboard.data.source

import com.google.gson.Gson
import com.nagwa.connect.core.extension.POST
import com.nagwa.connect.modules.whiteboard.data.model.SubscribeResponse
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject

@WhiteboardScope
class RejoinCallRemoteDS @Inject constructor(
    private val network: NANetwork,
    private val gson: Gson
) {

    fun subscribe(subscriberParam: SubscriberParam): Single<SubscribeResponse> {
        return "https://director.millicast.com/api/".POST(
            path = "director/subscribe",
            headers = mutableMapOf(
                "accept" to "application/json",
                "content-type" to "application/json"
            ),
            network = network,
            body = mutableMapOf(
                "streamName" to subscriberParam.streamID,
                "streamAccountId" to subscriberParam.streamAccountId,
                "unauthorizedSubscribe" to true
            ),
            gson = gson,
            resultType = SubscribeResponse::class.java
        )
    }
}

data class SubscriberParam(val streamID: String, val streamAccountId: String)