package com.nagwa.connect.modules.whiteboard.data.model.mapper

import com.nagwa.connect.modules.sessions.data.model.MillicastProviderResponse
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.millicast.MillicastCallParams


fun MillicastProviderResponse.mapToMCParams(autoRejoin:Boolean): MillicastCallParams {
    return MillicastCallParams(
        autoRejoin = autoRejoin,
        publisherToken = publishingToken,
        accountId = accountId,
        isBroadcaster = false,
        wsUrl = "${url!!}?token=${jwt!!}",
        streamID = streamName!!,
        servers = milliCastIceServers!!.iceServersWrapperResponse!!.iceServers!!.map {
            it!!
            MillicastCallParams.MillicastIceServer(
                url = it.url!!,
                username = it.username,
                credential = it.credential
            )
        }
    )
}

fun CallStatus.toEntity(): CallStatusEntity = when (this) {
    CallStatus.NotStarted, CallStatus.Starting -> CallStatusEntity.Initializing
    CallStatus.Connected -> CallStatusEntity.Connected
    CallStatus.Disconnected -> CallStatusEntity.DisConnected
    CallStatus.FailedToReConnect -> CallStatusEntity.RejoinFailed
    CallStatus.Ended -> CallStatusEntity.Ended
}
