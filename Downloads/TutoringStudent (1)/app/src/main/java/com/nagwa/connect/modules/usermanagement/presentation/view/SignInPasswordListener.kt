package com.nagwa.connect.modules.usermanagement.presentation.view

interface SignInPasswordListener {
    fun onValidPassword()
    fun onPaymentError(isDeactivated: Boolean)
    fun onCancel()
    fun disableInteraction(disable: Boolean)
}

interface SignInEmailListener {
    fun onExistingStudent(email: String, userId: String, portalId: String)
    fun onNewStudent(email: String, userId: String, portalId: String)
    fun disableInteraction(disable: Boolean)
}