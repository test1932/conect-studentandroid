package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import io.reactivex.Flowable
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class GetChatMessagesUpdates @Inject constructor(
    private val messengerRepository: MessengerRepository
) : FlowableUseCase<Unit, List<ChatMessageEntity>>() {
    private val oldMessages = mutableListOf<ChatMessageEntity>()
    override fun build(params: Unit): Flowable<List<ChatMessageEntity>> {
        return messengerRepository.getChatMessage()
            .map { newMessage: ChatMessageEntity ->
                when {
                    isSenderNameBlank(newMessage) -> oldMessages
                    isSameLastSender(newMessage) -> appendNewMsgToLastMsg(
                        newMessage
                    )
                    else -> oldMessages.apply { add(0, newMessage) }
                }
            }
    }

    private fun isSenderNameBlank(newMsg: ChatMessageEntity) =
        newMsg.studentName.isBlank()

    private fun isSameLastSender(
        newMsg: ChatMessageEntity
    ) = oldMessages.isNotEmpty() && oldMessages[0].studentId == newMsg.studentId

    private fun appendNewMsgToLastMsg(
        newMsg: ChatMessageEntity
    ): MutableList<ChatMessageEntity> = oldMessages.apply{
        val concatMessage = this[0].text + NEW_MSG_MARKER + newMsg.text
        this.removeAt(0)
        this.add(0, newMsg.copy(text = concatMessage))
        return this
    }

    companion object {
        const val NEW_MSG_MARKER =
            "[&NewMessage&]" //todo instead of making marker make text a list of strings
    }

}
