package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import io.reactivex.Completable

interface ViewerCallRouter {
    fun join(channelDataEntity: ChannelDataEntity, userId: String): Completable
    fun muteChannel(userId: String): Completable
    fun unMuteChannel(userId: String): Completable
    fun unHold(studentId: String):Completable
    fun endCall(userId: String): Completable
    fun endCall(): Completable
}