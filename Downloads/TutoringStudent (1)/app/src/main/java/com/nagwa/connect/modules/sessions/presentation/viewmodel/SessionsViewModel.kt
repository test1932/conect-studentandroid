package com.nagwa.connect.modules.sessions.presentation.viewmodel

import addTo
import android.os.Parcelable
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.StateViewModel
import com.nagwa.connect.base.presentation.viewmodel.getDefaultErrorMessages
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.core.presentation.view.StringsProvider
import com.nagwa.connect.modules.attachments.domain.interactor.AttachmentState
import com.nagwa.connect.modules.attachments.domain.interactor.CancelAttachmentDownload
import com.nagwa.connect.modules.attachments.domain.interactor.FailedToDownload
import com.nagwa.connect.modules.payment.domain.interactor.CheckPaymentStateUseCase
import com.nagwa.connect.modules.payment.domain.interactor.GetPaidSessionsUseCase
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.connect.modules.sessions.domain.interactor.JoinGoingSession
import com.nagwa.connect.modules.sessions.domain.interactor.JoinSessionState
import com.nagwa.connect.modules.sessions.domain.interactor.SessionNotJoinableError
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentRequiredError
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentUserDeactivatedError
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.parcel.Parcelize
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@SessionsScope
class SessionsViewModel @Inject constructor(
    private val stringsProvider: StringsProvider,
    private val joinSession: JoinGoingSession,
    private val cancelAttachmentDownload: CancelAttachmentDownload,
    private val getPaidSessionsUseCase: GetPaidSessionsUseCase,
    private val checkPaymentStateUseCase: CheckPaymentStateUseCase
) : StateViewModel<SessionsListUIState, SessionsListUIModel>(INITIAL_STATE) {
    init {
        refreshSessions()
    }

    val openWhiteBoard = SingleLiveEvent<OpenSessionNavParam>()
    val logoutUser = SingleLiveEvent<Boolean>()

    private var startSessionDisposable: Disposable? = null

    fun checkPaymentState() {
        checkPaymentStateUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribe({}, { throwable ->
                when (throwable) {
                    is PaymentRequiredError -> logoutUser.value = false
                    is PaymentUserDeactivatedError -> logoutUser.value = true
                    else -> {
                        updateState(state.copy(error = throwable))
                        updateStateSilently(state.copy(error = throwable))
                    }
                }
            })
            .addTo(compositeDisposable)
    }

    fun joinSession(uiModel: SessionUIModel) {
        Timber.d("onPositiveButtonClicked() called with: uiModel = [${uiModel.title},state=[$state]]")
        updateState(state.copy(sessionAction = SessionAction(uiModel.id, ACTION_START)))
        joinSession.build(uiModel.id).applyAsyncSchedulers()
            .subscribe({ onJoinSessionStateUpdate(it) }, ::onSessionActionFailed)
            .let {
                startSessionDisposable = it
                compositeDisposable.add(it)
            }
    }

    fun cancelJoining() {
        startSessionDisposable?.takeIf { !it.isDisposed }?.run { dispose() }
        startSessionDisposable = null
        state.sessionAction?.run {
            cancelAttachmentDownload.build(sessionID)
                .applyAsyncSchedulers()
                .subscribe({ Timber.d("downloading of $sessionID stopped") }, Timber::e)
                .addTo(compositeDisposable)
        }
        updateState { copy(sessionAction = null) }
    }

    private fun onJoinSessionStateUpdate(sessionState: JoinSessionState): Unit =
        when (sessionState) {
            is JoinSessionState.Joining -> {
                Timber.d("onStartSessionStateUpdate: Joining")
                updateState { copy(sessionAction = sessionAction?.copy(progress = 100)) }
            }
            is JoinSessionState.Joined -> {
                Timber.d("onStartSessionStateUpdate: Joined successfully")
                onSessionOpened(sessionState.sessionID)
            }
            is JoinSessionState.PreparingAttachment -> {
                Timber.d("onStartSessionStateUpdate: PreparingAttachment")
                updateState(state.mapAttachmentStateToProgress(sessionState.state))
            }
        }

    private fun SessionsListUIState.mapAttachmentStateToProgress(
        state: AttachmentState
    ): SessionsListUIState {
        return when (state) {
            is AttachmentState.GettingDownloadURL -> {
                copy(sessionAction = sessionAction?.copy(progress = 0))
            }
            is AttachmentState.Downloading -> {
                copy(sessionAction = sessionAction?.copy(progress = state.event.progressPercentage))
            }
            is AttachmentState.UnZipping, is AttachmentState.Ready -> {
                copy(sessionAction = sessionAction?.copy(progress = 100))
            }
        }
    }

    private fun onSessionOpened(sessionID: String) {

        val session = state.list.first { it.id == sessionID }
        openWhiteBoard.postValue(
            OpenSessionNavParam(
                sessionID = sessionID,
                sessionTitle = session.title,
                educatorName = session.teacherName,
                educatorId = session.teacherId
            )
        )
        updateState(state.copy(sessionAction = null))
    }

    private fun onSessionActionFailed(it: Throwable?) {
        Timber.e(it, "onSessionActionFailed: ")
        updateState(state.copy(sessionAction = state.sessionAction?.copy(error = it)))
    }

    fun dismissSessionActionFailure() {
        updateState(state.copy(sessionAction = null, error = null))
    }

    fun refreshSessions() {
        updateState(state.copy(isRefreshing = state.initialized, error = null))
        getPaidSessionsUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribeBy(onSuccess = { ls ->
                val stateList = ls.map {
                    SessionUIState(
                        id = it.id,
                        title = it.title,
                        startingDate = it.startingDate,
                        sessionType = it.sessionType,
                        studentsNumber = it.studentsNumber,
                        subject = it.subject,
                        duration = it.duration,
                        teacherName = it.teacherName,
                        teacherId = it.teacherId
                    )
                }
                updateState(
                    state.copy(
                        list = stateList,
                        dataReplaced = true,
                        initialized = true,
                        isRefreshing = false,
                        error = null
                    )
                )
            }, onError = {
                when (it) {
                    is PaymentRequiredError -> logoutUser.value = false
                    is PaymentUserDeactivatedError -> logoutUser.value = true
                    else -> {
                        updateState(state.copy(isRefreshing = false, error = it))
                        updateStateSilently(state.copy(isRefreshing = false, error = it))
                    }
                }
            })
            .addTo(compositeDisposable)
    }

    override fun mapStateToUIModel(
        oldState: SessionsListUIState,
        newState: SessionsListUIState
    ): SessionsListUIModel = with(newState) {
        return SessionsListUIModel(
            list = list.map { it.toUIModel(stringsProvider) },
            resetData = dataReplaced,
            showRefreshing = isRefreshing,
            showData = initialized && list.isNotEmpty(),
            showEmptyView = initialized && list.isEmpty(),
            showLoading = !initialized,
            showSessionPreparationLoading = isPreparingSessionWithNoError(sessionAction),
            preparationPercentage = if (isPreparingSessionWithNoError(sessionAction)) sessionAction!!.progress else 0,
            hideSessionPreparationLoading = isPreparingSession(oldState.sessionAction) && newState.sessionAction == null,
            screenErrorMessage = error?.takeIf { list.isEmpty() }?.mapToStartErrorMessage(),
            nonInterruptedErrorMessage = sessionAction?.error?.mapToStartErrorMessage()
                ?: error?.takeIf { list.isNotEmpty() }?.mapErrorToMessage()
        )
    }

    private fun isPreparingSession(sessionAction: SessionAction?) =
        sessionAction != null && sessionAction.actionType == ACTION_START

    private fun isPreparingSessionWithNoError(sessionAction: SessionAction?) =
        isPreparingSession(sessionAction) && sessionAction!!.error == null

    private fun Throwable.mapErrorToMessage(): Int = getDefaultErrorMessages(this)
    private fun Throwable.mapToStartErrorMessage(): Int {
        return when (this) {
            is SessionNotJoinableError -> R.string.teacher_not_joined_yet
            is FailedToDownload -> R.string.session_download_problem
            else -> getDefaultErrorMessages(this)
        }
    }

    companion object {
        val INITIAL_STATE = SessionsListUIState(
            list = emptyList(),
            dataReplaced = false,
            isRefreshing = false,
            initialized = false,
            error = null
        )
    }
}

@Parcelize
data class OpenSessionNavParam(
    val sessionID: String,
    val sessionTitle: String,
    val educatorName: String,
    val educatorId: String
) : Parcelable

data class SessionUIState(
    val id: String,
    val title: String,
    val subject: String,
    val teacherName: String,
    val startingDate: Long,
    val duration: Int,
    val studentsNumber: Int,
    val sessionType: SessionType,
    val teacherId: String
)

//if there were no difference between SessionUIState and SessionEntity just replace it
data class SessionsListUIState(
    val list: List<SessionUIState>,
    val dataReplaced: Boolean,
    val isRefreshing: Boolean,
    val initialized: Boolean,
    val error: Throwable? = null,
    val startingSessionID: String? = null,
    val sessionAction: SessionAction? = null
)

const val ACTION_START = 1;

data class SessionAction(
    val sessionID: String,
    val actionType: Int,
    val error: Throwable? = null,
    val progress: Int = 0
)

enum class SessionType { Scheduled, Going }