package com.nagwa.connect.core.extension

import android.content.res.Resources
import com.google.gson.Gson


fun Int.toDp(): Int {
    return (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()
}

fun<T> T?.isNull(action: ((T?) -> Unit)){
    if (this == null)
        action.invoke(this)
}

fun Any.toStringData(): String {
    return Gson().toJson(this)
}


inline fun <reified T> String.toObject(): T {
    return Gson().fromJson(this, T::class.java)
}