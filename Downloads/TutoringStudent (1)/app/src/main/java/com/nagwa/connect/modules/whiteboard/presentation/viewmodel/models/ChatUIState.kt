package com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models

import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity

data class ChatState(
    val list: List<ChatMessageState>,
    val studentFullName: String,
    val studentID: String
)

sealed class ChatMessageState(open val message: ChatMessageEntity) {
    data class PendingMessage(
        override val message: ChatMessageEntity,
        val resending: Boolean = false,
        val failed: Boolean = false
    ) : ChatMessageState(message = message)

    class ReceivedMessage(message: ChatMessageEntity) : ChatMessageState(message = message)

    fun id()= message.date
}