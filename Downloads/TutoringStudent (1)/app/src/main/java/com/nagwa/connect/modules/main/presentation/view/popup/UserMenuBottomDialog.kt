package com.nagwa.connect.modules.main.presentation.view.popup

import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.BaseDialogFragment
import com.nagwa.connect.core.extension.hideStatusBar
import com.nagwa.connect.core.extension.isLandscape
import com.nagwa.connect.core.extension.isTablet
import com.nagwa.connect.core.extension.onClick
import com.nagwa.connect.core.misc.NoParamFunction
import kotlinx.android.synthetic.main.dialog_signout.view.*

class UserMenuBottomDialog : BaseDialogFragment() {
    var onLogoutClicked: (() -> Unit)? = null
    var onDismissListener: NoParamFunction?= null

    override fun onDismiss(dialog: DialogInterface) {
        onDismissListener?.invoke()
        super.onDismiss(dialog)
    }

    override fun getLayoutResource(): Int = R.layout.dialog_signout

    override fun getDialogBackground(): Drawable =
        ContextCompat.getDrawable(requireContext(), android.R.color.transparent)!!

    override fun getDialogWidth(): Int =
        if (requireActivity().isLandscape()) (0.5 * resources.displayMetrics.widthPixels).toInt()
        else ViewGroup.LayoutParams.MATCH_PARENT

    override fun getDialogHeight(): Int = ViewGroup.LayoutParams.WRAP_CONTENT
    override fun onCreate(savedInstanceState: Bundle?) {
        if (activity?.isTablet() == false && activity?.isLandscape() == true) activity?.hideStatusBar()
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.btnSignout onClick { onLogoutClicked?.invoke(); dismiss() }
        view.btnCancel onClick ::dismiss
        view.txtUserEmail.text = arguments?.getString(USERNAME_KEY) ?: ""
    }

    companion object {
        private const val USERNAME_KEY = "USERNAME_KEY"
        fun newInstance(userName: String) = UserMenuBottomDialog().apply {
            arguments = Bundle().apply {
                putString(USERNAME_KEY, userName)
            }
        }
    }

}