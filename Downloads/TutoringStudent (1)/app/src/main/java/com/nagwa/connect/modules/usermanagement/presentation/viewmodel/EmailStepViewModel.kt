package com.nagwa.connect.modules.usermanagement.presentation.viewmodel


import addTo
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.StateViewModel
import com.nagwa.connect.base.presentation.viewmodel.getDefaultErrorMessages
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.entity.EmptyUserIDError
import com.nagwa.connect.modules.usermanagement.domain.interactor.StudentIdVerificationState
import com.nagwa.connect.modules.usermanagement.domain.interactor.VerifyUserUseCase
import com.nagwa.connect.modules.usermanagement.presentation.uimodel.EmailStepUIModel
import javax.inject.Inject

class EmailStepViewModel @Inject constructor(
    private val verifyUser: VerifyUserUseCase
) : StateViewModel<EmailStepViewModel.EmailStepState, EmailStepUIModel>(
    INITIAL_STATE
) {
    val loginNavigation = SingleLiveEvent<LoginNavigation>()

    fun verifyUser(email: String) {
        return verifyUser.build(email.trim())
            .doOnSubscribe {
                updateState(EmailStepState(email = email.trim(), processing = true))
            }
            .applyAsyncSchedulers()
            .subscribe(
                /* onSuccess =*/ {
                    updateState(
                        state.copy(
                            studentIdState = it.idVerificationState,
                            userId = it.userId,
                            portalId = it.portalId,
                            processing = false
                        )
                    )
                },
                /* onError =*/{ updateState(state.copy(error = it, processing = false)) })
            .addTo(compositeDisposable)

    }

    fun resetEmailState() {
        updateState(state.copy(studentIdState = null))
    }

    override fun onStateUpdated(oldState: EmailStepState, newState: EmailStepState) {
        super.onStateUpdated(oldState, newState)
        val loginStep = mapEmailStateToLoginStep(newState.studentIdState)
        if (loginStep != LoginStep.EMAIL_NOT_VERIFIED)
            loginNavigation.value = LoginNavigation(
                loginStep,
                newState.email!!,
                newState.userId ?: "",
                newState.portalId ?: ""
            )
    }

    override fun mapStateToUIModel(
        oldState: EmailStepState,
        newState: EmailStepState
    ): EmailStepUIModel {
        return with(newState) {
            val errorMap = mapStateToUIError(studentIdState, error)
            val blockedUser =
                studentIdState == StudentIdVerificationState.PaymentBlockedUser || studentIdState == StudentIdVerificationState.PaymentDeactivatedUser
            EmailStepUIModel(
                showLoading = processing,
                error = errorMap,
                email = email,
                errorBackgroundDrawable = if (blockedUser) R.drawable.shape_tv_payment_error else R.drawable.shape_tv_signin_error,
                errorTextColor = if (blockedUser) R.color.colorOceanBlue else R.color.colorRed
            )
        }
    }


    private fun mapEmailStateToLoginStep(studentIdState: StudentIdVerificationState?): LoginStep {
        return studentIdState?.let {
            when (studentIdState) {
                StudentIdVerificationState.Student -> LoginStep.STUDENT
                StudentIdVerificationState.NewStudent -> LoginStep.NEW_STUDENT
                else -> LoginStep.EMAIL_NOT_VERIFIED
            }
        } ?: LoginStep.EMAIL_NOT_VERIFIED

    }

    private fun mapStateToUIError(
        studentIdState: StudentIdVerificationState?,
        error: Throwable?
    ): Int? {
        return error?.let {
            when (it) {
                EmptyUserIDError -> R.string.msgMissingRequiredFields
                else -> getDefaultErrorMessages(it)
            }
        } ?: studentIdState?.let {
            when (it) {
                StudentIdVerificationState.NotAllowedUser -> R.string.msgNotAllowedUser
                StudentIdVerificationState.UserNotFound -> R.string.msgInvalidEmail
                StudentIdVerificationState.PaymentBlockedUser -> R.string.msgBlockedUser
                StudentIdVerificationState.PaymentDeactivatedUser -> R.string.msgDeactivatedUser
                else -> null
            }
        }
    }

    data class EmailStepState(
        val processing: Boolean = false,
        val email: String? = null,
        val userId: String? = null,
        val portalId: String? = null,
        val studentIdState: StudentIdVerificationState? = null,
        val error: Throwable? = null
    )

    data class LoginNavigation(
        val loginStep: LoginStep,
        val email: String,
        val userId: String,
        val portalId: String
    )

    enum class LoginStep { EMAIL_NOT_VERIFIED, NEW_STUDENT, STUDENT }

    companion object {
        val INITIAL_STATE = EmailStepState(false, null, null, null, null, null)
    }
}