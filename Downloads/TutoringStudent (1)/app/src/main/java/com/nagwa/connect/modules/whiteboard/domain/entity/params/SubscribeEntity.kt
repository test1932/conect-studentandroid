package com.nagwa.connect.modules.whiteboard.domain.entity.params

data class SubscribeEntity (
    val sessionId: Long?,
    val studentId: Long?,
    val StreamName: String?
)