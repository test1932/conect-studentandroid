package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.sessions.data.model.MILLICAST_RESPONSE_KEY
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.CallActionWithErrorEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.UnMuteOthersEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes.*
import com.nagwa.connect.modules.whiteboard.domain.entity.params.CallDataEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.SubscribeEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import com.nagwa.connect.modules.whiteboard.domain.repository.ViewerCallRouter
import io.reactivex.Completable
import io.reactivex.Flowable
import timber.log.Timber
import javax.inject.Inject

const val TAG = "Connect_Actions"

@WhiteboardScope
class GetActionsUpdates @Inject constructor(
    private val getCallDataUseCase: GetCallDataUseCase,
    private val actionsRepository: ActionsRepository,
    private val connectAsViewerUseCase: ConnectAsViewerUseCase,
    private val viewerCallRouter: ViewerCallRouter,
    private val unMuteUseCase: UnMuteUseCase,
    private val unMuteOthersUseCase: UnMuteOthersUseCase,
    private val muteUseCase: MuteUseCase
) :
    FlowableUseCase<String, CallActionWithErrorEntity>() {
    override fun build(params: String): Flowable<CallActionWithErrorEntity> {
        return getCallDataUseCase.build(params).toFlowable().flatMap { callData ->
            actionsRepository.getActions().filter { callAction ->
                filterActions(callAction, callData)
            }.concatMap { action ->
                handleActions(action, callData)
                    .onErrorResumeNext { err: Throwable ->
                        Flowable.just(
                            CallActionWithErrorEntity(
                                action,
                                false
                            )
                        )
                    }
            }
        }
    }

    private fun filterActions(action: ActionEntity, callData: CallDataEntity) =
        when (action.action) {
            RaiseHand.value -> action.studentId != callData.studentId && callData.provider.equals(
                MILLICAST_RESPONSE_KEY,
                true
            )
            UnMute.value -> true
            Mute.value -> true
            RESET.value -> true
            RAISE_HAND_FAILURE.value -> callData.provider.equals(MILLICAST_RESPONSE_KEY, true)
            else -> false
        }

    private fun handleActions(
        actionEntity: ActionEntity,
        callDataEntity: CallDataEntity
    ): Flowable<CallActionWithErrorEntity> {
        Timber.tag(TAG).d("action is ${actionEntity.action} ")
        Timber.tag(TAG)
            .d("student ID is ${actionEntity.studentId}, student name is ${actionEntity.studentName}")
        Timber.tag(TAG).d("My user ID is ${callDataEntity.studentId}")
        val result = when (actionEntity.action) {
            RaiseHand.value -> {
                connectAsViewer(callDataEntity, actionEntity)
            }
            UnMute.value -> {
                if (actionEntity.studentId == callDataEntity.studentId)
                    unMuteUseCase.build(callDataEntity)
                else
                    unMuteOthersUseCase.build(
                        UnMuteOthersEntity(
                            callDataEntity.provider,
                            actionEntity.studentId ?: ""
                        )
                    )

            }
            Mute.value -> {
                mute(actionEntity, callDataEntity)
            }
            RESET.value -> {
                if (actionEntity.senderId == callDataEntity.educatorId.toString()) {
                    muteUseCase.build(callDataEntity)
                } else {
                    if (callDataEntity.provider.equals(
                            MILLICAST_RESPONSE_KEY,
                            true
                        )
                    ) {
                        Timber.tag(TAG).d("GetActionsUpdates : Reset and endCall")
                        viewerCallRouter.endCall(actionEntity.studentId?:"")
                    }
                    else
                        Completable.complete()
                }
            }
            RAISE_HAND_FAILURE.value -> {
                mute(actionEntity, callDataEntity)
            }

            else -> Completable.error(IllegalStateException("action is not RaiseHand | Mute | Unmute"))
        }

        return result.andThen(Flowable.just(CallActionWithErrorEntity(actionEntity, true)))
    }

    private fun connectAsViewer(
        callDataEntity: CallDataEntity,
        actionEntity: ActionEntity
    ): Completable {
        return connectAsViewerUseCase.build(
            SubscribeEntity(
                callDataEntity.sessionId.toLong(),
                actionEntity.studentId?.toLong() ?: 0,
                actionEntity.streamName
            )
        )
    }

    private fun mute(
        actionEntity: ActionEntity,
        callDataEntity: CallDataEntity
    ): Completable {
        return if (actionEntity.studentId == callDataEntity.studentId) {
            muteUseCase.build(callDataEntity).doOnComplete{
                Timber.tag(TAG).d("GetActionsUpdates : mute for action ${actionEntity.action} and student ID ${actionEntity.studentId} is completed")
            }.doOnError {err->
                Timber.tag(TAG).d("GetActionsUpdates : mute for action ${actionEntity.action} and student ID ${actionEntity.studentId}  is failed $err")
            }
        } else {
            if (callDataEntity.provider.equals(
                    MILLICAST_RESPONSE_KEY,
                    true
                )
            )
                viewerCallRouter.endCall(actionEntity.studentId?:"").doOnComplete{
                    Timber.tag(TAG).d("GetActionsUpdates : endCall is completed")
                }.doOnError {err->
                    Timber.tag(TAG).d("GetActionsUpdates : endCall is failed $err")
                }
            else
                Completable.complete()
        }
    }
}