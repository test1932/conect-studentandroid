package com.nagwa.connect.modules.usermanagement.domain.entity

import com.nagwa.connect.core.domian.DomainException

object NotMatchedPasswords : DomainException("new password doesn't match confirm password")
object EmptyCreationPasswords :
    DomainException("new password and confirm password shouldn't be blank")

object NotWellFormattedPassword : DomainException("Password is not in a valid format")


object InvalidPassword : DomainException("this password doesn't match the email")
object EmptyPassword : DomainException("password shouldn't be blank")

object UserNotFoundError : DomainException("User not found")
object NotAllowedUserError : DomainException("User not allowed")
object EmptyUserIDError : DomainException("Email can't be empty")
object NotWellFormattedUserIDError : DomainException("Email not in a valid format")

object PaymentRequiredError : DomainException("Payment required, state = overdue")
object PaymentUserDeactivatedError : DomainException("Payment required, state = deactivated")