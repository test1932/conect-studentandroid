package com.nagwa.connect.core.presentation.view

import kotlin.math.roundToInt

data class GridRowMetrics(val spacing: Int, val cellWidthPX: Int, val columnsNo: Int) {

    companion object {
        fun getGridRowMetrics(
            cellWidth: Int,
            minCellWidth: Int,
            gridFullWidth: Int,
            requiredCellSpace: Int
        ): GridRowMetrics {
            val columnNo = getProperColumnsWithinRangeWidth(
                rowWidth = gridFullWidth,
                perfectWidth = cellWidth,
                minWidthAllowed = minCellWidth
            )
            val totalRemainingSpace = gridFullWidth - (columnNo * cellWidth)
            return if (columnNo > 1) {
                val cellSpace = (totalRemainingSpace / (columnNo.toFloat() - 1f)).toInt()
                val newCellWidth =
                    (cellWidth + (cellSpace - requiredCellSpace) * ((columnNo - 1) / columnNo.toFloat()))
                GridRowMetrics(
                    spacing = requiredCellSpace,
                    cellWidthPX = newCellWidth.toInt(),
                    columnsNo = columnNo
                )
            } else GridRowMetrics(
                spacing = requiredCellSpace,
                cellWidthPX = gridFullWidth,
                columnsNo = 1
            )
        }

        private fun getProperColumnsWithinRangeWidth(
            rowWidth: Int,
            perfectWidth: Int,
            minWidthAllowed: Int
        ): Int {
            val cols = (rowWidth / perfectWidth.toFloat())
            return if (cols.toInt() == cols.roundToInt())
                cols.toInt()
            else {
                val mnWidth = rowWidth / cols.roundToInt().toFloat()
                if (mnWidth >= minWidthAllowed)
                    cols.roundToInt()
                else
                    cols.toInt()
            }
        }
    }
}
