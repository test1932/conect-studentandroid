package com.nagwa.connect.modules.whiteboard.domain.repository

import io.reactivex.Completable
import io.reactivex.Single


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
interface WhiteboardRepository {
    fun endSession(sessionId: String): Completable
    fun getSessionStartTime(sessionName: String): Single<Long>
    fun saveSessionTime(sessionId: String, time: Long): Completable
}