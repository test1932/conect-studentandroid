package com.nagwa.connect.core.extension

import android.app.Activity
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import timber.log.Timber


fun Uri.fileSize(activity: Activity): Long {
    val size = activity.contentResolver
        .query(this, null, null, null, null)
        ?.use {
            it.moveToFirst()
            it.getLong(it.getColumnIndex(OpenableColumns.SIZE))
        } ?: -1L

    Timber.d("chooseImageResult   imageSize 0- > $size")

    return size
}

fun Uri.getMimeType(activity: Activity): String {
    return activity.contentResolver
        .query(this, arrayOf(MediaStore.MediaColumns.MIME_TYPE), null, null, null)
        ?.use {
            it.moveToFirst()
            it.getString(0)
        } ?: ""

}
