package com.nagwa.connect.modules.payment.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.payment.domain.entity.params.GetPaymentParam
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.payment.domain.repository.PaymentRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Authored by Mohamed Fathy on 19 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
class PaymentStateUseCase @Inject constructor(
    private val paymentRepository: PaymentRepository
) : SingleUseCase<GetPaymentParam, PaymentAction>() {

    override fun build(params: GetPaymentParam): Single<PaymentAction> {
        return paymentRepository.getInvoiceStatus(params.userId, params.portalId)
            .map { it.action }
    }
}