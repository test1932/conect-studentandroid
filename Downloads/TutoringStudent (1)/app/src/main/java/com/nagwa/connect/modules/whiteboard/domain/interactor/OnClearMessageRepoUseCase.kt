package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import javax.inject.Inject

@WhiteboardScope
class OnClearMessageRepoUseCase @Inject constructor(
    private val messengerRepository: MessengerRepository
) {
    fun build() {
        messengerRepository.onClear()
    }


}