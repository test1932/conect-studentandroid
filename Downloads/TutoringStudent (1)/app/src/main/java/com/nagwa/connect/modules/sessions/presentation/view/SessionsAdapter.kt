package com.nagwa.connect.modules.sessions.presentation.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.onClick
import com.nagwa.connect.core.extension.visible
import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionUIModel
import kotlinx.android.synthetic.main.item_session.view.*
import javax.inject.Inject

class SessionsAdapter @Inject constructor(context: Context) :
    RecyclerView.Adapter<SessionsAdapter.SessionViewHolder>() {
    var cellWidth: Int = context.resources.getDimensionPixelSize(R.dimen.width_session_item)
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var onPositiveActionClicked: ((SessionUIModel) -> Unit)? = null
    var sessionsList = listOf<SessionUIModel>()
        private set

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_session, parent, false)
        return SessionViewHolder(
            view,
            cellWidth
        )
    }

    override fun getItemId(position: Int): Long {
        return sessionsList[position].id.toLong()
    }


    override fun getItemCount(): Int = sessionsList.size

    override fun onBindViewHolder(holder: SessionViewHolder, position: Int) {
        val videoUIModel = sessionsList[position]
        holder.bindVideo(videoUIModel, onPositiveActionClicked)
    }

    fun swap(list: List<SessionUIModel>) {
        sessionsList = list
        notifyDataSetChanged()
    }

    class SessionViewHolder(private val view: View, private val cellWidth: Int) :
        RecyclerView.ViewHolder(view) {

        fun bindVideo(
            item: SessionUIModel,
            onActionClicked: ((SessionUIModel) -> Unit)? = null
        ) = with(item) {
            with(view) {
                post {
                    layoutParams = layoutParams.apply {
                        width = cellWidth
                        height = height
                    }
                }
                txtSessionTitle.text = title
                txtSessionInstructor.text = instructorName
                txtSessionStatus.text = resources.getString(statusText)
                txtSessionStatus.setTextColor(ContextCompat.getColor(context, statusColor))
                txtSessionStatus.background = (ContextCompat.getDrawable(context, statusBg))
                txtSessionDescription.text = description
                txtSessionStartingDate.text = startingDate
                positiveActionText?.let {
                    btnSessionPositive.visible(true)
                    btnSessionPositive.text = resources.getString(it)
                    btnSessionPositive onClick { onActionClicked?.invoke(item) }
                } ?: run { btnSessionPositive.visible(false) }

            }
        }

    }

}