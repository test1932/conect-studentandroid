package com.nagwa.connect.modules.main.presentation.view.popup

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.getInflatedView
import com.nagwa.connect.core.extension.onClick
import kotlinx.android.synthetic.main.popup_signout.view.*

class UserMenuPopup constructor(private val context: Context, private val onSignout: () -> Unit) {
    //other
    private var popupWindow: PopupWindow
    private val rootView: View
    var isOpen = false
    var fullName: String? = null
        set(value) {
            rootView.txtFullName.text = value
            field = value
        }

    init {
        val width = context.resources.getDimension(R.dimen.pop_up_width_with_shadow)
        rootView = getContentView()
        onViewCreated()
        popupWindow = getPopup(width)
    }

    private fun getContentView(): View {
        return context.getInflatedView(R.layout.popup_signout, null)
    }

    private fun onViewCreated() = with(rootView) {
        containerSignout onClick {
            onSignout.invoke()
            closePopupWindow()
        }

    }

    fun openPopupWindow(
        view: View,
        popUpY: Int = context.resources.getDimension(R.dimen.bg_popup_marge_top).toInt()
    ) {
        if (!isOpen) {
            isOpen = true
            popupWindow.showAsDropDown(view, 0, popUpY)
        } else {
            closePopupWindow()
        }

    }

    fun closePopupWindow() {
        isOpen = false
        popupWindow.dismiss()

    }

    private fun getPopup(width: Float): PopupWindow {
        return PopupWindow(rootView, width.toInt(), LinearLayout.LayoutParams.WRAP_CONTENT)
    }

}