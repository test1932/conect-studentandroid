package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.SubscribeEntity
import io.reactivex.Single

interface CreateChannelRepository {
    fun publishStream(sessionId: String, userId: String): Single<ChannelDataEntity>
    fun subscribeToStream(subscribeEntity: SubscribeEntity): Single<ChannelDataEntity>
}