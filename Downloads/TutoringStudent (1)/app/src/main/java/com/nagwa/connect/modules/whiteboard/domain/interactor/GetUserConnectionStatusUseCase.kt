package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.ConnectionStatus
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import io.reactivex.Flowable
import javax.inject.Inject

class GetUserConnectionStatusUseCase @Inject constructor(
    private val messengerRepository: MessengerRepository
) : FlowableUseCase<Unit, ConnectionStatus>() {

    override fun build(params: Unit): Flowable<ConnectionStatus> {
        // true if connected, false if trying to connect
        return messengerRepository.getUserConnectionStatus()
    }
}