package com.nagwa.connect.core.application

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Youssef Ebrahim Elerian on 4/15/20.
 * youssef.elerian@gmail.com
 */

@GlideModule
class TutoringGlideModule : AppGlideModule()
