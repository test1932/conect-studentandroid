package com.nagwa.connect.modules.sessions.data.source

import android.content.SharedPreferences
import androidx.core.content.edit
import com.nagwa.connect.core.extension.fromJSON
import com.nagwa.connect.core.extension.toJson
import com.nagwa.connect.modules.sessions.data.model.MillicastProviderResponse
import com.nagwa.connect.modules.sessions.data.model.SessionResponse
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject


class SessionsLocalDS @Inject constructor(private val prefs: SharedPreferences) {
    //this will clear all old data and save only the new
    fun saveSessions(sessions: List<SessionWithProviderModel>): Completable =
        Completable.fromAction {
            prefs.edit(commit = true) {
                clear()
                sessions.map {
                    putString(it.session.sessionId.toString(), it.toJson())
                }
            }
        }

    fun saveProvider(
        sessionID: String,
        providerName: String,
        millicastInfo: MillicastProviderResponse?
    ): Completable =
        getSession(sessionID)
            .map {
                SessionWithProviderModel(
                    session = it.session,
                    providerName = providerName,
                    provider = millicastInfo
                )
            }
            .flatMapCompletable(::saveSession)


    fun getSessions(): Single<List<SessionWithProviderModel>> = Single.fromCallable {
        prefs.all.values
            .map { (it as String).fromJSON<SessionWithProviderModel>()!! }
    }

    fun getSession(sessionID: String): Single<SessionWithProviderModel> = Single.fromCallable {
        prefs.getString(sessionID, null)?.let {
            it.fromJSON<SessionWithProviderModel>()!!
        } ?: error("Session ID can't be found")

    }

    fun saveSession(sessionData: SessionWithProviderModel): Completable = Completable.fromAction {
        prefs.edit(commit = true) {
            putString(sessionData.session.sessionId.toString(), sessionData.toJson())
        }
    }

}

data class SessionWithProviderModel(
    val session: SessionResponse,
    val providerName: String,
    val provider: MillicastProviderResponse? = null
)