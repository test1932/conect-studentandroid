package com.nagwa.connect.modules.payment.domain.entity.params

/**
 * Authored by Mohamed Fathy on 19 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
data class GetPaymentParam(val userId: String, val portalId: String)