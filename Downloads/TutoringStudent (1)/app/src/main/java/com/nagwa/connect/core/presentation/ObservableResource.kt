package com.nagwa.connect.core.presentation

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.networkmanager.domain.excepation.NAException


class ObservableResource<T> : ReplayLiveData<T>() {
    val error: SingleLiveEvent<NAException> = ErrorLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()

    fun observe(
        owner: LifecycleOwner,
        successObserver: Observer<in T>,
        loadingObserver: Observer<Boolean>? = null,
        commonErrorObserver: Observer<in NAException> = Observer { },
        httpErrorConsumer: Observer<NAException>? = null,
        networkErrorConsumer: Observer<NAException>? = null,
        unExpectedErrorConsumer: Observer<NAException>? = null
    ) {
        super.observe(owner, successObserver)
        loadingObserver?.let { loading.observe(owner, it) }
        (error as ErrorLiveData).observe(
            owner,
            commonErrorObserver,
            httpErrorConsumer,
            networkErrorConsumer,
            unExpectedErrorConsumer
        )
    }

    override fun removeObservers(owner: LifecycleOwner) {
        super.removeObservers(owner)
        loading.removeObservers(owner)
        error.removeObservers(owner)
    }

    fun replayObservation() {
        super.replay()
    }

    fun toggleObservation(keepObserving: Boolean) {
        super.toggleObservationState(keepObserving)
    }

    fun observe(
        owner: LifecycleOwner,
        doOnSuccess: (T) -> Unit,
        doOnLoading: (() -> Unit)? = null,
        doOnError: (NAException) -> Unit = { },
        doOnHttpError: ((NAException) -> Unit)? = null,
        doOnNetworkError: ((NAException) -> Unit)? = null,
        doOnUnExpectedError: ((NAException) -> Unit)? = null,
        doOnTerminate: (() -> Unit)? = null
    ) {
        super.observe(owner, Observer {
            it?.let {
                doOnSuccess.invoke(it)
            } /*?: doOnComplete?.invoke()*/
        })

        loading.observe(owner, Observer { isLoading ->
            if (isLoading)
                doOnLoading?.invoke()
            else
                doOnTerminate?.invoke()
        })

        (error as ErrorLiveData).observe(owner,
            Observer { doOnError.invoke(it) },
            doOnHttpError?.let { Observer(doOnHttpError::invoke) },
            doOnNetworkError?.let { Observer(doOnNetworkError::invoke) },
            doOnUnExpectedError?.let { Observer(doOnUnExpectedError::invoke) })
    }

    class ErrorLiveData : SingleLiveEvent<NAException>() {
        private var ownerRef: LifecycleOwner? = null
        private var httpErrorConsumer: Observer<NAException>? = null
        private var networkErrorConsumer: Observer<NAException>? = null
        private var unExpectedErrorConsumer: Observer<NAException>? = null
        private var commonErrorConsumer: Observer<in NAException>? = null
        override fun setValue(t: NAException?) {
            ownerRef?.also {
                removeObservers(it)
                addProperObserver(t)
                super.setValue(t)
            }
        }

        override fun postValue(value: NAException) {
            ownerRef?.also {
                removeObservers(it)
                addProperObserver(value)
                super.postValue(value)
            }
        }

        private fun addProperObserver(value: NAException?) {
            when (value?.kind) {
                NAException.Kind.NETWORK -> networkErrorConsumer?.let { observe(ownerRef!!, it) }
                    ?: observe(ownerRef!!, commonErrorConsumer!!)
                NAException.Kind.HTTP -> httpErrorConsumer?.let { observe(ownerRef!!, it) }
                    ?: observe(ownerRef!!, commonErrorConsumer!!)
                NAException.Kind.UNEXPECTED -> unExpectedErrorConsumer?.let {
                    observe(
                        ownerRef!!,
                        it
                    )
                }
                    ?: observe(ownerRef!!, commonErrorConsumer!!)
                else -> {
                }
            }
        }


        fun observe(
            owner: LifecycleOwner,
            commonErrorConsumer: Observer<in NAException>,
            httpErrorConsumer: Observer<NAException>? = null,
            networkErrorConsumer: Observer<NAException>? = null,
            unExpectedErrorConsumer: Observer<NAException>? = null
        ) {
            super.observe(owner, commonErrorConsumer)
            this.ownerRef = owner
            this.commonErrorConsumer = commonErrorConsumer
            this.httpErrorConsumer = httpErrorConsumer
            this.networkErrorConsumer = networkErrorConsumer
            this.unExpectedErrorConsumer = unExpectedErrorConsumer
        }
    }
}