package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.core.extension.hasItemWith
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Completable
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
class ClearExpiredSessionsData @Inject constructor(
    private val repository: AttachmentsRepository,
    private val clearSessionData: ClearSessionData
) :
    CompletableUseCase<List<String>>() {
    override fun build(validSessionIDS: List<String>): Completable {
        return repository.getAllAttachmentsInfo()
            .flattenAsFlowable { ls -> ls.map { it.sessionID } }
            .filter { savedID -> !validSessionIDS.hasItemWith { savedID == it } } //gets expired sessions only
            .flatMapCompletable { clearSessionData.build(it) }
    }
}