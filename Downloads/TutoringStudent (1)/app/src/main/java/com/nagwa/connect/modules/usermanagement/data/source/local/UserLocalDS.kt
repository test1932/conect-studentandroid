package com.nagwa.connect.modules.usermanagement.data.source.local

import com.google.gson.Gson
import com.nagwa.connect.modules.usermanagement.domain.entity.UserWithPortalIdEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class UserLocalDS @Inject constructor(private val userCache: UserCache, private val gson: Gson) {
    fun saveUser(userEntity: UserWithPortalIdEntity) =
        Completable.fromAction { userCache.saveUserData(gson.toJson(userEntity)) }

    fun getUserData(): Maybe<UserWithPortalIdEntity> {
        val userData = userCache.getUserData()
        return Maybe.fromCallable {
            gson.fromJson(
                userData,
                UserWithPortalIdEntity::class.java
            )
        }
    }

    fun clearUserData(): Completable = Completable.fromAction { userCache.clearUserData() }
    fun saveResetToken(resetToken: String) =
        Completable.fromAction { userCache.saveResetToken(resetToken) }

    fun getResetToken(): Single<String> =
        Single.fromCallable { userCache.getResetToken() ?: error("Reset Token not found") }

    fun savePortalID(portalID: String) = Completable.fromAction { userCache.savePortalID(portalID) }
    fun getPortalID(): Single<String> =
        Single.fromCallable { userCache.getPortalID() ?: error("Portal ID not found") }
}
