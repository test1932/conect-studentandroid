package com.nagwa.connect.modules.whiteboard.domain.entity.params

data class DrawingParam(
    val lastClearId: Int,
    val height: Int,
    val width: Int
)