package  com.nagwa.connect.core.extension

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog


fun Context.createAlertWithPositiveButton(
    title: String? = null, message: String? = null, positiveText: String,
    positiveButtonListener: ((DialogInterface) -> Unit)? = null
): AlertDialog = with(AlertDialog.Builder(this)) {
    setTitle(title)
    setMessage(message)
    setCancelable(false)
    setPositiveButton(positiveText) { dialogInterface, _ ->
        positiveButtonListener?.invoke(dialogInterface)
    }
    return create()
}

fun Context.createAlertWithTwoButtons(
    title: String? = null, message: String? = null, positiveText: String,
    negativeText: String? = null,
    positiveButtonListener: ((DialogInterface) -> Unit)? = null,
    negativeButtonListener: ((DialogInterface) -> Unit)? = null
): AlertDialog = with(AlertDialog.Builder(this)) {
    setTitle(title)
    setMessage(message)
    setPositiveButton(positiveText) { dialogInterface, _ ->
        positiveButtonListener?.invoke(dialogInterface)
    }
    if (negativeText != null)
        setNegativeButton(negativeText) { dialogInterface, _ ->
            negativeButtonListener?.invoke(
                dialogInterface
            )
        }

    return create()
}

fun AlertDialog.changeNegativeButtonColor(): AlertDialog {
    //  if (isShowing)
    // getButton(AlertDialog.BUTTON_NEGATIVE)
    // .setTextColor(context.getResColor(R.color.colorBlack))
    return this
}

fun AlertDialog.Builder.createCustomAlert(
    @DrawableRes icon: Int? = null, title: String = "",
    message: String? = null,
    positiveText: String,
    negativeText: String? = null,
    positiveListener: View.OnClickListener? = null,
    negativeListener: View.OnClickListener? = null,
    cancelable: Boolean = true
): Dialog {
    var dialog = Dialog(context)
    icon?.let { setIcon(icon) }
    setTitle(title)
    message?.let { setMessage(message) }
    setPositiveButton(positiveText) { _, _ ->
        dialog.dismiss()
        positiveListener?.onClick(View(context))
    }
    negativeText?.let {
        setNegativeButton(it) { _, _ ->
            dialog.dismiss()
            negativeListener?.onClick(View(context))
        }
    }
    setCancelable(cancelable)
    dialog.findViewById<TextView>(android.R.id.message)?.isSingleLine = false
    dialog.findViewById<TextView>(android.R.id.title)?.isSingleLine = false

    dialog = create()
    dialog.show()

    //change negative color
    // if (negativeText != null)
    //  dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
    // .setTextColor(context.getResColor(R.color.colorBlack))


    return dialog

}