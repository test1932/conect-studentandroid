package com.nagwa.connect.modules.whiteboard.presentation.view.adapter

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.nagwa.connect.R
import com.nagwa.connect.core.domian.CustomTypefaceSpan
import com.nagwa.connect.core.extension.onClick
import com.nagwa.connect.core.extension.visible
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.ChatMessageUIModel
import kotlinx.android.synthetic.main.item_chat_message.view.*
import java.util.*
import javax.inject.Inject

class ChatMessagesAdapter @Inject constructor(context: Context) :
    RecyclerView.Adapter<ChatMessagesAdapter.ChatMessageVH>() {
    private val semiBold =
        CustomTypefaceSpan(null, ResourcesCompat.getFont(context, R.font.opensans_font_semibold))
    private val bold =
        CustomTypefaceSpan(null, ResourcesCompat.getFont(context, R.font.opensans_font_extrabold))
    private val regular =
        CustomTypefaceSpan(null, ResourcesCompat.getFont(context, R.font.opensans_font_regular))

    private var data: List<ChatMessageUIModel> = ArrayList()
    var onItemClicked: ((Long) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessageVH {
        return ChatMessageVH(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat_message, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ChatMessageVH, position: Int) =
        holder.bind(data[position])

    fun swapData(data: List<ChatMessageUIModel>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun swapDataWithOneNewItem(data: List<ChatMessageUIModel>) {
        this.data = data
        notifyItemInserted(data.size - 1)
    }

    inner class ChatMessageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ChatMessageUIModel) = with(itemView) {
            val spanString = SpannableStringBuilder().apply {
                append(item.sender, semiBold, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                append(": ", bold, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                append(item.text, regular, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }

            txtChatMessage.text = spanString

            errorChatMessage.visible(item.showError)
            progressBarChatMessage.visible(item.showLoading)
            if (item.isDimmed) {
                errorChatMessage.alpha = .4f
                txtChatMessage.alpha = .4f
            } else {
                errorChatMessage.alpha = 1f
                txtChatMessage.alpha = 1f
            }
            itemView.onClick { onItemClicked?.invoke(item.id) }
        }
    }
}