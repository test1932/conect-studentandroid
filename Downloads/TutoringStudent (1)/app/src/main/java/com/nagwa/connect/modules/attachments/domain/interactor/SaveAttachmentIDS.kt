package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import com.nagwa.connect.modules.attachments.domain.SessionFileID
import io.reactivex.Completable
import javax.inject.Inject

class SaveAttachmentIDS @Inject constructor(private val repository: AttachmentsRepository) :
    CompletableUseCase<List<SessionFileID>>() {
    override fun build(params:List<SessionFileID>): Completable = repository.saveInfo(params)

}