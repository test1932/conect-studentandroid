package com.nagwa.connect.modules.payment.di

import com.nagwa.connect.modules.payment.data.PaymentRepositoryImpl
import com.nagwa.connect.modules.payment.domain.repository.PaymentRepository
import dagger.Binds
import dagger.Module

/**
 * Authored by Mohamed Fathy on 19 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
@Module
abstract class PaymentModule {

    @Binds
    internal abstract fun bindPaymentRepo(impl: PaymentRepositoryImpl): PaymentRepository
}