package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.core.domian.ApplicationIntegration
import com.nagwa.connect.core.extension.isNetworkConnected
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.IceServersListEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.MilliCastIceServersEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class RaiseHandUseCase @Inject constructor(
    private val getCallDataUseCase: GetCallDataUseCase,
    private val connectAsPublisherUseCase: ConnectAsPublisherUseCase,
    private val actionsRepository: ActionsRepository
) :
    CompletableUseCase<String>() {
    override fun build(params: String): Completable {
        return getCallDataUseCase.build(params)
            .flatMapCompletable { callData ->
                connectAsPublisherUseCase.build(callData)
                    .defaultIfEmpty(
                        ChannelDataEntity(
                            MilliCastIceServersEntity(IceServersListEntity(emptyList()), ""), "", "", "", "", "", ""
                        )
                    ).retry(3) {
                        ApplicationIntegration.getApplication().isNetworkConnected()
                    }
                    .flatMapCompletable { channelData ->
                        actionsRepository.sendAction(
                            ActionEntity(
                                action = CallActionTypes.RaiseHand.value,
                                studentId = callData.studentId,
                                streamName = channelData.streamName
                            )
                        ).ignoreElement()
                    }
            }
    }


}