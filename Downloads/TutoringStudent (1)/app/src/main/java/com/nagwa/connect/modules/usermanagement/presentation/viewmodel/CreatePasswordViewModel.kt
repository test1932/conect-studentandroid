package com.nagwa.connect.modules.usermanagement.presentation.viewmodel

import addTo
import androidx.annotation.StringRes
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.base.presentation.viewmodel.getDefaultErrorMessages
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.entity.*
import com.nagwa.connect.modules.usermanagement.domain.entity.param.CreatePasswordParam
import com.nagwa.connect.modules.usermanagement.domain.interactor.CreateUserPasswordUseCase
import javax.inject.Inject

class CreatePasswordViewModel @Inject constructor(private val createUserPassword: CreateUserPasswordUseCase) :
    TutoringViewModel() {
    val passwordStepUIModel = SingleLiveEvent<CreatePasswordUIModel>()

    fun createUserPassword(
        email: String,
        password: String,
        confirmPassword: String,
        userId: String,
        portalId: String
    ) {
        passwordStepUIModel.value = CreatePasswordUIModel(
            showLoading = true,
            error = null,
            showNextPage = false
        )
        createUserPassword.build(
            CreatePasswordParam(password, confirmPassword, email, userId, portalId)
        )
            .map { it.user }
            .applyAsyncSchedulers()
            .subscribe({ mapResultToUIModel() }, ::mapErrorToUIModel)
            .addTo(compositeDisposable)
    }

    private fun mapErrorToUIModel(throwable: Throwable) {
        passwordStepUIModel.value = when (throwable) {
            is PaymentRequiredError -> {
                CreatePasswordUIModel(
                    showLoading = false,
                    error = null,
                    showNextPage = false,
                    returnToEmailStep = true
                )
            }
            is PaymentUserDeactivatedError -> {
                CreatePasswordUIModel(
                    showLoading = false,
                    error = null,
                    showNextPage = false,
                    returnToEmailStep = true,
                    isDeactivated = true
                )
            }
            else -> {
                CreatePasswordUIModel(
                    showLoading = false,
                    error = getErrorMessage(throwable),
                    showNextPage = false
                )
            }
        }
    }

    private fun getErrorMessage(throwable: Throwable): Int {
        return when (throwable) {
            EmptyCreationPasswords -> R.string.msgMissingRequiredFields
            NotMatchedPasswords -> R.string.msgNotMatchingPasswords
            NotWellFormattedPassword -> R.string.msgNotWellFormattedNewPassword
            else -> getDefaultErrorMessages(throwable)
        }
    }

    private fun mapResultToUIModel() {
        passwordStepUIModel.value =
            CreatePasswordUIModel(
                showLoading = false,
                error = null,
                showNextPage = true
            )
    }
}

data class CreatePasswordUIModel(
    val showLoading: Boolean,
    @StringRes val error: Int?,
    val showNextPage: Boolean,
    val returnToEmailStep: Boolean = false,
    val isDeactivated: Boolean = false
)