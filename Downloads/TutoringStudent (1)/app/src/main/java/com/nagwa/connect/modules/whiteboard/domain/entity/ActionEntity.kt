package com.nagwa.connect.modules.whiteboard.domain.entity


data class ActionEntity(
    val action: Int,
    val studentId: String?,
    var streamName: String? = null,
    var studentName: String? = null,
    val senderId: String? = "",
)