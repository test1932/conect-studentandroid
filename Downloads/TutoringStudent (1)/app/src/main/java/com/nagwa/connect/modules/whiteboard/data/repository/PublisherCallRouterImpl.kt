package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.whiteboard.data.model.mapper.mapToPublishMilliecastParams
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toModel
import com.nagwa.connect.modules.whiteboard.di.AGORA_BINDING_KEY
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.PublisherCallRouter
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.millicast.HoldableCall
import com.nagwa.sessionlibrary.session.millicast.MillicastHoldableCallProvider
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Provider

@WhiteboardScope
class PublisherCallRouterImpl @Inject constructor(
    private val millicastFactory: MillicastHoldableCallProvider.Companion.Factory,
    private val callProviders: Map<String, @JvmSuppressWildcards Provider<NagwaCallProvider>>
) :
    PublisherCallRouter {

    private var callProvider: NagwaCallProvider? = null

    override fun join(channelDataEntity: ChannelDataEntity): Completable {
        callProvider = millicastFactory.create()
        return callProvider?.run {
            startCall(channelDataEntity.toModel().mapToPublishMilliecastParams(false))
                .filter { it == CallStatus.Connected }
                .takeUntil { it == CallStatus.Connected }
                .flatMapCompletable {
                    (callProvider as HoldableCall).setCallOnHold(true) //auto rejoin for onHold can fail we must send mute action if so
                }
        }
            ?: nullProviderError()
    }

    override fun switchRole(isBroadCaster: Boolean): Completable {
        callProvider = callProviders[AGORA_BINDING_KEY]?.get()
        return callProvider?.switchRole(isBroadCaster) ?: nullProviderError()
    }

    override fun muteChannel(): Completable =
        callProvider?.muteCall() ?: nullProviderError()

    override fun unMuteChannel(): Completable =
        callProvider?.unMuteCall() ?: nullProviderError()

    override fun unHold(): Completable {
        return (callProvider as? HoldableCall)?.setCallOnHold(false)
            ?: Completable.error(java.lang.IllegalStateException("call not found to perform unHold"))
    }

    override fun endCall(): Completable =
        callProvider?.endCall()?.doOnComplete {
            callProvider = null
        } ?: nullProviderError()

    private fun nullProviderError() =
        Completable.error(IllegalStateException("Call Provider is Null !"))

}