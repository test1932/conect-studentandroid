package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.MayableUseCase
import com.nagwa.connect.modules.usermanagement.domain.entity.UserWithPortalIdEntity
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import io.reactivex.Maybe
import javax.inject.Inject

class GetUserUseCase @Inject constructor(private val userRepository: UserRepository) :
    MayableUseCase<Unit, UserWithPortalIdEntity>() {

    override fun build(params: Unit): Maybe<UserWithPortalIdEntity> {
        return userRepository.getUser()
    }
}