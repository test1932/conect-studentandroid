package com.nagwa.connect.modules.whiteboard.domain.entity.params

data class PointsRequestParam(
    val sessionID :String,
    val lastID: Int = 0,
    val drawingSize: DrawingSize,
    val educatorDrawingSize :DrawingSize? = null,
    val drawing: DrawingParam? = null
)