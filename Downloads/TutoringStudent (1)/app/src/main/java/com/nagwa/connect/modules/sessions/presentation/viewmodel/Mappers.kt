package com.nagwa.connect.modules.sessions.presentation.viewmodel

import com.nagwa.connect.R
import com.nagwa.connect.core.extension.convertToUiDate
import com.nagwa.connect.core.presentation.view.StringsProvider
import java.util.*

fun SessionUIState.toUIModel(stringsProvider: StringsProvider): SessionUIModel {
    return SessionUIModel(
        id = id,
        title = title,
        instructorName = teacherName,
        statusText = sessionType.toText(),
        statusColor = sessionType.toTextColor(),
        statusBg = sessionType.toBG(),
        positiveActionText = sessionType.toPositiveText(),
        startingDate = stringsProvider.provideString(
            R.string.session_start_date,
            Date(startingDate).convertToUiDate("yyyy-MM-dd, hh:mm a")
        ),
        description = stringsProvider.provideString(
            R.string.session_description,
            subject,
            studentsNumber,
            duration
        )
    )
}

fun SessionType.toText(): Int = when (this) {
    SessionType.Going -> R.string.session_status_in_progress
    SessionType.Scheduled -> R.string.session_status_scheduled
}

fun SessionType.toTextColor(): Int = when (this) {
    SessionType.Going -> R.color.colorPrimary
    SessionType.Scheduled -> R.color.colorPeaGreen
}

fun SessionType.toBG(): Int = when (this) {
    SessionType.Going -> R.drawable.shape_session_status_inprogress_bg
    SessionType.Scheduled -> R.drawable.shape_session_status_scheduled_bg
}

fun SessionType.toPositiveText(): Int? = when (this) {
    SessionType.Going -> R.string.session_btn_join
    SessionType.Scheduled -> null
}