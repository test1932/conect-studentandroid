package com.nagwa.connect.modules.sessions.presentation.view

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.core.content.ContextCompat
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.BaseDialogFragment
import com.nagwa.connect.core.extension.getResDimenPx
import com.nagwa.connect.core.extension.observe
import com.nagwa.connect.core.extension.onClick
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import kotlinx.android.synthetic.main.dialog_session_preperation.*

class SessionPreparingDialog : BaseDialogFragment() {
    var onCancelClicked: (() -> Unit)? = null
    val progressStream = SingleLiveEvent<Int>()
    override fun getDialogGravity(): Int = Gravity.CENTER
    override fun getLayoutResource(): Int = R.layout.dialog_session_preperation
    override fun getDialogBackground(): Drawable =
        ContextCompat.getDrawable(requireContext(), R.drawable.bg_session_preparing_dialog)!!

    override fun getDialogWidth(): Int =
        requireContext().getResDimenPx(R.dimen.preparing_dialog_width)

    override fun getDialogHeight(): Int =
        requireContext().getResDimenPx(R.dimen.preparing_dialog_height)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnCancelPreparing onClick {
            onCancelClicked?.invoke()
            dismiss()
        }
        progressStream.observe(viewLifecycleOwner, {
            txtProgress.text = "$it%"
            progress_horizontal.progress = it
        })
    }

    companion object {
        fun newInstance(): SessionPreparingDialog {
            return SessionPreparingDialog()
        }
    }
}