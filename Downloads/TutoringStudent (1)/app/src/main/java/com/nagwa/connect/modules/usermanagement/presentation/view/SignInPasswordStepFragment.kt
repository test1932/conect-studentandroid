package com.nagwa.connect.modules.usermanagement.presentation.view

import android.content.Context
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringFragment
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.PasswordStepUIModel
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.PasswordStepViewModel
import kotlinx.android.synthetic.main.fragment_password_step.*
import javax.inject.Inject

class SignInPasswordStepFragment : TutoringFragment() {
    @Inject
    lateinit var mViewModelFactory: TutoringViewModelFactory
    private lateinit var mListener: SignInPasswordListener
    private lateinit var mRootView: View
    private val mViewModel by lazy {
        ViewModelProvider(this, mViewModelFactory)
            .get(PasswordStepViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.getInflatedView(R.layout.fragment_password_step, container)
        activity?.hideKeyboard()
        return mRootView;
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservation()
        initListeners()
    }

    private fun initViews() = with(mRootView) {
        signInPasswordEt.transformationMethod = PasswordTransformationMethod()
        signInPasswordEt.onKeyboardGoActionClicked { signInUser() }
        txtUserEmail.text = getEmail()
        hideError()

    }

    private fun initListeners() {
        signInPasswordEt onKeyboardGoActionClicked ::signInUser
        signInBtn onClick ::signInUser
    }

    private fun signInUser() {
        txtError.text = ""
        mViewModel.signIn(
            email = getEmail(),
            password = signInPasswordEt.text.toString(),
            userId = getUserId(),
            portalId = getPortalId()
        )
    }

    private fun initObservation() = with(mViewModel) {
        passwordStepUIModel.observe(viewLifecycleOwner, ::render)
    }

    private fun render(uiModel: PasswordStepUIModel) {
        with(uiModel) {
            if (showLoading) onDisable() else onEnable()
            error?.let(::showError) ?: hideError()
            showNextPage.takeIf { it }?.let { mListener.onValidPassword() }
            returnToEmailStep.takeIf { it }?.let { mListener.onPaymentError(isDeactivated) }
        }
    }

    private fun onDisable() {
        activity?.hideKeyboard()
        signInBtn.disable(true)
        mListener.disableInteraction(true)
        progressSignIn.visible(true)
    }


    private fun onEnable() {
        signInBtn.enable(true)
        mListener.disableInteraction(false)
        progressSignIn.visible(false)
    }

    private fun showError(@StringRes message: Int) {
        txtError.text = getString(message)
        txtError.visible(true)
    }

    private fun hideError() {
        txtError.visible(false)
        txtError.text = ""
    }

    override fun onAttach(context: Context) {
        mListener = if (context is SignInPasswordListener) context
        else error("Activity should implement SingInPasswordListener")
        super.onAttach(context)
    }


    private fun getEmail(): String = arguments?.getString(EMAIL_KEY) ?: error("email not found")

    private fun getUserId(): String = arguments?.getString(USER_ID_KEY) ?: error("userId not found")

    private fun getPortalId(): String =
        arguments?.getString(PORTAL_ID_KEY) ?: error("portalId not found")

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment SignInEmailStepFragment.
         */
        private const val EMAIL_KEY = "EMAIL_KEY"
        private const val USER_ID_KEY = "USER_ID_KEY"
        private const val PORTAL_ID_KEY = "PORTAL_ID_KEY"

        @JvmStatic
        fun newInstance(email: String, userId: String, portalId: String) =
            SignInPasswordStepFragment().apply {
                arguments = Bundle().apply {
                    putString(EMAIL_KEY, email)
                    putString(USER_ID_KEY, userId)
                    putString(PORTAL_ID_KEY, portalId)
                }
            }

    }
}
