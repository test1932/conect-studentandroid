package com.nagwa.connect.modules.payment.domain.repository

import com.nagwa.connect.modules.payment.domain.entity.PaymentEntity
import io.reactivex.Single

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
interface PaymentRepository {

    fun getInvoiceStatus(userId: String, portalId: String): Single<PaymentEntity>
}