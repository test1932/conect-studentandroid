package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.core.domian.StudentNotFound
import com.nagwa.connect.modules.whiteboard.data.model.mapper.mapToViewerMilliecastParams
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toModel
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.interactor.TAG
import com.nagwa.connect.modules.whiteboard.domain.repository.ViewerCallRouter
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.millicast.HoldableCall
import com.nagwa.sessionlibrary.session.millicast.MillicastHoldableCallProvider
import io.reactivex.Completable
import io.reactivex.Flowable
import timber.log.Timber
import javax.inject.Inject

@WhiteboardScope
class ViewerCallRouterImpl @Inject constructor(
    private val factory: MillicastHoldableCallProvider.Companion.Factory
) :
    ViewerCallRouter {
    val store: MutableMap<String, NagwaCallProvider> = hashMapOf()
    override fun join(channelDataEntity: ChannelDataEntity, userId: String): Completable {
        store[userId] = factory.create()
        Timber.tag(TAG).d("student ID $userId is added to the store")
        return store[userId]?.run {
            startCall(channelDataEntity.toModel().mapToViewerMilliecastParams(true))
                .filter { it == CallStatus.Connected }
                .takeUntil { it == CallStatus.Connected }
                .flatMapCompletable {
                    (store[userId] as HoldableCall).setCallOnHold(true) //auto rejoin for onHold can fail we must send mute action if so
                }
        } ?: noProviderError(userId)
    }

    override fun muteChannel(userId: String): Completable =
        store[userId]?.muteCall() ?: noProviderError(userId)

    override fun unMuteChannel(userId: String): Completable =
        store[userId]?.unMuteCall() ?: noProviderError(userId)

    override fun unHold(studentId: String): Completable {
        Timber.tag(TAG).d("unHold student ID $studentId ")
        return (store[studentId] as? HoldableCall)?.setCallOnHold(false)
            ?: Completable.error(StudentNotFound("student call not found to perform unHold"))
    }

    override fun endCall(userId: String): Completable =
        store[userId]?.endCall()?.doOnComplete {
            removeProvider(userId)
        } ?: noProviderError(userId)

    private fun removeProvider(userId: String) {
        Timber.tag(TAG).d("student ID $userId call ended")
        if (store.containsKey(userId)) {
            store.remove(userId)
            Timber.tag(TAG).d("student ID $userId removed")
        }
    }

    override fun endCall(): Completable {
        Timber.tag(TAG).d("ending all calls")
        return Flowable.fromIterable(store.values).flatMapCompletable {
            it.endCall()
        }
    }

    private fun noProviderError(userId: String) =
        Completable.error(IllegalStateException("No provider found for userId $userId"))
}