package com.nagwa.connect.modules.whiteboard.domain.entity.params

data class ChannelRegisterParam(
    val sessionID: String,
    val documentName: String,
    val collectionName: String
)