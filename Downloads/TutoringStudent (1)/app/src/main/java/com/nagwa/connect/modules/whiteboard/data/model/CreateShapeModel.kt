package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class CreateShapeModel(
    override val id: Int,
    @SerializedName("a")
    override val actionType: Int = DrawActionType.CREATE_SHAPE.type,
    @SerializedName("c")
    val color: String,
    @SerializedName("p")
    val points: List<PointModel>,
    @SerializedName("s")
    val strokeWidth: Int,
    @SerializedName("t")
    val typeOfDrawing: Int,
) : DrawModel()