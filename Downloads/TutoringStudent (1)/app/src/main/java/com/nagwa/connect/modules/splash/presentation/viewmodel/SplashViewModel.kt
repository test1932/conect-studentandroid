package com.nagwa.connect.modules.splash.presentation.viewmodel


import addTo
import applyAsyncSchedulers
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.interactor.IsUserLoggedInUseCase
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 3/12/20.
 * youssef.elerian@gmail.com
 */
class SplashViewModel @Inject constructor(
    private val isLoggedInUseCase: IsUserLoggedInUseCase
) : TutoringViewModel() {

    val isLoggedInLiveData: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun isLoggedIn() {
        isLoggedInUseCase.build(Unit)
            .delay(SPLASH_TIME, TimeUnit.MILLISECONDS)
            .applyAsyncSchedulers().subscribe({
                isLoggedInLiveData.value = it
            }, {}).addTo(compositeDisposable)
    }

    companion object {
        private const val SPLASH_TIME: Long = 3000 // 3 seconds
    }
}