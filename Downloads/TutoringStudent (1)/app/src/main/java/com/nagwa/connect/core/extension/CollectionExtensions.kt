package com.nagwa.connect.core.extension

fun <T> List<T>.replace(predicate: T.() -> Boolean, replacer: T.() -> T): List<T> {
    return map {
        if (predicate(it)) replacer(it) else it
    }
}

fun <T> List<T>.hasItemWith(predicate: (T) -> Boolean): Boolean {
    return firstOrNull { predicate(it) } != null
}


