package com.nagwa.connect.modules.whiteboard.presentation.viewmodel

import addTo
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.interactor.GetUserUseCase
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.CallActionWithErrorEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes
import com.nagwa.connect.modules.whiteboard.domain.interactor.CancelRaiseHandUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetActionsUpdates
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetEndingActionsUpdates
import com.nagwa.connect.modules.whiteboard.domain.interactor.RaiseHandUseCase
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import retryToSubscribe
import timber.log.Timber
import javax.inject.Inject

class ActionsViewModel @Inject constructor(
    getUserUseCase: GetUserUseCase,
    private val raiseHandUseCase: RaiseHandUseCase,
    private val cancelRaiseHandUseCaseUseCase: CancelRaiseHandUseCase,
    private val getActionsUpdates: GetActionsUpdates,
    private val getEndingActionsUpdates: GetEndingActionsUpdates
) : TutoringViewModel() {
    val educatorEndSessionLiveData = SingleLiveEvent<Int>()
    val raiseHandLiveData = SingleLiveEvent<RaiseHandBtnState>()
    val resetDrawingLiveData = SingleLiveEvent<Boolean>()
    val raiseHandErrorResLiveData = SingleLiveEvent<Int>()
    val currentPublisherStudentLiveData = SingleLiveEvent<String>()
    private var currentPublisherStudentId = ""
    val educatorBlockingEvent = SingleLiveEvent<Boolean>()
    private var studentId = ""
    private var sessionId = ""
    private var educatorId = ""

    private var raiseHandDisposable: Disposable? = null

    init {
        getUserUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribeBy(onSuccess = { studentId = it.user.userId }, onError = Timber::e)
            .addTo(compositeDisposable)
    }

    fun observeActionsUpdates(sessionId: String, educatorId: String) {
        this.sessionId = sessionId
        this.educatorId = educatorId

        observeOnEndingActions()

        getActionsUpdates.build(sessionId)
            .retryToSubscribe()
            .applyAsyncSchedulers()
            .subscribeBy(onComplete = {},
                onNext = {
                    handleAction(it)
                },
                onError = { Timber.e(it) })
            .addTo(compositeDisposable)
    }

    private fun observeOnEndingActions() {
        getEndingActionsUpdates.build(Unit)
            .applyAsyncSchedulers()
            .subscribeBy(onComplete = {},
                onNext = {
                    handleEndingAction(it)
                },
                onError = { Timber.e(it) })
            .addTo(compositeDisposable)
    }

    fun raiseHand() {
        raiseHandDisposable = raiseHandUseCase.build(sessionId)
            .applyAsyncSchedulers()
            .subscribeBy(
                onComplete = {},
                onError = {
                    raiseHandLiveData.value = RaiseHandBtnState.UN_RAISED
                    raiseHandErrorResLiveData.value = R.string.action_failed
                })
        // .addTo(compositeDisposable)
    }

    fun cancelRaiseHand() {
        raiseHandDisposable?.dispose()
        cancelRaiseHandUseCaseUseCase.build(sessionId)
            .applyAsyncSchedulers()
            .subscribeBy(
                onComplete = {},
                onError = {
                    raiseHandLiveData.value = RaiseHandBtnState.RAISING
                    raiseHandErrorResLiveData.value = R.string.action_failed
                })
            .addTo(compositeDisposable)
    }

    private fun handleAction(callActionEntity: CallActionWithErrorEntity) {
        when (callActionEntity.action.action) {
            CallActionTypes.RaiseHand.value -> {
            }
            CallActionTypes.UnMute.value -> {
                unmute(callActionEntity)
            }
            CallActionTypes.Mute.value -> {
                mute(callActionEntity, false)
            }
            CallActionTypes.RESET.value -> {
                reset(callActionEntity)
            }
            CallActionTypes.RAISE_HAND_FAILURE.value -> {
                mute(callActionEntity, true)
            }
        }
    }

    private fun unmute(callActionEntity: CallActionWithErrorEntity) {
        if (callActionEntity.action.studentId == studentId) {
            raiseHandLiveData.value =
                if (callActionEntity.isSuccess) RaiseHandBtnState.SPEAKING else RaiseHandBtnState.UN_RAISED
        }
        currentPublisherStudentLiveData.value =
            if (callActionEntity.isSuccess) callActionEntity.action.studentName else ""
        currentPublisherStudentId =
            if (callActionEntity.isSuccess) callActionEntity.action.studentId.toString() else ""
    }

    private fun reset(callActionEntity: CallActionWithErrorEntity) {
        if (callActionEntity.action.senderId == educatorId) {
            raiseHandLiveData.value = RaiseHandBtnState.UN_RAISED
           // resetDrawingLiveData.value = true
            currentPublisherStudentLiveData.value = ""
            currentPublisherStudentId = ""
        } else if (currentPublisherStudentId == callActionEntity.action.studentId) {
            currentPublisherStudentLiveData.value = ""
            currentPublisherStudentId = ""
        }
    }

    private fun mute(
        callActionEntity: CallActionWithErrorEntity, showError: Boolean
    ) {
        if (callActionEntity.action.studentId == studentId) {
            raiseHandLiveData.value = RaiseHandBtnState.UN_RAISED
            currentPublisherStudentLiveData.value = ""
            currentPublisherStudentId = ""
            if (showError) raiseHandErrorResLiveData.value = R.string.action_failed
        } else if (callActionEntity.action.studentId == currentPublisherStudentId) {
            currentPublisherStudentLiveData.value = ""
            currentPublisherStudentId = ""
        }
    }

    private fun handleEndingAction(actionEntity: ActionEntity) {
        when (actionEntity.action) {
            CallActionTypes.END_SESSION.value -> {
                educatorEndSessionLiveData.value = null
            }
            CallActionTypes.TERMINATED.value ->
                educatorEndSessionLiveData.value = R.string.msg_failed_to_connect_with_teacher
            CallActionTypes.INACTIVE.value ->
                educatorBlockingEvent.value = true
            CallActionTypes.ACTIVE.value ->
                educatorBlockingEvent.value = false
        }
    }

    enum class RaiseHandBtnState {
        SPEAKING, RAISING, UN_RAISED
    }

    override fun onCleared() {
        raiseHandDisposable?.dispose()
        super.onCleared()
    }
}