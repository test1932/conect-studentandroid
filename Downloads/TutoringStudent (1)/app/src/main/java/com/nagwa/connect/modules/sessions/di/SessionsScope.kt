package com.nagwa.connect.modules.sessions.di

import javax.inject.Scope


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SessionsScope