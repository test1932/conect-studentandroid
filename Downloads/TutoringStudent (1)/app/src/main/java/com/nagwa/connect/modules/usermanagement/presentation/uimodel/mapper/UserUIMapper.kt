package com.nagwa.connect.modules.usermanagement.presentation.uimodel.mapper

import com.nagwa.connect.modules.usermanagement.domain.entity.VerifyUserEntity
import com.nagwa.connect.modules.usermanagement.presentation.uimodel.UserRoleUI

fun <K : VerifyUserEntity, V : String> Pair<K, V>.toUI() =
    UserRoleUI(user = first, userRole = second)