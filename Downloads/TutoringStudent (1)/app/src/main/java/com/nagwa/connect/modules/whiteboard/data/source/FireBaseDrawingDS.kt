package com.nagwa.connect.modules.whiteboard.data.source

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.nagwa.connect.core.extension.toJson
import com.nagwa.connect.core.extension.toObject
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingParam
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class FireBaseDrawingDS @Inject constructor(private val fireStore: FirebaseFirestore) {

    private val pointsListSubject: PublishSubject<String> = PublishSubject.create()
    private var fireStoreChannel: CollectionReference? = null
    private var registrationListener: ListenerRegistration? = null
    private var isFirstCall  = true

    fun getLastClearId(sessionID: String): Single<Int> {
        return Single.create { emitter ->
            fireStore.collection(sessionID)
                .document(DRAWING_DOCUMENT).get()
                .addOnSuccessListener {
                    val clearedId = (it.data?.get(LAST_CLEAR_ID) ?: 0).toString().toInt()
                    emitter.onSuccess(clearedId)
                }
                .addOnFailureListener {
                    emitter.tryOnError(it)
                }
        }
    }

    fun getDrawingParams(sessionID: String): Single<DrawingParam> {
        return Single.create { emitter ->
            fireStore.collection(sessionID)
                    .document(DRAWING_DOCUMENT).get()
                    .addOnSuccessListener {
                        val param = it.data?.toJson()?.toObject<DrawingParam>()
                        param?.let { it1 -> emitter.onSuccess(it1) } ?: run {
                            emitter.onSuccess(DrawingParam(0, 0, 0))
                        }
                    }
                    .addOnFailureListener {
                        emitter.tryOnError(it)
                    }
        }
    }

    fun getMissedDrawing(
        sessionID: String,
        lastID: Int
    ): Single<List<String>> {
        return Single.create { emitter ->
            val instance = getChannelInstanceQuery(sessionID, lastID)
            instance.orderBy(ACTION_ID_Key).get()
                .addOnSuccessListener {
                    val pointsList = mutableListOf<String>()
                    it.documents.forEach {
                        it.data.toJson()?.let { it1 -> pointsList.add(it1) }
                    }
                    emitter.onSuccess(pointsList)
                }
                .addOnFailureListener { emitter.tryOnError(it) }
        }
    }

    private fun getChannelInstanceQuery(sessionID: String, lastID: Int): Query {
        val query = fireStore.collection(sessionID)
            .document(DRAWING_DOCUMENT)
            .collection(DRAWING_ACTIONS)
        return when {
            lastID != 0 -> query.whereGreaterThan(ACTION_ID_Key, lastID)
            else -> query
        }
    }

    fun observeOnDrawing(sessionID: String, lastID: Int): Flowable<String> {
        val query = getChannelInstanceQuery(sessionID, lastID)
        registrationListener = query
            .addSnapshotListener { querySnapShot, error ->
                querySnapShot?.let { snapShot ->
                    if (! isFirstCall)
                        snapShot.documentChanges.forEach {
                                it.document.data.toJson()?.let { it1 ->
                                    pointsListSubject.onNext(it1)
                                }
                        }
                    isFirstCall= false
                }

            }
        return pointsListSubject.toFlowable(BackpressureStrategy.LATEST)
    }

    fun unRegister(): Single<Boolean> {
        return Single.fromCallable {
            registrationListener?.remove()
            true
        }
    }

    companion object {
        private const val DRAWING_DOCUMENT = "Drawing"
        private const val DRAWING_ACTIONS = "Actions"
        private const val ACTION_ID_Key = "id"
        private const val LAST_CLEAR_ID = "lastClearId"
    }
}