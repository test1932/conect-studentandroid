package com.nagwa.connect.modules.whiteboard.domain.entity.enums

enum class ConnectionStatus (val status: Int){
    RECONNECTING(4),
    CONNECTED(3),
    ABORTED(5),
    UNKNOWN(-1)


}