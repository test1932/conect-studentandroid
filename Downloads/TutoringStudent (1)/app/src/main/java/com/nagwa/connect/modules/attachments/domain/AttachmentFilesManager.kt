package com.nagwa.connect.modules.attachments.domain

import io.reactivex.Completable
import io.reactivex.Single
import java.io.File

interface AttachmentFilesManager {
    // return path of extracted folder
    fun extractFiles(compressedFilePath: String,sessionID:String): Single<String>
    fun listOfImages(imagesDir: File): Single<List<File>>
    fun getDownloadFile(sessionID: String):Single<File>
    fun deleteAttachmentsFiles(sessionID: String):Completable
}