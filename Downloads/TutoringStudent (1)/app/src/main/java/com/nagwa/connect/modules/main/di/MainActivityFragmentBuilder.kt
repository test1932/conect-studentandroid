package com.nagwa.connect.modules.main.di

import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.main.presentation.viewmodel.MainViewModel
import com.nagwa.connect.modules.sessions.di.SessionsModule
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.connect.modules.sessions.di.SessionsViewModelModule
import com.nagwa.connect.modules.sessions.presentation.view.SessionsFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import javax.inject.Scope


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@Module
abstract class MainActivityFragmentBuilder {

    @SessionsScope
    @ContributesAndroidInjector(modules = [SessionsViewModelModule::class, SessionsModule::class])
    abstract fun contributeSessionsFragmentAndroidInjector(): SessionsFragment

}

@Module
abstract class MainModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(MainViewModel::class)
    @MainScope
    internal abstract fun provideMainViewModel(viewModel: MainViewModel): ViewModel
}

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope