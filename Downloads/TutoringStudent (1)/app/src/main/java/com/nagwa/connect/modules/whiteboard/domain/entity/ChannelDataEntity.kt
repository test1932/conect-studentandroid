package com.nagwa.connect.modules.whiteboard.domain.entity

data class ChannelDataEntity(
    val milliCastIceServers: MilliCastIceServersEntity,
    val publishingToken: String,
    val url: String,
    val jwt: String,
    val accountId: String,
    val streamName: String,
    val wsurl: String
)

data class MilliCastIceServersEntity(
    val iceServersList: IceServersListEntity,
    val status: String
)

data class IceServersListEntity(val iceServers: List<IceServersEntity>)

data class IceServersEntity(
    val url: String,
    val username: String?,
    val credential: String?
)