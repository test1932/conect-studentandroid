package com.nagwa.connect.core.application

import android.app.Application
import android.graphics.Bitmap
import androidx.annotation.StringRes
import com.nagwa.connect.BuildConfig
import com.nagwa.connect.core.di.DaggerAppComponent
import com.nagwa.connect.core.domian.ApplicationIntegration
import com.nagwa.connect.core.extension.loadBitmap
import com.nagwa.connect.core.presentation.view.BitmapProvider
import com.nagwa.connect.core.presentation.view.StringsProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 2019-11-05.
 * youssef.elerian@gmail.com
 */
class TutoringApplication : Application(), HasAndroidInjector, StringsProvider, BitmapProvider {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationIntegration.with(this)
        instance = this
        DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)


        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

    override fun provideString(@StringRes resID: Int): String = resources.getString(resID)
    override fun provideString(@StringRes resId: Int, vararg formatArgs: Any): String =
        resources.getString(resId, *formatArgs)

    override fun provideBitmapAsync(
        imagePath: String,
        width: Int,
        height: Int,
        listener: (Bitmap?) -> Unit
    ) = applicationContext.loadBitmap(imagePath, width, height, listener)


    companion object {
        lateinit var instance: TutoringApplication
            private set
    }
}

