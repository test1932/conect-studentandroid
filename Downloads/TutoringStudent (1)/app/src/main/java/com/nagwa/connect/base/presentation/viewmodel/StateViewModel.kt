package com.nagwa.connect.base.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.nagwa.connect.R
import com.nagwa.connect.core.domian.NotDeliveredRequest
import timber.log.Timber

abstract class StateViewModel<State, UIModel>(val initialState: State) : TutoringViewModel() {
    val uiModel = MutableLiveData<UIModel>()
    private var silentUpdate: Boolean = false
    private var immediateUpdate: Boolean = false
    protected var lastObservedState: State = initialState
        private set
    protected open var allowLogging = true

    @Volatile
    protected var state: State = initialState
        @Synchronized private set(value) {
            val oldVal = field
            field = value
            onStateUpdated(oldVal, value)
        }

    protected open fun onStateUpdated(oldState: State, newState: State) {
        if (allowLogging)
            logStates(newState)
        if (silentUpdate) return //don't propagate update to observers
        else {
            if (!immediateUpdate) uiModel.postValue(mapStateToUIModel(oldState, newState))
            else uiModel.value = mapStateToUIModel(lastObservedState, newState)
            if (uiModel.hasActiveObservers()) lastObservedState = newState
        }
    }

    protected abstract fun mapStateToUIModel(oldState: State, newState: State): UIModel

    protected open fun resetState() {
        state = initialState
    }

    protected open fun updateState(stateTransformer: State.() -> State) {
        this.state = stateTransformer(state)
    }

    protected open fun updateState(state: State) {
        this.state = state
    }

    protected open fun updateStateImmediately(state: State) {
        this.immediateUpdate = true
        this.state = state
        this.immediateUpdate = false
    }

    protected fun updateStateSilently(state: State) {
        this.silentUpdate = true
        this.state = state
        this.silentUpdate = false
    }

    protected open fun logStates(newVal: State) {
        Timber.d("onStateUpdated() called with: newState = [$newVal]")
    }
}

fun getDefaultErrorMessages(err: Throwable): Int {
    return if (err is NotDeliveredRequest) R.string.msgNoInternetConnection
    else R.string.msgSomethingWentWrong
}