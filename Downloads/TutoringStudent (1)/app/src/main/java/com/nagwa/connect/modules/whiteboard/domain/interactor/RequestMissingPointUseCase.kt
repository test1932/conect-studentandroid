package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.PointsRequestParam
import com.nagwa.connect.modules.whiteboard.domain.repository.DrawingRepository
import io.reactivex.Single
import javax.inject.Inject


class RequestMissingPointUseCase @Inject constructor(private val drawingRepository: DrawingRepository) :
        SingleUseCase<PointsRequestParam, Boolean>() {

    override fun build(params: PointsRequestParam): Single<Boolean> {
        return if (params.drawing!!.lastClearId > params.lastID)
            drawingRepository.requestMissedPoints(
                    params.copy(lastID = params.drawing.lastClearId,
                            educatorDrawingSize = DrawingSize(params.drawing.width, params.drawing.height)))
        else
            drawingRepository.requestMissedPoints(
                    params.copy(educatorDrawingSize = DrawingSize(params.drawing.width, params.drawing.height)))
    }
}