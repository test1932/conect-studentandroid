package com.nagwa.connect.modules.usermanagement.domain.entity.param

data class SignInParam constructor(
    val username: String,
    val password: String,
    val userId: String = "",
    val portalId: String = ""
)