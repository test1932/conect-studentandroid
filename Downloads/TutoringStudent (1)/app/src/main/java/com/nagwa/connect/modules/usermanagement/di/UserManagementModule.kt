package com.nagwa.connect.modules.usermanagement.di

import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.usermanagement.data.repository.AuthenticationRepositoryImpl
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.CreatePasswordViewModel
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.EmailStepViewModel
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.PasswordStepViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton


@Module
abstract class SignInModule {

    @Binds
    @AuthenticationScope
    internal abstract fun bindAuthRepo(impl: AuthenticationRepositoryImpl): AuthenticationRepository

}

@Module
abstract class EmailStepModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(EmailStepViewModel::class)
    internal abstract fun provideVerifyUserViewModel(viewModel: EmailStepViewModel): ViewModel
}

@Module
abstract class PasswordStepModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(PasswordStepViewModel::class)
    internal abstract fun provideSignInViewModel(viewModel: PasswordStepViewModel): ViewModel
}

@Module
abstract class CreatePasswordStepModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(CreatePasswordViewModel::class)
    internal abstract fun provideCreatePasswordViewModel(viewModel: CreatePasswordViewModel): ViewModel

}

@Module
abstract class UserManagementModule {
    @Binds
    @Singleton
    internal abstract fun bindUserRepo(impl: AuthenticationRepositoryImpl): UserRepository
}