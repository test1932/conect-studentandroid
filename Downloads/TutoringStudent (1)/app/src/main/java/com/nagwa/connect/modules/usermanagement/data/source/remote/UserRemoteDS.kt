package com.nagwa.connect.modules.usermanagement.data.source.remote

import com.google.gson.Gson
import com.nagwa.connect.BuildConfig.PORTAL_API
import com.nagwa.connect.BuildConfig.USER_API
import com.nagwa.connect.core.extension.GET
import com.nagwa.connect.core.extension.POST
import com.nagwa.connect.core.extension.encrypt
import com.nagwa.connect.core.extension.genericType
import com.nagwa.connect.modules.usermanagement.domain.entity.param.UserRoleParam
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class UserRemoteDS @Inject constructor(
    private val network: NANetwork,
    @Named("UserManagementAuth") private val userManagementAuthScope: NAAuthNetwork,
    private val gson: Gson
) {

    fun verifyUser(username: String): Single<BaseResponse<VerifyUserResponse>> {
        val type = genericType<BaseResponse<VerifyUserResponse>>()
        return USER_API.POST(
            path = "VerifyUsernameOrEmail/",
            headers = mutableMapOf("LoginNameEncrypted" to username.encrypt()),
            network = network,
            naAuthNetwork = userManagementAuthScope,
            body = mutableMapOf("loginName" to username),
            gson = gson,
            resultType = type,
            apiVersion = USER_API_VERSION
        )
    }

    //only used with non internal users
    fun getUserRole(userRoleParam: UserRoleParam): Single<UserSignInResponse> {
        return PORTAL_API.GET(
            path = "portal/user/signin",
            params = mapOf("portalId" to userRoleParam.portalId, "userId" to userRoleParam.userId),
            network = network,
            naAuthNetwork = userManagementAuthScope,
            resultType = UserSignInResponse::class.java,
            apiVersion = PORTALS_API_VERSION
        )
    }

    fun confirmPassword(token: String, newPassword: String): Single<BaseResponse<String>> {
        // send the body value in the params argument instead of body (and set
        val type = genericType<BaseResponse<String>>()
        return USER_API.POST(
            path = "ConfirmResetPassword",
            network = network,
            naAuthNetwork = userManagementAuthScope,
            params = mutableMapOf("token" to token, "newPassword" to newPassword),
            gson = gson,
            resultType = type,
            apiVersion = USER_API_VERSION,
            hasContentType = false
        )
    }

    fun authenticateUser(
        loginName: String,
        password: String,
        portalID: String
    ): Single<BaseResponse<UserInfoResponse>> {
        val type = genericType<BaseResponse<UserInfoResponse>>()
        return USER_API.POST(
            path = "PortalsLogin",
            network = network,
            naAuthNetwork = userManagementAuthScope,
            body = mutableMapOf(
                "loginName" to loginName,
                "password" to password,
                "portalID" to portalID
            ),
            gson = gson,
            resultType = type,
            apiVersion = USER_API_VERSION
        )
    }

    companion object {
        private const val USER_API_VERSION = "3"
        private const val PORTALS_API_VERSION = "2"
    }
}