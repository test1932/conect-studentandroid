package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

data class PointModel(
    @SerializedName("x")
    val xPosition: Float,
    @SerializedName("y")
    val yPosition: Float
)