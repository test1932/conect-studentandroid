package com.nagwa.connect.modules.whiteboard.domain.entity

data class UnMuteOthersEntity (val provider: String, val studentId:String)