package com.nagwa.connect.core.extension

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Surface
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.nagwa.connect.R


@SuppressLint("SourceLockedOrientationActivity")
fun Activity.setScreenOrientation(orientation: Int) {
    requestedOrientation = orientation
}

fun Activity.hideKeyboard() {
    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = currentFocus
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.requestPermission(
    permission: String,
    permissions: Array<String>? = null,
    code: Int,
    message: String
): Boolean {
    if (ContextCompat.checkSelfPermission(this, permission)
        != PackageManager.PERMISSION_GRANTED
    ) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            val dialog = this.createAlertWithPositiveButton(
                title = getString(R.string.openSettings),
                message = message,
                positiveText = getString(R.string.ok),
                positiveButtonListener = { })
                .apply { setCancelable(false) }
            dialog.show()
            dialog.changeNegativeButtonColor()
        } else {
            ActivityCompat.requestPermissions(this, permissions ?: arrayOf(permission), code)
        }
        return false
    } else
        return true
}

fun Activity.chooseImage(requestCode: Int, @StringRes title: Int) {
    val mimeTypes = arrayOf("image/png", "image/jpg", "image/jpeg")

    val i = Intent().apply {
        type = "image/*"
        addCategory(Intent.CATEGORY_OPENABLE)
        putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        action = Intent.ACTION_GET_CONTENT
        flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
    }
    val customChooserIntent = Intent.createChooser(i, getString(title))
    startActivityForResult(customChooserIntent, requestCode)
}

fun runWithDelay(delay: Long, function: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed(function, delay)
}

fun Activity.openUrl(url: String) {
    val builder = CustomTabsIntent.Builder()
    builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
    val customTabsIntent = builder.build()
    customTabsIntent.launchUrl(this, Uri.parse(url))
}

fun Activity.screenHeight(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun Activity.screenWidth(): Int {
    val displayMetrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun Context.isTablet(): Boolean = resources.getBoolean(R.bool.isTablet)
fun Context.getFloat(@DimenRes resID: Int): Float {
    val typedValue = TypedValue()
    resources.getValue(resID, typedValue, true)
    return typedValue.float
}

fun Activity.isLandscape(): Boolean {
    val display = (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
    return when (display.rotation) {
        Surface.ROTATION_90, Surface.ROTATION_270 -> true
        else -> false
    }
}

fun Activity.hideStatusBar() {
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
}