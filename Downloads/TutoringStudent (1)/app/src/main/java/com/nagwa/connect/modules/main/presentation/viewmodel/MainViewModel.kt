package com.nagwa.connect.modules.main.presentation.viewmodel

import addTo
import androidx.lifecycle.MutableLiveData
import applyAsyncSchedulers
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.core.presentation.view.SingleLiveEventMultiObservers
import com.nagwa.connect.modules.sessions.presentation.viewmodel.OpenSessionNavParam
import com.nagwa.connect.modules.usermanagement.domain.entity.UserEntity
import com.nagwa.connect.modules.usermanagement.domain.interactor.GetUserUseCase
import com.nagwa.connect.modules.usermanagement.domain.interactor.SignOutUseCase
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
class MainViewModel @Inject constructor(
    userDataUseCase: GetUserUseCase,
    private val signOutUseCase: SignOutUseCase
) : TutoringViewModel() {
    val navigateBackToSessions = SingleLiveEventMultiObservers<Unit>()
    val openWhiteboardLiveData = SingleLiveEvent<OpenSessionNavParam>()
    val onUserAvailable = MutableLiveData<UserEntity>()
    val onUserSignedOut = SingleLiveEvent<Pair<Boolean, Boolean>>()

    init {
        userDataUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribe({ onUserAvailable.value = it.user }, Timber::e)
            .addTo(compositeDisposable)
    }

    fun signOut(showPaymentMsg: Boolean = false, isDeactivated: Boolean = false) {
        signOutUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribe({ onUserSignedOut.value = Pair(showPaymentMsg, isDeactivated) }, Timber::e)
            .addTo(compositeDisposable)
    }
}