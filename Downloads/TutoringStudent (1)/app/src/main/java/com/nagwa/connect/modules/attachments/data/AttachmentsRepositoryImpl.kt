package com.nagwa.connect.modules.attachments.data

import com.nagwa.connect.core.domian.attachDomainErrorMapper
import com.nagwa.connect.modules.attachments.data.source.AttachmentsLocalDS
import com.nagwa.connect.modules.attachments.data.source.AttachmentsRemoteDS
import com.nagwa.connect.modules.attachments.domain.AttachmentInfo
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import com.nagwa.connect.modules.attachments.domain.SessionFileID
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class AttachmentsRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val remoteDS: AttachmentsRemoteDS,
    private val localDS: AttachmentsLocalDS
) : AttachmentsRepository {
    override fun getDownloadUrl(sessionID: String): Single<String> =
        userRepository.getUser().map { it.user.userId }
            .flatMapSingle { userID ->
                localDS.getAttachmentInfo(sessionID).map { userID to it.fileID }
            }.flatMap {
                remoteDS.getSessionAttachmentUrl(userID = it.first, attachmentID = it.second)
            }
            .map { it.url }
            .attachDomainErrorMapper()


    override fun getImagesDir(sessionID: String): Single<File> =
        localDS.getAttachmentInfo(sessionID).map { it.filePath!! }
            .map { File(File(it), IMAGES_DIR) }


    override fun savePath(sessionID: String, filepath: String) =
        localDS.updateAttachmentPath(sessionID = sessionID, filePath = filepath)

    override fun getFileID(sessionID: String): Single<Long> =
        localDS.getAttachmentInfo(sessionID).map { it.fileID }

    override fun hasAttachment(sessionID: String): Single<Boolean> {
        return getFileID(sessionID).map { it != 0L }
    }

    override fun isAttachmentDownloaded(sessionID: String): Single<Boolean> =
        localDS.getAttachmentInfo(sessionID).map {
            it.filePath?.isNotEmpty() ?: false
        }

    override fun getAllAttachmentsInfo(): Single<List<AttachmentInfo>> =
        localDS.getAllAttachmentsInfo()

    override fun clearAttachmentInfo(sessionID: String): Completable =
        localDS.clearSessionInfo(sessionID)


    override fun saveInfo(info: List<SessionFileID>): Completable =
        Flowable.fromIterable(info)
            .flatMapCompletable {
                localDS.upsertAttachmentInfo(
                    sessionID = it.sessionID,
                    attachmentID = it.fileID
                )
            }

    companion object {
        const val IMAGES_DIR = "images" //attachment/images
    }
}