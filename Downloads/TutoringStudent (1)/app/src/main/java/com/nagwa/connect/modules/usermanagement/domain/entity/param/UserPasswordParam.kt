package com.nagwa.connect.modules.usermanagement.domain.entity.param

data class UserPasswordParam constructor(val userName: String, val newPassword: String)