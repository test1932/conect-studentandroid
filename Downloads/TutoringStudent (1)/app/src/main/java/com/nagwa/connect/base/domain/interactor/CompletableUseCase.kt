package com.nagwa.connect.base.domain.interactor

import io.reactivex.Completable


abstract class CompletableUseCase<in Params> {
    abstract fun build(params: Params): Completable
}