package com.nagwa.connect.modules.attachments.data

import android.content.Context
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import io.reactivex.Completable
import io.reactivex.Single
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import javax.inject.Inject

class AttachmentFilesManagerImpl @Inject constructor(private val context: Context) :
    AttachmentFilesManager {
    override fun extractFiles(compressedFilePath: String, sessionID: String): Single<String> {
        return createAttachmentDir(sessionID).flatMap { dir ->
            val zipFile = File(compressedFilePath)
            unzip(zipFile = zipFile, targetDir = dir)
                .andThen(Single.just(dir.absolutePath))
        }
    }

    override fun listOfImages(dirPath: File) = Single.fromCallable {
        dirPath.listFiles()!!.toList()
    }

    override fun getDownloadFile(sessionID: String): Single<File> = Single.fromCallable {
        val downloadFile = File(
            createDownloadDir(sessionID),
            getZIPFileName(sessionID)
        )
        downloadFile.createNewFile()
        downloadFile
    }

    override fun deleteAttachmentsFiles(sessionID: String): Completable = Completable.fromAction {
        File(getDownloadDir(), getZIPFileName(sessionID)).delete()
        getAttachmentDir(sessionID).deleteRecursively()
    }

    private fun getZIPFileName(sessionID: String) = "$sessionID.zip"


    @Throws(IOException::class)
    private fun unzip(zipFile: File, targetDir: File) = Completable.fromAction {
        val zis = ZipInputStream(BufferedInputStream(FileInputStream(zipFile)))
        zis.use { safeZIS ->
            var ze: ZipEntry?
            var count: Int
            val buffer = ByteArray(8192)
            while (safeZIS.nextEntry.also { ze = it } != null) {
                val file = File(targetDir, ze!!.name)
                val dir: File = if (ze!!.isDirectory) file else file.parentFile
                if (!dir.isDirectory && !dir.mkdirs()) throw FileNotFoundException(
                    "Failed to ensure directory: " +
                            dir.absolutePath
                )
                if (ze!!.isDirectory) continue
                val fout = FileOutputStream(file)
                fout.use { fout ->
                    while (safeZIS.read(buffer).also { count = it } != -1) fout.write(
                        buffer,
                        0,
                        count
                    )
                }
            }
        }

    }

    private fun createAttachmentDir(sessionID: String) = Single.fromCallable {
        val sessionDir = getAttachmentDir(sessionID)
        sessionDir.mkdirs()
        sessionDir
    }

    private fun getAttachmentDir(sessionID: String): File {
        return File(
            createSessionsDir(),
            "$sessionID/$ATTACHMENTS_DIR"
        )
    }

    private fun createDownloadDir(sessionID: String): File {
        val downloadsDir = getDownloadDir()
        downloadsDir.mkdirs()
        return downloadsDir
    }

    private fun getDownloadDir(): File {
        return File(createSessionsDir(), DOWNLOADS_DIR)
    }

    private fun getSessionsDir(): File {
        return File(
            File(context.filesDir.absolutePath),
            SESSIONS_DIR
        )
    }

    private fun createSessionsDir(): File {
        val sessionsDir = getSessionsDir()
        sessionsDir.mkdirs()
        return sessionsDir
    }

    companion object {
        // internal storage dirs names
        const val SESSIONS_DIR = "sessions" //files/sessions
        const val DOWNLOADS_DIR = "downloads" //files/sessions/downloads
        const val ATTACHMENTS_DIR = "attachments" //files/sessions/{sessionID}/attachments
    }

}