package com.nagwa.connect.modules.sessions.data.model


import com.google.gson.annotations.SerializedName

data class JoinResponse(
    @SerializedName("sessionProvider") val provider: String,
    @SerializedName("sessionMillicastModel") val millicastModel: MillicastProviderResponse?
)

data class MillicastProviderResponse(
    @SerializedName("accountId")
    val accountId: String,
    @SerializedName("jwt")
    val jwt: String?,
    @SerializedName("milliCastIceServers")
    val milliCastIceServers: MilliCastIceServers?,
    @SerializedName("publishingToken")
    val publishingToken: String,
    @SerializedName("streamName")
    val streamName: String?,
    @SerializedName("url")
    val url: String?,
    @SerializedName("wsurl")
    val wsurl: String?
)

data class MilliCastIceServers(
    @SerializedName("s")
    val s: String?,
    @SerializedName("v")
    val iceServersWrapperResponse: ICEServersWrapperResponse?
)

data class IceServer(
    @SerializedName("credential")
    val credential: String?,
    @SerializedName("url")
    val url: String?,
    @SerializedName("username")
    val username: String?
)

data class ICEServersWrapperResponse(
    @SerializedName("iceServers")
    val iceServers: List<IceServer?>?
)
