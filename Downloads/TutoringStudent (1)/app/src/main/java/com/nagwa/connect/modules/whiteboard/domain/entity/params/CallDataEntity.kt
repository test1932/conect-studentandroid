package com.nagwa.connect.modules.whiteboard.domain.entity.params

data class CallDataEntity(
    val sessionId: String,
    val educatorId : Long? = null,
    var studentId: String,
    val provider: String,
    var streamName: String? = null
)