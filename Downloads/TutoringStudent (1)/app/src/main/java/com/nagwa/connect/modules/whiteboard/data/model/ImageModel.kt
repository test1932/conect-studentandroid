package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

data class ImageModel(
    @SerializedName("n")
    val name: String,
    @SerializedName("w")
    val width: Int,
    @SerializedName("h")
    val height: Int,
    @SerializedName("x")
    val xPosition: Float,
    @SerializedName("y")
    val yPosition: Float
)