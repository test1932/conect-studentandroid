package com.nagwa.connect.modules.whiteboard.presentation.viewmodel

import addTo
import applyAsyncSchedulers
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.usermanagement.domain.entity.UserEntity
import com.nagwa.connect.modules.usermanagement.domain.interactor.GetUserUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetTimerUpdatesUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.JoinSessionUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.LeaveSessionUseCase
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class WhiteboardViewModel @Inject constructor(
    private val joinSessionUseCase: JoinSessionUseCase,
    private val leaveSessionUseCase: LeaveSessionUseCase,
    private val getTimerUpdatesUseCase: GetTimerUpdatesUseCase,
    userDataUseCase: GetUserUseCase
) : TutoringViewModel() {

    private var sessionName: String = ""
    val joinSessionLiveData = SingleLiveEvent<Unit>()
    val leaveSessionLiveData = SingleLiveEvent<Boolean>()

    // event to start ending session, and hold reason msg for ending
    val endSessionLiveEvent = SingleLiveEvent<Int>()
    val notJoinedSessionLiveEvent = SingleLiveEvent<Unit>()
    val timerLiveEvent = SingleLiveEvent<String>()
    val onUserAvailable = SingleLiveEvent<UserEntity>()

    val educatorBlockingEvent = SingleLiveEvent<Boolean>()
    val connectionBlockingEvent = SingleLiveEvent<Boolean>()
    private var isSessionJoined = false

    init {
        userDataUseCase.build(Unit)
            .applyAsyncSchedulers()
            .subscribe({ onUserAvailable.value = it.user }, Timber::e)
            .addTo(compositeDisposable)
    }

    fun joinSession(sessionName: String, educatorId: String) {
        this.sessionName = sessionName
        joinSessionUseCase.build(
            JoinSessionUseCase.JoinSessionParam(
                sessionID = sessionName,
                eduactorID = educatorId
            )
        )
            .applyAsyncSchedulers()
            .subscribeBy(
                onNext = {
                    Timber.d("joinSession: $it")
                    when (it) {
                        CallStatusEntity.Connected -> {
                            if (isEducatorStatusBlockingScreen())
                                educatorBlockingEvent.value = false
                            if (isConnectionBlockingScreen())
                                connectionBlockingEvent.value = false
                            if (!isSessionJoined)
                                onSessionJoined()
                        }
                        CallStatusEntity.EducatorOffline -> educatorBlockingEvent.value = true
                        CallStatusEntity.DisConnected ->
                            connectionBlockingEvent.value = true
                        CallStatusEntity.RejoinFailed ->
                            endSessionLiveEvent.value = R.string.msgSomethingWentWrong

                        CallStatusEntity.EducatorTerminated ->
                            endSessionLiveEvent.value = R.string.msg_failed_to_connect_with_teacher
                        CallStatusEntity.RejoinTimeout ->
                            endSessionLiveEvent.value = R.string.msg_failed_to_connect_again
                        CallStatusEntity.AccountJoinedWithOtherDevice ->
                            endSessionLiveEvent.value = R.string.msg_reason_different_devices

                    }
                },
                onError = {
                    Timber.e(it, "joinSession: ")
                    isSessionJoined = false
                    notJoinedSessionLiveEvent.postValue(Unit)
                })
            .addTo(compositeDisposable)
    }

    private fun isEducatorStatusBlockingScreen(): Boolean = educatorBlockingEvent.value == true

    private fun isConnectionBlockingScreen() = connectionBlockingEvent.value == true

    private fun onSessionJoined() {
        observeTimerUpdates(sessionName)
        joinSessionLiveData.value = Unit
        isSessionJoined = true
    }

    fun leaveSession(sessionId: String, byEducator: Boolean) {
        if (isSessionJoined) {
            /*this is set to false to avoid multi call of leave session
            because of getting action end and action terminate */
            isSessionJoined = false
            leaveSessionUseCase.build(sessionId)
                .applyAsyncSchedulers()
                .subscribe({ leaveSessionLiveData.value = byEducator },
                    { leaveSessionLiveData.value = byEducator })
                .addTo(compositeDisposable)
        }
    }


    private fun observeTimerUpdates(sessionName: String) {
        getTimerUpdatesUseCase.build(sessionName)
            .applyAsyncSchedulers()
            .subscribeBy(
                onNext = { timerLiveEvent.value = convertToTimeFormat(it) },
                onError = Timber::e
            )
            .addTo(compositeDisposable)
    }

    private fun convertToTimeFormat(millieSeconds: Long): String {
        val totalSeconds = millieSeconds / 1000
        val hours = totalSeconds / 3600
        val minutes = (totalSeconds- hours * 3600 )/ 60
        val seconds = totalSeconds % 60

        return if (hours == 0.toLong())
            String.format("%02d:%02d", minutes, seconds)
        else  String.format("%02d:%02d:%02d", hours,minutes, seconds)
    }

}