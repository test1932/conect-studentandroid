package com.nagwa.connect.modules.attachments.domain

import io.reactivex.Completable
import io.reactivex.Single
import java.io.File

data class SessionFileID(val sessionID: String, val fileID: Long)

interface AttachmentsRepository {
    fun getDownloadUrl(sessionID: String): Single<String>
    fun getImagesDir(sessionID: String): Single<File>
    fun saveInfo(info: List<SessionFileID>): Completable
    fun savePath(sessionID: String, filepath: String): Completable
    fun getFileID(sessionID: String): Single<Long>
    fun hasAttachment(sessionID: String): Single<Boolean>
    fun isAttachmentDownloaded(sessionID: String): Single<Boolean>
    fun getAllAttachmentsInfo(): Single<List<AttachmentInfo>>
    fun clearAttachmentInfo(sessionID: String):Completable
}

data class AttachmentInfo(val sessionID: String, val fileID: Long, val filePath: String? = null)