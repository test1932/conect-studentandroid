package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.payment.domain.entity.params.GetPaymentParam
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.payment.domain.interactor.PaymentStateUseCase
import com.nagwa.connect.modules.usermanagement.domain.entity.EmptyUserIDError
import com.nagwa.connect.modules.usermanagement.domain.entity.NotAllowedUserError
import com.nagwa.connect.modules.usermanagement.domain.entity.UserNotFoundError
import com.nagwa.connect.modules.usermanagement.domain.entity.VerifyUserEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.param.UserRoleParam
import com.nagwa.connect.modules.usermanagement.domain.interactor.StudentIdVerificationState.*
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import flatMapScoped
import io.reactivex.Single
import toSingle
import java.util.*
import javax.inject.Inject

class VerifyUserUseCase @Inject constructor(
    private val authRepo: AuthenticationRepository,
    private val paymentStateUseCase: PaymentStateUseCase
) : SingleUseCase<String, StudentStateWithData>() {

    override fun build(email: String): Single<StudentStateWithData> {
        return if (email.isBlank()) Single.error(EmptyUserIDError)
        else authRepo.verifyUser(email)
            .flatMapScoped {
                getExternalUserState(userId, portalId, needPasswordReset)
                    .flatMap { studentData ->
                        val state = studentData.idVerificationState
                        if (state == Student || state == NewStudent) {
                            paymentState(studentData, userId, portalId)
                        } else {
                            studentData.toSingle()
                        }
                    }
            }.onErrorResumeNext { getTypeOfErrors(it) }

    }

    private fun paymentState(
        studentData: StudentStateWithData,
        userId: String,
        portalId: String
    ): Single<StudentStateWithData> {
        return paymentStateUseCase.build(GetPaymentParam(userId, portalId))
            .flatMapScoped {
                when (this) {
                    PaymentAction.BLOCK_USER -> {
                        studentData.copy(idVerificationState = PaymentBlockedUser).toSingle()
                    }
                    PaymentAction.DEACTIVATED_USER -> {
                        studentData.copy(idVerificationState = PaymentDeactivatedUser).toSingle()
                    }
                    else -> studentData.toSingle()
                }
            }
    }

    private fun getTypeOfErrors(it: Throwable): Single<StudentStateWithData> {
        return when (it) {
            NotAllowedUserError -> StudentStateWithData(idVerificationState = NotAllowedUser).toSingle()
            UserNotFoundError -> StudentStateWithData(idVerificationState = UserNotFound).toSingle()
            else -> Single.error(it)
        }
    }

    private fun getUserRole(userID: String, portalID: String) =
        authRepo.getUserRole(UserRoleParam(portalId = portalID, userId = userID))
            .map { isStudentUser(it.userRoles) }


    private fun isStudentUser(roles: List<String>): Boolean =
        roles.any { it.toLowerCase(Locale.ENGLISH) == STUDENT_ROLE }

    private fun getStudentState(needPasswordReset: Boolean): StudentIdVerificationState {
        return if (needPasswordReset)
            NewStudent
        else
            Student
    }

    private fun getExternalUserState(
        userID: String,
        portalID: String,
        needPasswordReset: Boolean
    ): Single<StudentStateWithData> =
        getUserRole(userID, portalID)
            .map { isStudent ->
                if (isStudent) {
                    StudentStateWithData(
                        idVerificationState = getStudentState(needPasswordReset),
                        userId = userID,
                        portalId = portalID
                    )
                } else {
                    StudentStateWithData(idVerificationState = NotAllowedUser)
                }
            }

    private fun VerifyUserEntity.toUserPortalID(email: String): UserPortalID =
        UserPortalID(userId, portalId, email)

    data class UserPortalID(val userID: String, val portalID: String, val email: String)

    companion object {
        const val NAGWA_DOMAIN = "nagwa.com"
        const val NAGWA_PORTAL = "464161291713"
        const val STUDENT_ROLE = "student"
    }

}

enum class StudentIdVerificationState {
    Student,
    NewStudent,
    NotAllowedUser,
    UserNotFound,
    PaymentBlockedUser,
    PaymentDeactivatedUser
}

data class StudentStateWithData(
    val idVerificationState: StudentIdVerificationState,
    val userId: String? = null,
    val portalId: String? = null
)

