package com.nagwa.connect.core.di

import android.app.Application
import android.content.Context
import androidx.work.NetworkType
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.nagwa.connect.core.application.TutoringApplication
import com.nagwa.connect.core.presentation.view.BitmapProvider
import com.nagwa.connect.core.presentation.view.StringsProvider
import com.nagwa.connect.modules.sessions.data.source.SessionsLocalDS
import com.nagwa.rxdownloadmanger.IDownloadManger
import com.nagwa.rxdownloadmanger.NDownloadManger
import com.nagwa.rxdownloadmanger.downloaders.SerialDownloader
import dagger.Module
import dagger.Provides
import io.reactivex.BackpressureStrategy
import javax.inject.Singleton


/**
 * Created by Youssef Ebrahim Elerian on 2019-11-05.
 * youssef.elerian@gmail.com
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

    @Provides
    @Singleton
    fun provideStringProvider(application: Application): StringsProvider =
        application as TutoringApplication

    @Provides
    @Singleton
    fun provideBitmapProvider(application: Application): BitmapProvider =
        application as TutoringApplication

    @Provides
    @Singleton
    fun provideSharedPreference(context: Context) =
        context.getSharedPreferences("NagwaTutoringStudent", 0)

    @Provides
    fun provideSeriesDownloader(application: Context): SerialDownloader =
        SerialDownloader(context = application, networkType = NetworkType.NOT_REQUIRED)

    @Provides
    @Singleton
    fun provideDownloadManager(
        context: Context,
        downloader: SerialDownloader
    ): IDownloadManger =
        NDownloadManger(context.applicationContext, downloader, BackpressureStrategy.LATEST)


    @Provides
    @Singleton
    fun provideSessionsLocalDS(context: Context): SessionsLocalDS {
        val shard =  context.getSharedPreferences("SessionsData", 0)
        return SessionsLocalDS(shard)
    }


    @Provides
    @Singleton
    fun provideFirebaseFireStore(): FirebaseFirestore = FirebaseFirestore.getInstance()

}


