package com.nagwa.connect.modules.whiteboard.domain.entity.enums


/**
 * Created by Youssef Ebrahim Elerian on 5/18/20.
 * youssef.elerian@gmail.com
 */
enum class DrawActionType(val type: Int) {
    CREATE_SHAPE(1),
    CLEAR_ALL(2),
    ADD_IMAGE(3)
}