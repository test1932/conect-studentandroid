package com.nagwa.connect.core.data.auth

import com.nagwa.networkmanager.network.NAAuthNetwork

class TutoringScope : NAAuthNetwork {
    override fun scope(): String {
        return "TutoringApi"
    }

    override fun params(): MutableMap<String, String> {
        val params = mutableMapOf<String, String>()
        params["grant_type"] = "client_credentials"
        params["scope"] = "tutoring.api"
        params["client_id"] = "tutoring.sessions.mobile.nagwa"
        params["client_secret"] = "ab5p9wslVv4rzxv3dv8CemHA9X8eyFhtJHVSNH6AB5"
        return params
    }
}
