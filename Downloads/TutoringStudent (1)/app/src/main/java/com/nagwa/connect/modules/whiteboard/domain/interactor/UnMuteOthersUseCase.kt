package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.sessions.data.model.AGORA_RESPONSE_KEY
import com.nagwa.connect.modules.sessions.data.model.MILLICAST_RESPONSE_KEY
import com.nagwa.connect.modules.whiteboard.domain.entity.UnMuteOthersEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.ViewerCallRouter
import io.reactivex.Completable
import timber.log.Timber
import javax.inject.Inject

class UnMuteOthersUseCase @Inject constructor(private val viewerCallRouter: ViewerCallRouter) :
    CompletableUseCase<UnMuteOthersEntity>() {
    override fun build(params: UnMuteOthersEntity): Completable {
        return when {
            params.provider.equals(MILLICAST_RESPONSE_KEY, true) -> {
                viewerCallRouter.unHold(params.studentId).doOnComplete {
                    Timber.tag(TAG).d("UnMuteOthersUseCase : unHold is completed")
                }.doOnError {
                    Timber.tag(TAG).d("UnMuteOthersUseCase : unHold is failed $it")
                }
            }
            params.provider.equals(AGORA_RESPONSE_KEY, true) -> {
                Completable.complete()
            }
            else -> Completable.error(IllegalStateException("The api returned unknown provider = [$params.provider] , we only support [Agora,Millicast]"))
        }
    }
}