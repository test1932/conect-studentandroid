package  com.nagwa.connect.core.extension

import android.content.Context
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.nagwa.connect.BuildConfig.*
import java.io.File
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import kotlin.math.roundToInt

/**
 * Created by Youssef Ebrahim on 12/27/18.
 */
inline fun <reified T> String?.fromJSON(): T? {
    return this?.run { Gson().fromJson(this, T::class.java) }
}

inline fun <reified T> T?.toJson(): String? {
    return this?.run { Gson().toJson(this, T::class.java) }
}

fun Any.objectToMap(): HashMap<String, Any?> {
    val json = Gson().toJson(this)
    return Gson().fromJson(json, HashMap<String, Any?>()::class.java)
}

fun String.isExistsPath(): Boolean {
    return this.isNotBlank() && File(this).exists()
}

fun String.setDiffColorRange(startIndex: Int, endIndex: Int = this.length, color: Int): Spannable {
    val spanText: Spannable = SpannableString(this)
    val spanColor = ForegroundColorSpan(color)
    spanText.setSpan(spanColor, startIndex, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    return spanText
}

fun Boolean.toInt() = if (this) 1 else 0

fun Int.toBoolean() = this == 1

fun Int.percentage(total: Int): Int {
    val num = (this.toFloat() * 100f / total.toFloat()).roundToInt()
    return if (num <= 100) num else 100
}

fun Double.format(): String {
    val df = DecimalFormat("###.#")
    return df.format(this)
}

fun Float.format(): String {
    val df = DecimalFormat("###.#")
    return df.format(this)
}

fun String.locale(languageCode: String): String {
    return try {
        val locale = Locale(languageCode)
        val nf = NumberFormat.getInstance(locale)
        nf.format(this.toDouble())
    } catch (e: Exception) {
        this
    }
}

fun String.localeArabic(): String {
    return this.locale("ar")
}

fun String.isArabic(): Boolean {
    return toLowerCase() == "ar"
}

fun String.lengthInRange(range: IntRange): Boolean {
    return length in range
}

fun String.getWidth(
    context: Context,
    textSize: Float = 20.toFloat(),
    fontFamilyAssets: String = "Helvetica.ttf"
): Int {
    val paint = Paint()
    paint.textSize = textSize
    val typeface = Typeface.createFromAsset(context.assets, fontFamilyAssets)
    paint.typeface = typeface
    //paint.setColor(Color.BLACK)
    paint.style = Paint.Style.FILL
    val result = Rect()
    paint.getTextBounds(this, 0, this.length, result)
    return result.width()
}

fun SpannableStringBuilder.setSpanState(
    context: Context,
    text: String,
    onClick: () -> Unit = {},
    textColor: Int? = null,
    withUnderLine: Boolean = false
) {
    append(text)
    setSpan(object : ClickableSpan() {
        override fun onClick(widget: View) {
            onClick()
        }

        override fun updateDrawState(textPaint: TextPaint) {
            if (textColor != null)
                textPaint.color = context.getResColor(textColor)

            textPaint.isUnderlineText = withUnderLine    // this remove the underline

        }
    }, length - text.length, length, 0)

}

fun String.getLabelWidthWithTextView(textView: TextView): Int {
    val bounds = Rect()
    val textPaint = textView.paint
    textPaint.getTextBounds(this, 0, this.length, bounds)
    return bounds.width()
}


fun String.encrypt(): String {
    val iv = IvParameterSpec(ENCRYPTION_IV.toByteArray())
    val skeySpec = SecretKeySpec(ENCRYPTION_KEY.toByteArray(), ENCRYPTION_ALGORITHM)

    val cipher = Cipher.getInstance(ENCRYPTION_TYPE)
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv)

    val encrypted = cipher.doFinal(this.toByteArray())
    val data = Base64.encodeToString(encrypted, Base64.NO_WRAP)
    /*
    * #we have to apply .trim() fun on the resulted encrypted string to eliminate an invisible 'new line' char.
    * please check: https://github.com/square/retrofit/issues/1153#issuecomment-144769497
    *
    * #note: we can use Base64.NO_WRAP to avoid trim() fun. eg. Base64.encodeToString(encrypted, Base64.NO_WRAP)
    * and also when we trying to encrypt too long string
    * please check: https://github.com/square/retrofit/issues/1153#issuecomment-187718088
    * */
    return data.trim()
}

fun Int.convertColor(): String {
    return String.format("#%06X", 0xFFFFFF and this)
}
