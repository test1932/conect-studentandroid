package com.nagwa.connect.modules.whiteboard.domain.entity

data class PointEntity(
    val xPosition: Float,
    val yPosition: Float
)