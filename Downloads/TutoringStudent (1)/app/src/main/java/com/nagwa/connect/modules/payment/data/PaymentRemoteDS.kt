package com.nagwa.connect.modules.payment.data

import com.nagwa.connect.BuildConfig.PAYMENT_API
import com.nagwa.connect.BuildConfig.PAYMENT_API_VERSION
import com.nagwa.connect.core.extension.GET
import com.nagwa.connect.core.extension.genericType
import com.nagwa.connect.modules.payment.data.model.PaymentResponse
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
class PaymentRemoteDS @Inject constructor(
    private val network: NANetwork,
    @Named("PaymentAuth") private val paymentScope: NAAuthNetwork
) {

    fun getInvoiceStatus(userId: String, portalId: String): Single<PaymentResponse> {
        val type = genericType<PaymentResponse>()
        return PAYMENT_API.GET(
            path = "status",
            params = mapOf("userId" to userId, "portalId" to portalId),
            network = network,
            naAuthNetwork = paymentScope,
            resultType = type,
            apiVersion = PAYMENT_API_VERSION
        )
    }
}