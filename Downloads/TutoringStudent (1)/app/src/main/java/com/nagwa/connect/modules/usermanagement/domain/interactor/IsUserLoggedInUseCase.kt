package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class IsUserLoggedInUseCase @Inject constructor(private val userRepository: UserRepository) :
    SingleUseCase<Unit, Boolean>() {

    override fun build(params: Unit): Single<Boolean> {
        return userRepository.getUser()
            .flatMapSingle { Single.just(true) }
            .onErrorReturn { false }
    }
}