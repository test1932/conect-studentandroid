package com.nagwa.connect.core.di

import android.app.Application
import com.nagwa.connect.core.application.TutoringApplication
import com.nagwa.connect.modules.usermanagement.di.UserManagementModule
import com.nagwa.networkmanager.di.NetworkManagerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


/**
 * Created by Youssef Ebrahim Elerian on 2019-11-05.
 * youssef.elerian@gmail.com
 */
@Singleton
@Component(
    modules = [AndroidInjectionModule::class, AppModule::class, ActivitiesModule::class,
        NetworkManagerModule::class, NetworkAuthModule::class, UserManagementModule::class,
        ExecutorsModule::class]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: TutoringApplication)
}