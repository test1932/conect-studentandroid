package com.nagwa.connect.modules.whiteboard.di

import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.whiteboard.data.repository.*
import com.nagwa.connect.modules.whiteboard.domain.repository.*
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ActionsViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ChatViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.DrawingViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.WhiteboardViewModel
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.agora.AgoraCallProvider
import com.nagwa.sessionlibrary.session.millicast.MillicastCallProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
const val AGORA_BINDING_KEY = "AGORA"
const val MILLICAST_BINDING_KEY = "MILLICAST"

@Module
abstract class WhiteboardModule {
    @Binds
    @WhiteboardScope
    internal abstract fun bindWhiteboardRepo(impl: WhiteboardRepositoryImpl): WhiteboardRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindMessengerRepo(impl: MessengerRepositoryImpl): MessengerRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindActionsRepo(impl: ActionsRepositoryImpl): ActionsRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindFireBaseDrawingRepo(impl: DrawingRepositoryImpl): DrawingRepository
 /*   @Binds
    @WhiteboardScope
    internal abstract fun bindDrawingRepo(impl: DrawingRepositoryImpl): DrawingRepository*/

    @Binds
    @WhiteboardScope
    internal abstract fun bindCreateChannelRepo(impl: CreateChannelRepositoryImpl): CreateChannelRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindPublisherCallRouter(impl: PublisherCallRouterImpl): PublisherCallRouter

    @Binds
    @WhiteboardScope
    internal abstract fun bindViewerCallRouter(impl: ViewerCallRouterImpl): ViewerCallRouter

    @Binds
    @WhiteboardScope
    internal abstract fun bindCallRouter(impl: CallRouterImpl): CallRouter

   }

@Module
abstract class CallModule {
    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(AGORA_BINDING_KEY)
    internal abstract fun bindAgoraCallProvider(impl: AgoraCallProvider): NagwaCallProvider

    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(MILLICAST_BINDING_KEY)
    internal abstract fun bindMillicastProvider(impl: MillicastCallProvider): NagwaCallProvider
}

@Module
abstract class WhiteboardViewModelModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(WhiteboardViewModel::class)
    @WhiteboardScope
    internal abstract fun provideWhiteboardViewModel(viewModel: WhiteboardViewModel): ViewModel


    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(ChatViewModel::class)
    internal abstract fun bindChatViewModel(viewModel: ChatViewModel): ViewModel

    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(DrawingViewModel::class)
    internal abstract fun bindDrawingViewModel(viewModel: DrawingViewModel): ViewModel


    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(ActionsViewModel::class)
    internal abstract fun bindActionsViewModel(viewModel: ActionsViewModel): ViewModel


}
