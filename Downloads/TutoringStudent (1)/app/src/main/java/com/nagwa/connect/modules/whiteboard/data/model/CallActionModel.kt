package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

class CallActionModel(
    @SerializedName("a")
    val action: Int,
    @SerializedName("si")
    val studentId: String?,
    @SerializedName("sn")
    var streamName: String? = null,
    @SerializedName("n")
    var studentName: String? = null
)