package com.nagwa.connect.modules.usermanagement.domain.entity.param

data class CreatePasswordParam(
    val password: String,
    val confirmPassword: String,
    val email: String,
    val userId: String,
    val portalId: String
)