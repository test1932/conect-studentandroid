package com.nagwa.connect.modules.usermanagement.data.source.remote

import com.google.gson.annotations.SerializedName

class BaseResponse<T> constructor(
    val success: Boolean,
    val resultCode: Int,
    val result: T,
    val errors: Array<String>
)

data class VerifyUserResponse constructor(
    @SerializedName("portalID") val portalId: String,
    @SerializedName("userID") val userId: String,
    @SerializedName("accountStatus") val accountStatus: String,
    @SerializedName("needPasswordReset") val needPasswordReset: Boolean,
    @SerializedName("resetToken") val resetToken: String
)

data class UserRoleResponse constructor(@SerializedName("userRoles") val userRoles: List<String>)

data class UserSignInResponse constructor(
    @SerializedName("portalInfo") val portalInfo: PortalResponse,
    @SerializedName("userRoles") val userRoles: List<String>,
    @SerializedName("gradeTitle") val gradeTitle: String?
)

data class UserInfoResponse constructor(
    @SerializedName("PortalID") val portalId: String,
    @SerializedName("Code") val userId: String,
    @SerializedName("UserName") val userName: String?,
    @SerializedName("FirstName") val firstName: String,
    @SerializedName("LastName") val lastName: String,
    @SerializedName("Email") val email: String?,
    @SerializedName("Language") val language: String,
    @SerializedName("Country") val countryCode: String
)

data class PortalResponse constructor(
    @SerializedName("portalId") val portalId: String,
    @SerializedName(value = "portalTitle", alternate = ["title"]) val portalTitle: String,
    @SerializedName("url") val url: String?,
    @SerializedName("portalCountryCode", alternate = ["country"]) val countryCode: String?,
    @SerializedName("portalLanguage", alternate = ["language"]) val languageCode: String?,
    @SerializedName("portalLocale") val portalLocale: String?,
    @SerializedName("portalLogoURL", alternate = ["logoURL"]) val portalLogoURL: String,
    @SerializedName("timeZone") val timeZone: String?
)