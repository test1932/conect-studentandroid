package com.nagwa.connect.modules.sessions.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.core.domian.DomainException
import com.nagwa.connect.modules.attachments.domain.interactor.AttachmentState
import com.nagwa.connect.modules.attachments.domain.interactor.PrepareSessionAttachment
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.connect.modules.sessions.domain.repository.SessionsRepository
import io.reactivex.Flowable
import javax.inject.Inject

@SessionsScope
@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
class JoinGoingSession @Inject constructor(
    private val repository: SessionsRepository,
    private val getSessions: GetSessions,
    private val prepareSessionAttachment: PrepareSessionAttachment
) :
    FlowableUseCase<String, JoinSessionState>() {
    override fun build(sessionID: String): Flowable<JoinSessionState> {
        return getSessions.build(Unit).flatMapPublisher { // refresh sessions data before joining
            prepareSessionAttachment.build(sessionID).flatMap {
                if (it != AttachmentState.Ready)
                    Flowable.just(
                        JoinSessionState.PreparingAttachment(sessionID, it) as JoinSessionState
                    )
                else joinSession(sessionID)
            }
        }
    }

    private fun joinSession(sessionID: String): Flowable<JoinSessionState> {
        return repository.joinSession(sessionID = sessionID)
            .andThen(Flowable.just(JoinSessionState.Joined(sessionID) as JoinSessionState))
            .startWith(Flowable.just(JoinSessionState.Joining(sessionID) as JoinSessionState))
    }
}

sealed class JoinSessionState(val sessionID: String) {
    class Joining(sessionID: String) : JoinSessionState(sessionID)
    class Joined(sessionID: String) : JoinSessionState(sessionID)
    class PreparingAttachment(sessionID: String, val state: AttachmentState) :
        JoinSessionState(sessionID)
}

object SessionNotJoinableError :
    DomainException("Session join date has passed or session cancelled")