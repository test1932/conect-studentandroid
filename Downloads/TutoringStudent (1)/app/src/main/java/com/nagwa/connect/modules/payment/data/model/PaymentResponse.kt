package com.nagwa.connect.modules.payment.data.model


import com.google.gson.annotations.SerializedName

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
data class PaymentResponse(
  @SerializedName("status") val status: String?,
  @SerializedName("overdueInvoicesCount") val overdueInvoicesCount: Int,
  @SerializedName("pendingInvoicesCount") val pendingInvoicesCount: Int
)