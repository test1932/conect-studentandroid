package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.repository.WhiteboardRepository
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@WhiteboardScope
class GetTimerUpdatesUseCase @Inject constructor(private val repository: WhiteboardRepository) :
    FlowableUseCase<String, Long>() {
    override fun build(params: String): Flowable<Long> {
        return repository.getSessionStartTime(params).flatMapPublisher { sessionStartTime ->
            val elapsedTime = System.currentTimeMillis() - sessionStartTime
            val initialDelay = 1000 - (elapsedTime % 1000)
            Flowable.interval(initialDelay, 1000, TimeUnit.MILLISECONDS)
                .map { elapsedTime + ((it + 1) * 1000) }
        }
    }
}