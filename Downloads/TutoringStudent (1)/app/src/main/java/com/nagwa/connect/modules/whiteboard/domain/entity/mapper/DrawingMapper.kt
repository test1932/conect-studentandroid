package com.nagwa.connect.modules.whiteboard.domain.entity.mapper

import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize

 fun DrawingSize.getScreenPercentage(screenWidth: Int, screenHeight: Int): Float {
    val widthPercentage = (drawingWidth.toFloat() / screenWidth.toFloat())
    val heightPercentage = (drawingHeight.toFloat() / screenHeight.toFloat())
    return if (widthPercentage < heightPercentage) widthPercentage else heightPercentage
}

 fun getSize(width: Int, percentage: Float): Int = (percentage * width.toFloat()).toInt()