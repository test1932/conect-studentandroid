package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.whiteboard.data.model.mapper.toChannelDataEntity
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toModel
import com.nagwa.connect.modules.whiteboard.data.source.CreateChannelRemoteDS
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChannelDataEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.SubscribeEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.CreateChannelRepository
import io.reactivex.Single
import javax.inject.Inject

@WhiteboardScope
class CreateChannelRepositoryImpl @Inject constructor(private val createChannelRemoteDS: CreateChannelRemoteDS) :
    CreateChannelRepository {

    override fun publishStream(sessionId: String, userId: String): Single<ChannelDataEntity> {
        return createChannelRemoteDS.publishStream(sessionId, userId).map {
            it.toChannelDataEntity()
        }
    }

    override fun subscribeToStream(subscribeEntity: SubscribeEntity): Single<ChannelDataEntity> {
        return createChannelRemoteDS.subscribeToStream(subscribeEntity.toModel()).map {
            it.toChannelDataEntity()
        }
    }
}