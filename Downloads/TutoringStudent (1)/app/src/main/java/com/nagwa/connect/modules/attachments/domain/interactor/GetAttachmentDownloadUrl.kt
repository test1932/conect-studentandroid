package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Single
import javax.inject.Inject

class GetAttachmentDownloadUrl @Inject constructor(private val repository: AttachmentsRepository) :
    SingleUseCase<String, String>() {
    override fun build(params: String): Single<String> =
        repository.getDownloadUrl(sessionID = params)

}