package com.nagwa.connect.modules.whiteboard.presentation.uimodel.mapper

import com.nagwa.connect.modules.whiteboard.domain.entity.CreateShapeEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.PointEntity
import com.nagwa.drawingengine.data.model.PointDraw
import com.nagwa.drawingengine.presentation.uimodel.PathPlayAction


/**
 * Created by Youssef Ebrahim Elerian on 6/14/20.
 * youssef.elerian@gmail.com
 */


fun CreateShapeEntity.toUiModel() = PathPlayAction(
    id = id,
    color = color,
    strokeWidth = strokeWidth,
    eraserType = typeOfDrawing,
    points = points.map { it.toUiModel() }.toMutableList()
)


fun PointEntity.toUiModel() = PointDraw(
    xPos = xPosition,
    yPos = yPosition
)

