package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.sessions.data.model.AGORA_RESPONSE_KEY
import com.nagwa.connect.modules.sessions.data.model.MILLICAST_RESPONSE_KEY
import com.nagwa.connect.modules.sessions.data.model.MillicastProviderResponse
import com.nagwa.connect.modules.sessions.data.source.SessionWithProviderModel
import com.nagwa.connect.modules.sessions.data.source.SessionsLocalDS
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import com.nagwa.connect.modules.whiteboard.data.model.mapper.mapToMCParams
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toEntity
import com.nagwa.connect.modules.whiteboard.di.AGORA_BINDING_KEY
import com.nagwa.connect.modules.whiteboard.di.MILLICAST_BINDING_KEY
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.CallRouter
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.agora.AgoraCallParams
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Provider

@WhiteboardScope
class CallRouterImpl @Inject constructor(
    private val callProviders: Map<String, @JvmSuppressWildcards Provider<NagwaCallProvider>>,
    private val sessionsLocalDS: SessionsLocalDS,
    private val userRepository: UserRepository
) :
    CallRouter {
    private var callProvider: NagwaCallProvider? = null
    private var sessionId = ""
    override fun startCall(sessionID: String): Flowable<CallStatusEntity> {
        sessionId = sessionID
        return sessionsLocalDS.getSession(sessionID).map {
            val provider = it.session.provider
            initiateProivder(provider)
            Pair(sessionID, it)
        }.flatMapPublisher(::routeCallToProvider)
            .map { it.toEntity() }
    }

    private fun routeCallToProvider(
        idProviderPair: Pair<String, SessionWithProviderModel>
    ): Flowable<CallStatus> {
        val (sessionID, sessionInfo) = idProviderPair
        val provider = sessionInfo.session.provider
        return when {
            provider.equals(MILLICAST_RESPONSE_KEY, true) -> {
                startMillicastCall(sessionInfo.provider ?: error("millicast info not found"))
            }
            provider.equals(AGORA_RESPONSE_KEY, true) -> {
                startAgoraCall(sessionID)
            }
            else -> error("The api returned unknown provider = [$provider] , we only support [Agora,Millicast]")
        }
    }

    private fun startMillicastCall(provider: MillicastProviderResponse): Flowable<CallStatus> {
        val params = provider.mapToMCParams(autoRejoin = true)
        return callProvider?.startCall(params)
            ?: Flowable.error(Throwable("callProvider is null (startMillicastCall)"))
    }

    private fun startAgoraCall(sessionID: String): Flowable<CallStatus> {
        return userRepository.getUser().flatMapPublisher {
            callProvider?.startCall(
                AgoraCallParams(
                    isBroadcaster = false,
                    channelID = sessionID,
                    userID = it.user.userId
                )
            ) ?: Flowable.error(Throwable("callProvider  is null (startAgoraCall) "))
        }
    }

    private fun initiateProivder(provider: String) {
        callProvider = when {
            provider.equals(MILLICAST_RESPONSE_KEY, true) -> (callProviders[MILLICAST_BINDING_KEY]
                ?: error("millecast provider is null in $callProviders")).get()
            provider.equals(AGORA_RESPONSE_KEY, true) -> (callProviders[AGORA_BINDING_KEY]
                ?: error("agora provider is null in $callProviders")).get()
            else -> error("The api returned unknown provider = [$provider] , we only support [Agora,Millicast]")
        }

    }

    override fun endCall(): Completable =
        callProvider?.endCall() ?: Completable.error(Throwable("callProvider  is null (endCall)"))


    override fun muteCall(): Completable =
        callProvider?.muteCall() ?: Completable.error(Throwable("callProvider  is null (muteCall)"))

    override fun unMuteCall(): Completable = callProvider?.unMuteCall() ?: Completable.error(
        Throwable("callProvider  is null (unMuteCall)")
    )

}
