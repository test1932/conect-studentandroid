package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.core.domian.ApplicationIntegration
import com.nagwa.connect.core.extension.isNetworkConnected
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.params.SubscribeEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.CreateChannelRepository
import com.nagwa.connect.modules.whiteboard.domain.repository.ViewerCallRouter
import io.reactivex.Completable
import timber.log.Timber
import javax.inject.Inject

@WhiteboardScope
class ConnectAsViewerUseCase @Inject constructor(
    private val createChannelRepository: CreateChannelRepository,
    private val viewerCallRouter: ViewerCallRouter
) :
    CompletableUseCase<SubscribeEntity>() {
    override fun build(params: SubscribeEntity): Completable {
        return createChannelRepository.subscribeToStream(params)
            .doOnSuccess{
                Timber.tag(TAG).d("ConnectAsViewerUseCase : subscribeToStream is success}")
            }.doOnError {
                Timber.tag(TAG).d("ConnectAsViewerUseCase : subscribeToStream is failed $it")
            }
            .flatMapCompletable {
                viewerCallRouter.join(it, params.studentId.toString()).doOnComplete{
                    Timber.tag(TAG).d("ConnectAsViewerUseCase : join is completed")
                }.doOnError {err->
                    Timber.tag(TAG).d("ConnectAsViewerUseCase : join is failed $err")
                }
            }
            .retry(3) {
                ApplicationIntegration.getApplication().isNetworkConnected()
            }
    }
}