package com.nagwa.connect.modules.usermanagement.di

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AuthenticationScope