package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.RejoinEntity
import io.reactivex.Flowable

/**
 * Authored by Mohamed Fathy on 18 Oct, 2020.
 * mohamed.eldesoky@nagwa.com
 */
interface RejoinCallPublisherRepository {

//    fun rejoinCall(sessionId: String): Completable

    fun rejoinCallState(): Flowable<RejoinEntity>
}