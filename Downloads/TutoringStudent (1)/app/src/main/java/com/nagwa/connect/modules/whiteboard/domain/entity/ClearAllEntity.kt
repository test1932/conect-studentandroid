package com.nagwa.connect.modules.whiteboard.domain.entity

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class ClearAllEntity(
    override val id: Int,
    override val actionType: DrawActionType = DrawActionType.CLEAR_ALL
) : DrawEntity()