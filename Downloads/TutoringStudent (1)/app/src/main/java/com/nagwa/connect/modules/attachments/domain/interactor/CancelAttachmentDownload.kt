package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.rxdownloadmanger.IDownloadManger
import io.reactivex.Completable
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
class CancelAttachmentDownload @Inject constructor(private val repository: IDownloadManger) :
    CompletableUseCase<String>() {
    override fun build(sessionID: String) = Completable.fromAction { repository.stop(sessionID) }
}