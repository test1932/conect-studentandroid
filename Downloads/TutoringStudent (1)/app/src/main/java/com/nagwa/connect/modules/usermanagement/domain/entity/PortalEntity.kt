package com.nagwa.connect.modules.usermanagement.domain.entity

data class PortalEntity constructor(
    val portalId: String,
    val portalTitle: String,
    val url: String? = null,
    val countryCode: String?,
    val languageCode: String?,
    val portalLocale: String? = null,
    val portalLogoURL: String,
    val timeZone: String?
)