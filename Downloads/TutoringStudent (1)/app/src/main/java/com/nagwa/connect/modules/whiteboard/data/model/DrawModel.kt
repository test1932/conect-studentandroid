package com.nagwa.connect.modules.whiteboard.data.model


abstract class DrawModel {
    abstract val actionType: Int
    abstract val id: Int
}
