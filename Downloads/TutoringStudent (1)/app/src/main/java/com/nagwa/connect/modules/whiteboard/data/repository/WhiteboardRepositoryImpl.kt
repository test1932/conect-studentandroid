package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import com.nagwa.connect.modules.whiteboard.data.source.LeaveSessionDS
import com.nagwa.connect.modules.whiteboard.data.source.WhiteboardLocalDS
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.repository.WhiteboardRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class WhiteboardRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val leaveSessionDS: LeaveSessionDS,
    private val localDS: WhiteboardLocalDS
) : WhiteboardRepository {

    override fun endSession(sessionId: String): Completable {
        return userRepository.getUser().flatMapCompletable {
            leaveSessionDS.leaveSession(userID = it.user.userId, sessionID = sessionId)
        }
    }

    override fun getSessionStartTime(sessionName: String): Single<Long> =
        localDS.getSessionStartingTime(sessionName)

    override fun saveSessionTime(sessionId: String, time: Long) =
        localDS.saveSessionTime(sessionId, time)


}