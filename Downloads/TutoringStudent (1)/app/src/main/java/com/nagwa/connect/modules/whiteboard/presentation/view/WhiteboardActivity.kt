package com.nagwa.connect.modules.whiteboard.presentation.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.updateLayoutParams
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringActivity
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.view.ErrorView
import com.nagwa.connect.core.presentation.view.KeyboardLayoutObserver
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.sessions.presentation.viewmodel.OpenSessionNavParam
import com.nagwa.connect.modules.whiteboard.domain.entity.ClearAllEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.CreateShapeEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.presentation.uimodel.mapper.toUiModel
import com.nagwa.connect.modules.whiteboard.presentation.view.adapter.ChatMessagesAdapter
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ActionsViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ChatViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.DrawingViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.WhiteboardViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.AddImageUIModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.ChatMessageUIModel
import kotlinx.android.synthetic.main.activity_whiteboard.*
import kotlinx.android.synthetic.main.layout_main_tab.*
import kotlinx.android.synthetic.main.layout_tools_bar_whiteboard.*
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/16/20.
 * youssef.elerian@gmail.com
 */
class WhiteboardActivity : TutoringActivity() {

    @Inject
    lateinit var viewModelFactory: TutoringViewModelFactory
    private val keyboardLayoutObserver by lazy { KeyboardLayoutObserver() }

    private var floatingChatBoxDialog: FloatingChatBoxDialog? = null
    private var alertDialog: AlertDialog? = null
    private var networkErrorAlertDialog: AlertDialog? = null

    @Inject
    lateinit var chatMessagesAdapter: ChatMessagesAdapter

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(WhiteboardViewModel::class.java)
    }
    private val chatViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(ChatViewModel::class.java)
    }

    private val drawingViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(DrawingViewModel::class.java)
    }

    private val actionsViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(ActionsViewModel::class.java)
    }

    override fun layoutId(): Int = R.layout.activity_whiteboard
    val session by lazy {
        intent?.getParcelableExtra<OpenSessionNavParam>(SESSION_PARAM)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        super.onCreate(savedInstanceState)
        if (!isTablet() && isLandscape()) hideStatusBar()

        userNameTv.isEnabled = false
        getDrawingViewSize()
        viewModelObservers()
        initAnimations()
        initViews()
        whiteboardDrawingEngine.initDrawEngine(null, false)
        whiteboardDrawingEngine.isBlockDrawing = true
        actions()
        showProgress()
        lifecycle.addObserver(drawingViewModel)
        joinSession()
    }

    private fun joinSession() {
        session?.let {
            viewModel.joinSession(it.sessionID, it.educatorId)
            actionsViewModel.observeActionsUpdates(it.sessionID, it.educatorId)
        }
    }

    override fun onResume() {
        super.onResume()
        keyboardLayoutObserver.addKeyboardObserver(this) { isKeyboardShown, _, rect ->
            if (isKeyboardShown) {
                if (isTablet())
                    expandMessageAreaSize(rect.bottom)
            } else {
                if (isTablet())
                    collapseMessageAreaSize()
            }
        }
        etMessageOverview onClick ::showFloatingChatbox
    }

    override fun onPause() {
        super.onPause()
        keyboardLayoutObserver.removeKeyboardObserver(this)
    }

    private fun initAnimations() {
        currentSpeakerStudentWrapper.scaleYAxis(
            100,
            amountToMoveDown = 1F,
            startFrom = 0F,
            onAnimationEnd = {
                currentSpeakerStudentWrapper.scaleYAxis(
                    100,
                    amountToMoveDown = 0F,
                    startFrom = 0F
                )
            })
    }

    private fun initViews() {
        chatMessagesAdapter.onItemClicked = chatViewModel::onMessageClicked
        whiteboardChatRecyclerV.adapter = chatMessagesAdapter
        txtInstructorWhiteBoard.text = session?.educatorName
        txtSessionWhiteBoard.text = session?.sessionTitle
        whiteboardChatRecyclerV.setHasFixedSize(true)

        if (isTablet()) {
            tabletUiBehaviour()
        } else
            mobileUiBehaviour()

    }

    private fun tabletUiBehaviour() {
        whiteboardToolsBar.visibility = View.VISIBLE
        etMessage.doAfterTextChanged {
            btnSendMessage.visible(it?.toString()?.isNotBlank() ?: false)
        }
        btnSendMessage onClick {
            sendMessage()
            btnSendMessage.visibility = View.GONE
        }
    }

    private fun mobileUiBehaviour() {
        whiteboardToolsBar.visibility = View.GONE
        etMessage.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND && etMessage.text.toString().isNotBlank()) {
                sendMessage()
                true
            } else {
                etMessage.text?.clear()
                false
            }

        }


        whiteboardParentOfDrawing.setOnClickListener {
            whiteboardToolsBar.visible(true)
        }
        whiteboardToolsBar.setOnClickListener { whiteboardToolsBar.visible(false) }
    }

    private fun sendMessage() {
        chatViewModel.onSendChatMessageClicked(etMessage.text.toString())
        etMessage.text?.clear()
        hideKeyboard()
    }

    private fun getDrawingViewSize() {
        val vto = whiteboardDrawingEngine.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                whiteboardDrawingEngine?.let {
                    val drawingWidth = it.width
                    val drawingHeight = it.height
                    if (drawingHeight > 0 && drawingWidth > 0) {
                        it.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        drawingViewModel.getDrawActions(
                            session?.sessionID ?: "",
                            DrawingSize(drawingWidth, drawingHeight)
                        )

                    }
                }

            }
        })
    }


    private fun viewModelObservers() {
        viewModel.joinSessionLiveData.observe(this, ::hideProgress)

        viewModel.leaveSessionLiveData.observe(this, ::showSessionTerminationAlert)

        drawingViewModel.drawingParamsEvent.observe(this, Observer {
            drawingEngineScreenSize(it.drawingWidth , it.drawingHeight)
        })

        drawingViewModel.clearAllLiveData.observe(this, ::clearAllAction)

        drawingViewModel.addImageLiveData.observe(this, ::drawImageAction)

        drawingViewModel.getDrawingActionsLiveData.observe(this, ::drawCreateShape)

        chatViewModel.uiModel.observe<List<ChatMessageUIModel>>(this) { ls ->
            chatMessagesAdapter.swapData(ls)
        }

        viewModel.endSessionLiveEvent.observe(this) {
            showProgress()
            session?.let {
                viewModel.leaveSession(it.sessionID, false)
            }
        }
        viewModel.connectionBlockingEvent.observe(this) {
            // todo ask for this msg
            if (it)
                blockScreen(getString(R.string.msg_student_connection_lost))
            else showConnectedView()
        }
        viewModel.educatorBlockingEvent.observe(this) {
            if (it) blockScreen(getString(R.string.msg_teacher_connection_lost)) else unBlockScreen()
        }
        actionsViewModel.educatorBlockingEvent.observe(this) {
            if (it) blockScreen(getString(R.string.msg_teacher_connection_lost)) else unBlockScreen()
        }
        actionsViewModel.educatorEndSessionLiveData.observe(this) {
            // if it was null, then educator end session in a normal behaviour
            viewModel.endSessionLiveEvent.value =
                it  // to check which msg to display after leaving session
            showProgress()
            session?.let {
                viewModel.leaveSession(it.sessionID, viewModel.endSessionLiveEvent.value == null)
            }
        }
        drawingViewModel.endSession.observe(this) {
            viewModel.endSessionLiveEvent.value = R.string.msgSomethingWentWrong
            showProgress()
            session?.let {
                viewModel.leaveSession(it.sessionID, false)
            }
        }

        actionsViewModel.resetDrawingLiveData.observe(this) {
            clearAllAction()
        }

        viewModel.notJoinedSessionLiveEvent.observe(
            this,
            ::showSessionNotStartedError
        )
        viewModel.timerLiveEvent.observe(this) { timer ->
            whiteboardTimeTv.text = timer
        }
        actionsViewModel.raiseHandLiveData.observe(this, ::switchRaiseHand)
        actionsViewModel.raiseHandErrorResLiveData.observe(this, ::raiseHandError)

        actionsViewModel.currentPublisherStudentLiveData.observe(
            this,
            ::setCurrentSpeakerStudent
        )
        viewModel.onUserAvailable.observe(this) {
            userNameTv.text = it.firstName
        }
    }

    private fun showConnectedView() {
        resetUI()
        unBlockScreen()
    }

    private fun resetUI() {
        switchRaiseHand(ActionsViewModel.RaiseHandBtnState.UN_RAISED)
        setCurrentSpeakerStudent("")
    }

    private fun switchRaiseHand(isRaised: ActionsViewModel.RaiseHandBtnState) {
        when (isRaised) {
            ActionsViewModel.RaiseHandBtnState.UN_RAISED -> {
                micBtn.visible(false)
                raiseHandBtn.setImageResource(R.drawable.ic_raise_hand)
                raiseHandBtn.tag = R.drawable.ic_raise_hand
                raiseHandBtn.visible(true)
            }
            ActionsViewModel.RaiseHandBtnState.RAISING -> {
                micBtn.visible(false)
                raiseHandBtn.setImageResource(R.drawable.ic_raised_hand)
                raiseHandBtn.tag = R.drawable.ic_raised_hand
                raiseHandBtn.visible(true)
            }
            ActionsViewModel.RaiseHandBtnState.SPEAKING -> {
                micBtn.visible(true)
                raiseHandBtn.visible(false)
            }
        }
    }

    private fun raiseHandError(msgResId: Int) {
        errorView.run {
            duration = ErrorView.ErrorViewDuration.INFINITE
            showError(getString(msgResId), actionIcon = getResDrawable(R.drawable.ic_close_error))
        }
    }

    private fun setCurrentSpeakerStudent(studentName: String) {
        when {
            currentStudentSpeakerNameTv.text.isEmpty() && studentName.isNotEmpty() -> {
                currentSpeakerStudentWrapper.visible(true)
                currentSpeakerStudentWrapper.scaleYAxis(
                    amountToMoveDown = 1F,
                    startFrom = 0F,
                    onAnimationStart = {
                        currentStudentSpeakerNameTv.text = studentName
                    })
            }
            currentStudentSpeakerNameTv.text.isNotEmpty() && studentName.isEmpty() -> {
                currentSpeakerStudentWrapper.scaleYAxis(
                    amountToMoveDown = 0F,
                    startFrom = 0F,
                    onAnimationEnd = {
                        currentStudentSpeakerNameTv.text = studentName
                    })
            }
            currentStudentSpeakerNameTv.text.isNotEmpty() && studentName.isNotEmpty() -> {
                currentSpeakerStudentWrapper.scaleYAxis(
                    amountToMoveDown = 0F,
                    startFrom = 0F,
                    onAnimationEnd = {
                        currentStudentSpeakerNameTv.text = ""
                        currentSpeakerStudentWrapper.visible(true)
                        currentSpeakerStudentWrapper.scaleYAxis(
                            amountToMoveDown = 1F,
                            startFrom = 0F,
                            onAnimationStart = {
                                currentStudentSpeakerNameTv.text = studentName
                            })
                    })
            }
        }
    }

    private fun clearAllAction(clearAllEntity: ClearAllEntity? = null) {
        whiteboardDrawingEngine.onClear()
    }

    private fun drawImageAction(imageModel: AddImageUIModel) {
        imageModel.apply {
            whiteboardDrawingEngine.onDrawImage(image, xPos, yPos)
        }
    }

    private fun drawCreateShape(createShapeEntity: CreateShapeEntity) {
            whiteboardDrawingEngine.onDrawAction(createShapeEntity.toUiModel())
    }

    private fun drawingEngineScreenSize(screenWidth: Int, screenHeight: Int) {
        if (whiteboardDrawingEngine.width != screenWidth || whiteboardDrawingEngine.height != screenHeight) {
            Timber.d("drawingEngineScreenSize $screenWidth -- $screenHeight ")
            whiteboardDrawingEngine.setSize(screenWidth, screenHeight)
        }
    }

    private fun actions() {
        whiteboardCloseIBtn.setOnClickListener {
            showEndSessionConfirmation()
        }

        raiseHandBtn.tag = R.drawable.ic_raise_hand
        raiseHandBtn onClick {
            if (isNetworkConnected()) {
                if (audioPermission()) {
                    raiseHand()
                }
            } else {
                showConnectionError()
            }

        }
    }

    private fun raiseHand() {
        when ((raiseHandBtn.tag as Int)) {
            R.drawable.ic_raise_hand -> {
                raiseHandBtn.setImageResource(R.drawable.ic_raised_hand)
                raiseHandBtn.tag = R.drawable.ic_raised_hand
                actionsViewModel.raiseHand()
            }
            R.drawable.ic_raised_hand -> {
                raiseHandBtn.setImageResource(R.drawable.ic_raise_hand)
                raiseHandBtn.tag = R.drawable.ic_raise_hand
                actionsViewModel.cancelRaiseHand()
            }
        }
    }

    private fun audioPermission(): Boolean {
        val permissions =
            arrayOf(Manifest.permission.RECORD_AUDIO)
        return requestPermission(
        permission = Manifest.permission.RECORD_AUDIO,
        code = AUDIO_PERMISSION,
        permissions = permissions,
        message = getString(R.string.allowAccessMicrophonePermission)
    )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        if (requestCode == AUDIO_PERMISSION) {
            if (permissions.isNotEmpty() && grantResults.isNotEmpty())
                if (permissions[0] == Manifest.permission.RECORD_AUDIO && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    raiseHand()
        }
    }

    private fun unBlockScreen() {
        blockingView.unblock()
    }

    private fun blockScreen(msg: String) {
        blockingView.block(msg, true)
    }

    private fun showProgress() {
        whiteboardCloseIBtn.disable(false)
        whiteboardDisable.visibility = View.VISIBLE
        whiteboardDisable.setOnClickListener { }
        whiteboardLoadPb.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        whiteboardCloseIBtn.enable(false)
        whiteboardDisable.visibility = View.GONE
        whiteboardDisable.setOnClickListener(null)
        whiteboardLoadPb.visibility = View.GONE
    }


    //mobile only
    private fun showFloatingChatbox() {
        floatingChatBoxDialog.isNull {
            floatingChatBoxDialog = FloatingChatBoxDialog.newInstance()
            supportFragmentManager.add(
                fragment = floatingChatBoxDialog!!.apply {
                    onSendClicked = chatViewModel::onSendChatMessageClicked
                    onLastTextBeforeDismiss = {
                        etMessage?.setText(it)
                        floatingChatBoxDialog = null
                    }
                },
                tag = FloatingChatBoxDialog::class.simpleName!!,
                addToBackStack = false
            )
        }

    }

    private fun expandMessageAreaSize(keyboardTop: Int) {
        Timber.d("expandMessageAreaSize: parent bottom ${(containerMessageWriting.parent as ViewGroup).bottom}")
        if (isTablet())
            containerMessageWriting.post {
                containerMessageWriting.updateLayoutParams {
                    val params = this as ConstraintLayout.LayoutParams
                    params.height = ConstraintSet.MATCH_CONSTRAINT_SPREAD
                    params.topToBottom = R.id.whiteboardToolsBar
                    params.bottomToBottom =
                        (containerMessageWriting.parent as ViewGroup).id;
                    containerMessageWriting.requestLayout()
                    etMessage.gravity = Gravity.START
                }
            }
    }

    private fun collapseMessageAreaSize() {
        if (isTablet()) {
            Timber.d("collapseMessageAreaSize:parent bottom ${(containerMessageWriting.parent as ViewGroup).bottom}")
            containerMessageWriting.updateLayoutParams {
                val params = this as ConstraintLayout.LayoutParams
                params.height = getResDimenPx(R.dimen.message_area_height)
                params.topToBottom = -1
                params.bottomToBottom = (containerMessageWriting.parent as ViewGroup).id
                containerMessageWriting.requestLayout()
                etMessage.gravity = Gravity.CENTER_VERTICAL
            }
        }
    }


    private fun showConnectionError() {
        hideProgress()
        networkErrorAlertDialog.isNull {
            networkErrorAlertDialog = createAlertWithPositiveButton(
                message = getString(R.string.msgConnectionError),
                positiveText = getString(R.string.ok),
                positiveButtonListener = {
                    networkErrorAlertDialog?.dismiss()
                }
            ).apply { setOnDismissListener { networkErrorAlertDialog = null } }
            networkErrorAlertDialog?.show()
        }

    }

    private fun showSessionNotStartedError() {
        hideProgress()
        alertDialog.isNull {
            alertDialog = createAlertWithPositiveButton(
                message = getString(R.string.msgSomethingWentWrong),
                positiveText = getString(R.string.ok),
                positiveButtonListener = {
                    finishWhiteBoard(withError = false)
                }
            ).apply { setOnDismissListener { alertDialog = null } }
            alertDialog?.show()
        }
    }

    private fun showEndSessionConfirmation() {
        alertDialog.isNull {
            alertDialog = createAlertWithTwoButtons(
                message = getString(R.string.leaveSessionMessage),
                positiveText = getString(R.string.yes),
                positiveButtonListener = {
                    showProgress()
                    session?.let {
                        viewModel.leaveSession(it.sessionID, false)
                    }
                },
                negativeText = getString(R.string.no)
            ).apply { setOnDismissListener { alertDialog = null } }
            alertDialog?.show()
        }

    }

    private fun showSessionTerminationAlert(byEducator: Boolean) {
        unBlockScreen()
        hideProgress()
        val msg = if (byEducator) getString(R.string.sessionEndedMessage)
        else {
            viewModel.endSessionLiveEvent.value?.let {
                getString(it)
            } ?: run {
                getString(R.string.msg_student_end_session)
            }
        }

        alertDialog.isNull {
            alertDialog = createAlertWithPositiveButton(
                message = msg,
                positiveText = getString(R.string.label_back_to_sessions),
                positiveButtonListener = {
                    finishWhiteBoard(withError = true)
                }
            ).apply { setOnDismissListener { alertDialog = null } }
        }
        if (!alertDialog!!.isShowing) alertDialog?.show()
    }

    private fun finishWhiteBoard(withError: Boolean) {
        setResult(if (withError) Activity.RESULT_CANCELED else Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        showEndSessionConfirmation()
    }

    companion object {
        const val REQUEST_CODE = 409
        private const val AUDIO_PERMISSION = 125
        private const val SESSION_PARAM = "SESSION_PARAM_KEY"
        fun startInstance(activity: Activity, params: OpenSessionNavParam) {
            val intent = Intent(activity, WhiteboardActivity::class.java)
            intent.putExtra(SESSION_PARAM, params)
            activity.startActivityForResult(intent, REQUEST_CODE)
            activity.overridePendingTransition(0, 0)
        }
    }
}