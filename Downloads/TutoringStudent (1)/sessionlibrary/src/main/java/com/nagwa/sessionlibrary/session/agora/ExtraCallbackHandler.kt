package com.nagwa.sessionlibrary.session.agora

import com.nagwa.sessionlibrary.session.agora.data.model.AGEventHandler
import io.agora.rtc.IRtcEngineEventHandler
import timber.log.Timber

fun doHandleExtraCallback(type: Int, vararg data: Any) {
    var peerUid: Int
    //val muted: Boolean
    when (type) {
        AGEventHandler.EVENT_TYPE_ON_USER_AUDIO_MUTED -> {
            val dataZero = data.getOrNull(0)
            if (dataZero is String)
                peerUid = dataZero.toIntOrNull() ?: 0
            // muted = data[1].toString().toBoolean()
            // notifyMessageChanged("mute: " + (peerUid and 0xFFFFFFFFL.toInt()) + " " + muted)
        }
        AGEventHandler.EVENT_TYPE_ON_AUDIO_QUALITY -> {
            val dataZero = data.getOrNull(0)
            if (dataZero is String)
                peerUid = dataZero.toIntOrNull() ?: 0
            //  val quality = data[1] as Int
            // val delay = data[2] as Short
            //  val lost = data[3] as Short
            // notifyMessageChanged("quality: " + (peerUid and 0xFFFFFFFFL.toInt()) + " " + quality + " " + delay + " " + lost)
        }
        AGEventHandler.EVENT_TYPE_ON_SPEAKER_STATS -> {
            val infos = data[0]
            if (infos is Array<*>) {
                val info = infos[0]
                // local guy, ignore it
                if (infos.size == 1 && info is IRtcEngineEventHandler.AudioVolumeInfo && info.uid == 0) return
                val volumeCache = StringBuilder()
                for (each in infos) {
                    if (each is IRtcEngineEventHandler.AudioVolumeInfo) {
                        peerUid = each.uid
                        val peerVolume = each.volume
                        if (peerUid == 0) {
                            continue
                        }
                        volumeCache.append("volume: ").append(peerUid and 0xFFFFFFFFL.toInt())
                            .append(" ").append(peerVolume).append("\n")
                    }
                }

                if (volumeCache.isNotEmpty()) {
                    val volumeMsg = volumeCache.substring(0, volumeCache.length - 1)
                    //  notifyMessageChanged(volumeMsg)
                    if (System.currentTimeMillis() / 1000 % 10 == 0L) {
                        Timber.d("NagwaSession  $volumeMsg")
                    }
                }
            }
        }
        AGEventHandler.EVENT_TYPE_ON_APP_ERROR -> {
            //val subType = data[0]
            //if (subType == SessionConstant.AppError.NO_NETWORK_CONNECTION) {
            // showLongToast(getString(R.string.msg_no_network_connection))
            // }
        }
        AGEventHandler.EVENT_TYPE_ON_AGORA_MEDIA_ERROR -> {
            //  val error = data[0] as Int
            // val description = data[1].toString()
            //   notifyMessageChanged("$error $description")
        }
        AGEventHandler.EVENT_TYPE_ON_AUDIO_ROUTE_CHANGED -> {
            /*  val d = data[0]
              var routing = 0
              if (d is String)
                  routing = d.toInt()*/

        }
    }
}