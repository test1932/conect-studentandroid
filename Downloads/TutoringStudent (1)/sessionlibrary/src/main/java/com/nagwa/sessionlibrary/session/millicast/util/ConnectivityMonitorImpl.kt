package com.nagwa.sessionlibrary.session.millicast.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import com.nagwa.sessionlibrary.session.millicast.di.RTCSessionScope
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.properties.Delegates

@RTCSessionScope
class ConnectivityMonitorImpl @Inject constructor(val context: Context) {
    var registered = false
    var currentConnection: ConnectionType by Delegates.observable(context.isWifiConnected()
        .toConnectionType(),
        { _, _, new -> connectionTypeStream.onNext(new) }
    )
    val connectionTypeStream = PublishSubject.create<ConnectionType>()
    private val networkCallback: NetworkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network) { // network available
            Timber.d("onAvailable:")
            currentConnection = context.isWifiConnected().toConnectionType()
        }

        override fun onLost(network: Network) { // network unavailable
            Timber.d("onLost:")
            currentConnection = ConnectionType.NOT_CONNECTED
            /* Completable.timer(2, TimeUnit.SECONDS).subscribeOn(Schedulers.io())
                 .subscribe {
                     currentConnection = context.isWifiConnected().toConnectionType() //onLost may called before NetworkCapabilities changes
                     Timber.d("onLost after 2 secs: $currentConnection")
                 }*/
        }
    }

    fun updateCurrentConnection() = context.isWifiConnected().toConnectionType().also {
        currentConnection = it
    }

    fun getLastConnectionType() = currentConnection
    fun isOnWifi(): Boolean = currentConnection == ConnectionType.WIFI

    fun getConnectionChanges(): Flowable<ConnectionType> =
        connectionTypeStream.toFlowable(BackpressureStrategy.LATEST)
            .doOnSubscribe { updateCurrentConnection() }
            .distinctUntilChanged()

    fun initObservation() {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        } else {
            val request = NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build()
            connectivityManager.registerNetworkCallback(request, networkCallback)
        }
        registered = true
    }

    fun releaseObservation() {
        if (!registered) return
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.unregisterNetworkCallback(networkCallback)
        registered = false
    }

    private fun Boolean?.toConnectionType(): ConnectionType {
        return when (this) {
            null -> ConnectionType.NOT_CONNECTED
            true -> ConnectionType.WIFI
            false -> ConnectionType.CELLULAR
        }
    }
}


fun Context.isWifiConnected(): Boolean? {
    var result: Boolean? = null
    val connectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connectivityManager.activeNetwork ?: return null
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return null
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> false
            //   actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.run {
                result = when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> false
                    //        ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }
            } ?: return null
        }
    }

    return result
}

enum class ConnectionType { NOT_CONNECTED, CELLULAR, WIFI }
