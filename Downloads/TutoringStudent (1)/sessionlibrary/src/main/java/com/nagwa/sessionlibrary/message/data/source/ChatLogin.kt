package com.nagwa.sessionlibrary.message.data.source

import android.content.Context
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.agora.rtm.*
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.concurrent.thread


/**
 * Created by Youssef Ebrahim Elerian on 6/1/20.
 * youssef.elerian@gmail.com
 */
@Singleton
class ChatLogin @Inject constructor(context: Context) : RtmClientListener {

    private val mChatManager: ChatManager =
        ChatManager(context)
    private lateinit var mRtmClient: RtmClient

    val connectionPublisher: PublishRelay<Int> =
        PublishRelay.create()

    val userStatusPublisher: BehaviorRelay<MutableMap<String, Int>> =
        BehaviorRelay.create()

    fun login(userId: String): Single<Boolean> {
        Timber.d("ChatRoom ChatLogin login   $userId")
        return mChatManager.init(this).flatMap {
            mRtmClient = it
            loginRTM(userID = userId)
        }
    }


    /**
     * API CALL: logout from RTM server
     */
    fun logout(): Single<Boolean> {
        return Single.fromCallable {
            thread(true,block = {  mRtmClient.logout(null)})
            mChatManager.unregisterListener()
            true
        }
    }

    /**
     * API CALL: login RTM server
     */
    private fun loginRTM(userID: String): Single<Boolean> {
        Timber.d("ChatRoom ChatLogin loginRTM   $userID    ")
        return Single.create { emitter ->
            mRtmClient.login(null, userID, object : ResultCallback<Void?> {
                override fun onSuccess(responseInfo: Void?) {
                    emitter.onSuccess(true)
                }

                override fun onFailure(errorInfo: ErrorInfo) {
                    Timber.d("ChatRoom ChatLogin loginRTM   ${errorInfo.errorDescription}")
                    emitter.onError(Throwable(errorInfo.errorDescription))
                }
            })
        }

    }

    /**
     * create channel
     */

    fun createChannel(roomName: String, rtmChannelListener: RtmChannelListener): RtmChannel? {
        return mRtmClient.createChannel(roomName, rtmChannelListener)
    }

    fun createMessage(): RtmMessage {
        return mRtmClient.createMessage()
    }

    fun observeOnUserStatus(educatorId :String) =
        mRtmClient.subscribePeersOnlineStatus(mutableSetOf(educatorId),null )

    fun isUserConnected(userId: String): Single<Boolean> {
        return Single.create<Boolean> { emmiter ->
            mRtmClient.queryPeersOnlineStatus(mutableSetOf(userId), object :
                ResultCallback<MutableMap<String, Boolean>> {
                override fun onSuccess(p0: MutableMap<String, Boolean>?) {
                    val isOnline = p0?.get(userId) ?: false
                    emmiter.onSuccess(isOnline)
                }
                override fun onFailure(p0: ErrorInfo?) {
                    emmiter.onSuccess(false)
                }
            })
        }
    }

    /**
     * API CALLBACK: rtm event listener
     */
    override fun onConnectionStateChanged(state: Int, reason: Int) {
        Timber.d("ChatRoom onConnectionStateChanged  state $state  -  reason   $reason")
        when (state) {
            RtmStatusCode.ConnectionState.CONNECTION_STATE_CONNECTED,
            RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING,
            RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED-> {
                connectionPublisher.accept(state)
            }
        }
    }

    override fun onMessageReceived(message: RtmMessage, peerId: String) {
        //val content = message.text
        Timber.d("ChatRoom ChatLogin onMessageReceived  ${message.text}  -    $peerId")
    }

    override fun onTokenExpired() {
        Timber.d("ChatRoom ChatLogin onTokenExpired  ")
    }

    override fun onPeersOnlineStatusChanged(p0: MutableMap<String, Int>?) {
        p0?.let { userStatusPublisher.accept(it) }
    }


}