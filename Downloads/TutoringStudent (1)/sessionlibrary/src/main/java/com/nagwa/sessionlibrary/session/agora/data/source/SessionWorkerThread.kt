package com.nagwa.sessionlibrary.session.agora.data.source

import android.content.Context
import android.os.Looper
import android.os.Message
import android.util.Log
import androidx.preference.PreferenceManager
import com.nagwa.sessionlibrary.model.SessionConstants
import com.nagwa.sessionlibrary.session.agora.data.model.EngineConfig
import com.nagwa.sessionlibrary.session.agora.data.model.SessionConstant
import io.agora.rtc.Constants
import io.agora.rtc.RtcEngine
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
class SessionWorkerThread @Inject constructor(private val context: Context) : Thread() {

    private var mWorkerHandler: SessionHandler? = null
    private var mReady = false
    var rtcEngine: RtcEngine? = null
        private set
    val engineConfig: EngineConfig =
        EngineConfig()
    private val mEngineEventHandler: MyEngineEventHandler


    init {
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        engineConfig.mUid = pref.getInt(SessionConstant.PrefManager.PREF_PROPERTY_UID, 0)
        mEngineEventHandler =
            MyEngineEventHandler(
                engineConfig
            )
    }

    fun waitForReady() {
        while (!mReady) {
            try {
                sleep(20)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            Timber.d("NagwaSession   wait for  ${SessionWorkerThread::class.java.simpleName}")
        }
    }

    override fun run() {
        Timber.d("NagwaSession    start to run run")
        Looper.prepare()
        mWorkerHandler =
            SessionHandler(
                this
            )
        ensureRtcEngineReadyLock()
        mReady = true

        // enter thread looper
        Looper.loop()
    }


    fun joinChannel(channel: String, userID: String) {
        if (currentThread() !== this) {
            Timber.d("NagwaSession   joinChannel()   worker thread asynchronously $channel $userID")
            val envelop = Message()
            envelop.what =
                ACTION_WORKER_JOIN_CHANNEL
            envelop.obj = arrayOf(channel,userID)
            mWorkerHandler?.sendMessage(envelop)
            return
        }
        try {
            ensureRtcEngineReadyLock()
            rtcEngine?.joinChannelWithUserAccount(null, channel, userID);
            //rtcEngine?.joinChannel(null, channel, "OpenVCall", uid)
            engineConfig.mChannel = channel
            Timber.d("NagwaSession  joinChannel  $channel $userID")
        } catch (e: SecurityException) {
            Timber.e(e)
        }
    }

    fun leaveChannel(channel: String?) {
        if (currentThread() !== this) {
            Timber.d("NagwaSession  leaveChannel()   worker thread asynchronously $channel")
            val envelop = Message()
            envelop.what =
                ACTION_WORKER_LEAVE_CHANNEL
            envelop.obj = channel
            mWorkerHandler?.sendMessage(envelop)
            return
        }
        rtcEngine?.removeHandler(mEngineEventHandler.mRtcEventHandler)
        rtcEngine?.leaveChannel()
        RtcEngine.destroy()
        engineConfig.reset()
        Timber.d("NagwaSession  leaveChannel $channel")
    }

    fun switchRole(cRole: Int) {
        engineConfig.mClientRole = cRole
        rtcEngine?.setClientRole(cRole)
        Timber.d("NagwaSession  switchRole  $cRole")
    }

    fun configEngine(cRole: Int) {
        if (currentThread() !== this) {
            Timber.d("NagwaSession  configEngine()  worker thread asynchronously $cRole")
            val envelop = Message()
            envelop.what =
                ACTION_WORKER_CONFIG_ENGINE
            envelop.obj = arrayOf<Any>(cRole)
            mWorkerHandler?.sendMessage(envelop)
            return
        }
        ensureRtcEngineReadyLock()
        engineConfig.mClientRole = cRole
        rtcEngine?.setClientRole(cRole)
        Timber.d("NagwaSession  configEngine $cRole")
    }

    private fun ensureRtcEngineReadyLock(): RtcEngine? {
        if (rtcEngine == null) {
            try {
                rtcEngine = RtcEngine.create(
                    context,
                    SessionConstants.getAgoraAppId(context),
                    mEngineEventHandler.mRtcEventHandler
                )
            } catch (e: Exception) {
                Timber.e(Log.getStackTraceString(e))
                throw RuntimeException(
                    "NEED TO check rtc sdk init fatal error${
                        Log.getStackTraceString(
                            e
                        )
                    }".trimIndent()
                )
            }
            rtcEngine?.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
            rtcEngine?.enableAudioVolumeIndication(200, 3, false) // 200 ms
        }
        return rtcEngine
    }

    fun eventHandler(): MyEngineEventHandler {
        return mEngineEventHandler
    }

    /**
     * call this method to exit
     * should ONLY call this method when this thread is running
     */
    fun exit() {
        if (currentThread() != this) {
            Timber.d("NagwaSession  exit()   exit app thread asynchronously")
            mWorkerHandler?.sendEmptyMessage(ACTION_WORKER_THREAD_QUIT)
            return
        }
        mReady = false

        // TODO should remove all pending(read) messages
        Timber.d("NagwaSession  exit()  start")

        // exit thread looper
        Looper.myLooper()?.quit()
        mWorkerHandler?.release()
        Timber.d("NagwaSession  exit()  end")
    }

    companion object {
        const val ACTION_WORKER_THREAD_QUIT = 0X1010 // quit this thread
        const val ACTION_WORKER_JOIN_CHANNEL = 0X2010
        const val ACTION_WORKER_LEAVE_CHANNEL = 0X2011
        const val ACTION_WORKER_CONFIG_ENGINE = 0X2012

    }


}