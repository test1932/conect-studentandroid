package com.nagwa.sessionlibrary.session.millicast

import com.google.gson.Gson
import com.nagwa.sessionlibrary.session.SessionCallParams
import com.nagwa.sessionlibrary.session.SessionNotStarted
import com.nagwa.sessionlibrary.session.millicast.model.Payload
import com.nagwa.sessionlibrary.session.millicast.model.PayloadData
import com.nagwa.sessionlibrary.session.millicast.rtc.MillicastSdpObserver
import com.nagwa.sessionlibrary.session.millicast.rtc.PeerConnectionController
import com.nagwa.sessionlibrary.session.millicast.rtc.PeerConnectionStore
import com.nagwa.sessionlibrary.session.millicast.network.WebSocketClientWrapper
import com.nagwa.sessionlibrary.session.millicast.util.WebRTCExecutor
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import org.webrtc.MediaConstraints
import org.webrtc.PeerConnection
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named
import kotlin.concurrent.thread


/**
 * Created by Omar El Hefny on 7/22/20.
 * o.hefny@gmail.com
 */
class MillicastCaller constructor(
    private val peerConnectionStore: PeerConnectionStore,
    private val webRTCScheduler: Scheduler
) {
    private var ended: Boolean = false
    lateinit var mCallParams: Params
    private var pcController: PeerConnectionController? = null
    var onDisconnectListener: (() -> Unit)? = null
    lateinit var callStateEmitter: CompletableEmitter
    private var webSocketEcho: WebSocketClientWrapper? = null
    private var createOffer = false
    private var sdpObserver: SdpObserver = object : MillicastSdpObserver() {

        override fun onCreateSuccess(sessionDescription: SessionDescription) {
            onSDPCreatedSuccessfully(sessionDescription, this)
        }

        override fun onCreateFailure(s: String) {
            super.onCreateFailure(s)
            onEstablishingConnectionFailed("onSdpCreationFailure with error=$s")
        }

        override fun onSetFailure(s: String) {
            super.onSetFailure(s)
            onEstablishingConnectionFailed("onSdpSettingFailure with error=$s")
        }
    }

    fun startCall(callParams: Params) = Completable.create { emitter ->
        completableFrom(webRTCScheduler) {
            try {
                callStateEmitter = emitter
                mCallParams = callParams
                connectToWebSocket(mCallParams.wsUrl)
            } catch (error: Throwable) {
                emitter.tryOnError(error)
            }
        }.subscribe()
    }.observeOn(Schedulers.io())


    private fun initializePeerConnection() {
        val servers = mCallParams.servers.mapToICEServers()
        pcController = peerConnectionStore.createPeerConnection(
            getRTCConfig(servers),
            mCallParams.isBroadcaster
        )
        pcController?.setConnectionStatusListener(::onIceConnectionChanged)
    }

    private fun onIceConnectionChanged(iceConnectionState: PeerConnection.IceConnectionState) {
        when (iceConnectionState) {
            PeerConnection.IceConnectionState.CONNECTED -> callStateEmitter.onComplete()
            PeerConnection.IceConnectionState.DISCONNECTED, PeerConnection.IceConnectionState.FAILED -> {
                onDisconnectListener?.invoke()
                disconnect(false)
            }
            else -> {
                Timber.d("onIceConnectionChanged: $iceConnectionState")
            }
        }
    }

    fun endCall() {
        Timber.d("endCall: ${Thread.currentThread()}")
        disconnect(true)
    }

    private fun disconnect(allowStoreDisposal: Boolean) =
        completableFrom(scheduler = webRTCScheduler) {
            if (!ended) {
                Timber.d("disconnect() called on Thread ${Thread.currentThread()}")
                pcController?.let { peerConnectionStore.disposePC(it, allowStoreDisposal) }
                webSocketEcho?.stop()
                webSocketEcho = null
                pcController = null
                ended = true
            }
        }.subscribe()


    fun muteCall() = completableFrom(scheduler = webRTCScheduler) {
        Timber.d("muteCall: on Thread ${Thread.currentThread()}")
        pcController?.muteMic()
    }

    fun unMuteCall() = completableFrom(scheduler = webRTCScheduler) {
        Timber.d("unMuteCall: on Thread ${Thread.currentThread()}")
        pcController?.unMuteMic()
    }

    private fun getRTCConfig(iceServers: List<PeerConnection.IceServer>): PeerConnection.RTCConfiguration {
        val rtcConfig = PeerConnection.RTCConfiguration(iceServers)
        // TCP candidates are only useful when connecting to a server that supports
        // ICE-TCP.
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy =
            PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        // Enable DTLS for normal calls and disable for loopback calls.
        rtcConfig.sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN
        return rtcConfig
    }

    private fun connectToWebSocket(wsFullUrl: String) {
        webSocketEcho = WebSocketClientWrapper(
            onOpened = { onSocketOpened() },
            onClosed = { _, _ -> onEstablishingConnectionFailed("Socket:onClosed") },
            onFailure = { onEstablishingConnectionFailed("Socket:onFailure") },
            onMessageReceived = ::onMessageReceived
        )
        webSocketEcho?.connect(url = wsFullUrl)
    }

    private fun onSocketOpened() {
        Timber.d("onSocketOpened: ")
        initializePeerConnection()
        createOffer()
    }

    private fun onEstablishingConnectionFailed(reason: String) {
        Timber.d("onEstablishingConnectionFailed() called with: reason = [$reason]")
        if (callStateEmitter.isDisposed) return
        else {
            callStateEmitter.tryOnError(SessionNotStarted())
            disconnect(true)
        }
    }


    private fun onMessageReceived(msg: String) {
        val jsonObject = JSONObject(msg)
        if (isOfferResponse(jsonObject))
            setRemoteDescription(jsonObject)
        /* else if (isMillicastEvent(jsonObject))
             emitMillicastEvents(jsonObject)*/
    }

    /*private fun isMillicastEvent(jsonObject: JSONObject) =
        jsonObject.optString("type", "") == "event"

    private fun emitMillicastEvents(jsonObject: JSONObject) {
        when (jsonObject.optString("name", "")) {
            "active" -> callState = CallStatus.PublisherActive
            "inactive" -> callState = CallStatus.PublisherInActive
            "stopped" -> callState = CallStatus.StreamStopped
            else -> {
            }
        }
    }*/
    private fun isOfferResponse(jsonObject: JSONObject) =
        jsonObject.optString("type", "") == "response"

    private fun setRemoteDescription(jsonObject: JSONObject) = completableFrom(webRTCScheduler) {
        val sdp = jsonObject.getJSONObject("data")
            .getString("sdp")
        Timber.d("$TAG  ->  %s", "Setting Remote SDP")
        pcController?.connection?.setRemoteDescription(
            sdpObserver,
            SessionDescription(SessionDescription.Type.ANSWER, sdp)
        ) ?: onEstablishingConnectionFailed("peerconnection is null")
    }.subscribe()


    private fun onSDPCreatedSuccessfully(
        sessionDescription: SessionDescription,
        millicastSdpObserver: MillicastSdpObserver
    ) = completableFrom(webRTCScheduler) {
        Timber.d("onCreateSuccess: $sessionDescription")
        if (sessionDescription.type == SessionDescription.Type.OFFER && createOffer) {
            pcController?.connection?.setLocalDescription(
                millicastSdpObserver,
                sessionDescription
            )?.run {
                sendOffer(
                    offerSDP = sessionDescription.description,
                    isBroadcaster = mCallParams.isBroadcaster,
                    streamID = mCallParams.channelID
                )
            } ?: onEstablishingConnectionFailed("peerconnection is null")
            createOffer = false
        }

    }.subscribe()

    private fun createOffer() {
        Timber.d("$TAG  ->  %s", "createOffer() called")
        createOffer = true
        pcController?.connection?.createOffer(
            sdpObserver,
            createSDPConstrains(mCallParams!!.isBroadcaster)
        ) ?: onEstablishingConnectionFailed("peerconnection is null")
    }

    private fun sendOffer(offerSDP: String, isBroadcaster: Boolean, streamID: String) {
        val payload = createPayload(streamID, offerSDP, isBroadcaster)
        webSocketEcho?.sendMessage(Gson().toJson(payload))
    }

    private fun createPayload(
        streamID: String,
        offerSDP: String,
        isBroadcaster: Boolean
    ): Payload {
        val data =
            PayloadData(name = streamID, sdp = offerSDP, codec = pcController!!.getPublishCodec())
        return Payload.create(data = data, isBroadcaster = isBroadcaster)
    }

    private fun List<MillicastCallParams.MillicastIceServer>.mapToICEServers(): List<PeerConnection.IceServer> =
        map {
            PeerConnection.IceServer.builder(listOf(it.url))
                .setUsername(it.username ?: "")
                .setPassword(it.credential ?: "")
                .createIceServer()
        }

    private fun createSDPConstrains(publisher: Boolean) = MediaConstraints().apply {
        mandatory.add(
            MediaConstraints.KeyValuePair(
                "offerToReceiveAudio", if (publisher) "false" else "true"
            )
        )
        mandatory.add(MediaConstraints.KeyValuePair("offerToReceiveVideo", "false"))
    }

    data class Params(
        val isBroadcaster: Boolean,
        val channelID: String,
        val wsUrl: String,
        val servers: List<MillicastCallParams.MillicastIceServer>
    )

    companion object {
        class Factory @Inject constructor(
            private val peerConnectionStore: PeerConnectionStore, @Named("WebRTCScheduler")
            private val webRTCScheduler: Scheduler
        ) {
            fun create(): MillicastCaller =
                MillicastCaller(peerConnectionStore, webRTCScheduler)
        }

        private const val SDP_MID = "sdpMid"
        private const val SDP_M_LINE_INDEX = "sdpMLineIndex"
        private const val SDP = "sdp"


        private const val TAG = "MillicastSession"
    }
}

fun completableFrom(scheduler: Scheduler, action: () -> Unit): Completable {
    return Completable.fromAction(action).subscribeOn(scheduler)
} //to grantee that the action will be executed in the required thread

fun <T> singleFrom(scheduler: Scheduler, callable: () -> T): Single<T> {
    return Single.fromCallable(callable).subscribeOn(scheduler)
}

fun <T> flowableFrom(scheduler: Scheduler, callable: () -> T): Flowable<T> {
    return Flowable.fromCallable(callable).subscribeOn(scheduler)
}
