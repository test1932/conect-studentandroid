package com.nagwa.sessionlibrary.message

import android.content.Context
import com.jakewharton.rxrelay2.PublishRelay
import com.nagwa.sessionlibrary.core.extensions.isNetworkConnected
import com.nagwa.sessionlibrary.message.data.model.SessionMessageModel
import com.nagwa.sessionlibrary.message.data.source.ChatLogin
import io.agora.rtm.*
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject
import kotlin.concurrent.thread


/**
 * Created by Youssef Ebrahim Elerian on 5/11/20.
 * youssef.elerian@gmail.com
 */
class ChatRoom @Inject constructor(private val login: ChatLogin, private val context: Context) {
    private val hasMessage: PublishRelay<SessionMessageModel> =
        PublishRelay.create()

    private val memberJoinedStatus: PublishRelay<Pair<String, Boolean>> =
        PublishRelay.create()


    private var mRtmChannel: RtmChannel? = null
    var memberCount = 1
        private set

    /**
     * API CALL: get channel member list
     */
    fun getMemberList(): Single<List<String>> {
        return Single.create { emitter ->
            if (mRtmChannel == null) {
                emitter.onError(Throwable("mRtmChannel is null "))
            } else {
                mRtmChannel?.getMembers(object : ResultCallback<List<RtmChannelMember?>> {
                    override fun onSuccess(responseInfo: List<RtmChannelMember?>) {
                        val names = mutableListOf<String>()
                        responseInfo.forEach {
                            if (it != null) {
                                Timber.d("ChatRoom   getChannelMemberList forEach   ${it.userId}    ")
                                names.add(it.userId)
                            }
                        }
                        memberCount = responseInfo.size
                        emitter.onSuccess(names)
                    }

                    override fun onFailure(errorInfo: ErrorInfo) {
                        Timber.d("ChatRoom   getChannelMemberList onFailure   ${errorInfo.errorDescription}")
                        emitter.onError(Throwable(errorInfo.errorDescription))
                    }
                })
            }
        }

    }

    /**
     * API CALL: create and join channel
     */
    fun createChannel(roomName: String): Single<Boolean> {
        Timber.d("ChatRoom   createAndJoinChannel   $roomName")
        return Single.create { emitter ->
            // step 1: create a channel instance
            mRtmChannel = login.createChannel(roomName, MyChannelListener())
            if (mRtmChannel == null) {
                emitter.onError(Throwable("mRtmChannel is null "))
            } else {
                // step 2: join the channel
                mRtmChannel?.join(object : ResultCallback<Void?> {
                    override fun onSuccess(responseInfo: Void?) {
                        emitter.onSuccess(true)
                    }

                    override fun onFailure(errorInfo: ErrorInfo) {
                        Timber.d("ChatRoom   createAndJoinChannel   ${errorInfo.errorDescription}")
                        emitter.onError(Throwable(errorInfo.errorDescription))
                    }
                })
            }
        }
    }

    /**
     * API CALL: leave and release channel
     */
    fun leaveChannel(): Single<Boolean> = Single.fromCallable {
        thread(true, block = { mRtmChannel?.leave(null) })
        thread(true, block = { mRtmChannel?.release() })
        mRtmChannel = null
        true
    }


    /**
     * API CALL: send message to a channel
     */
    fun sendMessage(messageData: String): Single<Boolean> {
        val singleSource: Single<Boolean> = Single.create { emitter ->
            // step 1: create a message
            val message = login.createMessage()
            message.text = messageData
            // step 2: send message to channel
            Timber.d("ChatRoom    sendChannelMessage 1  $messageData")
            if (mRtmChannel == null) {
                emitter.onError(Throwable("mRtmChannel is null or message is null"))
            } else {
                mRtmChannel?.sendMessage(message, object : ResultCallback<Void?> {
                    override fun onSuccess(aVoid: Void?) {
                        emitter.onSuccess(true)
                    }

                    override fun onFailure(errorInfo: ErrorInfo) {
                        //  when ( errorInfo.errorCode) {
                        //    RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT, RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE
                        // }
                        Timber.d("ChatRoom   sendChannelMessage 2   ${errorInfo.errorDescription}")
                        emitter.onError(Throwable(errorInfo.errorCode.toString()))
                    }
                })
            }
        }

        return singleSource.retry(3) {
            if (mRtmChannel == null) false else it.shouldApplyRetry()
        }
    }

    private fun Throwable.shouldApplyRetry(): Boolean {
        return if (context.isNetworkConnected()) {
            (message == RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT.toString() ||
                    message == RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE.toString())
        } else false

    }

    fun hasMessage(): Flowable<SessionMessageModel> =
        hasMessage.toFlowable(BackpressureStrategy.LATEST)

    fun getMemberJoinedStatus(): Flowable<Pair<String, Boolean>> =
        memberJoinedStatus.toFlowable(BackpressureStrategy.LATEST)

    fun getUserConnectionStatus(): Flowable<Int> =
        login.connectionPublisher.toFlowable(BackpressureStrategy.LATEST)

    fun getUserStatus(educatorId: String): Flowable<Int> {
        login.observeOnUserStatus(educatorId)
        return login.userStatusPublisher.toFlowable(BackpressureStrategy.LATEST)
            .filter { it.keys.contains(educatorId) }
            .map { it.values.first() }
    }

    fun isUserOnline(userId: String): Single<Boolean> {
        return login.isUserConnected(userId)
    }


    /**
     * API CALLBACK: rtm channel event listener
     */
    private inner class MyChannelListener : RtmChannelListener {
        override fun onMemberCountUpdated(i: Int) {
            Timber.d("ChatRoom onMemberCountUpdated     $i")
        }

        override fun onAttributesUpdated(list: List<RtmChannelAttribute>) {
            Timber.d("ChatRoom onAttributesUpdated     $list ")
        }

        override fun onMessageReceived(message: RtmMessage, fromMember: RtmChannelMember) {
            //  val userInfo = login.decodeUserName(fromMember.userId)
            hasMessage.accept(
                SessionMessageModel(
                    message = message.text,
                    userId = fromMember.userId
                )
            )
            Timber.d("ChatRoom onMessageReceived     ${message.text}  -- ${fromMember.userId}  -- ${fromMember.channelId}")
        }

        override fun onMemberJoined(member: RtmChannelMember) {
            memberCount++
            //     val userName = login.decodeUserName(member.userId)
            memberJoinedStatus.accept(Pair(member.userId, true))
            Timber.d("ChatRoom onMemberJoined     ${member.userId}")
        }

        override fun onMemberLeft(member: RtmChannelMember) {
            memberCount--
            //  val userName = login.decodeUserName(member.userId)
            memberJoinedStatus.accept(Pair(member.userId, false))
            Timber.d("ChatRoom onMemberLeft     ${member.userId}")
        }

    }


}