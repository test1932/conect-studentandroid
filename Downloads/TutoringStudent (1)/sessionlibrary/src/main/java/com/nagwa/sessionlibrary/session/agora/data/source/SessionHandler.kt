package com.nagwa.sessionlibrary.session.agora.data.source

import android.os.Handler
import android.os.Message
import timber.log.Timber


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
class SessionHandler constructor(private var mWorkerThread: SessionWorkerThread?) :
    Handler() {
    fun release() {
        mWorkerThread = null
    }

    override fun handleMessage(msg: Message) {
        if (mWorkerThread == null) {
            Timber.d("NagwaSession handlerIsAlready  Released $msg.what")
            return
        }
        when (msg.what) {
            SessionWorkerThread.ACTION_WORKER_THREAD_QUIT -> mWorkerThread?.exit()
            SessionWorkerThread.ACTION_WORKER_JOIN_CHANNEL -> {
                if (msg.obj is Array<*>) {
                    val data = msg.obj as Array<*>
                    val channel: Any? = data.getOrNull(0)
                    val userID: Any? = data.getOrNull(1)
                    if (channel != null && channel is String && userID != null && userID is String)
                        mWorkerThread?.joinChannel(channel, userID)
                }
            }
            SessionWorkerThread.ACTION_WORKER_LEAVE_CHANNEL -> {
                val channel = msg.obj as? String
                mWorkerThread?.leaveChannel(channel)
            }
            SessionWorkerThread.ACTION_WORKER_CONFIG_ENGINE -> {
                if (msg.obj is Array<*>) {
                    val configData = msg.obj as Array<*>
                    val cRole = configData.getOrNull(0)
                    if (cRole != null && cRole is Int)
                        mWorkerThread?.configEngine(cRole)
                }
            }
        }
    }


}
