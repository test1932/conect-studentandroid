package com.nagwa.sessionlibrary.session.millicast.util

import android.os.Build
import android.os.Handler
import android.os.Looper
import timber.log.Timber

/**
 * AppRTCUtils provides helper functions for managing thread safety.
 */
object AppRTCUtils {
    /** Helper method which throws an exception  when an assertion has failed.  */
    fun assertIsTrue(condition: Boolean) {
        if (!condition) {
            throw AssertionError("Expected condition to be true")
        }
    }

    /** Helper method for building a string of thread information. */
    val threadInfo: String
        get() = ("@[name=" + Thread.currentThread()
            .name + ", id=" + Thread.currentThread().id
                + "]")

    /** Information about the current build, taken from system properties.  */
    fun logDeviceInfo(tag: String) {
        Timber.d(
            tag, "Android SDK:  ${Build.VERSION.SDK_INT} ",
            "Release: ${Build.VERSION.RELEASE}",
            "Brand: ${Build.BRAND}",
            "Device: ${Build.DEVICE}",
            "Id: ${Build.ID}",
            "Hardware: ${Build.HARDWARE}",
            "Manufacturer: ${Build.MANUFACTURER}",
            "Model: ${Build.MODEL}",
            "Product: ${Build.PRODUCT}"
        )
    }
}
inline fun runOnMain(crossinline block: () -> Unit) {
    Handler(Looper.getMainLooper()).post {
        block()
    }
}