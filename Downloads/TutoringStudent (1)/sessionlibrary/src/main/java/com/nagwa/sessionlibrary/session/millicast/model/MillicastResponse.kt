package com.nagwa.sessionlibrary.session.millicast.model


import com.google.gson.annotations.SerializedName

data class PublishingResponse(
        @SerializedName("data")
        val info: StreamData?,
        @SerializedName("status")
        val status: String
)

data class StreamData(
        @SerializedName("jwt")
        val jwt: String,
        @SerializedName("streamAccountId")
        val streamAccountId: String,
        @SerializedName("subscribeRequiresAuth")
        val subscribeRequiresAuth: Boolean?,
        @SerializedName("urls")
        val urls: List<String?>?,
        @SerializedName("wsUrl")
        val wsUrl: String
)