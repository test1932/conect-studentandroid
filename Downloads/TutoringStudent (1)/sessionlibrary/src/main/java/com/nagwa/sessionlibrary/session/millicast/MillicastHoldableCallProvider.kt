package com.nagwa.sessionlibrary.session.millicast

import android.annotation.SuppressLint
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.SessionCallParams
import com.nagwa.sessionlibrary.session.millicast.model.StreamData
import com.nagwa.sessionlibrary.session.millicast.network.MillicastService
import com.nagwa.sessionlibrary.session.millicast.util.NetworkMonitoring
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class MillicastHoldableCallProvider @Inject constructor(
    private val factory: MillicastCaller.Companion.Factory,
    private val service: MillicastService,
    private val networkMonitoring: NetworkMonitoring
) :
    NagwaCallProvider, HoldableCall {
    val compositeDisposable = CompositeDisposable()
    var isCallMuted = false
    var mCall: MillicastCaller? = factory.create().apply {
        onDisconnectListener = ::onCallDisconnected
    }
    private lateinit var mSessionParams: MillicastCallParams
    private lateinit var mCallParams: MillicastCaller.Params
    private var onHold = false
    private val stateEmitter = BehaviorSubject.create<CallStatus>()
    private val callEndedError = IllegalAccessError("call already ended")
    private val callerNotAvailableError =
        IllegalStateException("can't make any call action while  mCall is null please wait call to rejoin")

    private var callState: CallStatus = CallStatus.NotStarted
        set(value) {
            field = value
            if (!stateEmitter.hasComplete())
                stateEmitter.onNext(value)
            else Timber.w("can't emit call state: the eventsStream is completed ")
        }

    override fun startCall(callParams: SessionCallParams): Flowable<CallStatus> {
        if (callParams !is MillicastCallParams)
            return Flowable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        if (callState > CallStatus.NotStarted)
            return Flowable.error(IllegalAccessError("another call is already started"))
        Timber.d("startCall() called with: callParams = [$callParams]")
        mSessionParams = callParams
        mCallParams = callParams.toParams()
        startCallInternally()
        return stateEmitter.toFlowable(BackpressureStrategy.LATEST)

    }

    private fun startCallInternally() {
        if (mCall == null) error("can't start call while mCall is null")
        mCall!!.startCall(mCallParams)
            .doOnSubscribe { callState = CallStatus.Starting }
            .subscribeOn(Schedulers.io())
            .subscribe({
                callState = CallStatus.Connected
            }, {
                stateEmitter.onError(it)
            }).addTo(compositeDisposable)
    }

    override fun startStatelessCall(callParams: SessionCallParams): Completable {
        if (callState > CallStatus.NotStarted)
            return Completable.error(IllegalAccessError("another call is already started"))
        if (callParams !is MillicastCallParams)
            return Completable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        createNewCall()
        return mCall!!.startCall(callParams.toParams())
    }

    override fun endCall(): Completable = Completable.fromAction {
        if (!assertCallNotEnded()) error(callEndedError)
        if (mCall == null) error("can't end call while mCall is null")
        else mCall!!.endCall()
    }.doOnComplete {
        Timber.d("endCall: completed")
        callState = CallStatus.Ended
        dispose()
    }

    private fun dispose() {
        mCall = null
        compositeDisposable.clear()
        stateEmitter.onComplete()
    }

    override fun muteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        return mCall?.run { muteCall().doOnComplete { isCallMuted = true } }
            ?: Completable.error(callerNotAvailableError)
    }

    override fun unMuteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        return mCall?.run { unMuteCall().doOnComplete { isCallMuted = true } }
            ?: Completable.error(callerNotAvailableError)
    }

    override fun switchRole(isBroadcaster: Boolean): Completable {
        return Completable.error(IllegalAccessError("switchRole not available yet in Millicast provider"))
    }

    override fun setCallOnHold(hold: Boolean): Completable {
        Timber.d("setCallOnHold() called with: hold = [$hold] hold was= [$onHold] and status = [$callState]")
        onHold = hold
        return if (!onHold) {
            when (callState) {
                CallStatus.Disconnected -> if (mSessionParams.autoRejoin) internalRejoinCall() else Completable.complete()
                CallStatus.Connected -> Completable.complete()
                else -> Completable.error(IllegalStateException("setCallOnHold() called with: hold = [$hold] and statue = [$callState]"))
            }
        } else {
            when (callState) {
                CallStatus.Connected -> Completable.complete()
                else -> Completable.error(IllegalStateException("setCallOnHold() called with: hold = [$hold] and statue = [$callState]"))
            }
        }
    }

    //invocation occurs in the main thread
    @SuppressLint("CheckResult")
    private fun onCallDisconnected() {
        callState = CallStatus.Disconnected
        val callAction = if (mSessionParams.autoRejoin) {
            if (!onHold)
                internalRejoinCall().doOnComplete { Timber.d("internalRejoinCall: completed ") }
            else {//when autoRejoin enabled and onHold is true do nothing
                mCall = null
                Completable.complete()
            }
        } else endCall()
        callAction.subscribeOn(Schedulers.io())
            .subscribe({}, Timber::e)
    }

    private fun createNewCall() {
        mCall = factory.create()
        mCall!!.onDisconnectListener = ::onCallDisconnected
    }

    fun rejoinCall() {
        internalRejoinCall().subscribe({
            callState = CallStatus.Connected
        }, {
            callState = CallStatus.FailedToReConnect
        }).addTo(compositeDisposable)
    }

    private fun internalRejoinCall(): Completable {
        return networkMonitoring.onInternetReachability()
            .filter { it }
            .flatMap { getStreamData().toFlowable() }
            .map { it.toParams() }
            .flatMapCompletable {
                createNewCall()
                mCall!!.startCall(it)
            }
            .retry(3)
            .subscribeOn(Schedulers.io())
            .andThen(if (isCallMuted) muteCall() else Completable.complete())
            .doOnComplete {
                callState = CallStatus.Connected
            }
            .doOnError {
                callState = CallStatus.FailedToReConnect
            }
    }

    private fun getStreamData(): Single<StreamData> {
        return if (mSessionParams.isBroadcaster)
            service.publishStream(
                streamID = mSessionParams.channelID,
                publisherToken = mSessionParams.publisherToken
            )
        else
            service.subscribeToStream(
                accountID = mSessionParams.accountId,
                streamID = mSessionParams.channelID
            )
    }


    private fun assertCallNotEnded() = callState != CallStatus.Ended


    private fun StreamData.toParams(): MillicastCaller.Params =
        mCallParams.copy(wsUrl = "${wsUrl}?token=${jwt}")

    private fun MillicastCallParams.toParams(): MillicastCaller.Params = MillicastCaller.Params(
        isBroadcaster = isBroadcaster,
        channelID = channelID,
        wsUrl = wsUrl,
        servers = servers
    )

    companion object {
        class Factory @Inject constructor(
            private val factory: MillicastCaller.Companion.Factory,
            private val service: MillicastService,
            private val networkMonitoring: NetworkMonitoring
        ) {
            fun create(): MillicastHoldableCallProvider =
                MillicastHoldableCallProvider(factory, service, networkMonitoring)
        }

    }
}

interface HoldableCall {
    fun setCallOnHold(hold: Boolean): Completable
}