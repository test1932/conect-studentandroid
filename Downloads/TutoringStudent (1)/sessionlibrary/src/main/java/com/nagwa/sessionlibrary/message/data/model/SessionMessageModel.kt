package com.nagwa.sessionlibrary.message.data.model


/**
 * Created by Youssef Ebrahim Elerian on 6/2/20.
 * youssef.elerian@gmail.com
 */

data class SessionMessageModel(val message: String, val userId :String)