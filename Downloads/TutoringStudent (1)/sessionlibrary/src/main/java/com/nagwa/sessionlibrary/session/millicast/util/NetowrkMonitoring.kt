package com.nagwa.sessionlibrary.session.millicast.util

import com.nagwa.sessionlibrary.session.millicast.di.RTCSessionScope
import io.reactivex.Flowable
import io.reactivex.rxkotlin.combineLatest
import timber.log.Timber
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@RTCSessionScope
class NetworkMonitoring @Inject constructor(private val connectivityMonitor: ConnectivityMonitorImpl) {
    var observersNum = 0

    fun onInternetReachability(
        stopOnReach: Boolean = true,
        testIntervalMS: Long = 2000
    ): Flowable<Boolean> {
        val interval = if (testIntervalMS < 2000) 2000 else testIntervalMS
        return Flowable.interval(0, interval, TimeUnit.MILLISECONDS)
            .doOnSubscribe { Timber.d("onInternetReachability: ${Thread.currentThread().name}") }
            .combineLatest(Flowable.defer { onNetworkConnected() })
            .doOnNext { Timber.d("onInternetReachability: $it") }
            .filter { it.second || it.first == 0L } //emit for first value only if network not connected
            .map { testReachability() }
            .distinctUntilChanged()
            .takeUntil { it && stopOnReach }
    }

    private fun onNetworkConnected(): Flowable<Boolean> {
        connectivityMonitor.initObservation()
        return connectivityMonitor.getConnectionChanges()
            .map { it != ConnectionType.NOT_CONNECTED }
            .doOnNext { Timber.d("onNetworkConnected: $it") }
            .doOnCancel { Timber.d("doOnCancel: ");connectivityMonitor.releaseObservation() }
        //.doOnTerminate { Timber.d("doOnTerminate: "); connectivityMonitor.releaseObservation() }
    }

    private fun testReachability(): Boolean {
        Timber.d("testReachability() called")
        return try {
            val timeoutMS = 1500
            // Connect to Google DNS to check for connection
            val socket = Socket()
            val socketAddress = InetSocketAddress("8.8.8.8", 53)

            socket.connect(socketAddress, timeoutMS)
            socket.close()
            true
        } catch (e: Exception) {
            Timber.d("testReachability:error $e")
            false
        }
    }
}



