package com.nagwa.sessionlibrary.session.agora

import com.nagwa.sessionlibrary.session.SessionCallParams


data class AgoraCallParams(
    override val isBroadcaster: Boolean,
    override val channelID: String,
    val userID: String
) : SessionCallParams(isBroadcaster, channelID)
