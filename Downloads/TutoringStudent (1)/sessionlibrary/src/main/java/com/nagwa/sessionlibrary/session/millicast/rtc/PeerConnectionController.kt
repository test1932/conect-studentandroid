package com.nagwa.sessionlibrary.session.millicast.rtc

import org.webrtc.AudioTrack
import org.webrtc.MediaStreamTrack
import org.webrtc.PeerConnection
import org.webrtc.RtpTransceiver
import timber.log.Timber

class PeerConnectionController constructor(
    private val peerConnectionCreator: (PeerConnection.Observer) -> PeerConnection
) {
    val id: Long = System.currentTimeMillis()
    private var connectionObserver: MillicastPeerConnectionObserver =
        MillicastPeerConnectionObserver()
    var connection: PeerConnection? = null
    var localAudioTrack: AudioTrack? = null
    var disposed = false
        private set

    fun create(localAudioTrackCreator: () -> AudioTrack, isPublisher: Boolean): () -> Unit {
        connection = peerConnectionCreator.invoke(connectionObserver)
        if (isPublisher) {
            this.localAudioTrack = localAudioTrackCreator.invoke()
            addSenderTransceiver()
        } else addReceiveTransceiver()
        return ::dispose
    }

    private fun addSenderTransceiver() {
        val transceiver = RtpTransceiver.RtpTransceiverDirection.SEND_ONLY
        connection?.addTransceiver(
            localAudioTrack,
            RtpTransceiver.RtpTransceiverInit(transceiver, mediaStreamLabels)
        )
    }

    private fun addReceiveTransceiver() {
        val transceiver =
            RtpTransceiver.RtpTransceiverInit(RtpTransceiver.RtpTransceiverDirection.RECV_ONLY)
        connection?.addTransceiver(
            MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO,
            transceiver
        )
    }

    private fun dispose() {
        if (disposed) return
        Timber.d("disposing on state: ${connection?.connectionState()}")
        disposed = true
        localAudioTrack?.dispose()
        connection?.dispose()
    }

    fun setConnectionStatusListener(listener: (PeerConnection.IceConnectionState) -> Unit) {
        connectionObserver.onConnectionStateChangeListener = listener
    }

    fun muteMic() {
        localAudioTrack?.apply {
            if (!disposed) {
                setEnabled(false)
                setVolume(100.0)
            }
        }
    }
    fun unMuteMic() {
        localAudioTrack?.apply {
            if (!disposed) {
                setEnabled(true)
                setVolume(100.0)
            }
        }
    }

    fun getPublishCodec(): String = AUDIO_CODEC_H264

    companion object {
        private var mediaStreamLabels = listOf("ARDAMS")
        const val AUDIO_CODEC_H264 = "h264"
    }
}