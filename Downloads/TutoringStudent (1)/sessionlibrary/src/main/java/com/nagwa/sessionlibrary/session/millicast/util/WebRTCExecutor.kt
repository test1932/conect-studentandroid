package com.nagwa.sessionlibrary.session.millicast.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors

class WebRTCExecutor : Executor {

    val executor: Executor by lazy {
        Executors.newSingleThreadExecutor()
    }

    override fun execute(command: Runnable?) {
        executor.execute(command)
    }
}