package com.nagwa.sessionlibrary.session.agora.data.source

import com.nagwa.sessionlibrary.session.agora.data.model.AGEventHandler
import com.nagwa.sessionlibrary.session.agora.data.model.EngineConfig
import com.nagwa.sessionlibrary.session.agora.data.model.SessionConstant
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import timber.log.Timber

class MyEngineEventHandler(private val mConfig: EngineConfig) {
    private var mEventHandler: AGEventHandler? = null
    fun addEventHandler(handler: AGEventHandler) {
        mEventHandler = handler
    }

    fun removeEventHandler() {
        mEventHandler = null
    }

    val mRtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onUserJoined $uid")
        }

        override fun onUserOffline(uid: Int, reason: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onUserOffline $uid     reason    $reason")

            // FIXME this callback may return times
            mEventHandler?.onUserOffline(uid, reason)
        }

        override fun onRtcStats(stats: RtcStats) {}
        override fun onAudioVolumeIndication(
            speakerInfos: Array<AudioVolumeInfo>,
            totalVolume: Int
        ) {
            mEventHandler?.onExtraCallback(
                AGEventHandler.EVENT_TYPE_ON_SPEAKER_STATS,
                speakerInfos as Any
            )
        }

        override fun onLeaveChannel(stats: RtcStats) {}
        override fun onLastmileQuality(quality: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onLastmileQuality $quality")
        }

        override fun onError(error: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onError $error")
            mEventHandler?.onExtraCallback(
                AGEventHandler.EVENT_TYPE_ON_AGORA_MEDIA_ERROR,
                error,
                RtcEngine.getErrorDescription(error)
            )

        }

        override fun onAudioQuality(uid: Int, quality: Int, delay: Short, lost: Short) {
            mEventHandler?.onExtraCallback(
                AGEventHandler.EVENT_TYPE_ON_AUDIO_QUALITY,
                uid,
                quality,
                delay,
                lost
            )
        }

        override fun onConnectionLost() {
            Timber.d("NagwaSession  MyEngineEventHandler onConnectionLost")
            mEventHandler?.onConnectionLost()

        }

        override fun onConnectionInterrupted() {
            Timber.d("NagwaSession  MyEngineEventHandler onConnectionInterrupted")
            mEventHandler?.onConnectionInterrupted()
        }

        override fun onJoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onJoinChannelSuccess  channel -$channel      uid -   $uid     elapsed    $elapsed")
            mConfig.mUid = uid
            mEventHandler?.onJoinChannelSuccess(channel, uid, elapsed)

        }

        override fun onRejoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
            mEventHandler?.onRejoinChannelSuccess(channel, uid, elapsed)
            Timber.d("NagwaSession  MyEngineEventHandler onRejoinChannelSuccess channel -$channel      uid -   $uid     elapsed    $elapsed ")
        }

        override fun onWarning(warn: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onWarning $warn")
        }

        override fun onAudioRouteChanged(routing: Int) {
            Timber.d("NagwaSession  MyEngineEventHandler onAudioRouteChanged $routing")
            mEventHandler?.onExtraCallback(
                AGEventHandler.EVENT_TYPE_ON_AUDIO_ROUTE_CHANGED,
                routing
            )

        }
    }

}