package com.nagwa.sessionlibrary.session.millicast.socket

import android.os.Build
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import timber.log.Timber
import java.net.URI
import javax.inject.Inject
import javax.net.ssl.*


class WebSocketClientWrapper @Inject constructor(
    val onOpened: () -> Unit,
    val onClosed: (code: Int, reason: String?) -> Unit,
    val onFailure: (Throwable) -> Unit,
    val onMessageReceived: (String) -> Unit
) {
    private var socketClient: WebSocketClient? = null

    //pass full url (url+token)
    fun connect(url: String) {
        Timber.d("trying to connect to $url")
        //Bug on pre N version https://github.com/TooTallNate/Java-WebSocket/wiki/No-such-method-error-setEndpointIdentificationAlgorithm
        socketClient = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            createSocketClient(url)
        } else
            createPre24SocketClient(url)
        connectAndValidate(url)
        Timber.d("connected")
    }

    private fun connectAndValidate(url: String) {
        socketClient?.connectBlocking() ?: error("socket client can't be null")
        //Bug on pre N version https://github.com/TooTallNate/Java-WebSocket/wiki/No-such-method-error-setEndpointIdentificationAlgorithm
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            validateHostname(url)
    }

    private fun validateHostname(url: String) {
        val hv: HostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier()
        val socket: SSLSocket = socketClient!!.socket as SSLSocket
        val sslSession: SSLSession = socket.session
        val host = URI(url).host
        if (!hv.verify(host, sslSession)) {
            throw SSLHandshakeException("Expected $host, found " + sslSession.peerPrincipal)
        } else {
            Timber.d("validateHostname: hostname validated successfully")
        }
    }

    fun stop() {
        Timber.d("closing")
        socketClient?.closeBlocking()
        socketClient = null
        Timber.d("closed")
    }

    fun sendMessage(message: String) {
        socketClient?.send(message) ?: error("socket client can't be null")
    }

    private fun onSocketOpened() = onOpened()
    private fun onSocketClosed(code: Int, reason: String?) = onClosed(code, reason)
    private fun onSocketMessage(message: String?) {
        message?.let { onMessageReceived(it) }
    }

    private fun onSocketFailure(e: Exception) = onFailure(e)

    private fun createPre24SocketClient(url: String): WebSocketClient {
        return object : WebSocketClient(URI(url)) {
            //leave this method as it is  //Bug on pre N version https://github.com/TooTallNate/Java-WebSocket/wiki/No-such-method-error-setEndpointIdentificationAlgorithm
            override fun onSetSSLParameters(sslParameters: SSLParameters?) {
                // super.onSetSSLParameters(sslParameters)
            }

            override fun onOpen(handshakedata: ServerHandshake?) = onSocketOpened()
            override fun onClose(code: Int, reason: String?, remote: Boolean) =
                onSocketClosed(code, reason)

            override fun onMessage(message: String?) = onSocketMessage(message)
            override fun onError(e: java.lang.Exception) = onSocketFailure(e)
        }
    }

    private fun createSocketClient(url: String): WebSocketClient {
        return object : WebSocketClient(URI(url)) {
            override fun onOpen(handshakedata: ServerHandshake?) = onSocketOpened()
            override fun onClose(code: Int, reason: String?, remote: Boolean) = onSocketClosed(
                code,
                reason
            )

            override fun onMessage(message: String?) = onSocketMessage(message)
            override fun onError(e: java.lang.Exception) = onSocketFailure(e)
        }
    }

}