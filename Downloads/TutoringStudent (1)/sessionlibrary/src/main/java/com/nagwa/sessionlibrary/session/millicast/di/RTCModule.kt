package com.nagwa.sessionlibrary.session.millicast.di

import android.content.Context
import android.os.Handler
import com.nagwa.sessionlibrary.BuildConfig
import com.nagwa.sessionlibrary.session.millicast.audioutils.AppRTCAudioManager
import com.nagwa.sessionlibrary.session.millicast.rtc.PeerConnectionStore
import com.nagwa.sessionlibrary.session.millicast.util.WebRTCExecutor
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.Executor
import javax.inject.Named
import javax.inject.Scope

@Module
class RTCModule {


    @Provides
    @RTCSessionScope
    internal fun provideAppRTCAudioManager(context: Context): AppRTCAudioManager =
        Handler(context.mainLooper).run { AppRTCAudioManager.create(context) }

    @Provides
    @RTCSessionScope
    internal fun providePeerConnectionStore(
        context: Context,
        rtcAudioManager: AppRTCAudioManager
    ): PeerConnectionStore =
        PeerConnectionStore(context, rtcAudioManager)

    @Provides
    @RTCSessionScope
    internal fun provideOkHttpClient(
        logging: HttpLoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder().run {
            if (BuildConfig.DEBUG)
                addInterceptor(logging)
            build()
        }

    @Provides
    @RTCSessionScope
    internal fun provideOkHttpLogging(
    ): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @RTCSessionScope
    @Named("WebRTCScheduler")
    internal fun provideWebRTCExecutor(
    ): Scheduler = Schedulers.from(WebRTCExecutor())

}

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RTCSessionScope