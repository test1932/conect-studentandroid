package com.nagwa.sessionlibrary.session.millicast

import android.annotation.SuppressLint
import android.os.Looper
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.SessionCallParams
import com.nagwa.sessionlibrary.session.millicast.model.StreamData
import com.nagwa.sessionlibrary.session.millicast.network.MillicastService
import com.nagwa.sessionlibrary.session.millicast.util.NetworkMonitoring
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class MillicastCallProvider @Inject constructor(
    private val factory: MillicastCaller.Companion.Factory,
    private val service: MillicastService,
    private val networkMonitoring: NetworkMonitoring
) :
    NagwaCallProvider {
    val compositeDisposable = CompositeDisposable()
    var isCallMuted = false
    var mCall = factory.create().apply {
        onDisconnectListener = ::onCallDisconnected
    }
    private lateinit var mSessionParams: MillicastCallParams
    private lateinit var mCallParams: MillicastCaller.Params

    private val stateEmitter = BehaviorSubject.create<CallStatus>()
    private val callEndedError = IllegalAccessError("call already ended")
    private var callState: CallStatus = CallStatus.NotStarted
        set(value) {
            field = value
            if (!stateEmitter.hasComplete())
                stateEmitter.onNext(value)
            else Timber.w("can't emit call state: the eventsStream is completed ")
        }

    override fun startCall(callParams: SessionCallParams): Flowable<CallStatus> {
        if (callParams !is MillicastCallParams)
            return Flowable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        if (callState > CallStatus.NotStarted)
            return Flowable.error(IllegalAccessError("another call is already started"))
        mSessionParams = callParams
        mCallParams = callParams.toParams()
        startCallInternally()
        return stateEmitter.toFlowable(BackpressureStrategy.LATEST)

    }

    private fun startCallInternally() {
        mCall.startCall(mCallParams)
            .doOnSubscribe { callState = CallStatus.Starting }
            .subscribeOn(Schedulers.io())
            .subscribe({
                stateEmitter.onNext(CallStatus.Connected)
            }, {
                stateEmitter.onError(it)
            }).addTo(compositeDisposable)
    }

    override fun startStatelessCall(callParams: SessionCallParams): Completable {
        if (callState > CallStatus.NotStarted)
            return Completable.error(IllegalAccessError("another call is already started"))
        if (callParams !is MillicastCallParams)
            return Completable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        return factory.create().startCall(callParams.toParams())
    }

    override fun endCall(): Completable = Completable.fromAction {
        if (!assertCallNotEnded()) error(callEndedError)
        else mCall.endCall()
    }.doOnComplete {
        Timber.d("endCall: completed")
        stateEmitter.onNext(CallStatus.Ended)
        dispose()
    }

    private fun dispose() {
        compositeDisposable.clear()
        stateEmitter.onComplete()
    }

    override fun muteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        return mCall.muteCall().doOnComplete { isCallMuted = true }
    }

    override fun unMuteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        return mCall.unMuteCall().doOnComplete { isCallMuted = false }
    }

    override fun switchRole(isBroadcaster: Boolean): Completable {
        return Completable.error(IllegalAccessError("switchRole not available yet in Millicast provider"))
    }

    //invocation occurs in the main thread
    @SuppressLint("CheckResult")
    private fun onCallDisconnected() {
        stateEmitter.onNext(CallStatus.Disconnected)
        if (mSessionParams.autoRejoin) {
            rejoinCall()
        } else {
            endCall()
                .subscribeOn(Schedulers.io())
                .subscribe({}, Timber::e)
        }
    }

    private fun createNewCall() {
        mCall = factory.create()
        mCall.onDisconnectListener = ::onCallDisconnected
    }

    private fun rejoinCall() {
        networkMonitoring.onInternetReachability()
            .filter { it }
            .flatMap { getStreamData().toFlowable() }
            .map { it.toParams() }
            .flatMapCompletable {
                createNewCall()
                mCall.startCall(it)
            }
            .retry(4)
            .subscribeOn(Schedulers.io())
            .andThen(if (isCallMuted) muteCall() else Completable.complete())
            .subscribe({
                stateEmitter.onNext(CallStatus.Connected)
            }, {
                stateEmitter.onNext(CallStatus.FailedToReConnect)
            }).addTo(compositeDisposable)
    }


    private fun getStreamData(): Single<StreamData> {
        return if (mSessionParams.isBroadcaster)
            service.publishStream(
                streamID = mSessionParams.channelID,
                publisherToken = mSessionParams.publisherToken
            )
        else
            service.subscribeToStream(
                accountID = mSessionParams.accountId,
                streamID = mSessionParams.channelID
            )
    }

    private fun assertCallNotEnded() = callState != CallStatus.Ended


    private fun StreamData.toParams(): MillicastCaller.Params =
        mCallParams.copy(wsUrl = "${wsUrl}?token=${jwt}")


    private fun MillicastCallParams.toParams(): MillicastCaller.Params = MillicastCaller.Params(
        isBroadcaster = isBroadcaster,
        channelID = channelID,
        wsUrl = wsUrl,
        servers = servers
    )

    companion object {
        class Factory @Inject constructor(
            private val factory: MillicastCaller.Companion.Factory,
            private val service: MillicastService,
            private val networkMonitoring: NetworkMonitoring
        ) {
            fun create(): MillicastCallProvider =
                MillicastCallProvider(factory, service, networkMonitoring)
        }
    }
}
