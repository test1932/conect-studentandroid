package com.nagwa.sessionlibrary.session.agora

import android.content.Context
import com.nagwa.sessionlibrary.session.*
import com.nagwa.sessionlibrary.session.agora.data.model.AGEventHandler
import com.nagwa.sessionlibrary.session.agora.data.source.SessionWorkerThread
import io.agora.rtc.Constants
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
class AgoraCallProvider constructor(private val context: Context) :
    AGEventHandler, NagwaCallProvider,
    NagwaSwitchableCallProvider {

    @Volatile
    private var mAudioRouting = -1 // Default  Speakerphone = 3

    @get:Synchronized
    private var workerThread: SessionWorkerThread? = null
    val compositeDisposable = CompositeDisposable()
    lateinit var mCallParams: AgoraCallParams
    lateinit var callStateEmitter: FlowableEmitter<CallStatus>
    private var callState: CallStatus = CallStatus.NotStarted
        set(value) {
            field = value
            if (::callStateEmitter.isInitialized && !callStateEmitter.isCancelled)
                callStateEmitter.onNext(value)
            else Timber.w("can't emit call state: the eventsStream is completed ")
        }


    override fun startCall(callParams: SessionCallParams) =
        Flowable.create<CallStatus>({
            try {
                callStateEmitter = it
                callStateEmitter.setDisposable(compositeDisposable)
                startCallInternal(callParams)
            } catch (error: Throwable) {
                it.tryOnError(error)
            }
        }, BackpressureStrategy.LATEST)

    override fun startStatelessCall(callParams: SessionCallParams): Completable =
        startCall(callParams)
            .flatMap { callStatus ->
                if (callStatus > CallStatus.Connected) Flowable.error(SessionNotStarted())
                else Flowable.just(callStatus)
            }
            .filter { it == CallStatus.Connected }
            .ignoreElements()
            .doOnComplete { compositeDisposable.clear() }

    override fun startTalking(): Completable = Completable.fromAction {
        workerThread?.switchRole(Constants.CLIENT_ROLE_BROADCASTER)
    }

    override fun stopTalking(): Completable = Completable.fromAction {
        workerThread?.switchRole(Constants.CLIENT_ROLE_AUDIENCE)
    }

    override fun endCall() = Completable.fromAction {
        Timber.d("endCall() called")
        if (callState == CallStatus.Ended) return@fromAction
        workerThread?.eventHandler()?.removeEventHandler()
        workerThread?.leaveChannel(workerThread?.engineConfig?.mChannel)
        workerThread?.exit()
        workerThread = null
        callState = CallStatus.Ended
        compositeDisposable.clear()
    }

    override fun muteCall() = Completable.fromAction {
        workerThread?.rtcEngine?.muteLocalAudioStream(true)
    }

    override fun unMuteCall() = Completable.fromAction {
        workerThread?.rtcEngine?.muteLocalAudioStream(false)
    }

    private fun startCallInternal(callParams: SessionCallParams) {
        if (callParams !is AgoraCallParams) error("make sure callParams is instance of AgoraCallParams")
        mCallParams = callParams
        callState = CallStatus.Starting
        initWorkerThread()
        workerThread?.eventHandler()?.addEventHandler(this)
        workerThread?.configEngine(if (mCallParams.isBroadcaster) Constants.CLIENT_ROLE_BROADCASTER else Constants.CLIENT_ROLE_AUDIENCE)
        workerThread?.joinChannel(
            mCallParams.channelID,
            mCallParams.userID
        )
        //workerThread?.exit()
    }

    private fun switchSpeaker() {
        //switch External speaker or Internal speaker
        val rtcEngine = workerThread?.rtcEngine
        rtcEngine?.setEnableSpeakerphone(mAudioRouting != 3)
    }


    @Synchronized
    private fun initWorkerThread() {
        if (workerThread == null) {
            workerThread =
                SessionWorkerThread(
                    context
                )
            workerThread?.start()
            workerThread?.waitForReady()
        }
    }

    @Synchronized
    private fun deInitWorkerThread() {
        workerThread?.exit()
        try {
            workerThread?.join()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        workerThread = null
    }

    override fun onJoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
        val msg =
            "onJoinChannelSuccess " + channel + " " + (uid and 0xFFFFFFFF.toInt()) + " " + elapsed
        Timber.d("NagwaSession  $msg")
        callState = CallStatus.Connected
    }

    override fun onRejoinChannelSuccess(channel: String, uid: Int, elapsed: Int) {
        callState = CallStatus.Connected
    }

    override fun onUserOffline(uid: Int, reason: Int) {
        val msg = "onUserOffline " + (uid and 0xFFFFFFFF.toInt()) + " " + reason
        Timber.d("NagwaSession    $msg")
    }

    //called 10 seconds after connection interrupted didn't rejoined
    override fun onConnectionLost() {
        //callState = CallStatus.Disconnected //already have been set onConnectionInterrupted
    }

    //called first on connection interrupted
    override fun onConnectionInterrupted() {
        callState = CallStatus.Disconnected
    }

    override fun onExtraCallback(type: Int, vararg data: Any?) {
        doHandleExtraCallback(type, data)
    }

}