package com.nagwa.sessionlibrary.session.millicast.network

import com.google.gson.Gson
import com.nagwa.sessionlibrary.session.millicast.model.ICEServersResponse
import com.nagwa.sessionlibrary.session.millicast.model.IceServer
import com.nagwa.sessionlibrary.session.millicast.model.PublishingResponse
import com.nagwa.sessionlibrary.session.millicast.model.StreamData
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

class MillicastService @Inject constructor(private val client: OkHttpClient) {
    private val gson = Gson()
    private val mediaType = "application/json".toMediaTypeOrNull() ?: error("")
    fun subscribeToStream(accountID: String, streamID: String): Single<StreamData> =
        Single.create { emitter ->

            val bodyContent =
                "{\"streamAccountId\": \"$accountID\",\"streamName\": \"$streamID\",\"unauthorizedSubscribe\": true}"
            val body: RequestBody =
                bodyContent.toRequestBody(mediaType)
            val request: Request = Request.Builder()
                .url("https://director.millicast.com/api/director/subscribe")
                .post(body)
                .addHeader("accept", "application/json")
                .addHeader("content-type", "application/json")
                .build()
            try {
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    val responseBody =
                        gson.fromJson(response.body!!.string(), PublishingResponse::class.java)
                    emitter.onSuccess(responseBody.info!!)
                } else {
                    if (response.code == 400) emitter.onError(SessionNotPublished(response.body?.string()))
                    else emitter.tryOnError(IllegalStateException("Response isn't successful with body=[${response.body?.string()}]"))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.tryOnError(e)
            }
        }

    fun publishStream(streamID: String, publisherToken: String): Single<StreamData> =
        Single.create { emitter ->
            val bodyContent = "{\"streamName\":\"$streamID\"}"
            val body: RequestBody = bodyContent.toRequestBody(mediaType)
            val request: Request = Request.Builder()
                .url("https://director.millicast.com/api/director/publish")
                .post(body)
                .addHeader("accept", "application/json")
                .addHeader("content-type", "application/json")
                .addHeader("authorization", "Bearer $publisherToken")
                .build()

            try {
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    val responseBody =
                        gson.fromJson(response.body!!.string(), PublishingResponse::class.java)
                    emitter.onSuccess(responseBody.info!!)
                } else {
                    emitter.tryOnError(
                        IllegalStateException(
                            "Response isn't successful with body=[${response.body?.string()}]"
                        )
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.tryOnError(e)
            }
        }

    fun getTurnICEServers(): Single<List<IceServer>> = Single.create { emitter ->
        val request: Request = Request.Builder()
            .url("https://turn.millicast.com/webrtc/_turn")
            .put(RequestBody.create(mediaType, "{}"))
            .addHeader("content-type", "application/json")
            .addHeader("cache-control", "no-cache")
            .addHeader("postman-token", "0e23a2b8-3b97-ae06-4b17-b3b0b1235375")
            .build()
        try {
            val response = client.newCall(request).execute()
            if (response.isSuccessful) {
                val iceServersResponse =
                    gson.fromJson(response.body!!.string(), ICEServersResponse::class.java)
                emitter.onSuccess(iceServersResponse.response!!.servers)
            } else {
                emitter.tryOnError(IllegalStateException("Response isn't successful with body=[${response.body?.string()}"))
            }
        } catch (e: Exception) {
            emitter.tryOnError(e)
        }
    }

    class SessionNotPublished(errorData: String?) :
        IllegalStateException("Stream has not been published yet info=[$errorData]")
}