package com.nagwa.sessionlibrary.session.millicast

import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaSwitchableCallProvider
import com.nagwa.sessionlibrary.session.SwitchableCallParams
import com.nagwa.sessionlibrary.session.millicast.model.IceServer
import com.nagwa.sessionlibrary.session.millicast.model.MillicastCallParams
import com.nagwa.sessionlibrary.session.millicast.model.MillicastCallParams.MillicastIceServer
import com.nagwa.sessionlibrary.session.millicast.network.MillicastService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.zipWith
import timber.log.Timber
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class MillicastSwitchableCallProvider constructor(
    private val factory: MillicastCallProvider.Companion.Factory,
    private val service: MillicastService
) : NagwaSwitchableCallProvider {
    private val compositeDisposable = CompositeDisposable()
    private var publisherCall: MillicastCallProvider? = null
    private var subscriberCall: MillicastCallProvider? = null
    private lateinit var mCallParams: SwitchableCallParams

    override fun initCall(callParams: SwitchableCallParams?) {
        mCallParams = callParams!!
    }

    override fun startListening(): Completable {
        Timber.d("startListening() called")
        if (!::mCallParams.isInitialized) error("initCall must be called before startListening")
        //if (subscriberCall == null) error("subscriberCall can't be null in startListening")
        return if (subscriberCall?.callState == CallStatus.Connected)
            subscriberCall!!.unMuteCall()
        else requestNewSubscriberAsync()
            .andThen(getSubscriberCallParams(mCallParams.holdMicOnNoPublisher))
            .flatMapPublisher {
                //allow the internal rejoin of the subscriber call to use publishOnNotPublishedError which publish the stream if no publisher error occurs while trying to (rejoin)resubscribe
                if (mCallParams.holdMicOnNoPublisher)
                    subscriberCall!!.rejoinRetryPredicate = ::publishOnNotPublishedError
                subscriberCall!!.startCall(it)
            }
            .takeUntil { it == CallStatus.Connected }
            .timeout(20, TimeUnit.SECONDS)
            .ignoreElements()
    }

    override fun stopListening(): Completable {
        Timber.d("stopTalking() called")
        return subscriberCall?.muteCall()
            ?: Completable.error(IllegalStateException("Publisher Call can't be null on stopTalking"))
    }

    override fun requestTalking(): Completable {
        Timber.d("requestTalking: called while publisherState is ${publisherCall?.callState}")
        if (!::mCallParams.isInitialized) error("initCall must be called before requestTalking")
        return if (publisherCall?.callState == CallStatus.Ready)
            Completable.complete()
        else {
            requestNewPublisherAsync()
                .andThen(getPublisherCallParams())
                .flatMapCompletable {
                    publisherCall!!.initiateCall(it)
                }
        }
    }

    override fun startTalking(): Completable {
        Timber.d("startTalking() called while publisherState is ${publisherCall?.callState}")
        return publisherCall
            ?.run {
                connectCall().andThen(
                    subscriberCall?.muteCall()?.onErrorComplete() ?: Completable.complete()
                )
            }
            ?: Completable.error(IllegalStateException("Publisher Call can't be null on StartTalking"))
    }

    override fun stopTalking(): Completable {
        Timber.d("stopTalking() called")
        return publisherCall?.muteCall()
            ?.andThen(subscriberCall?.unMuteCall()?.onErrorComplete() ?: Completable.complete())
            ?: Completable.error(IllegalStateException("Publisher Call can't be null on stopTalking"))
    }


    override fun endCall(): Completable =
        endPublisherCall().andThen(endSubscriberCall())
            .doOnComplete {
                Timber.d("endCall: completed")
                dispose()
            }.also { Timber.d("endCall() called") }

    private fun activatePublisher(): Completable =
        requestTalking()
            .andThen(Completable.defer { startTalking() })
            .andThen(Completable.defer { stopTalking() })
            .doOnError { Timber.e(it, "holdMic: ") }

    private fun getSubscriberCallParams(publishOnNotPublished: Boolean): Single<MillicastCallParams> {
        return service.subscribeToStream(mCallParams.accountId, mCallParams.streamID)
            .run { if (publishOnNotPublished) retryWhen(::publishOnNotPublishedError) else this }
            .zipWith(service.getTurnICEServers().map { it.mapToMCServers() })
            .map {
                val (streamData, iceServers) = it
                mCallParams.toCallParams(false, streamData.getFullURL(), iceServers)
            }
    }

    private fun publishOnNotPublishedError(it: Flowable<Throwable>): Flowable<Unit> {
        val retries = AtomicInteger();
        return it.flatMap { err ->
            if (err is MillicastService.SessionNotPublished && retries.getAndIncrement() != 2)
                activatePublisher().andThen(Flowable.just(Unit))
            else Flowable.error<Unit>(err)
        }
    }

    private fun getPublisherCallParams(): Single<MillicastCallParams> {
        return service.publishStream(mCallParams.streamID, mCallParams.authToken)
            .zipWith(service.getTurnICEServers().map { it.mapToMCServers() })
            .map {
                val (streamData, iceServers) = it
                mCallParams.toCallParams(true, streamData.getFullURL(), iceServers)
            }
    }

    private fun requestNewPublisherAsync(): Completable =
        endPublisherCall().andThen(
            Completable.fromAction {
                publisherCall = factory.create()
                Timber.d("publisherCall() created")
            }
        ).also { Timber.d("requestNewPublisherAsync() called") }

    private fun requestNewSubscriberAsync(): Completable =
        endSubscriberCall().andThen(
            Completable.fromAction {
                subscriberCall = factory.create()
                Timber.d("subscriberCall() created")
            }
        ).also { Timber.d("requestNewSubscriberAsync() called") }

    private fun endPublisherCall(): Completable {
        Timber.d("endPublisherCall() called")
        return (publisherCall?.endCall()
            ?.doOnError { Timber.e("endPublisherCall: ${it.message} ") }
            ?.onErrorComplete()
            ?: Completable.complete())
    }


    private fun endSubscriberCall(): Completable {
        Timber.d("endSubscriberCall() called")
        return (subscriberCall?.endCall()
            ?.doOnError { Timber.e(it, "endSubscriberCall: ") }
            ?.onErrorComplete()
            ?: Completable.complete())
    }

    private fun dispose() {
        Timber.d("dispose() called")
        compositeDisposable.clear()
    }

    private fun List<IceServer>.mapToMCServers(): List<MillicastIceServer> =
        map { MillicastIceServer(credential = it.credential, url = it.url, username = it.username) }

    private fun SwitchableCallParams.toCallParams(
        isBroadcaster: Boolean,
        wsUrl: String,
        servers: List<MillicastIceServer>
    ): MillicastCallParams {
        return MillicastCallParams(
            isBroadcaster = isBroadcaster,
            streamID = streamID,
            publisherToken = authToken,
            accountId = accountId,
            wsUrl = wsUrl,
            servers = servers,
            autoRejoin = !isBroadcaster,
            allowMuteStateRecovery = false
        )
    }

    companion object {
        class Factory @Inject constructor(
            private val factory: MillicastCallProvider.Companion.Factory,
            private val service: MillicastService
        ) {
            fun create(): MillicastSwitchableCallProvider =
                MillicastSwitchableCallProvider(factory, service)
        }

    }

}

