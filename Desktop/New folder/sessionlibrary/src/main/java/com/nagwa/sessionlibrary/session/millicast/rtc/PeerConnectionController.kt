package com.nagwa.sessionlibrary.session.millicast.rtc

import org.webrtc.AudioTrack
import org.webrtc.MediaStreamTrack
import org.webrtc.PeerConnection
import org.webrtc.RtpTransceiver
import timber.log.Timber

class PeerConnectionController constructor(
    private val peerConnectionCreator: (PeerConnection.Observer) -> PeerConnection
) {
    val id: Long = System.currentTimeMillis()
    private var connectionObserver: MillicastPeerConnectionObserver =
        MillicastPeerConnectionObserver()
    var connection: PeerConnection? = null
    var localAudioTrack: AudioTrack? = null
    var disposed = false
        private set

    fun create(localAudioTrackCreator: () -> AudioTrack, isPublisher: Boolean): () -> Unit {
        connection = peerConnectionCreator.invoke(connectionObserver)
        if (isPublisher) {
            this.localAudioTrack = localAudioTrackCreator.invoke()
            addSenderTransceiver()
        } else addReceiveTransceiver()
        Timber.d("created with: localAudioTrack = [$localAudioTrack], isPublisher = [$isPublisher]")
        return ::dispose
    }

    private fun addSenderTransceiver() {
        val transceiverInit = RtpTransceiver.RtpTransceiverDirection.SEND_ONLY
        connection?.addTransceiver(
            localAudioTrack,
            RtpTransceiver.RtpTransceiverInit(transceiverInit, mediaStreamLabels)
        )
    }

    private fun addReceiveTransceiver() {
        val transceiverInit =
            RtpTransceiver.RtpTransceiverInit(RtpTransceiver.RtpTransceiverDirection.RECV_ONLY)
        connection?.addTransceiver(
            MediaStreamTrack.MediaType.MEDIA_TYPE_AUDIO,
            transceiverInit
        )
    }

    private fun dispose() {
        if (disposed) return
        Timber.d("disposing on state: ${connection?.connectionState()}")
        disposed = true
        localAudioTrack?.dispose()
        connection?.dispose()
        connection = null
        localAudioTrack = null
    }

    fun setConnectionStatusListener(listener: (PeerConnection.IceConnectionState) -> Unit) {
        connectionObserver.onConnectionStateChangeListener = listener
    }

    fun muteMic() {
        Timber.d(
            "muteMic() called with audio track state = [${
                localAudioTrack?.takeIf { !disposed }
                    ?.state()
            }]")
        localAudioTrack
            ?.takeIf { !disposed }
            ?.apply {
                setEnabled(false)
                Timber.d("Mic is Muted")
            } ?: Timber.d("can't muteMic connection is disposed")
    }

    fun unMuteMic() {
        Timber.d(
            "unMuteMic() called with audio track state = [${
                localAudioTrack?.takeIf { !disposed }
                    ?.state()
            }]")
        localAudioTrack
            ?.takeIf { !disposed }
            ?.apply {
                setEnabled(true)
                setMaxSound()
                Timber.d("Mic is UnMuted")
            } ?: Timber.d("can't unMuteMic connection is disposed")
    }


    fun muteSpeaker() {
        Timber.d(
            "muteSpeaker: with audio track state = [${
                connection?.receivers?.get(0)?.track()
                    ?.takeIf { !disposed }?.state()
            }]"
        )
        connection?.receivers?.get(0)?.track()
            ?.takeIf { !disposed }
            ?.setEnabled(false)
            ?.also {
                Timber.d("Speaker is Muted")
            } ?: Timber.d("can't muteSpeaker connection is disposed")
    }


    fun unMuteSpeaker() {
        Timber.d(
            "unMuteSpeaker: with audio track state = [${
                connection?.receivers?.get(0)?.track()
                    ?.takeIf { !disposed }?.state()
            }]"
        )
        connection?.receivers?.get(0)?.track()
            ?.takeIf { !disposed }
            ?.setEnabled(true)
            ?.also {
                Timber.d("Speaker is UnMuted")
                setMaxSound()
            } ?: Timber.d("can't unMuteSpeaker connection is disposed")
    }

    fun setMaxSound() {
        Timber.d("setMaxSound: ")
        localAudioTrack?.takeIf { !disposed }?.setVolume(VOLUME_MAX)
        connection?.receivers?.get(0)?.track()
            ?.takeIf { !disposed && it is AudioTrack }
            ?.let { (it as AudioTrack).setVolume(VOLUME_MAX) }
        connection?.senders?.get(0)?.track()
            ?.takeIf { !disposed && it is AudioTrack }
            ?.let { (it as AudioTrack).setVolume(VOLUME_MAX) }
    }

    fun getPublishCodec(): String = AUDIO_CODEC_H264

    companion object {
        private const val VOLUME_MAX = 10.0
        private var mediaStreamLabels = listOf("ARDAMS")
        const val AUDIO_CODEC_H264 = "h264"
    }
}