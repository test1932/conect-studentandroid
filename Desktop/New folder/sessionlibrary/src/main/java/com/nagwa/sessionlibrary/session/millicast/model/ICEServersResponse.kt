package com.nagwa.sessionlibrary.session.millicast.model


import com.google.gson.annotations.SerializedName

data class ICEServersResponse(
    @SerializedName("v")
    val response: ICEServers?,
    @SerializedName("s")
    val status: String?
)

data class ICEServers(
    @SerializedName("iceServers")
    val servers: List<IceServer>
)

data class IceServer(
    @SerializedName("credential")
    val credential: String?,
    @SerializedName("url")
    val url: String,
    @SerializedName("username")
    val username: String?
)