package com.nagwa.sessionlibrary.session

import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Created by Youssef Ebrahim Elerian on 7/21/20.
 * youssef.elerian@gmail.com
 */
interface NagwaCallProvider {
    fun startCall(callParams: SessionCallParams): Flowable<CallStatus>
    fun startStatelessCall(callParams: SessionCallParams): Completable
    fun endCall(): Completable
    fun muteCall(): Completable
    fun unMuteCall(): Completable
}

class SessionNotStarted : Throwable("Session couldn't be started")
enum class CallStatus { NotStarted, Starting, Ready, Connected, Disconnected, FailedToReConnect, Ended }
abstract class SessionCallParams(open val isBroadcaster: Boolean, open val channelID: String)