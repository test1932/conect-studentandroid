package com.nagwa.sessionlibrary.session.millicast.rtc

import android.content.Context
import com.nagwa.sessionlibrary.session.millicast.audioutils.AppRTCAudioManager
import com.nagwa.sessionlibrary.session.millicast.util.runOnMain
import org.webrtc.*
import org.webrtc.audio.AudioDeviceModule
import timber.log.Timber


class PeerConnectionStore constructor(
    private val context: Context,
    audioManagerFactory: AppRTCAudioManager.Companion.Factory
) {
    private lateinit var adm: AudioDeviceModule
    private lateinit var factory: PeerConnectionFactory
    private lateinit var audioManager: AppRTCAudioManager
    private var peerConnectionsControllers: MutableList<ControllerWithDisposer> = mutableListOf()
    private var disposed = false
    var onStoreDisposed: (() -> Unit)? = null
    private lateinit var audioSource: AudioSource
    private val audioConstraints = createAudioConstrains()

    init {
        audioManagerFactory.create {
            audioManager = it
        }
        Timber.d("Created with hash: ${hashCode()}")
    }

    fun createPeerConnection(
        rtcConfig: PeerConnection.RTCConfiguration,
        isPublisher: Boolean
    ): PeerConnectionController {
        if (disposed || !::factory.isInitialized) {
            //error("this store is already disposed .. please create a new one for new session")
            initializeStore()
        }
        val controller =
            PeerConnectionController { factory.createPeerConnection(rtcConfig, it)!! }
        startAudioManager()
        val disposer = controller.create(
            ::createAudioTrack,
            isPublisher
        ) //only store able to dispose the connection
        controller.setMaxSound()
        Timber.d("createPeerConnection: connection status:${controller.connection?.connectionState()} ")
        peerConnectionsControllers.add(ControllerWithDisposer(controller, disposer))
        Timber.d("createPeerConnection: current connections ${peerConnectionsControllers.count { !it.controller.disposed }}")
        return controller
    }

    private fun initializeStore() {
        adm = createJavaAudioDevice(context)
        factory = createPeerConnectionFactory()
        audioSource = factory.createAudioSource(audioConstraints)
    }

    private fun startAudioManager() {
        if (!::audioManager.isInitialized) error("audioManager is not intialized yet")
        if (audioManager.amState == AppRTCAudioManager.AudioManagerState.RUNNING) return
        //must be started on the main thread
        runOnMain { audioManager.start(audioManagerEvents) }
    }

    private fun createAudioTrack(): AudioTrack {
        return factory.createAudioTrack(AUDIO_TRACK_ID, audioSource).apply {
            setVolume(100.0)
            setEnabled(true)
        }
    }

    fun muteAll() {
        adm.setSpeakerMute(true)
    }

    fun unMuteAll() {
        adm.setSpeakerMute(false)
    }

    fun disposePC(pcController: PeerConnectionController, allowStoreDisposal: Boolean) {
        Timber.d("disposePC: ")
        //only dispose store when all created pcs are disposed
        peerConnectionsControllers.first { it.controller == pcController }
            .let {
                it.dispose()
                peerConnectionsControllers.remove(it)
            }
        val allDisposed =
            peerConnectionsControllers.isEmpty() || peerConnectionsControllers.all { it.controller.disposed }
        if (allDisposed && allowStoreDisposal) dispose()
    }

    private fun dispose() {
        Timber.d("is disposing:")
        if (disposed) return
        adm.release()
        factory.dispose()
        disposed = true
        audioSource.dispose()
        runOnMain {
            audioManager.stop()
        }
        onStoreDisposed?.invoke()

    }

    private fun createPeerConnectionFactory(): PeerConnectionFactory {
        PeerConnectionFactory.initialize(
            PeerConnectionFactory.InitializationOptions.builder(context)
                .setEnableInternalTracer(true)
                .createInitializationOptions()
        )
        return PeerConnectionFactory.builder()
            .setOptions(PeerConnectionFactory.Options())
            .setAudioDeviceModule(adm)
            .createPeerConnectionFactory()
        //.also { enableLogging() }

    }

    private fun enableLogging() {
        // Set default WebRTC tracing and INFO libjingle Timberging.
        // NOTE: this _must_ happen while |factory| is alive!
        Logging.enableLogToDebugOutput(Logging.Severity.LS_ERROR)
    }

    private data class ControllerWithDisposer(
        val controller: PeerConnectionController,
        val dispose: () -> Unit
    )

    private fun createAudioConstrains() = MediaConstraints().apply {
        mandatory.add(MediaConstraints.KeyValuePair(AUDIO_ECHO_CANCELLATION_CONSTRAINT, "true"))
        mandatory.add(MediaConstraints.KeyValuePair(AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT, "false"))
        mandatory.add(MediaConstraints.KeyValuePair(AUDIO_HIGH_PASS_FILTER_CONSTRAINT, "false"))
        mandatory.add(MediaConstraints.KeyValuePair(AUDIO_NOISE_SUPPRESSION_CONSTRAINT, "true"))
    }

    private val audioManagerEvents = object : AppRTCAudioManager.AudioManagerEvents {
        override fun onAudioDeviceChanged(
            selectedAudioDevice: AppRTCAudioManager.AudioDevice?,
            availableAudioDevices: Set<AppRTCAudioManager.AudioDevice>?
        ) {
            Timber.d(
                "%s1 ,  %s2  ",
                "onAudioManagerDevicesChanged: $availableAudioDevices",
                "selected: $selectedAudioDevice"
            )
        }
    }

    companion object {
        private const val AUDIO_ECHO_CANCELLATION_CONSTRAINT = "googEchoCancellation"
        private const val AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT = "googAutoGainControl"
        private const val AUDIO_HIGH_PASS_FILTER_CONSTRAINT = "googHighpassFilter"
        private const val AUDIO_NOISE_SUPPRESSION_CONSTRAINT = "googNoiseSuppression"
        private const val AUDIO_TRACK_ID = "ARDAMSa0"
    }
}