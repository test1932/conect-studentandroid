package com.nagwa.sessionlibrary.session

import io.reactivex.Completable

interface NagwaSwitchableCallProvider {
    fun initCall(callParams: SwitchableCallParams? = null) {}
    fun startListening(): Completable = Completable.complete()
    fun stopListening(): Completable = Completable.complete()
    fun requestTalking(): Completable = Completable.complete()
    fun startTalking(): Completable
    fun stopTalking(): Completable
    fun endCall(): Completable
}

data class SwitchableCallParams(
    val streamID: String,
    val authToken: String,
    val accountId: String,
    val holdMicOnNoPublisher: Boolean
)