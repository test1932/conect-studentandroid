package com.nagwa.sessionlibrary.message.data.source

import android.content.Context
import com.nagwa.sessionlibrary.model.SessionConstants
import io.agora.rtm.RtmClient
import io.agora.rtm.RtmClientListener
import io.agora.rtm.RtmMessage
import io.reactivex.Single
import timber.log.Timber

class ChatManager(private val mContext: Context) {


    private var mListener: RtmClientListener? = null

    fun init(listener: RtmClientListener?): Single<RtmClient> = Single.fromCallable {
        mListener = listener
        try {
            RtmClient.createInstance(
                mContext,
                SessionConstants.getAgoraAppId(mContext),
                object : RtmClientListener {
                    override fun onConnectionStateChanged(state: Int, reason: Int) {
                        mListener?.onConnectionStateChanged(state, reason)
                        Timber.d("ChatRoom   ChatManager onConnectionStateChanged    $state   - $reason")
                    }

                    override fun onMessageReceived(
                        rtmMessage: RtmMessage,
                        peerId: String
                    ) {
                        mListener?.onMessageReceived(rtmMessage, peerId)
                        Timber.d("ChatRoom  ChatManager  onMessageReceived    $rtmMessage")
                    }

                    override fun onTokenExpired() {
                        mListener?.onTokenExpired()
                        Timber.d("ChatRoom  ChatManager  onTokenExpired  ")
                    }

                    override fun onPeersOnlineStatusChanged(p0: MutableMap<String, Int>?) {
                        mListener?.onPeersOnlineStatusChanged(p0)
                        Timber.d("ChatRoom  ChatManager  onPeersOnlineStatusChanged      $p0 ")
                    }
                })
            // if (BuildConfig.DEBUG) {
            //   rtmClient?.setParameters("{\"rtm.Timber_filter\": 65535}")
            // }
        } catch (e: Exception) {
            Timber.e(e)
            throw RuntimeException("NEED TO check rtm sdk init fatal error\n$e")
        }
    }

    fun unregisterListener() {
        mListener = null
    }

}