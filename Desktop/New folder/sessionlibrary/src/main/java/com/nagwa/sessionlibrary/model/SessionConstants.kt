package com.nagwa.sessionlibrary.model

import android.content.Context
import com.nagwa.sessionlibrary.BuildConfig
import com.nagwa.sessionlibrary.R


/**
 * Created by Youssef Ebrahim Elerian on 7/8/20.
 * youssef.elerian@gmail.com
 */
object SessionConstants {

    fun getAgoraAppId(context: Context): String {
        return if (BuildConfig.BUILD_TYPE == "release")
            context.getString(R.string.agora_app_id_live)
        else context.getString(R.string.agora_app_id_development)
    }
}