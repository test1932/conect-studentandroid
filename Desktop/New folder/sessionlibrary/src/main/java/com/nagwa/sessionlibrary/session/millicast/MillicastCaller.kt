package com.nagwa.sessionlibrary.session.millicast

import com.google.gson.Gson
import com.nagwa.sessionlibrary.session.SessionNotStarted
import com.nagwa.sessionlibrary.session.millicast.model.MillicastCallParams
import com.nagwa.sessionlibrary.session.millicast.model.Payload
import com.nagwa.sessionlibrary.session.millicast.model.PayloadData
import com.nagwa.sessionlibrary.session.millicast.network.WebSocketClientWrapper
import com.nagwa.sessionlibrary.session.millicast.rtc.MillicastSdpObserver
import com.nagwa.sessionlibrary.session.millicast.rtc.PeerConnectionController
import com.nagwa.sessionlibrary.session.millicast.rtc.PeerConnectionStore
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.json.JSONObject
import org.webrtc.MediaConstraints
import org.webrtc.PeerConnection
import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by Omar El Hefny on 7/22/20.
 * o.hefny@gmail.com
 */
class MillicastCaller constructor(
    private val peerConnectionStore: PeerConnectionStore,
    private val webRTCScheduler: Scheduler
) {
    enum class PublisherState {
        UNKNOWN,
        InActive,
        Active,
        Stopped
    }

    var onDisconnectListener: (() -> Unit)? = null
    var onConnectionFailedToEstablish: (() -> Unit)? = null
    private var publisherStateStream = BehaviorSubject.create<PublisherState>()
    private var publisherState: PublisherState = PublisherState.UNKNOWN
        set(value) {
            field = value
            Timber.d("publisherState= $value ${tag()}")
            publisherStateStream.onNext(field)
        }

    private var ended: Boolean = false
    private lateinit var mCallParams: Params
    private var pcController: PeerConnectionController? = null
    private var startCallEmitter: CompletableEmitter? = null
    private var connectCallEmitter: CompletableEmitter? = null
    private var webSocketEcho: WebSocketClientWrapper? = null
    private var createOffer = false
    private var sdpObserver: SdpObserver = object : MillicastSdpObserver() {

        override fun onCreateSuccess(sessionDescription: SessionDescription) {
            onSDPCreatedSuccessfully(sessionDescription, this)
        }

        override fun onCreateFailure(s: String) {
            super.onCreateFailure(s)
            onEstablishingConnectionFailed("onSdpCreationFailure with error=$s", true)
        }

        override fun onSetFailure(s: String) {
            super.onSetFailure(s)
            onEstablishingConnectionFailed("onSdpSettingFailure with error=$s", true)
        }
    }

    fun startCall(callParams: Params) =
        initateCall(callParams).andThen(Completable.defer { connectCall() })

    fun initateCall(callParams: Params) = Completable.create { emitter ->
        completableFrom(webRTCScheduler) {
            try {
                startCallEmitter = emitter
                mCallParams = callParams
                connectToWebSocket(mCallParams.wsUrl)
            } catch (error: Throwable) {
                emitter.tryOnError(error)
            }
        }.subscribe()
    }.observeOn(Schedulers.io())

    fun connectCall(): Completable {
        return if (!isSocketConnected())
            Completable.error(IllegalStateException("Socket is Not Connected"))
        else if (isPeerConnected()) Completable.complete()
        //Completable.error(IllegalStateException("PeerConnection is already Connected"))
        else {
            Completable.create { emitter ->
                connectCallEmitter = emitter
                createOffer()
            }
        }
    }

    fun isSocketConnected(): Boolean = webSocketEcho?.isConnected() ?: false

    fun isPeerConnected(): Boolean =
        pcController?.connection?.iceConnectionState() == PeerConnection.IceConnectionState.CONNECTED
    //pcController?.connection?.connectionState() == PeerConnection.PeerConnectionState.CONNECTED

    fun muteMic() = completableFrom(scheduler = webRTCScheduler) {
        Timber.d("muteCall: on Thread ${Thread.currentThread()} ${tag()}")
        pcController?.muteMic()
    }


    fun unMuteMic() = completableFrom(scheduler = webRTCScheduler) {
        Timber.d("unMuteCall: on Thread ${Thread.currentThread()} ${tag()}")
        pcController?.unMuteMic()
    }

    fun muteSpeaker() = completableFrom(scheduler = webRTCScheduler) {
        pcController?.muteSpeaker()
    }

    fun unMuteSpeaker() = completableFrom(scheduler = webRTCScheduler) {
        pcController?.unMuteSpeaker()
    }

    fun getPublisherStateUpdates(): Flowable<PublisherState> =
        publisherStateStream.toFlowable(BackpressureStrategy.LATEST)

    fun endCall() {
        Timber.d("endCall: ${Thread.currentThread()} ${tag()}")
        disconnect(true)
    }

    private fun disconnect(allowStoreDisposal: Boolean) =
        completableFrom(scheduler = webRTCScheduler) {
            if (!ended) {
                ended = true
                publisherState = PublisherState.UNKNOWN
                Timber.d("disconnect() called on Thread ${Thread.currentThread()} ${tag()}")
                pcController?.let { peerConnectionStore.disposePC(it, allowStoreDisposal) }
                pcController = null
                webSocketEcho?.stop()
                webSocketEcho = null
            }
        }.subscribe()

    private fun connectToWebSocket(wsFullUrl: String) {
        webSocketEcho = WebSocketClientWrapper(
            onOpened = { onSocketOpened() },
            onClosed = { code, reason ->
                Timber.d("onClosed() called with: code = [$code], reason = [$reason] ${tag()}")
                //onEstablishingConnectionFailed("Socket:onClosed", false)
            },
            onFailure = {
                onEstablishingConnectionFailed("Socket:onFailure", false)
            },
            onMessageReceived = ::onMessageReceived
        )
        webSocketEcho?.connect(url = wsFullUrl)
    }

    private fun initializePeerConnection() {
        val servers = mCallParams.servers.mapToICEServers()
        pcController = peerConnectionStore.createPeerConnection(
            getRTCConfig(servers),
            mCallParams.isBroadcaster
        )
        pcController?.setConnectionStatusListener(::onIceConnectionChanged)
        onStartCallCompleted()
    }

    private fun onStartCallCompleted() {
        startCallEmitter?.onComplete()
        startCallEmitter = null

    }

    private fun onConnectCallCompleted() {
        Timber.d("onConnectCallCompleted() called with emitter=$connectCallEmitter ${tag()}")
        connectCallEmitter?.onComplete()
        connectCallEmitter = null
    }

    private fun onIceConnectionChanged(iceConnectionState: PeerConnection.IceConnectionState) {
        when (iceConnectionState) {
            PeerConnection.IceConnectionState.CONNECTED -> onConnectCallCompleted()
            PeerConnection.IceConnectionState.DISCONNECTED, PeerConnection.IceConnectionState.FAILED -> {
                onDisconnectListener?.invoke()
                disconnect(false)
            }
            else -> {
                Timber.d("onIceConnectionChanged: $iceConnectionState ${tag()}")
            }
        }
    }

    private fun getRTCConfig(iceServers: List<PeerConnection.IceServer>): PeerConnection.RTCConfiguration {
        val rtcConfig = PeerConnection.RTCConfiguration(iceServers)
        // TCP candidates are only useful when connecting to a server that supports
        // ICE-TCP.
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy =
            PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        // Enable DTLS for normal calls and disable for loopback calls.
        rtcConfig.sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN
        return rtcConfig
    }


    private fun onSocketOpened() {
        Timber.d("onSocketOpened: ${tag()}")
        initializePeerConnection()
        //createOffer()
    }

    private fun onEstablishingConnectionFailed(reason: String, isWebRTCFailure: Boolean) {
        Timber.d("onEstablishingConnectionFailed() called with: reason = [$reason], isWebRTCFailure = [$isWebRTCFailure] alreadyEnded = [$ended] ${tag()}")
        if (ended) return
        startCallEmitter?.tryOnError(SessionNotStarted())
        connectCallEmitter?.tryOnError(SessionNotStarted())
        startCallEmitter = null
        connectCallEmitter = null
        if (!isPeerConnected()) {
            disconnect(true)
            onConnectionFailedToEstablish?.invoke()
        }
    }


    private fun onMessageReceived(msg: String) {
        val jsonObject = JSONObject(msg)
        if (isOfferResponse(jsonObject))
            setRemoteDescription(jsonObject)
        else if (isMillicastEvent(jsonObject))
            emitMillicastEvents(jsonObject)
    }

    private fun isMillicastEvent(jsonObject: JSONObject) =
        jsonObject.optString("type", "") == "event"

    private fun emitMillicastEvents(jsonObject: JSONObject) {
        when (jsonObject.optString("name", "")) {
            "active" -> publisherState = PublisherState.Active
            "inactive" -> publisherState = PublisherState.InActive
            "stopped" -> publisherState = PublisherState.Stopped
            else -> {
            }
        }
    }

    private fun isOfferResponse(jsonObject: JSONObject) =
        jsonObject.optString("type", "") == "response"

    private fun setRemoteDescription(jsonObject: JSONObject) = completableFrom(webRTCScheduler) {
        val sdp = jsonObject.getJSONObject("data")
            .getString("sdp")
        Timber.d("Setting Remote SDP ${tag()}")
        pcController?.connection?.setRemoteDescription(
            sdpObserver,
            SessionDescription(SessionDescription.Type.ANSWER, sdp)
        ) ?: onEstablishingConnectionFailed("peerconnection is null", true)
    }.subscribe()


    private fun onSDPCreatedSuccessfully(
        sessionDescription: SessionDescription,
        millicastSdpObserver: MillicastSdpObserver
    ) = completableFrom(webRTCScheduler) {
        Timber.d("onCreateSuccess: $sessionDescription ${tag()}")
        if (sessionDescription.type == SessionDescription.Type.OFFER && createOffer) {
            pcController?.connection?.setLocalDescription(
                millicastSdpObserver,
                sessionDescription
            )?.let {
                sendOffer(
                    offerSDP = sessionDescription.description,
                    isBroadcaster = mCallParams.isBroadcaster,
                    streamID = mCallParams.channelID
                )
            } ?: onEstablishingConnectionFailed("peerconnection is null", true)
            createOffer = false
        }


    }.subscribe({}, { Timber.e(it) })

    private fun createOffer() {
        Timber.d("createOffer() called ${tag()}")
        createOffer = true
        pcController?.connection?.createOffer(
            sdpObserver,
            createSDPConstrains(mCallParams!!.isBroadcaster)
        ) ?: onEstablishingConnectionFailed("peerconnection is null", true)
    }

    private fun sendOffer(offerSDP: String, isBroadcaster: Boolean, streamID: String) {
        val payload = createPayload(streamID, offerSDP, isBroadcaster)
        webSocketEcho?.sendMessage(Gson().toJson(payload))
    }

    private fun createPayload(
        streamID: String,
        offerSDP: String,
        isBroadcaster: Boolean
    ): Payload {
        val data =
            PayloadData(name = streamID, sdp = offerSDP, codec = pcController!!.getPublishCodec())
        return Payload.create(data = data, isBroadcaster = isBroadcaster)
    }

    private fun List<MillicastCallParams.MillicastIceServer>.mapToICEServers(): List<PeerConnection.IceServer> =
        map {
            PeerConnection.IceServer.builder(listOf(it.url))
                .setUsername(it.username ?: "")
                .setPassword(it.credential ?: "")
                .createIceServer()
        }

    private fun createSDPConstrains(publisher: Boolean) = MediaConstraints().apply {
        mandatory.add(
            MediaConstraints.KeyValuePair(
                "offerToReceiveAudio", if (publisher) "false" else "true"
            )
        )
        mandatory.add(MediaConstraints.KeyValuePair("offerToReceiveVideo", "false"))
    }

    private fun tag() =
        if (::mCallParams.isInitialized) "  --[channel=${mCallParams.channelID},broadcaster=${mCallParams.isBroadcaster}]" else "Not Initialized"

    data class Params(
        val isBroadcaster: Boolean,
        val channelID: String,
        val wsUrl: String,
        val servers: List<MillicastCallParams.MillicastIceServer>,
    )

    companion object {
        class Factory @Inject constructor(
            private val peerConnectionStore: PeerConnectionStore, @Named("WebRTCScheduler")
            private val webRTCScheduler: Scheduler
        ) {
            fun create(): MillicastCaller =
                MillicastCaller(peerConnectionStore, webRTCScheduler)
        }

        private const val SDP_MID = "sdpMid"
        private const val SDP_M_LINE_INDEX = "sdpMLineIndex"
        private const val SDP = "sdp"


        private const val TAG = "MillicastSession"
    }
}

fun completableFrom(scheduler: Scheduler, action: () -> Unit): Completable {
    return Completable.fromAction { action() }.subscribeOn(scheduler)
} //to grantee that the action will be executed in the required thread

fun <T> singleFrom(scheduler: Scheduler, callable: () -> T): Single<T> {
    return Single.fromCallable { callable() }.subscribeOn(scheduler)
}

fun <T> flowableFrom(scheduler: Scheduler, callable: () -> T): Flowable<T> {
    return Flowable.fromCallable { callable() }.subscribeOn(scheduler)
}
