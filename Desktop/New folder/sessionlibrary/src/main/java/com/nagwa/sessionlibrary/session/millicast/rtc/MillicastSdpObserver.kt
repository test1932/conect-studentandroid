package com.nagwa.sessionlibrary.session.millicast.rtc

import org.webrtc.SdpObserver
import org.webrtc.SessionDescription
import timber.log.Timber

abstract class MillicastSdpObserver() : SdpObserver {
    abstract override fun onCreateSuccess(sessionDescription: SessionDescription)
    override fun onSetSuccess() {
        Timber.d("Done Setting SDP")
    }

    override fun onCreateFailure(s: String) {}
    override fun onSetFailure(s: String) {
        Timber.d("Failed to set SDP $s")
    }
}