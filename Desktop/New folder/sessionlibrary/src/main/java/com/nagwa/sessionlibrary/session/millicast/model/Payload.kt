package com.nagwa.sessionlibrary.session.millicast.model

import kotlin.random.Random


/**
 * Created by Youssef Ebrahim Elerian on 7/22/20.
 * youssef.elerian@gmail.com
 */

data class PayloadData(
    val name: String,
    val sdp: String,
    val codec: String
)

data class Payload(
    val type: String = "cmd",
    val transId: Int,
    val name: String,
    val data: PayloadData
) {
    companion object {
        private const val PUBLISH = "publish"
        private const val VIEW = "view"
        fun create(data: PayloadData, isBroadcaster: Boolean): Payload {
            return if (isBroadcaster) createPublishPayload(data)
            else createViewerPayload(data)
        }

        private fun createPublishPayload(data: PayloadData): Payload {
            return Payload(data = data, name = PUBLISH, transId = Random.nextInt() * 10000)
        }

        private fun createViewerPayload(data: PayloadData): Payload {
            return Payload(data = data, name = VIEW, transId = 0)
        }
    }
}