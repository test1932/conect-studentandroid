package com.nagwa.sessionlibrary.session.millicast.rtc

import org.webrtc.*
import timber.log.Timber

class MillicastPeerConnectionObserver constructor(
    var onConnectionStateChangeListener: ((PeerConnection.IceConnectionState) -> Unit)? = null,
    private val onIceCandidateAvailable: ((IceCandidate) -> Unit)? = null
) :
    PeerConnection.Observer {
    override fun onSignalingChange(signalingState: PeerConnection.SignalingState) {
        Timber.d("onSignalingChange:$signalingState")
    }

    override fun onIceCandidatesRemoved(p0: Array<out IceCandidate>?) {
        Timber.d("onIceCandidatesRemoved() candidate = [$p0]")
    }

    override fun onIceConnectionChange(iceConnectionState: PeerConnection.IceConnectionState) {
        Timber.d("onIceConnectionChange:$iceConnectionState")
        onConnectionStateChangeListener?.invoke(iceConnectionState)
    }

    override fun onIceConnectionReceivingChange(b: Boolean) {}
    override fun onIceGatheringChange(iceGatheringState: PeerConnection.IceGatheringState) {}
    override fun onIceCandidate(iceCandidate: IceCandidate) {
        onIceCandidateAvailable?.invoke(iceCandidate)
    }

    override fun onAddStream(mediaStream: MediaStream) {
        Timber.d("onAddStream: ${mediaStream.audioTracks}")
    }

    override fun onRemoveStream(mediaStream: MediaStream) {}
    override fun onDataChannel(dataChannel: DataChannel) {}
    override fun onRenegotiationNeeded() {}
    override fun onAddTrack(p0: RtpReceiver?, p1: Array<out MediaStream>?) {
        Timber.d("onAddTrack() receiver=[$p0],mediaA=[$p1]")
    }
}