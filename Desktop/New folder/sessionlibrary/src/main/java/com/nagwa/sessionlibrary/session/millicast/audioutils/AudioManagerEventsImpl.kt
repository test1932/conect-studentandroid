package com.nagwa.sessionlibrary.session.millicast.audioutils

import timber.log.Timber

class AudioManagerEventsImpl() : AppRTCAudioManager.AudioManagerEvents {
    override fun onAudioDeviceChanged(
        selectedAudioDevice: AppRTCAudioManager.AudioDevice?,
        availableAudioDevices: Set<AppRTCAudioManager.AudioDevice>?
    ) {
        // This method will be called each time the number of available audio
        // devices has changed.
        Timber.d(
            "%s1 ,  %s2  ",
            "onAudioManagerDevicesChanged: $availableAudioDevices",
            "selected: $selectedAudioDevice"
        )
    }
}