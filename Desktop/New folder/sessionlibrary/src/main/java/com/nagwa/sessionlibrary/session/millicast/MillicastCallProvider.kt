package com.nagwa.sessionlibrary.session.millicast

import android.annotation.SuppressLint
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.SessionCallParams
import com.nagwa.sessionlibrary.session.millicast.model.MillicastCallParams
import com.nagwa.sessionlibrary.session.millicast.model.StreamData
import com.nagwa.sessionlibrary.session.millicast.network.MillicastService
import com.nagwa.sessionlibrary.session.millicast.util.NetworkMonitoring
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

typealias AutoRejoinRetryPredicate = ((Flowable<Throwable>) -> Flowable<Unit>)

val NoRetryPredicate: AutoRejoinRetryPredicate =
    { errStream -> errStream.flatMap { Flowable.error<Unit>(it) } }

class MillicastCallProvider constructor(
    private val factory: MillicastCaller.Companion.Factory,
    private val service: MillicastService,
    private val networkMonitoring: NetworkMonitoring
) :
    NagwaCallProvider {
    val compositeDisposable = CompositeDisposable()
    var isCallMuted = false
    var mCall: MillicastCaller? = null
    var rejoinRetryPredicate: AutoRejoinRetryPredicate =
        NoRetryPredicate //the default is just to rethrow the same error to disable retry
    private lateinit var mSessionParams: MillicastCallParams
    private lateinit var mCallParams: MillicastCaller.Params
    private val stateEmitter = BehaviorSubject.create<CallStatus>()
    private val callEndedError = IllegalAccessError("call already ended")
    private val callerNotAvailableError =
        IllegalStateException("can't make any call action while  mCall is null please wait call to rejoin")

    var callState: CallStatus = CallStatus.NotStarted
        private set(value) {
            Timber.d("callState:$value ${tag()}")
            field = value
            if (!stateEmitter.hasComplete())
                stateEmitter.onNext(value)
            else Timber.w("can't emit call state: the eventsStream is completed ${tag()}")
        }

    init {
        createNewCall()
    }

    //one step for starting the call immediately
    override fun startCall(callParams: SessionCallParams): Flowable<CallStatus> {
        if (callParams !is MillicastCallParams)
            return Flowable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        if (callState > CallStatus.NotStarted)
            return Flowable.error(IllegalAccessError("another call is already started"))
        Timber.d("startCall() called with: callParams = [$callParams] ${tag()}")
        mSessionParams = callParams
        mCallParams = callParams.toParams()
        startCallInternally()
        return stateEmitter.toFlowable(BackpressureStrategy.LATEST)

    }

    //two steps for starting call initiateCall and connectCall or using one step startCall
    //you must either use (initiateCall then connectCall) or ( startCall) don't use both for the same call
    fun initiateCall(callParams: MillicastCallParams): Completable {
        if (mCall == null) error("can't initiate call while mCall is null")
        mSessionParams = callParams
        mCallParams = callParams.toParams()
        return mCall!!.initateCall(mCallParams)
            .doOnSubscribe { callState = CallStatus.Starting }
            .doOnComplete { callState = CallStatus.Ready }
            .doOnError { callState = CallStatus.NotStarted }

    }

    fun connectCall(): Completable {
        return if (callState != CallStatus.Ready) Completable.error(IllegalStateException("can't connect call while not ready"))
        else mCall!!.connectCall()
            .doOnComplete { callState = CallStatus.Connected }
            .doOnError { stateEmitter.onError(it) }
    }

    private fun startCallInternally() {
        if (mCall == null) error("can't start call while mCall is null")
        mCall!!.startCall(mCallParams)
            .doOnSubscribe { callState = CallStatus.Starting }
            .subscribeOn(Schedulers.io())
            .subscribe({
                callState = CallStatus.Connected
            }, {
                stateEmitter.onError(it)
            }).addTo(compositeDisposable)
    }

    override fun startStatelessCall(callParams: SessionCallParams): Completable {
        if (callState > CallStatus.NotStarted)
            return Completable.error(IllegalAccessError("another call is already started"))
        if (callParams !is MillicastCallParams)
            return Completable.error(IllegalArgumentException("make sure callParams instance of MillicastCallParams"))
        mSessionParams = callParams
        mCallParams = callParams.toParams()
        createNewCall()
        return mCall!!.startCall(callParams.toParams())
    }

    override fun endCall(): Completable = Completable.fromAction {
        if (!assertCallNotEnded()) error(callEndedError)
        if (mCall == null) error("can't end call while mCall is null")
        else mCall!!.endCall()
    }.doOnComplete {
        Timber.d("endCall: completed ${tag()}")
        callState = CallStatus.Ended
        dispose()
    }

    override fun muteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        if (!::mCallParams.isInitialized) return Completable.error(IllegalStateException("mCallParams not initialized"))
        return Completable.defer {
            if (mCallParams.isBroadcaster)
                muteMic().doOnError { Timber.e(it, "muteMic: ${tag()}") }
            else muteSpeaker().doOnError { Timber.e(it, "muteSpeaker: ${tag()}") }
        }
    }

    override fun unMuteCall(): Completable {
        if (!assertCallNotEnded()) return Completable.error(callEndedError)
        if (!::mCallParams.isInitialized) return Completable.error(IllegalStateException("mCallParams not initialized"))
        return if (mCallParams.isBroadcaster)
            unMuteMic().doOnError { Timber.e(it, "unMuteMic: ${tag()}") }
        else unMuteSpeaker().doOnError { Timber.e(it, "unMuteSpeaker: ${tag()}") }
    }

    fun rejoinCall(): Completable {
        return Completable.fromAction {
            mCall?.endCall()
            mCall = null
        }.andThen(internalRejoinCall())
            .doOnComplete { callState = CallStatus.Connected }
            .doOnError { callState = CallStatus.FailedToReConnect }
    }

    fun stateStream(): Flowable<CallStatus> {
        return stateEmitter.toFlowable(BackpressureStrategy.LATEST)
    }

    fun isConnected() = mCall?.isSocketConnected() ?: false


    private fun muteSpeaker(): Completable {
        return mCall?.run { muteSpeaker().doOnComplete { isCallMuted = true } }
            ?: Completable.error(callerNotAvailableError)
    }

    private fun unMuteSpeaker(): Completable {
        return mCall?.run { unMuteSpeaker().doOnComplete { isCallMuted = false } }
            ?: Completable.error(callerNotAvailableError)
    }

    private fun unMuteMic() = (mCall?.run { unMuteMic().doOnComplete { isCallMuted = false } }
        ?: Completable.error(callerNotAvailableError))

    private fun muteMic(): Completable {
        return mCall?.run { muteMic().doOnComplete { isCallMuted = true } }
            ?: Completable.error(callerNotAvailableError)
    }

    private fun dispose() {
        compositeDisposable.clear()
        stateEmitter.onComplete()
    }

    @SuppressLint("CheckResult")
    private fun onCallFailedToEstablish() {
        Timber.d("onCallFailedToEstablish() called ${tag()}")
        endCall().subscribeOn(Schedulers.io())
            .subscribe({}, Timber::e)
    }

    //invocation occurs in the main thread
    @SuppressLint("CheckResult")
    private fun onCallDisconnected() {
        callState = CallStatus.Disconnected
        val callAction = if (mSessionParams.autoRejoin)
            internalRejoinCall().doOnComplete { Timber.d("internalRejoinCall: completed ${tag()}") }
        else endCall()
        callAction.subscribeOn(Schedulers.io())
            .subscribe({}, Timber::e)
    }

    private fun createNewCall() {
        mCall = factory.create()
        mCall!!.onDisconnectListener = ::onCallDisconnected
        mCall!!.onConnectionFailedToEstablish = ::onCallFailedToEstablish
    }

    private fun internalRejoinCall(): Completable {
        Timber.d("internalRejoinCall() called ${tag()}")
        return networkMonitoring.onInternetReachability()
            .filter { it }
            .distinctUntilChanged()
            .flatMapCompletable {
                getStreamData().toFlowable()
                    .map { it.toParams() }
                    .flatMapCompletable { params ->
                        createNewCall()
                        mCall!!.startCall(params)
                    }
                    .retryWhen(rejoinRetryPredicate)
                    .retry(3) { it !is MillicastService.SessionNotPublished }
            }
            .subscribeOn(Schedulers.io())
            .andThen(Completable.defer {
                if (mSessionParams.allowMuteStateRecovery && isCallMuted) muteCall()
                else Completable.complete()
            })
            .doOnComplete {
                callState = CallStatus.Connected
            }
            .doOnError {
                callState = CallStatus.FailedToReConnect
            }
    }

    private fun getStreamData(): Single<StreamData> {
        return if (mSessionParams.isBroadcaster)
            service.publishStream(
                streamID = mSessionParams.channelID,
                publisherToken = mSessionParams.publisherToken
            )
        else
            service.subscribeToStream(
                accountID = mSessionParams.accountId,
                streamID = mSessionParams.channelID
            )
    }

    private fun tag() =
        if (::mCallParams.isInitialized) "  --[channel=${mCallParams.channelID},broadcaster=${mCallParams.isBroadcaster}]" else "Not Initialized"

    private fun assertCallNotEnded() = callState != CallStatus.Ended


    private fun StreamData.toParams(): MillicastCaller.Params =
        mCallParams.copy(wsUrl = getFullURL())

    private fun MillicastCallParams.toParams(): MillicastCaller.Params = MillicastCaller.Params(
        isBroadcaster = isBroadcaster,
        channelID = channelID,
        wsUrl = wsUrl,
        servers = servers
    )

    companion object {
        class Factory @Inject constructor(
            private val factory: MillicastCaller.Companion.Factory,
            private val service: MillicastService,
            private val networkMonitoring: NetworkMonitoring
        ) {
            fun create(): MillicastCallProvider =
                MillicastCallProvider(factory, service, networkMonitoring)
        }

    }
}