package com.nagwa.sessionlibrary.session.millicast.rtc

import android.content.Context
import org.webrtc.audio.AudioDeviceModule
import org.webrtc.audio.JavaAudioDeviceModule
import timber.log.Timber

fun createJavaAudioDevice(context: Context): AudioDeviceModule {
    val audioTrackErrorCallback: JavaAudioDeviceModule.AudioTrackErrorCallback = object :
        JavaAudioDeviceModule.AudioTrackErrorCallback {
        override fun onWebRtcAudioTrackInitError(errorMessage: String) {
            Timber.e("onWebRtcAudioTrackInitError: $errorMessage")
        }

        override fun onWebRtcAudioTrackStartError(
            errorCode: JavaAudioDeviceModule.AudioTrackStartErrorCode, errorMessage: String
        ) {
            Timber.e("onWebRtcAudioTrackStartError: $errorCode. $errorMessage")
        }

        override fun onWebRtcAudioTrackError(errorMessage: String) {
            Timber.e("onWebRtcAudioTrackError: $errorMessage")
        }
    }
    // Set audio track state callbacks.
    val audioTrackStateCallback: JavaAudioDeviceModule.AudioTrackStateCallback = object :
        JavaAudioDeviceModule.AudioTrackStateCallback {
        override fun onWebRtcAudioTrackStart() {
            Timber.d("Audio playout starts")
        }

        override fun onWebRtcAudioTrackStop() {
            Timber.d("Audio playout stops")
        }
    }
    return JavaAudioDeviceModule.builder(context)
        //.setSamplesReadyCallback(saveRecordedAudioToFile)
        .setUseHardwareAcousticEchoCanceler(false)
        .setUseHardwareNoiseSuppressor(false)
        .setAudioTrackErrorCallback(audioTrackErrorCallback)
        .setAudioTrackStateCallback(audioTrackStateCallback)
        .createAudioDeviceModule()
}