package com.nagwa.sessionlibrary.session.millicast.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import com.nagwa.sessionlibrary.session.millicast.di.RTCSessionScope
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import kotlin.properties.Delegates

@RTCSessionScope
class ConnectivityMonitorImpl @Inject constructor(val context: Context) {
    var registered = AtomicBoolean(false)
    var currentConnection: ConnectionType by Delegates.observable(context.isWifiConnected()
        .toConnectionType(),
        { _, _, new -> connectionTypeStream.onNext(new) }
    )
    val connectionTypeStream = BehaviorSubject.create<ConnectionType>()
    private val networkCallback: NetworkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network) { // network available
            Timber.d("onAvailable:")
            currentConnection = context.isWifiConnected().toConnectionType()
        }

        override fun onLost(network: Network) { // network unavailable
            Timber.d("onLost:")
            currentConnection = ConnectionType.NOT_CONNECTED
            /* Completable.timer(2, TimeUnit.SECONDS).subscribeOn(Schedulers.io())
                 .subscribe {
                     currentConnection = context.isWifiConnected().toConnectionType() //onLost may called before NetworkCapabilities changes
                     Timber.d("onLost after 2 secs: $currentConnection")
                 }*/
        }
    }

    fun updateCurrentConnection() = context.isWifiConnected().toConnectionType().also {
        Timber.d("updateCurrentConnection() called was=[$currentConnection] become=[$it]")
        currentConnection = it
    }

    fun getLastConnectionType() = currentConnection
    fun isOnWifi(): Boolean = currentConnection == ConnectionType.WIFI

    fun getConnectionChanges(): Flowable<ConnectionType> {
        updateCurrentConnection()
        return connectionTypeStream.toFlowable(BackpressureStrategy.LATEST)
            .doOnNext { Timber.d("getConnectionChanges: $it") }
    }
    // .distinctUntilChanged()

    fun initObservation() {
        if (registered.getAndSet(true)) return
        Timber.d("initObservation() called")
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        } else {
            val request = NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build()
            connectivityManager.registerNetworkCallback(request, networkCallback)
        }
    }

    fun releaseObservation() {
        if (!registered.getAndSet(false)) return
        Timber.d("releaseObservation() called")
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        try {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } catch (ex: Throwable) {
            Timber.e("releaseObservation: ${ex.message}")
        }
    }

    private fun Boolean?.toConnectionType(): ConnectionType {
        return when (this) {
            null -> ConnectionType.NOT_CONNECTED
            true -> ConnectionType.WIFI
            false -> ConnectionType.CELLULAR
        }
    }
}


fun Context.isWifiConnected(): Boolean? {
    var result: Boolean? = null
    val connectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connectivityManager.activeNetwork ?: return null
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return null
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> false
            //   actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.run {
                result = when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> false
                    //        ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }
            } ?: return null
        }
    }

    return result
}

enum class ConnectionType { NOT_CONNECTED, CELLULAR, WIFI }
