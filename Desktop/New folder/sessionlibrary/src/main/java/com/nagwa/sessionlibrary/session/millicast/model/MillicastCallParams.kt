package com.nagwa.sessionlibrary.session.millicast.model

import com.nagwa.sessionlibrary.session.SessionCallParams


/**
 * Created by Youssef Ebrahim Elerian on 7/22/20.
 * youssef.elerian@gmail.com
 */
class MillicastCallParams(
    isBroadcaster: Boolean,
    streamID: String,
    val publisherToken: String,
    val accountId: String,
    val wsUrl: String,
    val servers: List<MillicastIceServer>,
    val autoRejoin: Boolean,
    val allowMuteStateRecovery: Boolean //after rejoining restore mute state or not
) : SessionCallParams(isBroadcaster = isBroadcaster, channelID = streamID) {
    data class MillicastIceServer(
        val credential: String?,
        val url: String,
        val username: String?
    )
}