package com.nagwa.sessionlibrary.session.agora.data.model


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */

object SessionConstant {

    object PrefManager {
        const val PREF_PROPERTY_UID = "pOCXx_uid"
    }

    object AppError {
        const val NO_NETWORK_CONNECTION = 3
    }
}