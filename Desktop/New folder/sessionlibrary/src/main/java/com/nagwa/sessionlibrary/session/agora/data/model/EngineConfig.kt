package com.nagwa.sessionlibrary.session.agora.data.model

class EngineConfig internal constructor() {
    var mClientRole = 0
    var mUid = 0
    var mChannel: String? = null
    fun reset() {
        mChannel = null
    }
}