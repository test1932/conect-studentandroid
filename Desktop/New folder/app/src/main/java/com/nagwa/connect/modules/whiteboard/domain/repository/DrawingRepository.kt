package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingParam
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.PointsRequestParam
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface DrawingRepository {
    fun joinDrawingChannel(sessionID: String): Completable
    fun leaveDrawingChannel(): Completable
    fun getDrawingAction(drawingSize: DrawingSize): Flowable<DrawEntity>
    fun requestMissedPoints(pointsRequestParam: PointsRequestParam): Single<Boolean>
    fun getLastClearId(sessionID: String): Single<Int>
    fun observeOnDrawing(sessionID: String, lastId: Int): Flowable<DrawEntity>
    fun getDrawingParams(sessionID: String): Single<DrawingParam>
}