package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import io.reactivex.Completable
import javax.inject.Inject

class SignOutUseCase @Inject constructor(private val userRepository: UserRepository) :
    CompletableUseCase<Unit>() {
    override fun build(params: Unit): Completable = userRepository.signOut()

}
