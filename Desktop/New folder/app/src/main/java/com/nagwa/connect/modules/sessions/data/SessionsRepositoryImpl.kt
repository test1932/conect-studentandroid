package com.nagwa.connect.modules.sessions.data

import com.nagwa.connect.core.domian.attachDomainErrorMapper
import com.nagwa.connect.core.extension.convertToDate
import com.nagwa.connect.modules.sessions.data.model.SessionResponse
import com.nagwa.connect.modules.sessions.data.source.SessionWithProviderModel
import com.nagwa.connect.modules.sessions.data.source.SessionsLocalDS
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.connect.modules.sessions.domain.entity.SessionEntity
import com.nagwa.connect.modules.sessions.domain.interactor.EmptySessions
import com.nagwa.connect.modules.sessions.domain.interactor.SessionNotJoinableError
import com.nagwa.connect.modules.sessions.domain.repository.SessionsRepository
import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionType
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/18/20.
 * youssef.elerian@gmail.com
 */
@SessionsScope
class SessionsRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val remoteDS: SessionsRemoteDS,
    private val localDS: SessionsLocalDS
) :
    SessionsRepository {
    override fun getSessions(): Single<List<SessionEntity>> {
        var timeZone: String? = null
        return userRepository.getUser().map {
            timeZone = it.user.portalInfo?.timeZone
            it.user.userId
        }
            .flatMapSingle { userID -> remoteDS.getSessions(userID, 1000, 1) }
            .flatMap { ls ->
                localDS.saveSessions(ls.sessions.map {
                    SessionWithProviderModel(
                        it,
                        providerName = it.provider
                    )
                })
                    .andThen(Single.just(ls))
            }
            .flattenAsFlowable { it.sessions }
            .map { it.toEntity(timeZone) }
            .toList()
            .attachDomainErrorMapper(mapOf(404 to EmptySessions))

        //return Single.timer(3,TimeUnit.SECONDS).map{ emptyList<SessionEntity>()}
    }

    override fun joinSession(sessionID: String): Completable =
        userRepository.getUser().map { it.user.userId }
            .flatMapSingle { remoteDS.joinSession(it, sessionID) }
            .flatMapCompletable {
                localDS.saveProvider(
                    sessionID,
                    providerName = it.provider,
                    millicastInfo = it.millicastModel
                )
            }
            .attachDomainErrorMapper(mapOf(400 to SessionNotJoinableError))

}

fun SessionResponse.toEntity(timeZone: String?): SessionEntity =
    SessionEntity(
        id = sessionId.toString(),
        title = title,
        teacherName = teacherName,
        subject = subjectName,
        studentsNumber = studentsCount,
        startingDate = startDate.convertToDate(timeZone = timeZone)!!.time,
        duration = durationInMinutes,
        sessionType = if (statusId == 1) SessionType.Scheduled else SessionType.Going,
        teacherId = teacherId.toString(),
        fileID = fileID
    )
