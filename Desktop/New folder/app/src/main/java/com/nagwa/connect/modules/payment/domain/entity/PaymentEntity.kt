package com.nagwa.connect.modules.payment.domain.entity

import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
data class PaymentEntity(val action: PaymentAction)