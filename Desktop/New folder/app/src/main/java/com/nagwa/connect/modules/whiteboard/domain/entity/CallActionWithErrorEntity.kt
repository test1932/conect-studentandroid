package com.nagwa.connect.modules.whiteboard.domain.entity

data class CallActionWithErrorEntity(
    val action: ActionEntity,
    val isSuccess: Boolean
)