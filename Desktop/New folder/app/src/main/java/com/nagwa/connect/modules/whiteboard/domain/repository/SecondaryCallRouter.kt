package com.nagwa.connect.modules.whiteboard.domain.repository

import io.reactivex.Completable

interface SecondaryCallRouter {
    //initiates call with the main channel (listens to educator mainly)
    fun initCall(sessionID: String): Completable
    fun requestMic(): Completable
    fun acquireMic(): Completable
    fun leaveMic(): Completable
    fun unRequestMic(): Completable
    fun listenToMic(): Completable
    fun endCall(): Completable
}