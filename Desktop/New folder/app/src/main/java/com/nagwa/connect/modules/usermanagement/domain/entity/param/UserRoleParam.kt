package com.nagwa.connect.modules.usermanagement.domain.entity.param

data class UserRoleParam constructor(val portalId: String, val userId: String)