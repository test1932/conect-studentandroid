package com.nagwa.connect.core.domian

import com.nagwa.networkmanager.domain.excepation.NAException
import io.reactivex.Completable
import io.reactivex.Single

abstract class DomainException(msg: String, open var originalException: Throwable? = null) :
    RuntimeException(msg)

object NotDeliveredRequest :
    DomainException("Your request is can't be delivered because of IO issue")

class StudentNotFound(msg: String) :
    DomainException(msg)

object UnexpectedResponse : DomainException("Got unexpected response for the request")

fun <T> Single<T>.attachDomainErrorMapper(errorMappings: Map<Int, DomainException> = emptyMap()): Single<T> =
    this.onErrorResumeNext {
        val error = mapToDomainException(it, errorMappings)
        Single.error(error)
    }

fun Completable.attachDomainErrorMapper(errorMappings: Map<Int, DomainException> = emptyMap()): Completable =
    this.onErrorResumeNext {
        val error = mapToDomainException(it, errorMappings)
        error.originalException = it
        Completable.error(error)
    }/*.doOnError {
        Timber.e((it as DomainException).originalException, "mapToDomainError: original error ")
    }*/

private fun mapToDomainException(
    it: Throwable,
    errorMappings: Map<Int, DomainException>
): DomainException {
    return if (it is NAException) {
        when (it.kind) {
            NAException.Kind.HTTP -> errorMappings[it.statusCode] ?: UnexpectedResponse
            NAException.Kind.NETWORK -> NotDeliveredRequest
            else -> UnexpectedResponse
        }
    } else UnexpectedResponse
}