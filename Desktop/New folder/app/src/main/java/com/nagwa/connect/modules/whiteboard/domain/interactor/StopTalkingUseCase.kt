package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.domain.repository.SecondaryCallRouter
import io.reactivex.Completable
import timber.log.Timber
import javax.inject.Inject

class StopTalkingUseCase @Inject constructor(private val secondaryCallRouter: SecondaryCallRouter) :
    CompletableUseCase<Unit>() {
    override fun build(params: Unit): Completable {
        return secondaryCallRouter.leaveMic().doOnSubscribe { Timber.d("stopping") }
            .doOnError { Timber.e(it, "stopping failed") }
            .doOnComplete { Timber.d("stopped successfully") }
    }
}