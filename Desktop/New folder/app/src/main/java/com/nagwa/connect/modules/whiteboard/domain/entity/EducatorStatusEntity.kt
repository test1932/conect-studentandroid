package com.nagwa.connect.modules.whiteboard.domain.entity


/**
 * Created by Youssef Ebrahim Elerian on 6/2/20.
 * youssef.elerian@gmail.com
 */
data class EducatorStatusEntity(val id: String, val status: EducatorStatus? = null) {
    val isOnline = status == EducatorStatus.ONLINE
}

enum class EducatorStatus(val value: Int) {
    ONLINE(0),
    UNREACHABLE(1),
    OFFLINE(2),
    UNKNOWN(-1)
}