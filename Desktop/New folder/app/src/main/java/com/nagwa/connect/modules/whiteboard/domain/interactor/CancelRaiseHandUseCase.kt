package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import com.nagwa.connect.modules.whiteboard.domain.repository.SecondaryCallRouter
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class CancelRaiseHandUseCase @Inject constructor(
    private val getCallDataUseCase: GetCallDataUseCase,
    private val callRouter: SecondaryCallRouter,
    private val actionsRepository: ActionsRepository
) :
    CompletableUseCase<String>() {
    override fun build(params: String): Completable {
        return getCallDataUseCase.build(params)
            .flatMapCompletable { callData ->
                callRouter.unRequestMic()
                    .onErrorComplete()
                    .andThen(
                        actionsRepository.sendAction(
                            ActionEntity(
                                CallActionTypes.Mute.value,
                                callData.studentId
                            )
                        ).ignoreElement()
                    )
            }
    }


}