package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Single
import javax.inject.Inject

class IsSessionHasAttachment @Inject constructor(private val repository: AttachmentsRepository) :
    SingleUseCase<String, Boolean>() {
    override fun build(params: String): Single<Boolean> = repository.hasAttachment(params)
}