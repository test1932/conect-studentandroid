package com.nagwa.connect.modules.usermanagement.data.source.local

import android.content.SharedPreferences
import javax.inject.Inject


class UserCache @Inject constructor(private val sharedPreferences: SharedPreferences) {
    fun saveUserData(userData: String) =
        sharedPreferences.edit().putString(PRF_KEY_STUDENT, userData).commit()

    fun clearUserData() = sharedPreferences.edit().remove(PRF_KEY_STUDENT).commit()

    fun getUserData(): String? = sharedPreferences.getString(PRF_KEY_STUDENT, null)

    fun saveResetToken(resetToken: String) =
        sharedPreferences.edit().putString(PRF_KEY_RESET_TOKEN, resetToken).commit()

    fun getResetToken(): String? = sharedPreferences.getString(PRF_KEY_RESET_TOKEN, null)
    fun savePortalID(portalID: String) =
        sharedPreferences.edit().putString(PRF_KEY_PORTAL_ID, portalID).commit()

    fun getPortalID(): String? = sharedPreferences.getString(PRF_KEY_PORTAL_ID, null)

    companion object {
        const val PRF_KEY_STUDENT = "StudentData"
        const val PRF_KEY_RESET_TOKEN = "ResetToken"
        const val PRF_KEY_PORTAL_ID = "PortalID"
    }
}