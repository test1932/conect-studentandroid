package com.nagwa.connect.modules.whiteboard.domain.entity

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType


abstract class DrawEntity {
    abstract val actionType: DrawActionType
    abstract val id: Int
}
