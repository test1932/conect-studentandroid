package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class SendChatMessage @Inject constructor(
    private val messenger: MessengerRepository,
    private val userRepository: UserRepository
) :
    CompletableUseCase<String>() {
    override fun build(params: String): Completable =
        userRepository.getUser()
            .map {
                ChatMessageEntity(
                    studentName = it.user.firstName + " " + it.user.lastName,
                    studentId = it.user.userId,
                    text = params,
                    date = 0
                )
            }
            .flatMapSingle(messenger::sendChatMessage)
            .ignoreElement()
}