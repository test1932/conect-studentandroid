package  com.nagwa.connect.core.extension


import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.nagwa.connect.core.application.GlideApp
import com.nagwa.connect.core.application.GlideRequest


fun ImageView.loadImage(
    @DrawableRes resourceId: Int,
    placeholder: Int? = null,
    trans: MultiTransformation<Bitmap>? = null,
    listener: ((Bitmap?) -> Unit)? = null,
    width: Int = Target.SIZE_ORIGINAL,
    height: Int = Target.SIZE_ORIGINAL
) {
    val glideApp = GlideApp.with(context)
        .asBitmap()
        .load(resourceId)

    loadImageOptions(
        glideApp = glideApp,
        placeholder = placeholder,
        trans = trans,
        listener = listener,
        width = width,
        height = height
    )
}

fun Context.loadBitmap(
    imagePath: String,
    width: Int = Target.SIZE_ORIGINAL,
    height: Int = Target.SIZE_ORIGINAL,
    listener: ((Bitmap?) -> Unit)
) {
    GlideApp.with(this)
        .asBitmap()
        .load(imagePath)
        .override(width, height)
        .listener(object : RequestListener<Bitmap> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Bitmap>?,
                isFirstResource: Boolean
            ): Boolean {
                Handler(mainLooper).post {
                    listener.invoke(null)
                }
                return true
            }

            override fun onResourceReady(
                resource: Bitmap?,
                model: Any?,
                target: Target<Bitmap>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                Handler(mainLooper).post {
                    listener.invoke(resource)
                }
                return true
            }

        }).submit()
}


fun ImageView.loadImage(
    url: String,
    placeholder: Int? = null,
    trans: MultiTransformation<Bitmap>? = null,
    listener: ((Bitmap?) -> Unit)? = null,
    width: Int = Target.SIZE_ORIGINAL,
    height: Int = Target.SIZE_ORIGINAL
) {
    val glideApp = GlideApp.with(context)
        .asBitmap()
        .load(url)

    loadImageOptions(
        glideApp = glideApp,
        placeholder = placeholder,
        trans = trans,
        listener = listener,
        width = width,
        height = height
    )
}

fun ImageView.bitmapTransformation(
    bitmap: Bitmap,
    trans: MultiTransformation<Bitmap>,
    listener: ((Bitmap?) -> Unit)? = null,
    width: Int = Target.SIZE_ORIGINAL,
    height: Int = Target.SIZE_ORIGINAL
) {
    val glideApp = GlideApp.with(context)
        .asBitmap()
        .load(bitmap).apply(RequestOptions.bitmapTransform(trans))

    loadImageOptions(
        glideApp,
        trans = trans,
        listener = listener,
        width = width,
        height = height
    )
}

private fun ImageView.loadImageOptions(
    glideApp: GlideRequest<Bitmap>,
    placeholder: Int? = null,
    errorPlaceholder: Int? = null,
    trans: MultiTransformation<Bitmap>?,
    listener: ((Bitmap?) -> Unit)?,
    width: Int = Target.SIZE_ORIGINAL,
    height: Int = Target.SIZE_ORIGINAL
) {

    placeholder?.let {
        glideApp.placeholder(placeholder)
    }
    errorPlaceholder?.let {
        glideApp.error(errorPlaceholder)
    }
    trans?.let {
        glideApp.apply(RequestOptions.bitmapTransform(trans))
    }

    if (listener == null) {
        glideApp.dontAnimate().override(width, height).into(this)
    } else
        glideApp.dontAnimate().override(width, height).listener(object : RequestListener<Bitmap> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Bitmap>?,
                isFirstResource: Boolean
            ): Boolean {
                listener.invoke(null)
                return true
            }

            override fun onResourceReady(
                resource: Bitmap?,
                model: Any?,
                target: Target<Bitmap>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                listener.invoke(resource)
                return true
            }

        }).submit()
}



