package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

data class PublishStreamResponse(
    @SerializedName("milliCastIceServers") val milliCastIceServers: MilliCastIceServers,
    @SerializedName("publishingToken") val publishingToken: String,
    @SerializedName("url") val url: String,
    @SerializedName("jwt") val jwt: String,
    @SerializedName("accountId") val accountId: String,
    @SerializedName("streamName") val streamName: String,
    @SerializedName("wsurl") val wsurl: String
)

data class MilliCastIceServers(
    @SerializedName("v") val iCEServersWrapperResponse: ICEServersWrapperResponse,
    @SerializedName("s") val status: String
)

data class ICEServersWrapperResponse(
    @SerializedName("iceServers") val iceServers: List<IceServers>
)

data class IceServers(
    @SerializedName("url") val url: String,
    @SerializedName("username") val username: String?,
    @SerializedName("credential") val credential: String?
)