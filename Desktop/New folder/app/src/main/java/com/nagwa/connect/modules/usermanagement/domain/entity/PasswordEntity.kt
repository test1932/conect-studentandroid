package com.nagwa.connect.modules.usermanagement.domain.entity

data class PasswordEntity constructor(
    val portalId: Long = 640126927865,
    val userId: Long = 450182567560,
    val accountStatus: String,
    val needPasswordReset: Boolean
)