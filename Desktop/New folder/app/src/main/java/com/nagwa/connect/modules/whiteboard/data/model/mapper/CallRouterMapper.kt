package com.nagwa.connect.modules.whiteboard.data.model.mapper

import com.nagwa.connect.modules.sessions.data.model.MillicastProviderResponse
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import com.nagwa.sessionlibrary.session.CallStatus
import com.nagwa.sessionlibrary.session.millicast.model.MillicastCallParams


fun MillicastProviderResponse.mapToMCParams(
    autoRejoin: Boolean,
    allowMuteStateRecovery: Boolean
): MillicastCallParams {
    return MillicastCallParams(
        autoRejoin = autoRejoin,
        allowMuteStateRecovery = allowMuteStateRecovery,
        publisherToken = publishingToken,
        accountId = accountId,
        isBroadcaster = false,
        wsUrl = "${url!!}?token=${jwt!!}",
        streamID = teacherStreamName,
        servers = milliCastIceServers!!.iceServersWrapperResponse!!.iceServers!!.map {
            it!!
            MillicastCallParams.MillicastIceServer(
                url = it.url!!,
                username = it.username,
                credential = it.credential
            )
        }
    )
}

fun CallStatus.toEntity(): CallStatusEntity = when (this) {
    CallStatus.NotStarted, CallStatus.Starting, CallStatus.Ready -> CallStatusEntity.Initializing
    CallStatus.Connected -> CallStatusEntity.Connected
    CallStatus.Disconnected -> CallStatusEntity.DisConnected
    CallStatus.FailedToReConnect -> CallStatusEntity.RejoinFailed
    CallStatus.Ended -> CallStatusEntity.Ended
}
