package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.rxdownloadmanger.IDownloadManger
import com.nagwa.rxdownloadmanger.models.DownloadEvent
import io.reactivex.Flowable
import javax.inject.Inject

class ObserveAttachmentDownload @Inject constructor(private val repository: IDownloadManger) :
    FlowableUseCase<String, DownloadEvent>() {
    override fun build(params: String): Flowable<DownloadEvent> {
        return repository.getObservableItem(params)
    }
}