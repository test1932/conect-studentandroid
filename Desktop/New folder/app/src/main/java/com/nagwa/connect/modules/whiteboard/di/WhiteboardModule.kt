package com.nagwa.connect.modules.whiteboard.di

import android.content.Context
import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.whiteboard.data.repository.*
import com.nagwa.connect.modules.whiteboard.domain.repository.*
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ActionsViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.ChatViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.DrawingViewModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.WhiteboardViewModel
import com.nagwa.sessionlibrary.session.NagwaCallProvider
import com.nagwa.sessionlibrary.session.NagwaSwitchableCallProvider
import com.nagwa.sessionlibrary.session.agora.AgoraCallProvider
import com.nagwa.sessionlibrary.session.millicast.MillicastCallProvider
import com.nagwa.sessionlibrary.session.millicast.MillicastSwitchableCallProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
const val AGORA_BINDING_KEY = "AGORA"
const val MILLICAST_BINDING_KEY = "MILLICAST"

@Module
abstract class WhiteboardModule {
    @Binds
    @WhiteboardScope
    internal abstract fun bindWhiteboardRepo(impl: WhiteboardRepositoryImpl): WhiteboardRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindMessengerRepo(impl: MessengerRepositoryImpl): MessengerRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindActionsRepo(impl: ActionsRepositoryImpl): ActionsRepository

    @Binds
    @WhiteboardScope
    internal abstract fun bindFireBaseDrawingRepo(impl: DrawingRepositoryImpl): DrawingRepository
    /*   @Binds
       @WhiteboardScope
       internal abstract fun bindDrawingRepo(impl: DrawingRepositoryImpl): DrawingRepository*/


    @Binds
    @WhiteboardScope
    internal abstract fun bindCallRouter(impl: CallRouterImpl): CallRouter

    @Binds
    @WhiteboardScope
    internal abstract fun bindSecondaryCallRouter(impl: SecondaryCallRouterImpl): SecondaryCallRouter

}

@Module
class ProvidersModule {
    @Provides
    @WhiteboardScope
    fun provideAgoraCallProvider(context: Context): AgoraCallProvider =
        AgoraCallProvider(context)

    @Provides
    @WhiteboardScope
    fun provideMillicastCallProvider(factory: MillicastCallProvider.Companion.Factory): MillicastCallProvider =
        factory.create()

    @Provides
    @WhiteboardScope
    fun provideMillicastSwitchableCallProvider(factory: MillicastSwitchableCallProvider.Companion.Factory): MillicastSwitchableCallProvider =
        factory.create()
}

@Module
abstract class CallModule {
    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(AGORA_BINDING_KEY)
    internal abstract fun bindAgoraCallProvider(impl: AgoraCallProvider): NagwaCallProvider

    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(MILLICAST_BINDING_KEY)
    internal abstract fun bindMillicastProvider(impl: MillicastCallProvider): NagwaCallProvider

    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(AGORA_BINDING_KEY)
    internal abstract fun bindAgoraSwitchableCallProvider(impl: AgoraCallProvider): NagwaSwitchableCallProvider

    @Binds
    @IntoMap
    @WhiteboardScope
    @StringKey(MILLICAST_BINDING_KEY)
    internal abstract fun bindMillicastSwitchableCallProvider(impl: MillicastSwitchableCallProvider): NagwaSwitchableCallProvider
}

@Module
abstract class WhiteboardViewModelModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(WhiteboardViewModel::class)
    @WhiteboardScope
    internal abstract fun provideWhiteboardViewModel(viewModel: WhiteboardViewModel): ViewModel


    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(ChatViewModel::class)
    internal abstract fun bindChatViewModel(viewModel: ChatViewModel): ViewModel

    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(DrawingViewModel::class)
    internal abstract fun bindDrawingViewModel(viewModel: DrawingViewModel): ViewModel


    @Binds
    @IntoMap
    @WhiteboardScope
    @TutoringViewModelKey(ActionsViewModel::class)
    internal abstract fun bindActionsViewModel(viewModel: ActionsViewModel): ViewModel


}
