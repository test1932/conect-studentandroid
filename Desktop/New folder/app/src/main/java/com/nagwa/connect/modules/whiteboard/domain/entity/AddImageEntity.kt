package com.nagwa.connect.modules.whiteboard.domain.entity

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class AddImageEntity(
    override val id: Int,
    override val actionType: DrawActionType = DrawActionType.ADD_IMAGE,
    val imageWidth: Int,
    val imageHeight: Int,
    val xPos: Float,
    val yPos: Float,
    val imageName: String
) : DrawEntity()
