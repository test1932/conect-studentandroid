package com.nagwa.connect.modules.sessions.di

import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.sessions.data.SessionsRepositoryImpl
import com.nagwa.connect.modules.sessions.domain.repository.SessionsRepository
import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */

@Module
abstract class SessionsViewModelModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(SessionsViewModel::class)
    @SessionsScope
    internal abstract fun provideSessionsViewModel(viewModel: SessionsViewModel): ViewModel

}

@Module
abstract class SessionsModule {
    @Binds
    @SessionsScope
    internal abstract fun bindSessionsRepository(impl: SessionsRepositoryImpl): SessionsRepository
}

