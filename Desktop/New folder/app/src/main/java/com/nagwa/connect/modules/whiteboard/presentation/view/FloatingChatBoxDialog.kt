package com.nagwa.connect.modules.whiteboard.presentation.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.BaseDialogFragment
import com.nagwa.connect.core.extension.hideKeyboard
import com.nagwa.connect.core.extension.onClick
import kotlinx.android.synthetic.main.dialog_floating_chatbox.*


class FloatingChatBoxDialog : BaseDialogFragment() {
    var onSendClicked: ((String) -> Unit)? = null
    var onLastTextBeforeDismiss: ((String) -> Unit)? = null
    override fun getLayoutResource(): Int = R.layout.dialog_floating_chatbox
    override fun isFragmentCancelable(): Boolean = true
    override fun getDialogBackground(): Drawable =
        ContextCompat.getDrawable(requireContext(), android.R.color.transparent)!!

    override fun getDialogWidth(): Int = ViewGroup.LayoutParams.MATCH_PARENT
    override fun getDialogHeight(): Int = ViewGroup.LayoutParams.WRAP_CONTENT

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setDimAmount(0.0f);
        btnSendMessage onClick ::sendMessage
        etFloatingMessage.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND && etFloatingMessage.text.toString()
                    .isNotBlank()
            ) {
                sendMessage()
                true
            } else {
                //  etFloatingMessage.text?.clear()
                false
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        onLastTextBeforeDismiss?.invoke(etFloatingMessage?.text.toString())
    }

    override fun onStart() {
        super.onStart()
        etFloatingMessage.requestFocus()
        showKeyboard()
        dismissOnBackClicked()
    }

    private fun dismissOnBackClicked() {
        view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss() //todo pass text to the textbox
                true
            } else false
        })
    }

    override fun onStop() {
        super.onStop()
        closeKeyboard()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
    }

    private fun sendMessage() {
        if (etFloatingMessage.text.toString().isNotBlank()) {
            onSendClicked?.invoke(etFloatingMessage.text.toString())
            etFloatingMessage.text?.clear()
            activity?.hideKeyboard()
            dismiss()
        }
    }

    private fun showKeyboard() {
        val inputMethodManager: InputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun closeKeyboard() {
        val inputMethodManager: InputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    companion object {
        fun newInstance() = FloatingChatBoxDialog()
    }

}