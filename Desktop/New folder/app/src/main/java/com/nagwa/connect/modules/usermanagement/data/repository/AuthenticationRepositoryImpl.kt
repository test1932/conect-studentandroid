package com.nagwa.connect.modules.usermanagement.data.repository

import com.nagwa.connect.core.domian.attachDomainErrorMapper
import com.nagwa.connect.modules.usermanagement.data.model.mapper.toEntity
import com.nagwa.connect.modules.usermanagement.data.source.local.UserLocalDS
import com.nagwa.connect.modules.usermanagement.data.source.remote.UserRemoteDS
import com.nagwa.connect.modules.usermanagement.data.source.remote.UserRoleResponse
import com.nagwa.connect.modules.usermanagement.domain.entity.*
import com.nagwa.connect.modules.usermanagement.domain.entity.param.SignInParam
import com.nagwa.connect.modules.usermanagement.domain.entity.param.UserRoleParam
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import flatMapIf
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class AuthenticationRepositoryImpl @Inject constructor(
    private val userLocalDS: UserLocalDS,
    private val userRemoteDS: UserRemoteDS
) : AuthenticationRepository, UserRepository {

    init {
        Timber.i("UserRepositoryImpl: created with ${hashCode()} ")
    }

    lateinit var email: String
    override fun verifyUser(username: String): Single<VerifyUserEntity> {
        return userRemoteDS.verifyUser(username)
            .map { it.result }
            .map { it.toEntity() }
            .flatMap {
                userLocalDS.savePortalID(it.portalId)
                    .andThen(Single.just(it))
            }
            .flatMapIf({ it.needPasswordReset }) {
                userLocalDS.saveResetToken(it.resetToken!!)
                    .andThen(Single.just(it))
            }.attachDomainErrorMapper(
                mapOf(
                    404 to UserNotFoundError,
                    403 to NotAllowedUserError
                )
            )
    }

    override fun getUserRole(userRoleParam: UserRoleParam): Single<UserRoleEntity> =
        userRemoteDS.getUserRole(userRoleParam)
            .map { it.userRoles }
            .map { UserRoleResponse(it) }
            .map { it.toEntity() }
            .attachDomainErrorMapper(
                mapOf(
                    404 to UserNotFoundError,
                    403 to NotAllowedUserError
                )
            )

    override fun confirmPassword(newPassword: String): Single<String> =
        userLocalDS.getResetToken().flatMap { token ->
            userRemoteDS.confirmPassword(token, newPassword)
        }
            .map { it.result }
            .attachDomainErrorMapper(mapOf(400 to NotWellFormattedPassword))

    override fun signinUser(signInParam: SignInParam): Single<UserWithPortalIdEntity> {
        return userLocalDS.getPortalID().flatMap {
            userRemoteDS.authenticateUser(
                loginName = signInParam.username,
                password = signInParam.password,
                portalID = it
            )
        }
            .map { it.result }
            .map { UserWithPortalIdEntity(it.toEntity(null), it.portalId) }
            .attachDomainErrorMapper(mapOf(400 to InvalidPassword))

    }


    override fun getUser(): Maybe<UserWithPortalIdEntity> = userLocalDS.getUserData()

    override fun saveUser(user: UserWithPortalIdEntity): Completable = userLocalDS.saveUser(user)


    override fun signOut(): Completable = userLocalDS.clearUserData()


}