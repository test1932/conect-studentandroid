package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

data class ChatMessage(
    @SerializedName("n") val userName: String,
    @SerializedName("m") val message: String
)