package com.nagwa.connect.modules.usermanagement.data.model.mapper

import com.nagwa.connect.modules.usermanagement.data.source.remote.PortalResponse
import com.nagwa.connect.modules.usermanagement.data.source.remote.UserInfoResponse
import com.nagwa.connect.modules.usermanagement.data.source.remote.UserRoleResponse
import com.nagwa.connect.modules.usermanagement.data.source.remote.VerifyUserResponse
import com.nagwa.connect.modules.usermanagement.domain.entity.PortalEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.UserEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.UserRoleEntity
import com.nagwa.connect.modules.usermanagement.domain.entity.VerifyUserEntity

fun VerifyUserResponse.toEntity() =
    VerifyUserEntity(portalId, userId, accountStatus, needPasswordReset, resetToken)

fun UserRoleResponse.toEntity() = UserRoleEntity(userRoles)

fun UserInfoResponse.toEntity(portalEntity: PortalEntity?) = UserEntity(
    userId = userId,
    userName = userName,
    firstName = firstName,
    lastName = lastName,
    email = email,
    portalInfo = portalEntity
)

fun PortalResponse.toEntity() = PortalEntity(
    portalId, portalTitle, portalLogoURL, countryCode,
    languageCode, "$languageCode/$countryCode", portalLogoURL, timeZone
)