package com.nagwa.connect.modules.usermanagement.domain.entity

data class UserRoleEntity constructor(val userRoles: List<String>)