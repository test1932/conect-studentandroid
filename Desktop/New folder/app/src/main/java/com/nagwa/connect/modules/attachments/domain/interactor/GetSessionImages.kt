package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Single
import java.io.File
import javax.inject.Inject

class GetSessionImages @Inject constructor(
    private val filesManager: AttachmentFilesManager,
    private val attachmentsRepository: AttachmentsRepository,
    private val isSessionHasAttachment: IsSessionHasAttachment
) :
    SingleUseCase<String, List<File>>() {
    override fun build(params: String): Single<List<File>> =
        isSessionHasAttachment.build(params).flatMap {
            if (it) getImages(params)
            else Single.just(emptyList<File>())
        }

    private fun getImages(sessionID: String) = attachmentsRepository.getImagesDir(sessionID)
        .flatMap { filesManager.listOfImages(it) }
        .flattenAsFlowable { it }
        .toList()

}