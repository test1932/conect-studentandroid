package com.nagwa.connect.modules.whiteboard.presentation.viewmodel

import addTo
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import applyAsyncSchedulers
import com.nagwa.connect.base.presentation.viewmodel.TutoringViewModel
import com.nagwa.connect.core.presentation.view.BitmapProvider
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.connect.modules.attachments.domain.interactor.GetImagePathByImageName
import com.nagwa.connect.modules.attachments.domain.interactor.GetImagePathParam
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.AddImageEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.ClearAllEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.CreateShapeEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.mapper.getScreenPercentage
import com.nagwa.connect.modules.whiteboard.domain.entity.mapper.getSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingParam
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.PointsRequestParam
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetDrawingActionsUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetDrawingParamsUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.ObserveOnDrawingUseCase
import com.nagwa.connect.modules.whiteboard.domain.interactor.RequestMissingPointUseCase
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.AddImageUIModel
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.mapper.toUIModel
import io.reactivex.Flowable
import io.reactivex.Single
import retryToSubscribe
import timber.log.Timber
import javax.inject.Inject

@WhiteboardScope
class DrawingViewModel @Inject constructor(
    private val bitmapProvider: BitmapProvider,
    private val getDrawingActionsUseCase: GetDrawingActionsUseCase,
    private val getImagePathByImageName: GetImagePathByImageName,
    private val requestMissingPointUseCase: RequestMissingPointUseCase,
    private val observeOnDrawingUseCase: ObserveOnDrawingUseCase,
    private val getDrawingParamsUseCase: GetDrawingParamsUseCase
) : TutoringViewModel(), LifecycleObserver {

    val getDrawingActionsLiveData = SingleLiveEvent<CreateShapeEntity>()
    val addImageLiveData = SingleLiveEvent<AddImageUIModel>()
    val clearAllLiveData = SingleLiveEvent<ClearAllEntity>()
    val endSession = SingleLiveEvent<Boolean>()
    val drawingParamsEvent = SingleLiveEvent<DrawingSize>()
    var sessionName: String = ""
    var drawingSize: DrawingSize? = null
    private var lastDrawingID = 0
    private var isActiveState = true
    private val list = mutableListOf<DrawEntity>()

    fun getDrawActions(sessionName: String, drawingSize: DrawingSize) {
        this.sessionName = sessionName
        this.drawingSize = drawingSize
        getDrawingActionsUseCase.build(drawingSize)
            .concatMapEagerDelayError({ draw ->
                if (draw is AddImageEntity) {
                    loadActionImage(draw)
                } else {
                    Flowable.just(draw)
                }
            }, true).retryToSubscribe()
            .applyAsyncSchedulers()
            .subscribe({ action ->
                actions(action)
                lastDrawingID = action.id
            }, {
                Timber.d("getDrawActions   :  $it")
            })
            .addTo(compositeDisposable)

        requestDrawingParams(sessionName, drawingSize)


    }

    private fun requestDrawingParams(sessionName: String, drawingSize: DrawingSize) {
        getDrawingParamsUseCase.build(sessionName)
            .applyAsyncSchedulers()
            .subscribe({
                onDrawingParamSuccess(it, drawingSize)
            }, {
                endSession.value = true
            })
            .addTo(compositeDisposable)

    }

    private fun onDrawingParamSuccess(drawingParam: DrawingParam, drawingSize: DrawingSize) {
        if (drawingParam.width == 0 && drawingParam.height == 0)
            endSession.value = true
        else {
            setDrawingEngineSize(drawingParam, drawingSize)
            requestMissingPoints(
                PointsRequestParam(
                    sessionName, drawingSize = drawingSize,
                    drawing = drawingParam
                )
            )
            requestDrawingObservations(sessionName)
        }
    }

    private fun setDrawingEngineSize(drawingParam: DrawingParam, drawingSize: DrawingSize) {
        val percentage = drawingSize.getScreenPercentage(drawingParam.width, drawingParam.height)
        val width = getSize(drawingParam.width, percentage)
        val height = getSize(drawingParam.height, percentage)

        drawingParamsEvent.value = DrawingSize(width, height)
    }

    private fun requestMissingPoints(pointsRequestParam: PointsRequestParam) {
        requestMissingPointUseCase.build(
            pointsRequestParam.copy(sessionName, lastID = lastDrawingID)
        )
            .applyAsyncSchedulers()
            .subscribe({}, {
                endSession.value = true
            })
            .addTo(compositeDisposable)
    }

    private fun requestDrawingObservations(sessionID: String) {
        observeOnDrawingUseCase.build(sessionID)
            .applyAsyncSchedulers()
            .subscribe({}, {})
            .addTo(compositeDisposable)
    }

    private fun appendToNotObservedActions(action: DrawEntity) {
        if (action is ClearAllEntity) {
            list.clear()
        }
        list.add(action)
    }

    private fun actions(action: DrawEntity) {
        if (isActiveState) {
            when (action) {
                is ClearAllEntity -> clearAllLiveData.value = action
                is CreateShapeEntity -> getDrawingActionsLiveData.value = action
                is AddImageUIModel -> addImageLiveData.value = action
            }
        } else {
            appendToNotObservedActions(action)
        }
    }

    private fun loadActionImage(actionEntity: AddImageEntity): Flowable<AddImageUIModel> {
        return getImagePathByImageName.build(
            GetImagePathParam(sessionID = sessionName, imageName = actionEntity.imageName)
        ).flatMap {
            emitImageAsBitmap(actionEntity, it)
        }.toFlowable()


    }

    private fun emitImageAsBitmap(action: AddImageEntity, path: String) =
        Single.create<AddImageUIModel> { emitter ->
            bitmapProvider.provideBitmapAsync(
                imagePath = path,
                width = action.imageWidth,
                height = action.imageHeight
            ) { bitmap ->
                bitmap?.let { emitter.onSuccess(action.toUIModel(it)) }
                    ?: emitter.onError(IllegalStateException("Bitmap Can't Be Null"))
            }
        }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onLifeCycleStop() {
        isActiveState = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onLifeCycleResume() {
        isActiveState = true
        if (list.isNotEmpty()) {
            list.forEach {
                actions(it)
            }
            list.clear()
        }
    }

    companion object {
        private const val DRAWING_DOCUMENT = "Drawing"
        private const val DRAWING_ACTIONS = "Actions"
        private const val ACTION_ID_Key = "id"
    }


}