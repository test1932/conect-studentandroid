package com.nagwa.connect.core.domian

data class KTOptional<T>(val data: T?) {
    fun hasValue(): Boolean = data != null

    companion object {
        fun <T> empty() = KTOptional<T>(null)
    }
}