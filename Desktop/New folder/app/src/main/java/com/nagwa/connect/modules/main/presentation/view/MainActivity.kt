package com.nagwa.connect.modules.main.presentation.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringActivity
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.main.presentation.view.popup.UserMenuBottomDialog
import com.nagwa.connect.modules.main.presentation.view.popup.UserMenuPopup
import com.nagwa.connect.modules.main.presentation.viewmodel.MainViewModel
import com.nagwa.connect.modules.sessions.presentation.view.SessionsFragment
import com.nagwa.connect.modules.sessions.presentation.viewmodel.OpenSessionNavParam
import com.nagwa.connect.modules.usermanagement.presentation.view.SignInActivity
import com.nagwa.connect.modules.whiteboard.presentation.view.WhiteboardActivity
import kotlinx.android.synthetic.main.layout_main_tab.*
import javax.inject.Inject


class MainActivity : TutoringActivity() {

    override fun layoutId() = R.layout.activity_main

    @Inject
    lateinit var viewModelFactory: TutoringViewModelFactory
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }
    private val userMenuPopup by lazy {
        UserMenuPopup(
            this,
            onSignout = ::showSignoutConfirmation
        )
    }
    private var mUserMenuBottomDialog: UserMenuBottomDialog? = null
    private var mAlertDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userNameTv onClick {
            if (isTablet())
                userMenuPopup.openPopupWindow(userNameTv)
            else openUserMenuDialog()
        }
        viewModelObservers()
        if (savedInstanceState == null) openSessionFragment()
        else reattachDialogListeners()
        if (!isTablet() && isLandscape()) hideStatusBar()
    }

    private fun openUserMenuDialog() {
        viewModel.onUserAvailable.value?.let { userEntity ->
            mUserMenuBottomDialog.isNull {
                mUserMenuBottomDialog = UserMenuBottomDialog.newInstance(userEntity.fullName())
                supportFragmentManager.add(
                    fragment = mUserMenuBottomDialog
                        ?.apply {
                            onLogoutClicked = ::showSignoutConfirmation
                            onDismissListener = { mUserMenuBottomDialog = null }

                        }!!,
                    tag = UserMenuBottomDialog::class.simpleName!!,
                    addToBackStack = false
                )
            }
        }
    }


    private fun openSessionFragment() {
        supportFragmentManager.replaceFragment(
            fragment = SessionsFragment.newInstance(),
            frameId = R.id.containerTabsFragment,
            tag = SessionsFragment::class.java.name
        )
    }

    override fun clickOnScreen() {
        super.clickOnScreen()
        userMenuPopup.closePopupWindow()
    }

    private fun viewModelObservers() {
        viewModel.onUserAvailable.observe(this, Observer {
            userNameTv.text = it.firstName
            userMenuPopup.fullName = it.fullName() //student doesn't have email
        })
        viewModel.openWhiteboardLiveData.observe(this, ::openWhiteboardActivity)
        viewModel.onUserSignedOut.observe(this, ::navigateToLogin)
    }

    private fun navigateToLogin(paymentPair: Pair<Boolean, Boolean>) {
        val (showPaymentMsg, isDeactivated) = paymentPair
        SignInActivity.startInstance(this, showPaymentMsg, isDeactivated)
        this.finish()
    }

    private fun openWhiteboardActivity(param: OpenSessionNavParam) {
        WhiteboardActivity.startInstance(this, param)
    }

    private fun reattachDialogListeners() {
        //reattach signout listener to dialog fragment after destroy
        supportFragmentManager.findFragmentByTag(UserMenuBottomDialog::class.simpleName!!)
            ?.let {
                (it as UserMenuBottomDialog).onLogoutClicked = ::showSignoutConfirmation
                it.onDismissListener = {
                    mUserMenuBottomDialog = null
                }
            }
    }

    private fun showSignoutConfirmation() {
        mAlertDialog.isNull {
            mAlertDialog = createAlertWithTwoButtons(
                message = getString(R.string.msgSignoutAlert),
                positiveText = getString(R.string.yes),
                positiveButtonListener = {
                    viewModel.signOut()
                },
                negativeText = getString(R.string.no)
            ).apply {
                show()
                getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(context.getResColor(R.color.colorDangerAction))
                getButton(AlertDialog.BUTTON_NEGATIVE)
                    .setTextColor(context.getResColor(R.color.colorNeutralAction))
                setOnDismissListener { mAlertDialog = null }
            }
        }

    }

    override fun onBackPressed() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == WhiteboardActivity.REQUEST_CODE) {
            viewModel.navigateBackToSessions.value = Unit
        }
    }

    companion object {

        fun startInstance(activity: Activity) {
            activity.startActivity(Intent(activity, MainActivity::class.java))
            activity.finish()
        }

    }
}
