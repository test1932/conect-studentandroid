package com.nagwa.connect.modules.whiteboard.data.model.mapper

import com.nagwa.connect.modules.whiteboard.data.model.*
import com.nagwa.connect.modules.whiteboard.domain.entity.*
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.ConnectionStatus
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawingType
import com.nagwa.connect.modules.whiteboard.domain.entity.mapper.getScreenPercentage
import com.nagwa.connect.modules.whiteboard.domain.entity.mapper.getSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.drawingengine.data.model.enums.EraserType
import com.nagwa.drawingengine.extention.parseColor


/**
 * Created by Youssef Ebrahim Elerian on 5/18/20.
 * youssef.elerian@gmail.com
 */

fun ClearAllModel.toEntity() = ClearAllEntity(
    id = id
)

fun CreateShapeModel.toEntity(
    drawingSize: DrawingSize,
    educatorDrawingSize: DrawingSize
): CreateShapeEntity {
    val percentage = drawingSize.getScreenPercentage(
        educatorDrawingSize.drawingWidth,
        educatorDrawingSize.drawingHeight
    )
    return CreateShapeEntity(
        id = id,
        color = color.parseColor(),
        strokeWidth = strokeWidth.toFloat() * percentage,
        typeOfDrawing = if (typeOfDrawing == DrawingType.ERASER.type) EraserType.ALL else null,
        points = points.map { it.toModel(percentage) }
    )
}

fun Int.mapToConnectionStatus(): ConnectionStatus {
    return when (this) {
        ConnectionStatus.CONNECTED.status -> ConnectionStatus.CONNECTED
        ConnectionStatus.RECONNECTING.status -> ConnectionStatus.RECONNECTING
        ConnectionStatus.ABORTED.status -> ConnectionStatus.ABORTED
        else -> ConnectionStatus.UNKNOWN
    }
}


fun Int.mapToEducatorStatus(): EducatorStatus {
    return when (this) {
        EducatorStatus.ONLINE.value -> EducatorStatus.ONLINE
        EducatorStatus.UNREACHABLE.value -> EducatorStatus.UNREACHABLE
        EducatorStatus.OFFLINE.value -> EducatorStatus.OFFLINE
        else -> EducatorStatus.UNKNOWN
    }
}


fun PointModel.toModel(percentage: Float) = PointEntity(
    xPosition = getXPosition(percentage, xPosition),
    yPosition = getYPosition(percentage, yPosition)
)

fun AddImageModel.toEntity(
    drawingSize: DrawingSize,
    educatorDrawingSize: DrawingSize
): AddImageEntity {
    val percentage = drawingSize.getScreenPercentage(
        educatorDrawingSize.drawingWidth,
        educatorDrawingSize.drawingHeight
    )
    return AddImageEntity(
        id = id,
        imageWidth = getSize(imageModel.width, percentage),
        imageHeight = getSize(imageModel.height, percentage),
        xPos = getXPosition(percentage, imageModel.xPosition),
        yPos = getYPosition(percentage, imageModel.yPosition),
        imageName = imageModel.name
    )
}

fun PublishStreamResponse.toChannelDataEntity() = ChannelDataEntity(
    milliCastIceServers = milliCastIceServers.toEntity(),
    publishingToken = publishingToken,
    url = url,
    jwt = jwt,
    accountId = accountId,
    streamName = streamName,
    wsurl = wsurl
)

fun MilliCastIceServers.toEntity() = MilliCastIceServersEntity(
    iceServersList = iCEServersWrapperResponse.toEntity(),
    status = status
)

fun ICEServersWrapperResponse.toEntity(): IceServersListEntity {
    val list: List<IceServersEntity> = ArrayList()
    iceServers.forEach {
        list.toMutableList().add(it.toEntity())
    }
    return IceServersListEntity(list)
}

fun IceServers.toEntity() =
    IceServersEntity(url = url, username = username, credential = credential)

fun ChannelDataEntity.toModel() = PublishStreamResponse(
    milliCastIceServers = milliCastIceServers.toModel(),
    publishingToken = publishingToken,
    url = url,
    jwt = jwt,
    accountId = accountId,
    streamName = streamName,
    wsurl = wsurl
)

fun MilliCastIceServersEntity.toModel() = MilliCastIceServers(
    iCEServersWrapperResponse = iceServersList.toModel(),
    status = status
)

fun IceServersListEntity.toModel(): ICEServersWrapperResponse {
    val list: List<IceServers> = ArrayList()
    iceServers.forEach {
        list.toMutableList().add(it.toModel())
    }
    return ICEServersWrapperResponse(list)
}

fun IceServersEntity.toModel() =
    IceServers(url = url, username = username, credential = credential)


fun ActionEntity.toModel() = CallActionModel(action, studentId, streamName, studentName)

fun CallActionModel.toEntity(senderId: String) = ActionEntity(
    action = action, studentId = studentId ?: "",
    streamName = streamName, studentName = studentName, senderId = senderId
)


private fun getXPosition(percentage: Float, xPosition: Float): Float = percentage * xPosition

private fun getYPosition(percentage: Float, yPosition: Float): Float = percentage * yPosition


