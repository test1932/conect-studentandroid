package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType

data class AddImageModel(
    override val id: Int,
    @SerializedName("a")
    override val actionType: Int = DrawActionType.ADD_IMAGE.type,
    @SerializedName("i")
    val imageModel: ImageModel
) : DrawModel()