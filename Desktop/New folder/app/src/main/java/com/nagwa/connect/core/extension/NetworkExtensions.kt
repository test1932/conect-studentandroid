package com.nagwa.connect.core.extension

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nagwa.networkmanager.domain.entity.NAUploadEntity
import com.nagwa.networkmanager.domain.excepation.NAException
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import com.nagwa.networkmanager.network.NANetworkBuilder
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import okhttp3.RequestBody
import timber.log.Timber
import java.io.File
import java.lang.reflect.Type


const val ACCEPT_TYPE = "application/json"
const val CONTENT_TYPE = "application/json"
const val API_VERSION = "2"
const val API_VERSION_KEY = "api-version"
fun <T> String.GET(
    path: String,
    network: NANetwork,
    naAuthNetwork: NAAuthNetwork,
    body: Any? = null,
    gson: Gson? = null,
    resultType: Type? = null,
    params: Map<String, String>? = null,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf()
): Single<T> {
    val fullUrl: String = this + path + (params?.urlParams() ?: "")
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)
    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.GET, fullUrl)
        .setHeader(headers)
        .setResponse(resultType)
        .setReadTimeout(20_000)
        .setWriteTimeout(20_000)
        .setConnectTimeout(20_000)
        .setAuth(naAuthNetwork)
    if (body != null) {
        naNetworkBuilder.body = gson?.toJson(body)
    }
    return network.execute<T>(naNetworkBuilder)
        .map { it.data }
        .onErrorResumeNext { Single.error(it.naException()) }
        .also {
            Timber.d("GET() called with: path = [$path], resultType = [$resultType], params = [$params] , fullUrl = [$fullUrl]")
        }

}

fun <T> String.POST(
    path: String,
    network: NANetwork,
    body: Any? = null,
    params: Map<String, String>? = null,
    gson: Gson,
    resultType: Type? = null,
    hasContentType: Boolean = true,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf()
): Single<T> {
    val fullUrl: String = this + path
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)
    if (hasContentType) headers.putIfNull("content-type", CONTENT_TYPE)

    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.POST, fullUrl)
        .setHeader(headers)
        .setResponse(resultType)
        .setBody(gson.toJson(body))
        .setReadTimeout(20_000)
        .setWriteTimeout(20_000)
        .setConnectTimeout(20_000)
        .setAuth(null)

    body?.let {
        naNetworkBuilder.setBody(gson.toJson(body))
    } ?: run { params?.let { naNetworkBuilder.setParam(params) } }

    return network.execute<T>(naNetworkBuilder)
        .map { it.data }
        .onErrorResumeNext { Single.error(it.naException()) }
        .also {
            Timber.d("POST() called with: path = [$path], resultType = [$resultType], body = [$body] , fullUrl = [$fullUrl]")
        }

}


fun <T> String.POST(
    path: String,
    network: NANetwork,
    naAuthNetwork: NAAuthNetwork,
    body: Any? = null,
    params: Map<String, String>? = null,
    gson: Gson,
    resultType: Type? = null,
    hasContentType: Boolean = true,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf()
): Single<T> {
    val fullUrl: String = this + path
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)
    if (hasContentType) headers.putIfNull("content-type", CONTENT_TYPE)

    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.POST, fullUrl)
        .setHeader(headers)
        .setResponse(resultType)
        .setBody(gson.toJson(body))
        .setReadTimeout(20_000)
        .setWriteTimeout(20_000)
        .setConnectTimeout(20_000)
        .setAuth(naAuthNetwork)

    body?.let {
        naNetworkBuilder.setBody(gson.toJson(body))
    } ?: run { params?.let { naNetworkBuilder.setParam(params) } }

    return network.execute<T>(naNetworkBuilder)
        .map { it.data }
        .onErrorResumeNext { Single.error(it.naException()) }
        .also {
            Timber.d("POST() called with: path = [$path], resultType = [$resultType], body = [$body] , fullUrl = [$fullUrl]")
        }

}

fun String.NO_RESPONSE_POST(
    path: String,
    network: NANetwork,
    naAuthNetwork: NAAuthNetwork,
    body: Any? = null,
    params: Map<String, String>? = null,
    gson: Gson,
    hasContentType: Boolean = true,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf()
): Completable {
    val fullUrl: String = this + path
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)
    if (hasContentType) headers.putIfNull("content-type", CONTENT_TYPE)

    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.POST, fullUrl)
        .setHeader(headers)
        .setBody(gson.toJson(body))
        .setReadTimeout(20_000)
        .setWriteTimeout(20_000)
        .setConnectTimeout(20_000)
        .setAuth(naAuthNetwork)

    body?.let {
        naNetworkBuilder.setBody(gson.toJson(body))
    } ?: run { params?.let { naNetworkBuilder.setParam(params) } }

    return network.executeWithNoResponse(naNetworkBuilder)
        .onErrorResumeNext { Completable.error(it.naException()) }
        .also {
            Timber.d("POST() called with: path = [$path],body = [$body] , fullUrl = [$fullUrl]")
        }
}

fun String.PATCH(
    path: String,
    network: NANetwork,
    naAuthNetwork: NAAuthNetwork,
    body: Any,
    gson: Gson,
    resultType: Type? = null,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf()
): Completable {
    val fullUrl: String = this + path
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)
    headers.putIfNull("content-type", CONTENT_TYPE)

    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.PATCH, fullUrl)
        .setHeader(headers)
        .setResponse(resultType)
        .setBody(gson.toJson(body))
        .setReadTimeout(20_000)
        .setWriteTimeout(20_000)
        .setConnectTimeout(20_000)
        .setAuth(naAuthNetwork)
    return network.execute<Unit>(naNetworkBuilder)
        .onErrorResumeNext { Single.error(it.naException()) }
        .ignoreElement()
        .also { Timber.d("PATCH() called with: path = [$path], resultType = [$resultType], body = [$body] , fullUrl = [$fullUrl]") }

}

fun <T> String.UPLOAD(
    path: String,
    network: NANetwork,
    naAuthNetwork: NAAuthNetwork,
    file: File,
    responseType: Type?,
    apiVersion: String = API_VERSION,
    headers: MutableMap<String, String> = mutableMapOf(),
    params: Map<String, Any>? = null
): Flowable<NAUploadEntity<T>> {
    val fullUrl: String = this + path
    headers.putIfNull("accept", ACCEPT_TYPE)
    headers.putIfNull("api-version", apiVersion)

    val naNetworkBuilder = NANetworkBuilder.builder(NANetworkBuilder.Method.POST, fullUrl)
        .setHeader(headers)
        .setParam(params ?: mapOf<String, RequestBody>())
        .setResponse(responseType)
        .setAuth(naAuthNetwork)
        .setConnectTimeout(30 * 1000)
        .setReadTimeout(90 * 1000)
        .setWriteTimeout(90 * 1000)
    return network.uploadWithProgress<T>(naNetworkBuilder, file)
        .onErrorResumeNext { err: Throwable -> Flowable.error(err.naException()) }
        .also { Timber.d("UPLOAD() called with: path = [$path], file = [$file], responseType = [$responseType], headers = [$headers], params = [$params]") }
        .doOnSubscribe { Timber.d("UPLOAD() subscribed with: path = [$path], file = [${file.name}]") }
}

inline fun <reified T> genericType() = object : TypeToken<T>() {}.type

fun Map<String, String>.urlParams(): String {
    var result = "?"
    this.entries.forEachIndexed { idx, it ->
        result += if (idx != this.size - 1)
            (it.key + "=" + it.value + "&")
        else
            (it.key + "=" + it.value)
    }
    Timber.d("params() returned: $result")
    return result
}

private fun <K, V> MutableMap<K, V>.putIfNull(key: K, value: V) {
    if (!containsKey(key))
        this[key] = value
}

fun Throwable.naException(): NAException {
    //500 errors some time falls into https
    val naException =
        if (this is NAException && kind == NAException.Kind.HTTP && statusCode >= 500) {
            this.copy(kind = NAException.Kind.UNEXPECTED)
        } else if (this is NAException)
            this
        else
            NAException(NAException.Kind.UNEXPECTED, this)
    Timber.e("onRequestFailure: ${naException.toLogString()} ")
    return naException
}

private fun NAException.copy(
    message: String? = null,
    cause: Throwable? = null,
    data: String? = null,
    statusCode: Int? = null,
    errorCode: Int? = null,
    kind: NAException.Kind? = null,
    localizedMessage: String? = null
): NAException {
    val copiedException =
        NAException(kind ?: this.kind, message ?: this.message, cause ?: this.cause)
    copiedException.data = data ?: this.data
    copiedException.statusCode = statusCode ?: this.statusCode
    copiedException.errorCode = errorCode ?: this.errorCode
    copiedException.naLocalizeMessage = localizedMessage ?: this.localizedMessage
    return copiedException
}

fun NAException.toLogString(): String =
    "Kind =[$kind],Status Code=[$statusCode],Error Code=[$errorCode],Data=[$data],Message[$message],cause=[${cause?.message}]"

