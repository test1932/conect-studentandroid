package com.nagwa.connect.modules.whiteboard.data.repository

import addTo
import com.jakewharton.rxrelay2.PublishRelay
import com.nagwa.connect.core.extension.toObject
import com.nagwa.connect.core.extension.toStringData
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import com.nagwa.connect.modules.whiteboard.data.model.ChatMessage
import com.nagwa.connect.modules.whiteboard.data.model.mapper.mapToConnectionStatus
import com.nagwa.connect.modules.whiteboard.data.model.mapper.mapToEducatorStatus
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.EducatorStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.ConnectionStatus
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import com.nagwa.sessionlibrary.message.ChatRoom
import com.nagwa.sessionlibrary.message.data.model.SessionMessageModel
import com.nagwa.sessionlibrary.message.data.source.ChatLogin
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@WhiteboardScope
class MessengerRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val loginChat: ChatLogin,
    private val chatChannel: ChatRoom
) : MessengerRepository {
    private var chatMessagesStream = PublishRelay.create<ChatMessageEntity>()
    private val compositeDisposable = CompositeDisposable()
    override fun prepareChannels(): Completable {
        return userRepository.getUser().flatMapSingle {
            loginChat.login(userId = it.user.userId)
        }.ignoreElement()
    }

    override fun joinChatChannel(sessionID: String): Completable {
        return chatChannel.createChannel("ChatChannel:$sessionID").ignoreElement()
    }

    override fun leaveChatChannel(): Completable = loginChat.logout()
        .flatMap { chatChannel.leaveChannel() }
        .ignoreElement()

    override fun sendChatMessage(message: ChatMessageEntity): Single<Boolean> {
        return chatChannel.sendMessage(
            ChatMessage(
                message.studentName,
                message.text
            ).toStringData()
        ).doOnSuccess { chatMessagesStream.accept(message.copy(date = System.currentTimeMillis())) }
    }

    override fun getChatMessage(): Flowable<ChatMessageEntity> {
        getOthersMessageStream().subscribeOn(Schedulers.io())
            .subscribe({ convertMessage(it.message, it.userId) }, { Timber.e(it) })
            .addTo(compositeDisposable)
        return chatMessagesStream.toFlowable(BackpressureStrategy.LATEST)
    }

    override fun getUserConnectionStatus(): Flowable<ConnectionStatus> {
        return chatChannel.getUserConnectionStatus().map { it.mapToConnectionStatus() }
    }

    private fun getOthersMessageStream(): Flowable<SessionMessageModel> {
        return chatChannel.hasMessage()
    }

    private fun convertMessage(message: String, userId: String) {
        try {
            val chatMessage = message.toObject<ChatMessage>()
            val chatMessageEntity = ChatMessageEntity(
                text = chatMessage.message,
                studentId = userId,
                studentName = chatMessage.userName,
                date = System.currentTimeMillis()
            )
            chatMessagesStream.accept(chatMessageEntity)
        } catch (e: Exception) {
            Timber.d("convertMessage   $e")
        }

    }

    override fun onClear() {
        compositeDisposable.clear()
    }

    override fun getEducatorStatus(educatorId: String): Flowable<EducatorStatusEntity> {
        return chatChannel.getUserStatus(educatorId)
            .map {
                EducatorStatusEntity(id = educatorId, status = it.mapToEducatorStatus())
            }
    }

    override fun isUserOnline(userId: String): Single<Boolean> {
        return chatChannel.isUserOnline(userId)
    }

}