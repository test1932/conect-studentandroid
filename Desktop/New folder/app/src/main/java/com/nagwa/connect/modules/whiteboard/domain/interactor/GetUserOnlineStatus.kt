package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.repository.MessengerRepository
import io.reactivex.Single
import javax.inject.Inject


@WhiteboardScope
class GetUserOnlineStatus @Inject constructor(
    private val messengerRepository: MessengerRepository
) : SingleUseCase<String, Boolean>() {

    override fun build(userId: String): Single<Boolean> {
        return messengerRepository.isUserOnline(userId)
    }


}