import com.nagwa.connect.core.data.executors.PostExecutionThread
import com.nagwa.connect.core.data.executors.ThreadExecutor
import com.nagwa.connect.core.extension.shouldReload
import com.nagwa.connect.core.presentation.ObservableResource
import com.nagwa.connect.core.presentation.executors.JobExecutor
import com.nagwa.connect.core.presentation.executors.UIThread
import com.nagwa.connect.core.presentation.view.SingleLiveEvent
import com.nagwa.networkmanager.domain.excepation.NAException
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper
import io.reactivex.schedulers.Schedulers

fun <T> Single<T>.flatMapIf(condition: Boolean, mapper: (T) -> Single<T>) = flatMap {
    if (condition) mapper(it) else it.toSingle()
}

fun <T> Single<T>.flatMapIf(predicate: (T) -> Boolean, mapper: (T) -> Single<T>) = flatMap {
    if (predicate.invoke(it)) mapper(it) else it.toSingle()
}

fun <T, R> Single<T>.flatMapScoped(mapper: T.() -> Single<R>) = flatMap {
    it.mapper()
}

fun <T, R> Flowable<T>.flatMapScoped(mapper: T.() -> Flowable<R>) = flatMap {
    it.mapper()
}

fun <T> Flowable<T>.applyAsyncSchedulers(
    executionScheduler: Scheduler = Schedulers.io(),
    postExecutionScheduler: PostExecutionThread = UIThread()
): Flowable<T> =
    this.compose {
        it.subscribeOn(executionScheduler).observeOn(postExecutionScheduler.scheduler)
    }


fun <T> Single<T>.applyAsyncSchedulers(
    executor: ThreadExecutor,
    postExecutor: PostExecutionThread
): Single<T> =
    this.compose {
        it.subscribeOn(Schedulers.from(executor)).observeOn(postExecutor.scheduler)
    }

fun <T> Single<T>.applyAsyncSchedulers(
    executionScheduler: Scheduler = Schedulers.from(JobExecutor()),
    postExecutionScheduler: PostExecutionThread = UIThread()
): Single<T> =
    this.compose {
        it.subscribeOn(executionScheduler).observeOn(postExecutionScheduler.scheduler)
    }


fun <T> Observable<T>.applyAsyncSchedulers(
    executionScheduler: Scheduler = Schedulers.from(
        JobExecutor()
    ), postExecutionScheduler: PostExecutionThread = UIThread()
): Observable<T> =
    this.compose {
        it.subscribeOn(executionScheduler).observeOn(postExecutionScheduler.scheduler)
    }

fun Completable.applyAsyncSchedulers(
    executor: ThreadExecutor,
    postExecutor: PostExecutionThread
): Completable =
    this.compose {
        it.subscribeOn(Schedulers.from(executor)).observeOn(postExecutor.scheduler)
    }

fun Completable.applyAsyncSchedulers(
    executionScheduler: Scheduler = Schedulers.from(JobExecutor()),
    postExecutionScheduler: PostExecutionThread = UIThread()
): Completable =
    this.compose {
        it.subscribeOn(executionScheduler).observeOn(postExecutionScheduler.scheduler)
    }

fun <T> Maybe<T>.applyAsyncSchedulers(
    executionScheduler: Scheduler = Schedulers.from(JobExecutor()),
    postExecutionScheduler: PostExecutionThread = UIThread()
): Maybe<T> =
    this.compose {
        it.subscribeOn(executionScheduler).observeOn(postExecutionScheduler.scheduler)
    }

fun <T, R> Flowable<T>.publishToObservableResource(
    res: ObservableResource<R>,
    doOnNext: ((R) -> Unit)? = null,
    doOnComplete: (() -> Unit)? = null,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.map(mapper).applyAsyncSchedulers(Schedulers.from(executor), postExecutor)
        .subscribe({
            onSuccess(res, it)
            doOnNext?.invoke(it)
        }, {
            onError(it, res)
        }, {
            doOnComplete?.invoke()
        })
}

fun <T, R> Maybe<T>.publishToObservableResource(
    res: ObservableResource<R>,
    doOnSuccess: ((data: R) -> Unit)? = null,
    doOnError: ((data: Throwable) -> Unit)? = null,
    doOnComplete: (() -> Unit)? = null,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.map(mapper).applyAsyncSchedulers(Schedulers.from(executor), postExecutor)
        .subscribe({
            onSuccess(res, it)
            doOnSuccess?.invoke(it)
        }, {
            onError(it, res, doOnError)
        }, {
            doOnComplete?.invoke()
        })
}

fun <T, R> Observable<T>.publishToObservableResource(
    res: ObservableResource<R>,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.map(mapper).applyAsyncSchedulers(Schedulers.from(executor), postExecutor)
        .subscribe({
            onSuccess(res, it)
        }, {
            onError(it, res)
        })
}

fun <T, R> Single<T>.publishToObservableResource(
    res: ObservableResource<R>,
    doOnSuccess: ((data: R) -> Unit)? = null,
    doOnError: ((data: Throwable) -> Unit)? = null,
    doAfterError: ((data: Throwable) -> Unit)? = null,
    loading: Boolean = true,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    if (loading) res.loading.value = true

    return this.map(mapper).applyAsyncSchedulers(Schedulers.from(executor), postExecutor)
        .subscribe({
            onSuccess(res, it)
            doOnSuccess?.invoke(it)
        }, {
            onError(it, res, doOnError, doAfterError)
        })
}

fun <T, R> Single<List<T>>.appendToObservableResource(
    res: ObservableResource<List<R>>,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread(),
    doOnSuccess: ((data: List<R>) -> Unit)? = null
): Disposable {
    return if (res.loading.value != true) {
        res.loading.value = true
        this.flattenAsFlowable { it }.map(mapper).toList()
            .applyAsyncSchedulers(Schedulers.from(executor), postExecutor).subscribe({
                appendOnSuccess(res, it)
                doOnSuccess?.invoke(it)
            }, {
                onError(it, res)
            })
    } else
        DisposableHelper.DISPOSED
}

//use this only if the data won't be changed on subsequent calls
fun <T, R> Single<T>.publishOnceToObservableResource(
    res: ObservableResource<R>,
    doOnSuccess: ((data: R) -> Unit)? = null,
    doOnError: ((data: Throwable) -> Unit)? = null,
    doAfterError: ((data: Throwable) -> Unit)? = null,
    loading: Boolean = true,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    return if (res.shouldReload())
        publishToObservableResource(
            res,
            doOnSuccess,
            doOnError,
            doAfterError,
            loading,
            mapper,
            executor,
            postExecutor
        )
    /* else
         Single.just(res.value!!).publishToObservableResource(res, doOnSuccess, doOnError, loading, mapper = { it })
    */ else DisposableHelper.DISPOSED
}


fun <T, R> Single<T>.publishToSingleLive(
    resSuccess: SingleLiveEvent<R>,
    resError: SingleLiveEvent<Throwable>? = null,
    onSuccess: ((data: R) -> Unit)? = null,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    return this.map(mapper).applyAsyncSchedulers(Schedulers.from(executor), postExecutor)
        .subscribe({
            resSuccess.value = it
            onSuccess?.invoke(it)
        }, {
            resError?.value = it
        })
}

fun Completable.publishToObservableResource(
    res: ObservableResource<Unit>,
    doOnSuccess: (() -> Unit)? = null,
    doOnError: ((Throwable) -> Unit)? = null,
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.applyAsyncSchedulers(Schedulers.from(executor), postExecutor).subscribe({
        doOnSuccess?.invoke()
        onSuccess(res, Unit)
    }, {
        doOnError?.invoke(it)
        onError(it, res)
    })
}

fun <T, R> Single<List<T>>.publishListToObservableResource(
    res: ObservableResource<List<R>>,
    onSuccess: ((data: List<R>) -> Unit)? = null,
    onError: ((data: Throwable) -> Unit)? = null,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.flattenAsFlowable { it }.map(mapper).toList()
        .applyAsyncSchedulers(Schedulers.from(executor), postExecutor).subscribe({
            onSuccess(res, it)
            onSuccess?.invoke(it)
        }, {
            onError(it, res, onError)
        })
}

fun <T, R> Flowable<List<T>>.publishListToObservableResource(
    res: ObservableResource<List<R>>,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.flatMapIterable { ls -> ls }.map(mapper).toList()
        .applyAsyncSchedulers(Schedulers.from(executor), postExecutor).subscribe({
            onSuccess(res, it)
        }, {
            onError(it, res)
        })
}

fun <T, R> Observable<List<T>>.publishListToObservableResource(
    res: ObservableResource<List<R>>,
    mapper: (T) -> R = noMapper(),
    executor: ThreadExecutor = JobExecutor(),
    postExecutor: PostExecutionThread = UIThread()
): Disposable {
    res.loading.value = true
    return this.toFlowable(BackpressureStrategy.LATEST)
        .publishListToObservableResource(res, mapper, executor, postExecutor)
}

fun <T> Single<List<T>>.onNotFoundGetEmpty(): Single<List<T>> =
    this.compose {
        it.onErrorResumeNext { err ->
            if (err is NAException && err.errorCode == 404)
                Single.just(emptyList())
            else
                Single.error(err)
        }
    }

fun <T> Flowable<List<T>>.onNotFoundGetEmpty(): Flowable<List<T>> =
    this.compose {
        it.onErrorResumeNext { err: Throwable ->
            if (err is NAException && err.errorCode == 404) {
                Flowable.just(emptyList())
            } else
                Flowable.error(err)
        }
    }

fun Disposable?.addTo(composite: CompositeDisposable): Unit {
    if (this != null)
        composite.add(this)
}

fun <T> T.toSingle(): Single<T> = Single.just(this)

private fun <R> onSuccess(res: ObservableResource<R>, it: R) {
    res.loading.value = false
    res.value = it
}

private fun <R> appendOnSuccess(res: ObservableResource<List<R>>, it: List<R>) {
    res.loading.value = false
    if (res.value == null) {
        res.value = it.distinct()
    } else {
        val newList = mutableListOf<R>()
        newList.addAll(res.value!!)
        newList.addAll(it)
        res.value = newList.distinct()
    }
}

private fun <R> onError(
    err: Throwable,
    res: ObservableResource<R>,
    onError: ((data: Throwable) -> Unit)? = null,
    doAfterError: ((data: Throwable) -> Unit)? = null
) {
    res.loading.value = false

    if (onError != null) {
        onError.invoke(err)
    } else {
        when (err) {
            is NAException -> res.error.value = err
            else -> res.error.value =
                NAException(NAException.Kind.UNEXPECTED, "Mapping non NAException to UNEXPECTED")
        }
        doAfterError?.invoke(err)
    }
}

private fun <T, R> noMapper(): (T) -> R {
    return mapper@{
        try {
            with(it as R) {
                return@mapper this
            }
        } catch (e: Exception) {
            throw ClassCastException("Please provide mapper to publishToObservableResource method or make sure that ObservableResource of the same type as the Source e.g Signle,Observable,Maybe,Flowable")
        }
    }
}

fun <T> Flowable<T>.retryToSubscribe(): Flowable<T> = retry()



