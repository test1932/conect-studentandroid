package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.ChatMessageEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.EducatorStatusEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.ConnectionStatus
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MessengerRepository {
    fun prepareChannels(): Completable
    fun joinChatChannel(sessionID: String): Completable
    fun leaveChatChannel(): Completable
    fun sendChatMessage(message: ChatMessageEntity): Single<Boolean>
    fun getChatMessage(): Flowable<ChatMessageEntity>
    fun getUserConnectionStatus(): Flowable<ConnectionStatus>
    fun getEducatorStatus(educatorId: String): Flowable<EducatorStatusEntity>
    fun isUserOnline(userId: String): Single<Boolean>
    fun onClear()
}