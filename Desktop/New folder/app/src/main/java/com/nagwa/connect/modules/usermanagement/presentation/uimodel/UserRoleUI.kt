package com.nagwa.connect.modules.usermanagement.presentation.uimodel

import com.nagwa.connect.modules.usermanagement.domain.entity.VerifyUserEntity

class UserRoleUI(val user: VerifyUserEntity, val userRole: String)