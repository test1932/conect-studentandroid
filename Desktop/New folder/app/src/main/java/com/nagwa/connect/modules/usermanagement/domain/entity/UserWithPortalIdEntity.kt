package com.nagwa.connect.modules.usermanagement.domain.entity

data class UserWithPortalIdEntity constructor(val user: UserEntity, val portalId: String)