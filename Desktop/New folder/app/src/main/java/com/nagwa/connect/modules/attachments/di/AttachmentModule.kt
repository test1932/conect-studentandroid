package com.nagwa.connect.modules.attachments.di

import android.content.Context
import com.nagwa.connect.modules.attachments.data.AttachmentFilesManagerImpl
import com.nagwa.connect.modules.attachments.data.AttachmentsRepositoryImpl
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
abstract class AttachmentModule {
    @Binds
    internal abstract fun bindSessionAttachmentRepository(impl: AttachmentsRepositoryImpl): AttachmentsRepository

    @Binds
    internal abstract fun bindFileManager(impl: AttachmentFilesManagerImpl): AttachmentFilesManager

    @Module
    companion object {
        @Provides
        @Named("Attachments")
        fun provideSharedPreference(context: Context) =
            context.getSharedPreferences("SessionAttachments", 0)
    }

}