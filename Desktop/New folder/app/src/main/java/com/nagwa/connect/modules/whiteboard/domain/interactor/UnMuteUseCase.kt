package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes
import com.nagwa.connect.modules.whiteboard.domain.entity.params.CallDataEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class UnMuteUseCase @Inject constructor(
    private val startTalkingUseCase: StartTalkingUseCase,
    private val actionsRepository: ActionsRepository
) :
    CompletableUseCase<CallDataEntity>() {
    override fun build(params: CallDataEntity): Completable {
        return startTalkingUseCase.build(Unit)
            .andThen(
                actionsRepository.sendAction(
                    ActionEntity(
                        CallActionTypes.ACTIVE_STUDENT.value,
                        params.studentId,
                        params.streamName,
                        "",
                        ""
                    )
                ).ignoreElement()
            )

    }

}