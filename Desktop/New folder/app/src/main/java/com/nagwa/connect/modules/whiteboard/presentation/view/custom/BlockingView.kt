package com.nagwa.connect.modules.whiteboard.presentation.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import com.nagwa.connect.R
import com.nagwa.connect.core.extension.startCustomAnimation
import com.nagwa.connect.core.extension.visible
import kotlinx.android.synthetic.main.layout_blocking_view.view.*
import timber.log.Timber


open class BlockingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    fun block(msg: String? = null, withLoading: Boolean = false) {
        showState {
            val stateView = inflate(context, R.layout.layout_blocking_view, null)
            msg?.let { stateView.tvLoadingMsg.text = msg }
            stateView.pbLoading.visible(withLoading)
            this.startCustomAnimation(R.anim.slide_down)
            return@showState stateView
        }
        setOnTouchListener { _, _ ->
            true
        }
    }


    fun unblock() {
        this.startCustomAnimation(R.anim.slide_up)
        if (visibility != View.GONE) {
            removeAllViews()
            visibility = View.GONE
        }
        setOnTouchListener(null)
    }

    protected fun showState(inflater: (() -> View)) {
        visibility = View.VISIBLE
        this.post {
            Timber.d("showState: $measuredWidth")
            removeAllViews()//(width* SCREEN_PERCENT)
            //todo remove getting width directly and make it attr
            val frameParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            ).apply { gravity = Gravity.CENTER }
            addView(inflater.invoke(), frameParams)
        }
    }


}