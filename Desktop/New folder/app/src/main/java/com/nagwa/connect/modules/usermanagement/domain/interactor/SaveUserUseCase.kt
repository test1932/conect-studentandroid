package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.usermanagement.domain.entity.UserWithPortalIdEntity
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import io.reactivex.Completable
import javax.inject.Inject

class SaveUserUseCase @Inject constructor(private val authenticationRepository: AuthenticationRepository) :
    CompletableUseCase<UserWithPortalIdEntity>() {

    override fun build(user: UserWithPortalIdEntity): Completable {
        return authenticationRepository.saveUser(user)
    }
}