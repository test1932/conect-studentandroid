package com.nagwa.connect.modules.usermanagement.presentation.view

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.WindowManager
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringActivity
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.view.KeyboardLayoutObserver
import com.nagwa.connect.modules.main.presentation.view.MainActivity
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.layout_signin_header.*


class SignInActivity : TutoringActivity(), SignInPasswordListener, SignInEmailListener {
    private val keyboardLayoutObserver by lazy { KeyboardLayoutObserver() }
    override fun layoutId() = R.layout.activity_signin

    override fun getTabletScreenOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    override fun getMobileScreenOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    override fun onCreate(savedInstanceState: Bundle?) {
        if (!isTablet() && isLandscape()) hideStatusBar()
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        if (savedInstanceState == null) {
            val showPaymentMsg = intent?.extras?.getBoolean(SHOW_PAYMENT_MSG, false) ?: false
            val isDeactivated = intent?.extras?.getBoolean(PAYMENT_IS_DEACTIVATED, false) ?: false
            supportFragmentManager.replaceFragment(
                SignInEmailStepFragment.newInstance(showPaymentMsg, isDeactivated),
                R.id.containerSignIn,
                false,

                SignInEmailStepFragment::class.java.name
            )
        }
        initListeners()
        onBackStackChanged()
    }

    override fun onResume() {
        super.onResume()
        keyboardLayoutObserver.addKeyboardObserver(this) { isKeyboardShown, _, _ ->
            if (isKeyboardShown) scrollContainerSignIn.scrollToBottom()
        }
    }

    override fun onPause() {
        super.onPause()
        keyboardLayoutObserver.removeKeyboardObserver(this)
    }

    private fun initListeners() {
        disableView onClick {}
        signInHeader onClick ::onCancel
        supportFragmentManager.addOnBackStackChangedListener(::onBackStackChanged)
    }


    override fun onValidPassword() {
        MainActivity.startInstance(this)
        finish()
    }

    override fun onPaymentError(isDeactivated: Boolean) {
        supportFragmentManager.removeAllFragments()
        supportFragmentManager.replaceFragment(
            SignInEmailStepFragment.newInstance(showPaymentMsg = true, isDeactivated),
            R.id.containerSignIn,
            false,
            SignInEmailStepFragment::class.java.name
        )
    }

    override fun onCancel() {
        supportFragmentManager.findFragmentByTag(SignInEmailStepFragment::class.java.name)?.let {
            (it as SignInEmailStepFragment).resetEmailState()
        }
        supportFragmentManager.popBackStack()
    }


    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 1)
            onCancel()
        else
            super.onBackPressed()
    }

    override fun disableInteraction(disable: Boolean) {
        disableView.visible(disable)
    }

    override fun onExistingStudent(email: String, userId: String, portalId: String) {
        supportFragmentManager.replaceFragment(
            SignInPasswordStepFragment.newInstance(email, userId, portalId),
            R.id.containerSignIn,
            true,
            SignInPasswordStepFragment::class.java.name
        )
    }

    override fun onNewStudent(email: String, userId: String, portalId: String) {
        supportFragmentManager.replaceFragment(
            CreatePasswordFragment.newInstance(email, userId, portalId),
            R.id.containerSignIn,
            true,
            SignInPasswordStepFragment::class.java.name
        )
    }

    private fun onBackStackChanged() {
        val isSecondLevelFragment = supportFragmentManager.backStackEntryCount > 0
        toggleBackButton(isSecondLevelFragment)
        //don't show the note after passing email step
        txtAllowedUsersNote.visible(!isSecondLevelFragment)
    }

    private fun toggleBackButton(show: Boolean) {
        containerBtnBack.visible(show)
    }


    companion object {

        const val SHOW_PAYMENT_MSG = "show_payment_message"
        const val PAYMENT_IS_DEACTIVATED = "payment_is_deactivated"

        fun startInstance(
            activity: Activity,
            showPaymentMsg: Boolean = false,
            isDeactivated: Boolean = false
        ) {
            val intent = Intent(activity, SignInActivity::class.java)
            intent.putExtra(SHOW_PAYMENT_MSG, showPaymentMsg)
            intent.putExtra(PAYMENT_IS_DEACTIVATED, isDeactivated)
            activity.startActivity(intent)
            activity.finish()
        }
    }
}