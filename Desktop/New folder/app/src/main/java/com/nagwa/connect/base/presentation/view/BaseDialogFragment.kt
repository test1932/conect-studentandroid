package com.nagwa.connect.base.presentation.view

import android.app.Dialog
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import com.nagwa.connect.core.extension.getInflatedView
import com.nagwa.connect.core.extension.isTablet


abstract class BaseDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return initDialog()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.getInflatedView(getLayoutResource(), container)
    }

    private fun initDialog(): Dialog {
        var dialog: Dialog? = null
        val root = RelativeLayout(requireActivity())
        isCancelable = isFragmentCancelable()
        root.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window!!.setBackgroundDrawable(getDialogBackground())
        var dialogWidth = getDialogWidth()
        var dialogHeight = getDialogHeight()
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.window!!.setGravity(getDialogGravity())
        getTransitionsAnimation()?.let {
            dialog.window!!.attributes.windowAnimations = getTransitionsAnimation()!!
        }
        return dialog!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isFullScreen() && requireActivity().isTablet())
            dialog?.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            );
    }

    private fun isFullScreen() =
        getDialogHeight() == resources.displayMetrics.heightPixels && getDialogWidth() == resources.displayMetrics.widthPixels

    protected abstract fun getLayoutResource(): Int
    protected abstract fun getDialogBackground(): Drawable
    protected abstract fun getDialogWidth(): Int
    protected abstract fun getDialogHeight(): Int
    protected open fun getDialogGravity(): Int = Gravity.BOTTOM
    protected open fun getTransitionsAnimation(): Int? = null
    protected open fun isFragmentCancelable(): Boolean = false


}