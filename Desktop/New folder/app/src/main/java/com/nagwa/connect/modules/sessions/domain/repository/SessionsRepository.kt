package com.nagwa.connect.modules.sessions.domain.repository

import com.nagwa.connect.modules.sessions.domain.entity.SessionEntity
import io.reactivex.Completable
import io.reactivex.Single


/**
 * Created by Youssef Ebrahim Elerian on 5/18/20.
 * youssef.elerian@gmail.com
 */
interface SessionsRepository {
    fun getSessions(): Single<List<SessionEntity>>
    fun joinSession(sessionID: String): Completable
}