package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import io.reactivex.Completable
import javax.inject.Inject

@WhiteboardScope
class RaiseHandUseCase @Inject constructor(
    private val actionsRepository: ActionsRepository,
    private val requestTalkingUseCase: RequestTalkingUseCase
) :
    CompletableUseCase<String>() {
    override fun build(params: String): Completable {
        return requestTalkingUseCase.build(Unit)
            .andThen(
                actionsRepository.sendAction(
                    ActionEntity(
                        action = CallActionTypes.RaiseHand.value,
                        studentId = "",
                        streamName = ""
                    )
                ).ignoreElement()
            )
    }


}