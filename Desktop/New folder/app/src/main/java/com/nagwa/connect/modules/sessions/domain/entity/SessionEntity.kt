package com.nagwa.connect.modules.sessions.domain.entity

import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionType

data class SessionEntity(
    val id: String,
    val title: String,
    val subject: String,
    val startingDate: Long,
    val duration: Int,
    val studentsNumber: Int,
    val sessionType: SessionType,
    val teacherName: String,
    val teacherId: String,
    val fileID: Long
)