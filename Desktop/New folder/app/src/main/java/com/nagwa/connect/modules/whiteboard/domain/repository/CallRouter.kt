package com.nagwa.connect.modules.whiteboard.domain.repository

import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallStatusEntity
import io.reactivex.Completable
import io.reactivex.Flowable

//Route call actions to the proper provider
interface CallRouter {
    //starts call with the main channel (listens to educator mainly)
    fun startCall(sessionID: String): Flowable<CallStatusEntity>
    fun endCall(): Completable
    fun muteCall(): Completable
    fun unMuteCall(): Completable
}