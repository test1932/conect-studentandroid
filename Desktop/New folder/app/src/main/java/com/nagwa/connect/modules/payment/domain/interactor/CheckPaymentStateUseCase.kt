package com.nagwa.connect.modules.payment.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.payment.domain.entity.params.GetPaymentParam
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentRequiredError
import com.nagwa.connect.modules.usermanagement.domain.entity.PaymentUserDeactivatedError
import com.nagwa.connect.modules.usermanagement.domain.repository.UserRepository
import flatMapScoped
import io.reactivex.Single
import javax.inject.Inject

/**
 * Authored by Mohamed Fathy on 23 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
class CheckPaymentStateUseCase @Inject constructor(
    private val paymentStateUseCase: PaymentStateUseCase,
    private val userRepository: UserRepository
) : SingleUseCase<Unit, Boolean>() {

    override fun build(params: Unit): Single<Boolean> {
        return userRepository.getUser()
            .flatMapSingle {
                paymentStateUseCase.build(GetPaymentParam(it.user.userId, it.portalId))
            }
            .flatMapScoped {
                when (this) {
                    PaymentAction.BLOCK_USER -> Single.error(PaymentRequiredError)
                    PaymentAction.DEACTIVATED_USER -> Single.error(PaymentUserDeactivatedError)
                    else -> Single.just(true)
                }
            }
    }
}