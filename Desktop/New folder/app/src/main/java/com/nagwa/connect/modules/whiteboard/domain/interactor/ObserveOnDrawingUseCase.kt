package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.DrawingRepository
import io.reactivex.Flowable
import javax.inject.Inject


class ObserveOnDrawingUseCase @Inject constructor(private val drawingRepository: DrawingRepository) :
    FlowableUseCase<String, DrawEntity>() {

    override fun build(params: String): Flowable<DrawEntity> {
        return drawingRepository.getLastClearId(params).flatMapPublisher { lastClearId ->
            drawingRepository.observeOnDrawing(params, lastClearId)
        }
    }
}