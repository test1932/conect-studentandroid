package  com.nagwa.connect.core.extension

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ScrollView
import androidx.annotation.IdRes
import androidx.core.view.children
import com.nagwa.connect.base.presentation.view.OnSingleClickListener


fun ViewGroup.inflate(layout: Int) {
    LayoutInflater.from(context).inflate(layout, this)

}

fun Context.getInflatedView(layout: Int, container: ViewGroup?): View {
    val layoutInflater =
        getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    return layoutInflater.inflate(layout, container, false)
}

fun View.setEnableWithAlpha(enable: Boolean) {
    if (enable)
        enable()
    else
        disable()
}

fun View.disable(withAlpha: Boolean = true) {
    this.isEnabled = false
    if (withAlpha) this.alpha = 0.4f
}

fun View.enable(withAlpha: Boolean = true) {
    this.isEnabled = true
    if (withAlpha) this.alpha = 1.0f
}

fun View.findViewWithClick(@IdRes viewId: Int, click: ((viewId: Int) -> Unit)) {
    findViewById<View>(viewId).setOnClickListener {
        click.invoke(viewId)
    }
}

infix fun View.onClick(action: (() -> Unit)?) {
    this.setOnClickListener { action?.invoke() }
}

infix fun View.onSingleClick(action: (() -> Unit)?) {
    this.setOnClickListener(object : OnSingleClickListener() {
        override fun onSingleClick(v: View?) {
            action?.invoke()
        }
    })
}

fun View.setSize(width: Int, height: Int) {
    val lp = layoutParams
    lp.width = width
    lp.height = height
    layoutParams = lp
}

fun View.setPosition(xPos: Float, yPos: Float) {
    x = xPos
    y = yPos

}

fun ((viewIndex: Int, view: View) -> Unit).attachTo(vararg views: View?) {
    val listener = View.OnClickListener { v -> invoke(views.indexOf(v), v) }
    for (v in views)
        v?.setOnClickListener(listener)
}

infix fun ((viewIndex: Int, view: View) -> Unit).attachTo(views: List<View>) {
    val listener = View.OnClickListener { v -> invoke(views.indexOf(v), v) }
    for (v in views)
        v.setOnClickListener(listener)
}

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun EditText.afterTextChanged(afterTextChanged: ((editable: Editable?) -> Unit)) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

infix fun EditText.onKeyboardGoActionClicked(action: () -> Unit) {
    setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_GO) {
            action()
        }
        /*
        * Return:-
        * True: if you consumed the action (it'll keep the keyboard open)
        * False: if you don't consume the action (it'll close the keyboard after action clicked)
        * */
        false
    }
}

fun ViewGroup.getInflatedView(layout: Int): View {
    return LayoutInflater.from(context).inflate(layout, this, false)
}

fun LayoutInflater.getInflatedView(layout: Int, container: ViewGroup?): View {
    return inflate(layout, container, false)
}

fun View.toggleEnableRecursively(
    enable: Boolean,
    withAlpha: Boolean = true,
    vararg excludedViews: View
) {
    val applyAlpha = withAlpha && this !is ViewGroup
    if (enable) enable(applyAlpha) else disable(applyAlpha)
    isClickable = enable
    if (this is ViewGroup) {
        children
            .filter { !excludedViews.contains(it) }
            .forEach { it.toggleEnableRecursively(enable, withAlpha, *excludedViews) }
    }

}

inline fun View.waitForLayout(crossinline yourAction: () -> Unit): () -> Unit {
    val vto = viewTreeObserver
    var unsubscribeCallback: () -> Unit = {}
    vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            unsubscribeCallback = {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
            when {
                vto.isAlive -> {
                    vto.removeOnGlobalLayoutListener(this)
                    yourAction()
                }
            }
        }
    })
    return unsubscribeCallback
}

fun View.setVisibility(isVisible: Boolean) {
    if (isVisible)
        this.visible()
    else this.gone()
}


fun View.startCustomAnimation(animation: Int) {
    val slideUpAnimation = AnimationUtils.loadAnimation(context, animation)
    if (slideUpAnimation != null) {
        slideUpAnimation.reset()
        clearAnimation()
        startAnimation(slideUpAnimation)
    }
}

fun ScrollView.scrollToBottom() {
    val lastChild = getChildAt(childCount - 1)
    val bottom = lastChild.bottom + paddingBottom
    val delta = bottom - (scrollY + height)
    smoothScrollBy(0, delta)
}
