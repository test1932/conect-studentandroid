package com.nagwa.connect.modules.whiteboard.data.source

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.nagwa.connect.modules.whiteboard.data.model.AddImageModel
import com.nagwa.connect.modules.whiteboard.data.model.ClearAllModel
import com.nagwa.connect.modules.whiteboard.data.model.CreateShapeModel
import com.nagwa.connect.modules.whiteboard.data.model.DrawModel
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toEntity
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ClearAllEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType.*
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import java.lang.reflect.Type
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/20/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class DrawingDS @Inject constructor() {

    fun getDrawActions(
        action: String,
        drawingSize: DrawingSize,
        educatorDrawingSize: DrawingSize
    ): DrawEntity {
        return if (action.isNotBlank()) {
            val gb = GsonBuilder()
            gb.registerTypeAdapter(DrawModel::class.java, actionsJsonDeserializer)
            when (val model = gb.create().fromJson(action, DrawModel::class.java)) {
                is CreateShapeModel -> model.toEntity(drawingSize, educatorDrawingSize)
                is AddImageModel -> model.toEntity(drawingSize, educatorDrawingSize)
                else -> ClearAllEntity(model.id)
            }
        } else ClearAllEntity(0)

    }


    private val actionsJsonDeserializer = object : JsonDeserializer<DrawModel> {
        override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext
        ): DrawModel? {
            json?.let { return readActionInPeriod(json, context) }
            return null

        }


        private fun readActionInPeriod(
            jsonElement: JsonElement,
            context: JsonDeserializationContext
        ): DrawModel? {
            return when (jsonElement.asJsonObject.get("a").asInt) {
                CREATE_SHAPE.type -> context.deserialize(jsonElement, CreateShapeModel::class.java)
                CLEAR_ALL.type -> context.deserialize(jsonElement, ClearAllModel::class.java)
                ADD_IMAGE.type -> context.deserialize(jsonElement, AddImageModel::class.java)

                else -> null

            }
        }

    }
}