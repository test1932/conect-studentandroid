package com.nagwa.connect.modules.whiteboard.domain.entity


/**
 * Created by Youssef Ebrahim Elerian on 6/2/20.
 * youssef.elerian@gmail.com
 */
data class ChatMessageEntity(
    val studentName: String,
    val studentId: String,
    val text: String,
    val date: Long
)