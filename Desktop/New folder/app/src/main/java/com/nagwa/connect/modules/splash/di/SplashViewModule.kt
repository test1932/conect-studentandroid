package com.nagwa.connect.modules.splash.di

import androidx.lifecycle.ViewModel
import com.nagwa.connect.core.di.TutoringViewModelKey
import com.nagwa.connect.modules.splash.presentation.viewmodel.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


/**
 * Created by Youssef Ebrahim Elerian on 2019-12-22.
 * youssef.elerian@gmail.com
 */
@Module
abstract class SplashViewModule {
    @Binds
    @IntoMap
    @TutoringViewModelKey(SplashViewModel::class)
    internal abstract fun splashViewModel(viewModel: SplashViewModel): ViewModel

}