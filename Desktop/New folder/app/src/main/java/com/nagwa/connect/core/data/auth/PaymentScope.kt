package com.nagwa.connect.core.data.auth

import com.nagwa.networkmanager.network.NAAuthNetwork

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
class PaymentScope : NAAuthNetwork {

    override fun scope(): String {
        return "PaymentApi"
    }

    override fun params(): MutableMap<String, String> {
        val params = mutableMapOf<String, String>()
        params["grant_type"] = "client_credentials"
        params["scope"] = "payment.api"
        params["client_id"] = "tutoring.sessions.mobile.nagwa"
        params["client_secret"] = "ab5p9wslVv4rzxv3dv8CemHA9X8eyFhtJHVSNH6AB5"
        return params
    }
}