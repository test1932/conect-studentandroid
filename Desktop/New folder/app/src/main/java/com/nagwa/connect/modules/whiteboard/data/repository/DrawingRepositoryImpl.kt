package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.whiteboard.data.source.DrawingDS
import com.nagwa.connect.modules.whiteboard.data.source.FireBaseDrawingDS
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingParam
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.domain.entity.params.PointsRequestParam
import com.nagwa.connect.modules.whiteboard.domain.repository.DrawingRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

@WhiteboardScope
class DrawingRepositoryImpl @Inject constructor(
    private val drawActionDS: DrawingDS,
    private val fireBaseDrawingDS: FireBaseDrawingDS,
) : DrawingRepository {

    private var sessionID: String? = null
    private var drawingSize: DrawingSize? = null
    private var educatorDrawingSize: DrawingSize? = null
    private val drawEntitySubject: BehaviorSubject<DrawEntity> = BehaviorSubject.create()

    override fun joinDrawingChannel(sessionID: String): Completable {
        this.sessionID = sessionID
        return Completable.complete()
    }

    override fun leaveDrawingChannel(): Completable {
        return fireBaseDrawingDS.unRegister().ignoreElement()
    }

    override fun getDrawingAction(drawingSize: DrawingSize): Flowable<DrawEntity> {
        this.drawingSize = drawingSize
        return drawEntitySubject.toFlowable(BackpressureStrategy.LATEST)
    }

    override fun getDrawingParams(sessionID: String): Single<DrawingParam> {
        return fireBaseDrawingDS.getDrawingParams(sessionID)
    }

    override fun requestMissedPoints(pointsRequestParam: PointsRequestParam): Single<Boolean> {
        this.educatorDrawingSize = pointsRequestParam.educatorDrawingSize
        return fireBaseDrawingDS.getMissedDrawing(sessionID!!, pointsRequestParam.lastID)
            .map { pointsList ->
                pointsList.forEach {
                    val drawingEntity =
                        drawActionDS.getDrawActions(it, drawingSize!!, educatorDrawingSize!!)
                    drawEntitySubject.onNext(drawingEntity)
                }
                true
            }
    }

    override fun getLastClearId(sessionID: String): Single<Int> {
        return fireBaseDrawingDS.getLastClearId(sessionID)
    }

    override fun observeOnDrawing(sessionID: String, lastId: Int): Flowable<DrawEntity> {
        return fireBaseDrawingDS.observeOnDrawing(sessionID, lastId)
            .map {
                val drawingEntity =
                    drawActionDS.getDrawActions(it, drawingSize!!, educatorDrawingSize!!)
                drawEntitySubject.onNext(drawingEntity)
                drawingEntity
            }
    }

}