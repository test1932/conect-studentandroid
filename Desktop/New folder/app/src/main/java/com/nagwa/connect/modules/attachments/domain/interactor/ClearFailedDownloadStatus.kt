package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.rxdownloadmanger.IDownloadManger
import io.reactivex.Completable
import javax.inject.Inject

class ClearFailedDownloadStatus @Inject constructor(private val repository: IDownloadManger) :
    CompletableUseCase<String>() {
    override fun build(params: String) = Completable.fromAction { repository.remove(params) }
}