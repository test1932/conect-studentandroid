package com.nagwa.connect.modules.whiteboard.data.source

import android.content.SharedPreferences
import androidx.core.content.edit
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

@WhiteboardScope
//todo maybe we should move this to SessionsLocalDS  or remove it if will be retrieved from backedn
class WhiteboardLocalDS @Inject constructor(private val sharedPreferences: SharedPreferences) {

    fun saveSessionTime(sessionID: String, startingTime: Long): Completable =
        Completable.fromAction {
            //if (sharedPreferences.getLong(sessionID, -1L) == -1L) todo if we shall resume the time regardless how much time educator is gone offline
            sharedPreferences.edit(commit = true) {
                putLong(sessionID, startingTime)
            }
        }

    fun getSessionStartingTime(sessionID: String): Single<Long> = Single.fromCallable {
        sharedPreferences.getLong(
            sessionID,
            System.currentTimeMillis()
        ) //todo see what is best for this fallback value
    }

    fun removeSessionID(sessionID: String): Completable = Completable.fromAction {
        sharedPreferences.edit { remove(sessionID) }
    }
}