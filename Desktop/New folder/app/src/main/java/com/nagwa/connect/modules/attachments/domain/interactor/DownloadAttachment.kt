package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import com.nagwa.rxdownloadmanger.IDownloadManger
import com.nagwa.rxdownloadmanger.models.DownloadRequest
import io.reactivex.Single
import javax.inject.Inject

class DownloadAttachment @Inject constructor(
    private val downloadManager: IDownloadManger,
    private val filesManager: AttachmentFilesManager
) :
    SingleUseCase<AttachmentDownloadParam, String>() {
    override fun build(params: AttachmentDownloadParam): Single<String> {
        return filesManager.getDownloadFile(params.sessionID)
            .flatMap { file ->
                downloadManager.remove(params.sessionID) //remove old status
                downloadManager.start(
                    DownloadRequest(
                        url = params.url,
                        itemFile = file,
                        itemID = params.sessionID
                    )
                )
            }
    }
}

data class AttachmentDownloadParam(val url: String, val sessionID: String)