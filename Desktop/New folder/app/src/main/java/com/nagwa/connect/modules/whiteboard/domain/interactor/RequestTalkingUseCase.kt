package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.domain.repository.SecondaryCallRouter
import io.reactivex.Completable
import timber.log.Timber
import javax.inject.Inject

class RequestTalkingUseCase @Inject constructor(private val secondaryCallRouter: SecondaryCallRouter) :
    CompletableUseCase<Unit>() {
    override fun build(params: Unit): Completable {
        return secondaryCallRouter.requestMic()
            .doOnSubscribe { Timber.d("connection initiated") }
            .doOnError { Timber.e(it, "connection failed") }
            .doOnComplete { Timber.d("connected successfully") }
    }
}