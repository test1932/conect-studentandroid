package com.nagwa.connect.modules.sessions.data

import com.google.gson.Gson
import com.nagwa.connect.BuildConfig.*
import com.nagwa.connect.core.extension.GET
import com.nagwa.connect.core.extension.POST
import com.nagwa.connect.core.extension.genericType
import com.nagwa.connect.modules.sessions.data.model.JoinResponse
import com.nagwa.connect.modules.sessions.data.model.SessionsResponse
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.networkmanager.network.NAAuthNetwork
import com.nagwa.networkmanager.network.NANetwork
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

@SessionsScope
class SessionsRemoteDS @Inject constructor(
    private val network: NANetwork,
    private val gson: Gson,
    @Named("TutoringAuth") private val tutoringAuthScope: NAAuthNetwork
) {
    fun getSessions(userID: String, pageSize: Int, pageNumber: Int): Single<SessionsResponse> {
        val type = genericType<SessionsResponse>()
        return TUTORING_API.GET(
            path = "sessions/open",
            params = mapOf(
                "userId" to userID,
                "pageSize" to pageSize.toString(),
                "pageNumber" to pageNumber.toString()
            ),
            network = network,
            naAuthNetwork = tutoringAuthScope,
            resultType = type, apiVersion = TUTORING_API_VERSION

        )
    }

    fun joinSession(userID: String, sessionID: String): Single<JoinResponse> {
        return TUTORING_API.POST(
            path = "sessions/join",
            body = SessionActionBody(userID.toLong(), sessionID.toLong()),
            network = network,
            naAuthNetwork = tutoringAuthScope,
            resultType = JoinResponse::class.java,
            gson = gson,
            apiVersion = CONNECT_API_VERSION
        )

    }


    data class SessionActionBody(val userId: Long, val sessionId: Long)
    data class SessionStudentActionBody(val studentId: Long, val sessionId: Long)

}