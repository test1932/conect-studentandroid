package com.nagwa.connect.modules.sessions.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringFragment
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.view.GridRowMetrics
import com.nagwa.connect.core.presentation.view.GridSpacingItemDecoration
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.main.presentation.viewmodel.MainViewModel
import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionsListUIModel
import com.nagwa.connect.modules.sessions.presentation.viewmodel.SessionsViewModel
import kotlinx.android.synthetic.main.fragment_sessions.*
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/16/20.
 * youssef.elerian@gmail.com
 */
class SessionsFragment : TutoringFragment() {

    @Inject
    lateinit var viewModelFactory: TutoringViewModelFactory

    @Inject
    lateinit var sessionsAdapter: SessionsAdapter
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(SessionsViewModel::class.java)
    }

    private var preparingDialog: SessionPreparingDialog? = null

    private val mainViewModel by lazy {
        requireActivity().let {
            ViewModelProvider(it, viewModelFactory).get(MainViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_sessions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initObservation()
    }

    override fun onResume() {
        super.onResume()
        if (isAdded) viewModel.checkPaymentState()
    }

    private fun initObservation() {
        viewModel.openWhiteBoard.observe(viewLifecycleOwner) {
            mainViewModel.openWhiteboardLiveData.value = it
        }
        viewModel.uiModel.observe(viewLifecycleOwner, ::onSessionsStateUpdated)
        mainViewModel.navigateBackToSessions.observe(
            lifecycleOwner = viewLifecycleOwner,
            doOnObserve = viewModel::refreshSessions
        )
        viewModel.logoutUser.observe(viewLifecycleOwner) {
            mainViewModel.signOut(showPaymentMsg = true, isDeactivated = it)
        }
    }

    private fun onSessionsStateUpdated(uiModel: SessionsListUIModel) {
        with(uiModel) {
            Timber.d("SessionsUIModel: ${this.copy(list = emptyList())}")
            if (showEmptyView) sessionsStateView.setEmpty(
                R.string.empty_sessions_message,
                R.drawable.ic_empty_sessions
            )
            if (showLoading) sessionsStateView?.setLoading()
            if (showData) sessionsStateView?.setContent()
            if (sessionsSwipeToRefresh.isRefreshing != showRefreshing) //only update if not synced
                sessionsSwipeToRefresh.isRefreshing = showRefreshing
            if (resetData) sessionsAdapter.swap(list) //all the list has been replaced
            if (showSessionPreparationLoading) {
                if (preparingDialog == null) showPreparingProgressDialog()
                preparingDialog?.progressStream?.value = preparationPercentage
            }
            if (hideSessionPreparationLoading) clearPreparingDialog()
            nonInterruptedErrorMessage?.let(::showErrorDialog)
            screenErrorMessage?.let(sessionsStateView::setError)
        }
    }

    private fun showPreparingProgressDialog() {
        preparingDialog = SessionPreparingDialog().apply {
            onCancelClicked = viewModel::cancelJoining
        }
        requireActivity().supportFragmentManager.add(
            fragment = preparingDialog!!,
            tag = SessionPreparingDialog::class.java.name,
            addToBackStack = false
        )
    }

    private fun initUi() {
        initRecycler()
    }

    private fun initRecycler() {
        val rowMetrics = getRowMetrics()
        sessionsAdapter.cellWidth = rowMetrics.cellWidthPX
        sessionsAdapter.onPositiveActionClicked = {
            if (preparingDialog == null)
                viewModel.joinSession(it)
        }
        sessionsRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(context, rowMetrics.columnsNo)
            adapter = sessionsAdapter
        }
        sessionsRecyclerView.addItemDecoration(
            GridSpacingItemDecoration(
                spanCount = rowMetrics.columnsNo,
                spacing = rowMetrics.spacing,
                includeEdge = false,
                layoutDirection = requireContext().resources.configuration.layoutDirection
            )
        )
        sessionsSwipeToRefresh.setOnRefreshListener(viewModel::refreshSessions)
    }

    private fun getRowMetrics() = with(requireActivity()) {
        GridRowMetrics.getGridRowMetrics(
            gridFullWidth = screenWidth() - getResDimenPx(R.dimen.margin_sessions_content) * 2,
            cellWidth = getResDimenPx(R.dimen.width_session_item),
            minCellWidth = getResDimenPx(R.dimen.min_width_session_item),
            requiredCellSpace = getResDimenPx(R.dimen.spacing_session_item)
        )
    }

    private fun showErrorDialog(@StringRes error: Int) {
        clearPreparingDialog()
        requireContext().createAlertWithPositiveButton(
            message = getString(error),
            positiveText = getString(R.string.ok),
            positiveButtonListener = { viewModel.dismissSessionActionFailure() }
        ).show()
    }

    private fun clearPreparingDialog() {
        preparingDialog?.dismissAllowingStateLoss()
        preparingDialog = null
    }

    companion object {
        fun newInstance(): SessionsFragment {
            return SessionsFragment()
        }
    }
}