package com.nagwa.connect.core.data.auth

import com.nagwa.networkmanager.network.NAAuthNetwork

class UserManagementScope : NAAuthNetwork {
    override fun scope(): String {
        return "UserManagementApi"
    }

    override fun params(): MutableMap<String, String> {
        val params = mutableMapOf<String, String>()
        params["grant_type"] = "client_credentials"
        params["scope"] = "UserManagementApi"
        params["client_id"] = "mobile.to.usermanagement"
        params["client_secret"] = "Gynx2zMbrTsR27AfwyDMETFZF7xWhQRxGNn7yv5f"
        return params
    }
}
