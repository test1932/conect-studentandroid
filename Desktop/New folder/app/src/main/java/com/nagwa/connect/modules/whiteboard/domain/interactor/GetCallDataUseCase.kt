package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.sessions.data.source.SessionsLocalDS
import com.nagwa.connect.modules.usermanagement.domain.interactor.GetUserUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.params.CallDataEntity
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

@WhiteboardScope
class GetCallDataUseCase @Inject constructor(
    private val sessionsLocalDS: SessionsLocalDS,
    private val getUserUseCase: GetUserUseCase
) : SingleUseCase<String, CallDataEntity>() {
    override fun build(params: String): Single<CallDataEntity> {
        return Single.zip(
            getUserUseCase.build(Unit).toSingle(),
            sessionsLocalDS.getSession(params).map { it.session },
            BiFunction { userWithPortal, session ->
                CallDataEntity(
                    params,
                    session.teacherId,
                    userWithPortal.user.userId,
                    session.provider
                )
            })
    }
}