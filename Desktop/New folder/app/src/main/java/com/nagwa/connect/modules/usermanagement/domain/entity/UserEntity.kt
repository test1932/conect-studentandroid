package com.nagwa.connect.modules.usermanagement.domain.entity

data class UserEntity constructor(
    val userId: String,
    val userName: String?,
    val firstName: String,
    val lastName: String,
    val email: String?,
    val role: String = "Student",
    var portalInfo: PortalEntity? = null
) {
    fun fullName() = "$firstName $lastName"
}

