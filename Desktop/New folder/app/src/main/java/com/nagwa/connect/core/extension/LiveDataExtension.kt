package com.nagwa.connect.core.extension

import androidx.lifecycle.*
import com.nagwa.connect.core.presentation.ObservableResource


fun <T> LiveData<T>.observeOnce(observer: Observer<T>) {
    observeForever(object : Observer<T> {
        override fun onChanged(t: T?) {
            observer.onChanged(t)
            removeObserver(this)
        }
    })
}

fun <T> LiveData<T>.getDistinct(): LiveData<T> {
    val distinctLiveData = MediatorLiveData<T>()
    distinctLiveData.addSource(this, object : Observer<T> {
        private var initialized = false
        private var lastObj: T? = null
        override fun onChanged(obj: T?) {
            if (!initialized) {
                initialized = true
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            } else if ((obj == null && lastObj != null)
                || obj != lastObj
            ) {
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            }
        }
    })
    return distinctLiveData
}

fun <T> LiveData<T>.observe(lifecycleOwner: LifecycleOwner, doOnObserve: (T) -> Unit) {
    observe(lifecycleOwner, Observer { doOnObserve.invoke(it) })
}

fun <T> LiveData<T>.observeScoped(lifecycleOwner: LifecycleOwner, doOnObserve: T.() -> Unit) {
    observe(lifecycleOwner, Observer { it.doOnObserve() })
}

fun LiveData<Unit>.observe(lifecycleOwner: LifecycleOwner, doOnObserve: () -> Unit) {
    observe(lifecycleOwner, Observer { doOnObserve.invoke() })
}

fun <T> ObservableResource<T>.isLoading() = loading.value != null && loading.value == true
fun <T> ObservableResource<T>.hasError() = !isLoading() && this.error.value != null
fun <T> ObservableResource<T>.shouldReload() =
    !isLoading() && this.value == null //no value has been added either for error or no request is made

fun MutableLiveData<Unit>.fire() {
    value = Unit
}