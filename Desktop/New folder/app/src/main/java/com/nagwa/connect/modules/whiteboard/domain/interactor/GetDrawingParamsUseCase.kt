package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingParam
import com.nagwa.connect.modules.whiteboard.domain.repository.DrawingRepository
import io.reactivex.Single
import javax.inject.Inject


class GetDrawingParamsUseCase @Inject constructor(private val drawingRepository: DrawingRepository) :
    SingleUseCase<String, DrawingParam>() {

    override fun build(params: String): Single<DrawingParam> {
        return drawingRepository.getDrawingParams(params)

    }
}