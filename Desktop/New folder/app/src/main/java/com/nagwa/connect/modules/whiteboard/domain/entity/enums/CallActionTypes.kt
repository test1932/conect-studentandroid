package com.nagwa.connect.modules.whiteboard.domain.entity.enums

enum class CallActionTypes(val value: Int) {
    RaiseHand(1),
    UnMute(2),
    Mute(3),
    END_SESSION(4),
    INACTIVE(5),
    ACTIVE(6),
    TERMINATED(7),
    RESET(8),
    RAISE_HAND_FAILURE(9),
    ACTIVE_STUDENT(10),
    INACTIVE_STUDENT(11),
}