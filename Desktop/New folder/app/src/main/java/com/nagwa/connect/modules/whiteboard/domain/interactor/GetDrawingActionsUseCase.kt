package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.DrawEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.params.DrawingSize
import com.nagwa.connect.modules.whiteboard.domain.repository.DrawingRepository
import io.reactivex.Flowable
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/20/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class GetDrawingActionsUseCase @Inject constructor(private val drawingRepository: DrawingRepository) :
    FlowableUseCase<DrawingSize, DrawEntity>() {

    override fun build(params: DrawingSize): Flowable<DrawEntity> {
        return drawingRepository.getDrawingAction(params)
    }
}