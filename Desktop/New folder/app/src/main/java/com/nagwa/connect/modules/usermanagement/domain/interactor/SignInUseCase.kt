package com.nagwa.connect.modules.usermanagement.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.payment.domain.entity.params.GetPaymentParam
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.payment.domain.interactor.PaymentStateUseCase
import com.nagwa.connect.modules.usermanagement.domain.entity.*
import com.nagwa.connect.modules.usermanagement.domain.entity.param.SignInParam
import com.nagwa.connect.modules.usermanagement.domain.repository.AuthenticationRepository
import flatMapScoped
import io.reactivex.Single
import javax.inject.Inject

class SignInUseCase @Inject constructor(
    private val authRepo: AuthenticationRepository,
    private val saveUser: SaveUserUseCase,
    private val paymentStateUseCase: PaymentStateUseCase
) : SingleUseCase<SignInParam, UserWithPortalIdEntity>() {

    override fun build(params: SignInParam): Single<UserWithPortalIdEntity> =
        if (params.password.isBlank()) Single.error(EmptyPassword)
        else if (!isValidPassword(params.password)) Single.error(InvalidPassword)
        else paymentStateUseCase.build(GetPaymentParam(params.userId, params.portalId))
            .flatMapScoped {
                when (this) {
                    PaymentAction.BLOCK_USER -> Single.error(PaymentRequiredError)
                    PaymentAction.DEACTIVATED_USER -> Single.error(PaymentUserDeactivatedError)
                    else -> signIn(params)
                }
            }

    private fun signIn(params: SignInParam): Single<UserWithPortalIdEntity> {
        return authRepo.signinUser(params).flatMap {
            saveUser.build(it)
                .andThen(Single.just(it))
        }
    }

    private fun isValidPassword(password: String): Boolean = password.length >= 6
}
