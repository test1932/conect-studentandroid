package com.nagwa.connect.core.di

import androidx.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass


/**
 * Created by Youssef Ebrahim Elerian on 4/12/20.
 * youssef.elerian@gmail.com
 */


@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class TutoringViewModelKey(val value: KClass<out ViewModel>)
