package com.nagwa.connect.core.extension

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.core.content.ContextCompat


fun Context.getResColor(@ColorRes res: Int) = ContextCompat.getColor(this, res)

fun Context.getResDrawable(res: Int) = ContextCompat.getDrawable(this, res)

fun Context.getResDimenPx(res: Int) = resources.getDimensionPixelSize(res)

fun Context.dpToPx(valueInDp: Float): Int {

    val metrics = resources?.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics).toInt()
}

fun Context.pxToDP(px: Float): Float {
    return px / (resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun Context.getGradientDrawable(
    @ColorRes backgroundColor: Int,
    @DimenRes radi: Int
): GradientDrawable {
    val shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    val radii = resources.getDimension(radi).toInt()
    shape.cornerRadii = floatArrayOf(
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat(),
        radii.toFloat()
    )
    shape.setColor(getResColor(backgroundColor))
    return shape
}

fun Context.getSelectorDrawable(
    selectDrawable: GradientDrawable,
    defaultDrawable: GradientDrawable
): StateListDrawable {
    val res =
        StateListDrawable()
    res.setExitFadeDuration(100)
    //  res.addState(intArrayOf(android.R.attr.state_pressed), selectDrawable)
    res.addState(intArrayOf(android.R.attr.state_selected), selectDrawable)
    res.addState(
        intArrayOf(), defaultDrawable
    )
    return res
}

fun Context.isNetworkConnected(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT < 23) {
        val ni = cm.activeNetworkInfo
        if (ni != null) {
            return ni.isConnected && (ni.type == ConnectivityManager.TYPE_WIFI || ni.type == ConnectivityManager.TYPE_MOBILE)
        }
    } else {
        val n = cm.activeNetwork
        if (n != null) {
            val nc = cm.getNetworkCapabilities(n)
            if (nc != null)
                return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
                )
        }
    }
    return false
}