package com.nagwa.connect.modules.payment.data

import com.nagwa.connect.core.domian.attachDomainErrorMapper
import com.nagwa.connect.modules.payment.data.model.PaymentResponse
import com.nagwa.connect.modules.payment.domain.entity.PaymentEntity
import com.nagwa.connect.modules.payment.domain.entity.status.PaymentAction
import com.nagwa.connect.modules.payment.domain.repository.PaymentRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
class PaymentRepositoryImpl @Inject constructor(
    private val paymentRemoteDS: PaymentRemoteDS
) : PaymentRepository {

    override fun getInvoiceStatus(userId: String, portalId: String): Single<PaymentEntity> {
        return paymentRemoteDS.getInvoiceStatus(userId, portalId)
            .map { it.toEntity() }
            .attachDomainErrorMapper()
    }
}

fun PaymentResponse.toEntity() = PaymentEntity(
    action = when (this.status) {
        "pending", "clear" -> PaymentAction.PASS_USER
        "deactivated" -> PaymentAction.DEACTIVATED_USER
        else -> PaymentAction.BLOCK_USER
    }
)