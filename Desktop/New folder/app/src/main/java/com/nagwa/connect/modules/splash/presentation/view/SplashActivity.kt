package com.nagwa.connect.modules.splash.presentation.view

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringActivity
import com.nagwa.connect.core.extension.hideStatusBar
import com.nagwa.connect.core.extension.isLandscape
import com.nagwa.connect.core.extension.isTablet
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.main.presentation.view.MainActivity
import com.nagwa.connect.modules.splash.presentation.viewmodel.SplashViewModel
import com.nagwa.connect.modules.usermanagement.presentation.view.SignInActivity
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 3/12/20.
 * youssef.elerian@gmail.com
 */
class SplashActivity : TutoringActivity() {
    override fun layoutId() = R.layout.activity_splash

    override fun getTabletScreenOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    override fun getMobileScreenOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

    @Inject
    lateinit var viewModelFactory: TutoringViewModelFactory

    private val splashViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(SplashViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (!isTablet() && isLandscape()) hideStatusBar()
        super.onCreate(savedInstanceState)
        observation()
        splashViewModel.isLoggedIn()
    }

    private fun observation() {
        splashViewModel.isLoggedInLiveData.observe(this, Observer {
            if (it)
                MainActivity.startInstance(this)
            else
                SignInActivity.startInstance(this)
        })
    }

    companion object {
        fun startInstance(activity: Activity) {
            activity.startActivity(Intent(activity, SplashActivity::class.java))
            activity.finish()
        }
    }
}