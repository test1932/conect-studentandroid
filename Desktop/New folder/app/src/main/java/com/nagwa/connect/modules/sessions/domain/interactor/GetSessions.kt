package com.nagwa.connect.modules.sessions.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.core.domian.DomainException
import com.nagwa.connect.modules.attachments.domain.SessionFileID
import com.nagwa.connect.modules.attachments.domain.interactor.ClearExpiredSessionsData
import com.nagwa.connect.modules.attachments.domain.interactor.SaveAttachmentIDS
import com.nagwa.connect.modules.sessions.di.SessionsScope
import com.nagwa.connect.modules.sessions.domain.entity.SessionEntity
import com.nagwa.connect.modules.sessions.domain.repository.SessionsRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

@SessionsScope
class GetSessions @Inject constructor(
    private val repository: SessionsRepository,
    private val saveAttachmentIDS: SaveAttachmentIDS,
    private val clearExpiredSessionsData: ClearExpiredSessionsData
) :
    SingleUseCase<Unit, List<SessionEntity>>() {
    override fun build(params: Unit): Single<List<SessionEntity>> =
        repository.getSessions()
            //.map { ls -> ls.sortedBy { it.startingDate } }
            .flatMap { ls ->
                saveAttachmentsIDS(ls)
                    .andThen(Completable.defer { clearExpiredSessionsData.build(ls.map { it.id }) })
                    .andThen(Single.just(ls))
            }
            .onErrorResumeNext {
                if (it is EmptySessions) Single.just(emptyList<SessionEntity>())
                else Single.error<List<SessionEntity>>(it)
            }

    private fun saveAttachmentsIDS(ls: List<SessionEntity>): Completable {
        return saveAttachmentIDS.build(ls.map {
            SessionFileID(
                sessionID = it.id,
                fileID = it.fileID
            )
        })
    }

}

object EmptySessions : DomainException("no sessions available")


