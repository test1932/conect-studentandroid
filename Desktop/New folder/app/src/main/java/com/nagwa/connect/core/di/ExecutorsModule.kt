package com.nagwa.connect.core.di

import com.nagwa.connect.core.data.executors.SingleExecutor
import com.nagwa.connect.core.data.executors.ThreadExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ExecutorsModule {
    @Provides
    @Singleton
    @Named("SingleExecutor")
    fun provideSingleExecutor(): ThreadExecutor = SingleExecutor()
}