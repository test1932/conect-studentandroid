package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Completable
import javax.inject.Inject

class SaveAttachmentPath @Inject constructor(private val repository: AttachmentsRepository) :
    CompletableUseCase<AttachmentPathParam>() {
    override fun build(params: AttachmentPathParam): Completable =
        repository.savePath(sessionID = params.sessionID, filepath = params.path)

}

data class AttachmentPathParam(val sessionID: String, val path: String)