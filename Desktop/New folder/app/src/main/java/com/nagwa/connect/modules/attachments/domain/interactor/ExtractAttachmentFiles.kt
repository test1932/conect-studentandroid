package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.SingleUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class ExtractAttachmentFiles @Inject constructor(private val repository: AttachmentFilesManager) :
    SingleUseCase<ExtractAttachmentParam, String>() {
    override fun build(params: ExtractAttachmentParam): Single<String> {
        return repository.extractFiles(
            compressedFilePath = params.compressedFilePath,
            sessionID = params.sessionID
        ).doOnError { Timber.e(it, "build: ") }
    }
}

data class ExtractAttachmentParam(val sessionID: String, val compressedFilePath: String)