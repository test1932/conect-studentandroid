package com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.mapper

import android.graphics.Bitmap
import com.nagwa.connect.modules.whiteboard.domain.entity.AddImageEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.DrawActionType
import com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models.AddImageUIModel


/**
 * Created by Youssef Ebrahim Elerian on 7/7/20.
 * youssef.elerian@gmail.com
 */


fun AddImageEntity.toUIModel(image: Bitmap) = AddImageUIModel(
    id = id,
    xPos = xPos,
    yPos = yPos,
    image = image,
    actionType = DrawActionType.ADD_IMAGE
)