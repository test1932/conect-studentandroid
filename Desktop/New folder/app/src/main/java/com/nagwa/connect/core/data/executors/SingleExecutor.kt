package com.nagwa.connect.core.data.executors

import java.util.concurrent.Executor
import java.util.concurrent.Executors

class SingleExecutor : ThreadExecutor {
    val executor: Executor by lazy {
        Executors.newSingleThreadExecutor()
    }

    override fun execute(command: Runnable?) {
        executor.execute(command)
    }
}