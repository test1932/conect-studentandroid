package com.nagwa.connect.modules.sessions.data.model


import com.google.gson.annotations.SerializedName

data class SessionResponse(
    @SerializedName("durationInMinutes")
    val durationInMinutes: Int,
    @SerializedName("sessionId")
    val sessionId: Long,
    @SerializedName("startDate")
    val startDate: String,
    @SerializedName("statusId")
    val statusId: Int,
    @SerializedName("statusName")
    val statusName: String,
    @SerializedName("studentsCount")
    val studentsCount: Int,
    @SerializedName("subjectId")
    val subjectId: Long,
    @SerializedName("subjectName")
    val subjectName: String,
    @SerializedName("educatorId")
    val teacherId: Long,
    @SerializedName("educatorName")
    val teacherName: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("fileId")
    val fileID: Long,
    @SerializedName("sessionProvider")
    var provider: String
)

//provider keys
const val MILLICAST_RESPONSE_KEY = "Millicast"
const val AGORA_RESPONSE_KEY = "Agora"