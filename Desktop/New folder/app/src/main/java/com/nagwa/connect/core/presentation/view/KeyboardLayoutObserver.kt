package com.nagwa.connect.core.presentation.view

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewTreeObserver

class KeyboardLayoutObserver {
    private var layoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null
    private var fullVisibleRootViewHeight: Int = 0
    private var isKeyboardShown: Boolean = false

    fun addKeyboardObserver(
        activity: Activity,
        onKeyboardStateChanged: (Boolean, Int, Rect) -> Unit
    ) = with(activity) {
        val mRootWindow = window
        val mRootView = mRootWindow.decorView.findViewById<View>(android.R.id.content);
        layoutListener = ViewTreeObserver.OnGlobalLayoutListener {
            val r = Rect()
            val view: View = mRootWindow.decorView
            view.getWindowVisibleDisplayFrame(r)
            fullVisibleRootViewHeight = maxOf(r.height(), fullVisibleRootViewHeight)
            if (r.height() < fullVisibleRootViewHeight != isKeyboardShown) {
                isKeyboardShown = !isKeyboardShown
                onKeyboardStateChanged(isKeyboardShown, fullVisibleRootViewHeight, r)
            }
        }
        mRootView.viewTreeObserver.addOnGlobalLayoutListener(layoutListener)
    }

    fun removeKeyboardObserver(activity: Activity) = with(activity) {
        val mRootWindow = window
        val mRootView = mRootWindow.decorView.findViewById<View>(android.R.id.content);
        mRootView.viewTreeObserver.removeOnGlobalLayoutListener(layoutListener)
        layoutListener = null
    }
}