package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.modules.sessions.data.model.MillicastProviderResponse
import com.nagwa.connect.modules.sessions.data.source.SessionsLocalDS
import com.nagwa.connect.modules.whiteboard.di.AGORA_BINDING_KEY
import com.nagwa.connect.modules.whiteboard.di.MILLICAST_BINDING_KEY
import com.nagwa.connect.modules.whiteboard.domain.repository.SecondaryCallRouter
import com.nagwa.sessionlibrary.session.NagwaSwitchableCallProvider
import com.nagwa.sessionlibrary.session.SwitchableCallParams
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Provider

class SecondaryCallRouterImpl @Inject constructor(
    private val switchableCallProviders: Map<String, @JvmSuppressWildcards Provider<NagwaSwitchableCallProvider>>,
    private val sessionsLocalDS: SessionsLocalDS
) :
    SecondaryCallRouter {
    lateinit var switchableCallProvider: NagwaSwitchableCallProvider
    lateinit var providerType: CallRouterImpl.ProviderType
    override fun initCall(sessionID: String): Completable {
        return sessionsLocalDS.getSession(sessionID).map {
            providerType = CallRouterImpl.ProviderType.get(it.session.provider)
            initiateSwitchableProvider(providerType, getSwitchParams(it.provider))
        }.ignoreElement()
    }

    override fun requestMic(): Completable = Completable.defer {
        switchableCallProvider.requestTalking()
    }

    override fun acquireMic(): Completable = Completable.defer {
        switchableCallProvider.startTalking()
    }

    override fun leaveMic(): Completable = Completable.defer {
        switchableCallProvider.stopTalking()
    }

    override fun unRequestMic(): Completable = Completable.complete()

    override fun listenToMic(): Completable = Completable.defer {
        switchableCallProvider.startListening()
    }


    override fun endCall(): Completable = Completable.defer {
        switchableCallProvider.endCall()
    }

    private fun getSwitchParams(provider: MillicastProviderResponse?): SwitchableCallParams {
        return SwitchableCallParams(
            streamID = provider?.studentStreamName ?: "",
            accountId = provider?.accountId ?: "",
            authToken = provider?.publishingToken ?: "",
            holdMicOnNoPublisher = false
        )
    }

    private fun initiateSwitchableProvider(
        providerType: CallRouterImpl.ProviderType,
        switchParams: SwitchableCallParams? = null
    ) {
        switchableCallProvider = when (providerType) {
            CallRouterImpl.ProviderType.Millicast -> (switchableCallProviders[MILLICAST_BINDING_KEY]
                ?: error("millecast provider is null in $switchableCallProviders")).get()
            CallRouterImpl.ProviderType.Agora -> (switchableCallProviders[AGORA_BINDING_KEY]
                ?: error("agora provider is null in $switchableCallProviders")).get()
        }
        switchableCallProvider.initCall(switchParams)
    }
}