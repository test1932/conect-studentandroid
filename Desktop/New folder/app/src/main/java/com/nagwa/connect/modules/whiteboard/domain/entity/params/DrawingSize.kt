package com.nagwa.connect.modules.whiteboard.domain.entity.params


/**
 * Created by Youssef Ebrahim Elerian on 5/20/20.
 * youssef.elerian@gmail.com
 */
data class DrawingSize(val drawingWidth: Int, val drawingHeight: Int)