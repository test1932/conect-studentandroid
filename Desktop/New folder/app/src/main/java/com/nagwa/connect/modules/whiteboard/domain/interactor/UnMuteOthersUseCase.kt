package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class UnMuteOthersUseCase @Inject constructor(
    private val listenToStudentChannel: ListenToStudentChannelUseCase
) :
    CompletableUseCase<Unit>() {
    override fun build(params: Unit): Completable {
        return listenToStudentChannel.build(params)
    }
}