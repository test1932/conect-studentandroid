package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.repository.*
import io.reactivex.Completable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by Youssef Ebrahim Elerian on 5/17/20.
 * youssef.elerian@gmail.com
 */
@WhiteboardScope
class LeaveSessionUseCase @Inject constructor(
    private val whiteboardRepository: WhiteboardRepository,
    private val drawingRepository: DrawingRepository,
    private val callRouter: CallRouter,
    private val secondaryCallRouter: SecondaryCallRouter,
    private val messengerRepository: MessengerRepository,
    private val actionsRepository: ActionsRepository
) :
    CompletableUseCase<String>() {
    override fun build(params: String): Completable {
        return Completable.mergeArrayDelayError(
            whiteboardRepository.endSession(params),
            drawingRepository.leaveDrawingChannel(),
            messengerRepository.leaveChatChannel(),
            actionsRepository.leaveActionsChannel(),
            callRouter.endCall(),
            secondaryCallRouter.endCall()
        ).timeout(45, TimeUnit.SECONDS)
            .doOnError { Timber.e(it, "build: ") }
            .onErrorComplete()
    }
}