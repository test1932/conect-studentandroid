package com.nagwa.connect.core.extension

import android.animation.Animator
import android.view.View
import android.view.animation.AccelerateInterpolator


fun View.scaleYAxis(
    duration: Long = 300,
    amountToMoveDown: Float,
    startFrom: Float,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationRepeat: () -> Unit = {}
) {
    pivotY = startFrom
    animate()
        .scaleY(amountToMoveDown)
        .setInterpolator(AccelerateInterpolator())
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
                onAnimationRepeat.invoke()
            }

            override fun onAnimationEnd(animation: Animator?) {
                onAnimationEnd.invoke()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {
                clearAnimation()
                onAnimationStart.invoke()
            }
        })
        .duration = duration
}

fun View.slideDown(
    duration: Long = 1000,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationRepeat: () -> Unit = {}
) {
    alpha = 0.0f
    animate()
        .translationY(0f)
        .alpha(1.0f)
        .setInterpolator(AccelerateInterpolator())
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
                onAnimationRepeat.invoke()
            }

            override fun onAnimationEnd(animation: Animator?) {
                onAnimationEnd.invoke()
                clearAnimation()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {
                visible(true)
                onAnimationStart.invoke()
            }
        })
        .duration = duration
}

fun View.slideUp(
    duration: Long = 1000,
    onAnimationStart: () -> Unit = {},
    onAnimationEnd: () -> Unit = {},
    onAnimationRepeat: () -> Unit = {}
) {
    animate()
        .translationY(-height.toFloat())
        .alpha(0F)
        .setInterpolator(AccelerateInterpolator())
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
                onAnimationRepeat.invoke()
            }

            override fun onAnimationEnd(animation: Animator?) {
                onAnimationEnd.invoke()
                visibility = View.INVISIBLE
                clearAnimation()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {
                onAnimationStart.invoke()
            }
        })
        .duration = duration

}