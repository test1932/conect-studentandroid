package com.nagwa.connect.modules.usermanagement.presentation.uimodel

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class EmailStepUIModel(
    val showLoading: Boolean,
    val email: String?,
    @StringRes val error: Int?,
    @DrawableRes val errorBackgroundDrawable: Int,
    @ColorRes val errorTextColor: Int
)