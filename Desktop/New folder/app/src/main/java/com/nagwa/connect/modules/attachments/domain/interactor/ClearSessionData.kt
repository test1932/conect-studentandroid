package com.nagwa.connect.modules.attachments.domain.interactor

import com.nagwa.connect.base.domain.interactor.CompletableUseCase
import com.nagwa.connect.modules.attachments.domain.AttachmentFilesManager
import com.nagwa.connect.modules.attachments.domain.AttachmentsRepository
import io.reactivex.Completable
import javax.inject.Inject

class ClearSessionData @Inject constructor(
    private val repository: AttachmentsRepository,
    private val filesManager: AttachmentFilesManager
) : CompletableUseCase<String>() {
    override fun build(sessionID: String): Completable =
        filesManager.deleteAttachmentsFiles(sessionID)
            .andThen(repository.clearAttachmentInfo(sessionID))

}