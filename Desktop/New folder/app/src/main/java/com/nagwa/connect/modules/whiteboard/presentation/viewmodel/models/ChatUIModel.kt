package com.nagwa.connect.modules.whiteboard.presentation.viewmodel.models

import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import com.nagwa.connect.modules.whiteboard.domain.interactor.GetChatMessagesUpdates
import java.util.regex.Pattern


data class ChatMessageUIModel(
    val id: Long,
    val sender: String,
    val text: SpannableString,
    val showLoading: Boolean = false,
    val showError: Boolean = false,
    val isDimmed: Boolean = false
)

fun ChatMessageState.toUIModel(): ChatMessageUIModel {
    val uiModel = with(message) {
        ChatMessageUIModel(
            id = date,
            sender = studentName,
            text = text.formatMultipleMessages()
        )
    }
    return if (this is ChatMessageState.PendingMessage) {
        uiModel.copy(showLoading = resending, showError = failed, isDimmed = resending)
    } else
        uiModel
}

private fun String.formatMultipleMessages(): SpannableString {
    val message = replace(GetChatMessagesUpdates.NEW_MSG_MARKER, "\n\n")
    val spannableString = SpannableString(message)

    val matcher = Pattern.compile("\n\n").matcher(message)
    while (matcher.find()) {
        val start = matcher.start() + 1
        val end = matcher.end()
        if (start >= 0 && end >= 0)
            spannableString.setSpan(
                AbsoluteSizeSpan(5, true),
                start,
                end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
    }
    return spannableString
}

