package com.nagwa.connect.modules.whiteboard.domain.entity.enums

enum class CallStatusEntity {
    Initializing, Connected, DisConnected, RejoinFailed, RejoinTimeout, AccountJoinedWithOtherDevice, Ended, EducatorOffline, EducatorTerminated
}