package com.nagwa.connect.modules.whiteboard.data.model

import com.google.gson.annotations.SerializedName

class SubscribeResponse(
    @SerializedName("data") val streamData: StreamData?,
    @SerializedName("status") val status: String
) {
    data class StreamData(
        @SerializedName("jwt") val jwt: String,
        @SerializedName("streamAccountId") val streamAccountId: String,
        @SerializedName("subscribeRequiresAuth") val subscribeRequiresAuth: Boolean?,
        @SerializedName("urls") val urls: List<String?>?,
        @SerializedName("wsUrl") val wsUrl: String
    )
}
