package com.nagwa.connect.modules.whiteboard.data.repository

import com.nagwa.connect.core.extension.fromJSON
import com.nagwa.connect.core.extension.toJson
import com.nagwa.connect.modules.whiteboard.data.model.CallActionModel
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toEntity
import com.nagwa.connect.modules.whiteboard.data.model.mapper.toModel
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import com.nagwa.sessionlibrary.message.ChatRoom
import com.nagwa.sessionlibrary.message.data.source.ChatLogin
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

@WhiteboardScope
class ActionsRepositoryImpl @Inject constructor(
    private val loginChat: ChatLogin,
    private val actionsChannel: ChatRoom
) : ActionsRepository {


    override fun joinActionsChannel(sessionID: String): Completable {
        return actionsChannel.createChannel("ActionsChannel:$sessionID").ignoreElement()
    }

    override fun leaveActionsChannel(): Completable = loginChat.logout()
        .flatMap { actionsChannel.leaveChannel() }
        .ignoreElement()

    override fun sendAction(raiseHandActionEntity: ActionEntity): Single<Boolean> {
        return actionsChannel.sendMessage(raiseHandActionEntity.toModel().toJson() ?: "")
    }

    override fun getActions(): Flowable<ActionEntity> {
        return actionsChannel.hasMessage().map {
            it.message.fromJSON<CallActionModel>()?.toEntity(senderId = it.userId)
        }
    }
}