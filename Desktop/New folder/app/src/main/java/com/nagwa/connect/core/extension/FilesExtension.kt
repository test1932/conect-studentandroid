package  com.nagwa.connect.core.extension


import java.text.SimpleDateFormat
import java.util.*


fun Long.formatTime(): String =
    SimpleDateFormat("mm:shape_session_status_inprogress_bg", Locale("en"))
        .format(Date(this))