package com.nagwa.connect.modules.payment.domain.entity.status

/**
 * Authored by Mohamed Fathy on 18 Aug, 2020.
 * mohamed.eldesoky@nagwa.com
 */
enum class PaymentAction {

    BLOCK_USER,
    DEACTIVATED_USER,
    PASS_USER
}