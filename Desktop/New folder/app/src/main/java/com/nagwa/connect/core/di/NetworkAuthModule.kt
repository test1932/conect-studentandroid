package com.nagwa.connect.core.di

import com.nagwa.connect.core.data.auth.PaymentScope
import com.nagwa.connect.core.data.auth.TutoringScope
import com.nagwa.connect.core.data.auth.UserManagementScope
import com.nagwa.networkmanager.network.NAAuthNetwork
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkAuthModule {

    @Provides
    @Singleton
    @Named("UserManagementAuth")
    internal fun provideUserManagementScope(): NAAuthNetwork = UserManagementScope()

    @Provides
    @Singleton
    @Named("TutoringAuth")
    internal fun provideTutoringAuthScope(): NAAuthNetwork = TutoringScope()

    @Provides
    @Singleton
    @Named("PaymentAuth")
    internal fun providePaymentAuthScope(): NAAuthNetwork = PaymentScope()

}