package com.nagwa.connect.modules.whiteboard.domain.interactor

import com.nagwa.connect.base.domain.interactor.FlowableUseCase
import com.nagwa.connect.modules.whiteboard.di.WhiteboardScope
import com.nagwa.connect.modules.whiteboard.domain.entity.ActionEntity
import com.nagwa.connect.modules.whiteboard.domain.entity.enums.CallActionTypes.*
import com.nagwa.connect.modules.whiteboard.domain.repository.ActionsRepository
import io.reactivex.Flowable
import javax.inject.Inject

@WhiteboardScope
class GetEndingActionsUpdates @Inject constructor(
    private val actionsRepository: ActionsRepository
) :
    FlowableUseCase<Unit, ActionEntity>() {
    override fun build(params: Unit): Flowable<ActionEntity> {
        return actionsRepository.getActions()
            .filter { action -> action.isEndingAction() }
    }

    private fun ActionEntity.isEndingAction(): Boolean {
        return (this.action == END_SESSION.value
                || this.action == INACTIVE.value
                || this.action == ACTIVE.value
                || this.action == TERMINATED.value)
    }
}

