package com.nagwa.connect.core.di


import com.nagwa.connect.modules.attachments.di.AttachmentModule
import com.nagwa.connect.modules.main.di.MainActivityFragmentBuilder
import com.nagwa.connect.modules.main.di.MainModule
import com.nagwa.connect.modules.main.di.MainScope
import com.nagwa.connect.modules.main.presentation.view.MainActivity
import com.nagwa.connect.modules.payment.di.PaymentModule
import com.nagwa.connect.modules.splash.di.SplashViewModule
import com.nagwa.connect.modules.splash.presentation.view.SplashActivity
import com.nagwa.connect.modules.usermanagement.di.AuthenticationScope
import com.nagwa.connect.modules.usermanagement.di.SignInFragmentBuilder
import com.nagwa.connect.modules.usermanagement.di.SignInModule
import com.nagwa.connect.modules.usermanagement.presentation.view.SignInActivity
import com.nagwa.connect.modules.whiteboard.di.*
import com.nagwa.connect.modules.whiteboard.presentation.view.WhiteboardActivity
import com.nagwa.sessionlibrary.session.millicast.di.RTCModule
import com.nagwa.sessionlibrary.session.millicast.di.RTCSessionScope
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Created by Youssef Ebrahim Elerian on 2019-11-05.
 * youssef.elerian@gmail.com
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [SplashViewModule::class])
    abstract fun contributeSplashActivityAndroidInjector(): SplashActivity

    @AuthenticationScope
    @ContributesAndroidInjector(modules = [SignInFragmentBuilder::class, SignInModule::class, PaymentModule::class])
    abstract fun contributeSignInActivityAndroidInjector(): SignInActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MainActivityFragmentBuilder::class, MainModule::class, AttachmentModule::class, PaymentModule::class])
    abstract fun contributeMainActivityAndroidInjector(): MainActivity

    @WhiteboardScope
    @RTCSessionScope
    @ContributesAndroidInjector(
        modules = [RTCModule::class, ProvidersModule::class, CallModule::class, WhiteboardModule::class,
            WhiteboardViewModelModule::class, AttachmentModule::class]
    )
    abstract fun contributeWhiteboardActivityAndroidInjector(): WhiteboardActivity


}