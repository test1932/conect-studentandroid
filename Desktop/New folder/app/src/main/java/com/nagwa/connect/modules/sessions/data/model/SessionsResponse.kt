package com.nagwa.connect.modules.sessions.data.model


import com.google.gson.annotations.SerializedName

data class SessionsResponse(
    @SerializedName("sessions")
    val sessions: List<SessionResponse>,
    @SerializedName("totalCount")
    val totalCount: Int
)