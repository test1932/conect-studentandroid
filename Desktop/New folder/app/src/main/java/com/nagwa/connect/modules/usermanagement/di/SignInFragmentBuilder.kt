package com.nagwa.connect.modules.usermanagement.di

import com.nagwa.connect.modules.usermanagement.presentation.view.CreatePasswordFragment
import com.nagwa.connect.modules.usermanagement.presentation.view.SignInEmailStepFragment
import com.nagwa.connect.modules.usermanagement.presentation.view.SignInPasswordStepFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SignInFragmentBuilder {


    @ContributesAndroidInjector(modules = [PasswordStepModule::class])
    abstract fun contributeSignInFragmentAndroidInjector(): SignInPasswordStepFragment

    @ContributesAndroidInjector(modules = [CreatePasswordStepModule::class])
    abstract fun contributeCreatePasswordFragmentAndroidInjector(): CreatePasswordFragment

    @ContributesAndroidInjector(modules = [EmailStepModule::class])
    abstract fun contributeEmailFragmentAndroidInjector(): SignInEmailStepFragment

}