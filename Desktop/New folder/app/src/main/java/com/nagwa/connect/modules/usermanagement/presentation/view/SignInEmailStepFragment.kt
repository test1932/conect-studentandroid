package com.nagwa.connect.modules.usermanagement.presentation.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.nagwa.connect.R
import com.nagwa.connect.base.presentation.view.TutoringFragment
import com.nagwa.connect.core.extension.*
import com.nagwa.connect.core.presentation.viewmodel.TutoringViewModelFactory
import com.nagwa.connect.modules.usermanagement.presentation.uimodel.EmailStepUIModel
import com.nagwa.connect.modules.usermanagement.presentation.viewmodel.EmailStepViewModel
import kotlinx.android.synthetic.main.fragment_email_step.*
import javax.inject.Inject

class SignInEmailStepFragment : TutoringFragment() {
    @Inject
    lateinit var mViewModelFactory: TutoringViewModelFactory
    lateinit var mListener: SignInEmailListener

    private lateinit var mRootView: View

    private var showPaymentMsg: Boolean = false
    private var isDeactivated: Boolean = false

    private val mViewModel by lazy {
        ViewModelProvider(this, mViewModelFactory)
            .get(EmailStepViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            showPaymentMsg = getBoolean(SignInActivity.SHOW_PAYMENT_MSG, false)
            isDeactivated = getBoolean(SignInActivity.PAYMENT_IS_DEACTIVATED, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.getInflatedView(R.layout.fragment_email_step, container)
        return mRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservation()
        setupPaymentErrorView()
    }

    private fun setupPaymentErrorView() {
        if (showPaymentMsg) {
            showError(if (isDeactivated) R.string.msgDeactivatedUser else R.string.msgBlockedUser)
            txtError.setTextColor(ContextCompat.getColor(requireActivity(), R.color.colorOceanBlue))
            txtError.setBackgroundResource(R.drawable.shape_tv_payment_error)
        }
    }

    override fun onAttach(context: Context) {
        mListener = if (context is SignInEmailListener) context
        else error("Activity should implement SignInEmailListener")
        super.onAttach(context)
    }

    fun resetEmailState() = mViewModel.resetEmailState()

    //todo observe data coming from view model
    private fun initObservation() {
        mViewModel.uiModel.observe(viewLifecycleOwner, ::render)
        mViewModel.loginNavigation.observe(viewLifecycleOwner, ::navigateToNextStep)
    }

    private fun navigateToNextStep(it: EmailStepViewModel.LoginNavigation) {
        when (it.loginStep) {
            EmailStepViewModel.LoginStep.NEW_STUDENT -> mListener.onNewStudent(
                it.email,
                it.userId,
                it.portalId
            )
            EmailStepViewModel.LoginStep.STUDENT -> mListener.onExistingStudent(
                it.email,
                it.userId,
                it.portalId
            )
        }
    }

    private fun render(uiModel: EmailStepUIModel) {
        with(uiModel) {
            if (showLoading) onDisable() else onEnable()
            error?.let(::showError) ?: hideError()
            txtError.setTextColor(ContextCompat.getColor(requireActivity(), errorTextColor))
            txtError.setBackgroundResource(errorBackgroundDrawable)
        }
    }

    private fun initViews() = with(mRootView) {
        verifyEmailBtn onClick ::verifyUserEmail
        emailEt onKeyboardGoActionClicked ::verifyUserEmail
        hideError()
    }

    private fun verifyUserEmail() {
        mViewModel.verifyUser(emailEt.text.toString())
    }

    private fun hideError() {
        txtError.visible(false)
        txtError.text = ""
    }

    private fun onDisable() {
        activity?.hideKeyboard()
        verifyEmailBtn.disable(true)
        mListener.disableInteraction(true)
        progressEmailStep.visible(true)
    }

    private fun onEnable() {
        verifyEmailBtn.enable(true)
        mListener.disableInteraction(false)
        progressEmailStep.visible(false)
    }

    private fun showError(@StringRes message: Int) {
        txtError.text = getString(message)
        txtError.visible(true)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment SignInEmailStepFragment.
         */
        @JvmStatic
        fun newInstance(showPaymentMsg: Boolean, isDeactivated: Boolean) =
            SignInEmailStepFragment().apply {
                val args = bundleOf(
                    SignInActivity.SHOW_PAYMENT_MSG to showPaymentMsg,
                    SignInActivity.PAYMENT_IS_DEACTIVATED to isDeactivated
                )
                arguments = args
            }
    }
}
